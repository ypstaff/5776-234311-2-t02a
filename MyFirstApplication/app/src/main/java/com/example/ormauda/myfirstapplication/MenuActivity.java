package com.example.ormauda.myfirstapplication;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;

import java.util.ArrayList;

public class MenuActivity extends AppCompatActivity {

    private ListView menuListView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        ArrayList<String> values = new ArrayList<>();
        values.add("name1");
        values.add("name2");
        values.add("name3");

        menuListView = (ListView) findViewById(R.id.listViewMenu);

        ListAdapter adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, values);

        menuListView.setAdapter(adapter);

    }


}
