package com.example.keyboard.classes;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.keyboard.R;

import java.util.ArrayList;

/**
 * @author Or Mauda
 */
public class MenuListAdapter extends ArrayAdapter<String> {
    private final Context context;
    private ArrayList<String> values;

    public MenuListAdapter(Context context, ArrayList<String> values) {
        super(context, -1, values);
        this.context = context;
        this.values = values;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.menu_list_row, parent, false);
        TextView textView = (TextView) rowView.findViewById(R.id.listItemText);
        // TODO image view?
        textView.setText(values.get(position));
        return rowView;
    }

}
