package com.example.keyboard.classes;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.example.keyboard.activities.PhoneActivity;
import com.example.keyboard.activities.SmsActivity;

/**
 * @author Or Mauda.
 */
public enum CommunicationAction {
    SMS(0) {
        public Intent getCommunicationIntent(Bundle b, Context appContext) {
            Intent intent = new Intent(appContext, SmsActivity.class);;
            intent.putExtras(b);
            return intent;
        }
    },
    PHONE_CALL(1) {
        public Intent getCommunicationIntent(Bundle b, Context appContext) {
            Intent intent = new Intent(appContext, PhoneActivity.class);;
            intent.putExtras(b);
            return intent;
        }
    };

    public abstract Intent getCommunicationIntent(Bundle b, Context appContext);

    private final int position;
    final static String[] actionOptions = {
            "Send SMS",
            "Phone Call"
    };

    private CommunicationAction(int position) {
        this.position = position;
    }

    public int getAction(){
        return position;
    }

    public static String[] getActionOptions() {
        return actionOptions;
    }
};
