package com.example.keyboard.activities;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.Point;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Toast;

import com.example.keyboard.R;
import com.example.keyboard.classes.Painter;

import java.util.LinkedList;

/**
 * @author Inbar Donag
 */
public class PainterActivity extends Activity
{
    public enum Tool {CURSOR, BRUSH}
    Tool currTool = Tool.CURSOR;
    Tool prevTool = Tool.BRUSH;
    Painter painter;
    LinkedList<Stroke> strokes = new LinkedList<>(); // curr stroke is last in the list
    ImageButton toolBtn;
    ImageButton leftBtn;
    ImageButton rightBtn;
    private Handler handler = new Handler();
    boolean toolShouldStartAction = false; // true if the tool should start repeating its function (useTool runnable), false if should stop
    Cursor cursor = new Cursor();

    // happens every 10 msec after tool is started
    Runnable useTool = new Runnable()
    {
        @Override
        public void run()
        {
            cursor.advanceCursor();

            if (currTool == Tool.BRUSH)
                strokes.getLast().advanceStroke(cursor.getCursorPos());

            painter.invalidate();
            handler.postDelayed(useTool, 10);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_painter);
        painter = (Painter) findViewById(R.id.painter);
        toolBtn = (ImageButton) findViewById(R.id.toolBtn);
        leftBtn = (ImageButton) findViewById(R.id.turnLeftBtn);
        rightBtn = (ImageButton) findViewById(R.id.turnRightBtn);
        toolBtn.setBackgroundColor(Color.LTGRAY);

        painter.setStrokes(strokes);
        painter.setCursor(cursor);

        toolBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                toolShouldStartAction = !toolShouldStartAction;
                if (toolShouldStartAction) {
                    startUsingTool();
                }
                else {
                    stopUsingTool();
                }
            }
        });

        toolBtn.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {

                openToolMenu();
                return true;
            }
        });

        leftBtn.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {

                Toast.makeText(getApplicationContext(), "swapping to " + prevTool.toString(), Toast.LENGTH_SHORT).show();

                // swap to the latest tool used
                Tool temp = prevTool;
                prevTool = currTool;
                currTool = temp;

                if (currTool == Tool.BRUSH) {
                    strokes.addLast(new Stroke(cursor.getCursorPos()));

                }
                updateToolBtn();
                return true;
            }
        });


    }

    private void openToolMenu() {
        Toast.makeText(getApplicationContext(), "open tool menu", Toast.LENGTH_SHORT).show();
        // TODO
    }

    public void turnRight(View v) {

        cursor.turnRight();

    }

    public void turnLeft(View v) {

        cursor.turnLeft();
    }

    public void setTool(Tool t) {

        if (t == Tool.BRUSH) {
            strokes.addLast(new Stroke(cursor.getCursorPos()));
        }
        prevTool = currTool;
        currTool = t;
        updateToolBtn();
    }

    private void updateToolBtn()
    {
        switch (currTool) {
            case CURSOR:
                toolBtn.setImageResource(R.drawable.cursor);
                break;
            case BRUSH:
                toolBtn.setImageResource(R.drawable.brush);
                break;

        }

    }

    private void startUsingTool() {
        toolBtn.setBackgroundColor(Color.GREEN);
        handler.post(useTool);
    }

    private void stopUsingTool() {
        toolBtn.setBackgroundColor(Color.LTGRAY);
        handler.removeCallbacks(useTool);
    }

    //////////////////////////////////////// Stroke ////////////////////////////////////////////////

    public class Stroke
    {
        public LinkedList<Point> strokePoints = new LinkedList<>();

        public Stroke(Point firstPos) {
            strokePoints.addLast(firstPos);
        }

        public void setStrokeColor(int r, int g, int b, int a) {
            painter.setStrokeColor(a,r,g,b);
        }

        public void setStrokeWidth(int width) { // in pixels
            painter.setStrokeWidth(width);
        }

        public void advanceStroke(Point newPos) {
            strokePoints.addLast(newPos);
        }
    }

    //////////////////////////////////////// Cursor ////////////////////////////////////////////////
    public class Cursor
    {
        // directions
        public static final int LEFT = 0, UPPER_LEFT = 1, UP = 2, UPPER_RIGHT = 3,
                RIGHT = 4, LOWER_RIGHT = 5, DOWN = 6, LOWER_LEFT = 7;

        private int currentDirection = RIGHT;
        private Point cursorPos = new Point(0,0);

        public Point getCursorPos() {
            return cursorPos;
        }

        public int getCurrentDirection() {
            return currentDirection;
        }

        public void turnLeft() {

            // change direction of the stroke counterclockwise
            currentDirection--;
            if (currentDirection < 0) {
                currentDirection = 7;
            }
            painter.invalidate();
        }

        public void turnRight() {

            // change direction of the stroke clockwise
            currentDirection++;
            if (currentDirection > 7) {
                currentDirection = 0;
            }
            painter.invalidate();
        }

        private void advanceCursor()
        {
            int x = cursorPos.x;
            int y = cursorPos.y;

            switch (currentDirection) {
                case LEFT:
                    if (x-1 < 0) return;
                    cursorPos = new Point(x-1, y);
                    break;
                case UPPER_LEFT:
                    if (x-1 < 0 || y-1 < 0) return;
                    cursorPos = new Point(x-1,y-1);
                    break;
                case UP:
                    if (y-1 < 0) return;
                    cursorPos = new Point(x,y-1);
                    break;
                case UPPER_RIGHT:
                    if (y-1 < 0 || x+1 > painter.viewWidth) return;
                    cursorPos = new Point(x+1,y-1);
                    break;
                case RIGHT:
                    if (x+1 > painter.viewWidth) return;
                    cursorPos = new Point(x+1,y);
                    break;
                case LOWER_RIGHT:
                    if (y+1 > painter.viewHeight || x+1 > painter.viewWidth) return;
                    cursorPos = new Point(x+1,y+1);
                    break;
                case DOWN:
                    if (y+1 > painter.viewHeight) return;
                    cursorPos = new Point(x,y+1);
                    break;
                case LOWER_LEFT:
                    if (y+1 > painter.viewHeight || x-1 < 0) return;
                    cursorPos = new Point(x-1,y+1);
                    break;
            }

        }

    }
}
