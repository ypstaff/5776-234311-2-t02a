package com.example.keyboard.navigationlibrary;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.support.v4.content.res.ResourcesCompat;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sRoySvin on 11/10/2015.
 *
 * @author Roy Svinik
 */

public class IterableSingleView implements IterableGroup {

    View view;
    Drawable formerBackground;
    Context context;
    Drawable background;
    Drawable etBackground;
    int markColor = Color.YELLOW;

    public IterableSingleView(View view, Context context) {
        this.view = view;
        this.formerBackground = view.getBackground();
        this.context = context;
        background = ResourcesCompat.getDrawable(context.getResources(), yearlyproject.navigationlibrary.R.drawable.border, null);
        etBackground = ResourcesCompat.getDrawable(context.getResources(), yearlyproject.navigationlibrary.R.drawable.edit_text_border, null);
    }

    //customize drawable color
    public IterableSingleView(View view, Context context, Drawable d,
                             int markColor) {
        this(view, context);
        background = d;
        this.markColor = markColor;
    }

    @Override
    public boolean start() {
        Log.e("IterableSingleGroups", "start");
        markGroup();
        select();
        return false;
    }

    @Override
    public boolean iterate() {
        return false;
    }

    @Override
    public void select() {
        view.performClick();
    }

    @Override
    public boolean back() {
        return true;
    }

    @Override
    public void markGroup() {
        view.setBackgroundColor(markColor);
    }

    @Override
    public void unmarkGroup() {
        view.setBackground(formerBackground);
    }

    @Override
    public void markAllChilds() {
        markGroup();
    }

    @Override
    public void unmarkAllChilds() {
        unmarkGroup();
    }

    @Override
    public void circleGroup() {
        if(view instanceof EditText) {
            view.setBackground(etBackground);
        } else {
            view.setBackground(background);
        }
    }

    @Override
    public void uncircleGroup() {
        view.setBackground(formerBackground);
    }

    @Override
    public void circleAllChilds() {
        circleGroup();
    }

    @Override
    public void uncircleAllChilds() {
        uncircleGroup();
    }

    @Override
    public List<View> getSelectedViews() {
        List<View> views = new ArrayList<>();
        views.add(view);
        return views;
    }

    @Override
    public View getGroupView() {
        return view;
    }

    @Override
    public void stop() {
        Log.e("IterableSingleView", "stop");
        unmarkGroup();
        uncircleGroup();
    }
}
