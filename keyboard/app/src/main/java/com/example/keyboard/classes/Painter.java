package com.example.keyboard.classes;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.CornerPathEffect;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Point;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.View;

import com.example.keyboard.R;
import com.example.keyboard.activities.PainterActivity;

import java.util.LinkedList;


/**
 * @author Inbar Donag
 */
public class Painter extends View {

    private Context context;

    /////////////////////////////////////// graphics ///////////////////////////////////////////////
    // styles
    private Paint paintBackground;
    private Paint paintStroke;

    // bitmaps
    private BitmapFactory bitmapFactory; // used to create bitmap out of png file
    private Resources resources = getResources();

    private Bitmap arrowPic = bitmapFactory.decodeResource(resources, R.drawable.arrow, null);

    ///////////////////////////////////////// c'tors ///////////////////////////////////////////////

    public Painter(Context context) {
        super(context);
        init(context);
    }

    public Painter(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public Painter(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context);
    }


    // sizes
    public int viewWidth = 0;
    public int viewHeight = 0;


    @Override
    protected void onSizeChanged(int xNew, int yNew, int xOld, int yOld){
        super.onSizeChanged(xNew, yNew, xOld, yOld);

        viewWidth = xNew;
        viewHeight = yNew;
    }

    private void init(Context c) {

        context = c;
        paintBackground = new Paint();
        bitmapFactory = new BitmapFactory();

        // set background style
        paintBackground.setColor(Color.WHITE);

        // set stroke style
        paintStroke = new Paint();
        setStrokeColor(255, 25, 25, 25);
        setStrokeWidth(5);
        paintStroke.setStyle(Paint.Style.STROKE);
        paintStroke.setPathEffect(new CornerPathEffect(50));
        paintStroke.setStrokeJoin(Paint.Join.ROUND);
        paintStroke.setStrokeCap(Paint.Cap.ROUND);
        paintStroke.setAntiAlias(true);
    }

    //////////////////////////////////////// drawing ///////////////////////////////////////////////

    public boolean isOrdered(int a, int b, int c) {
        return (a > b && b > c) || (a < b && b < c);
    }

    public boolean isStraightLine(Point a, Point b, Point c) {


        if (a == null || b == null || c == null)
            return true;

        if (a.x == b.x && b.x == c.x)
            return true;

        if (a.y == b.y && b.y == c.y)
            return true;

        if (isOrdered(a.x,b.x,c.x) && isOrdered(a.y,b.y,c.y))
            return true;

        return false;

    }

    private LinkedList<PainterActivity.Stroke> strokes;
    private PainterActivity.Cursor cursor;

    public void setStrokes(LinkedList<PainterActivity.Stroke> s) {
        strokes = s;
    }

    public void setCursor(PainterActivity.Cursor c) {
        cursor = c;
    }

    public void setStrokeColor(int a, int r, int g, int b) {
        paintStroke.setARGB(a,r,g,b);
        invalidate();
    }

    public void setStrokeWidth(int w) { // in pixels

        paintStroke.setStrokeWidth(w);
        invalidate();
    }

    private Bitmap rotate(Bitmap bmp, int angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(bmp, 0, 0, bmp.getWidth(), bmp.getHeight(), matrix, true);
    }

    private Rect getRect(Point head) {

        return new Rect(Math.max(head.x - 15, 0), Math.max(head.y - 15, 0), Math.min(head.x + 15, viewWidth) , Math.min(head.y+15, viewHeight));
    }

    private void drawStroke(PainterActivity.Stroke stroke, Canvas canvas) {

        Path path = new Path();

        Point first = stroke.strokePoints.getFirst();

        path.moveTo(first.x, first.y);
        int size = stroke.strokePoints.size();

        for (int i = 0; i < size-1 ;)
        {
            Point p1 = stroke.strokePoints.get(i);
            Point p2 = stroke.strokePoints.get(i + 1);
            Point p3 = i + 2 < size ? stroke.strokePoints.get(i + 2) : null;

            if (isStraightLine(p1, p2, p3)) {
                path.lineTo(p2.x,p2.y);
                i++;
            } else {
                path.quadTo(p2.x, p2.y, p3.x, p3.y);
                i+=2;
            }
        }

        canvas.drawPath(path, paintStroke);
    }

    @Override
    protected void onDraw(Canvas canvas)
    {
        super.onDraw(canvas);

        // draw background
        canvas.drawRect(0, 0, viewWidth, viewHeight, paintBackground);

        if (strokes == null)
            return;

        // draw strokes
        for (int i=0; i<strokes.size(); i++)
            drawStroke(strokes.get(i), canvas);

        // draw cursor (arrow)
        int d = cursor.getCurrentDirection();
        Bitmap rotatedArrowPic = rotate(arrowPic, d*45);
        canvas.drawBitmap(rotatedArrowPic, null, getRect(cursor.getCursorPos()), null);
    }
}
