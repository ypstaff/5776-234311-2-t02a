package com.example.keyboard.activities;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Toast;

import com.example.keyboard.R;

import java.util.ArrayList;
import java.util.List;

import com.example.keyboard.navigationlibrary.EmergencyCallActivity;
import com.example.keyboard.navigationlibrary.IterableGroup;
import com.example.keyboard.navigationlibrary.IterableSingleView;
import com.example.keyboard.navigationlibrary.IterableViewGroup;
import com.example.keyboard.navigationlibrary.IterableViewGroups;

public class MainActivity extends EmergencyCallActivity implements View.OnClickListener {

    ImageButton ib_messages;
    ImageButton ib_phone;
    ImageButton ib_contacts;
    ImageButton ib_media_player;
    ImageButton ib_camera;
    ImageButton ib_snake;
    ImageButton ib_painter;
    ImageButton ib_browser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Getting all the image buttons
        ib_messages = (ImageButton) findViewById(R.id.ib_messages);
        ib_phone = (ImageButton) findViewById(R.id.ib_phone);
        ib_contacts = (ImageButton) findViewById(R.id.ib_contacts);
        ib_media_player = (ImageButton) findViewById(R.id.ib_media_player);
        ib_snake = (ImageButton) findViewById(R.id.ib_snake);
        ib_camera = (ImageButton) findViewById(R.id.ib_camera);
        ib_painter = (ImageButton) findViewById(R.id.ib_painter);
        ib_browser = (ImageButton) findViewById(R.id.ib_browser);

        //Setting this activity as the on click listener for all the image buttons
        ib_messages.setOnClickListener(this);
        ib_phone.setOnClickListener(this);
        ib_contacts.setOnClickListener(this);
        ib_media_player.setOnClickListener(this);
        ib_snake.setOnClickListener(this);
        ib_camera.setOnClickListener(this);
        ib_painter.setOnClickListener(this);
        ib_browser.setOnClickListener(this);

        //Setting the 3 buttons navigation system for the activity
        fab_left = (FloatingActionButton) findViewById(R.id.fab_left);
        fab_right = (FloatingActionButton) findViewById(R.id.fab_right);
        fab_middle = (FloatingActionButton) findViewById(R.id.fab_middle);

        //Setting the action buttons to be semi transparent
        fab_left.setAlpha((float) 0.5);
        fab_middle.setAlpha((float) 0.5);
        fab_right.setAlpha((float) 0.5);

        //Setting the listeners for the navigation buttons
        fab_left.setOnClickListener(this);
        fab_right.setOnClickListener(this);
        fab_middle.setOnClickListener(this);

        fab_left.setOnLongClickListener(this);
        fab_right.setOnLongClickListener(this);
        fab_middle.setOnLongClickListener(this);

        //Creating the groups for the navigation
        //Row 1 group
        List<View> row1_list = new ArrayList<>();
        row1_list.add(ib_phone);
        row1_list.add(ib_messages);

        IterableGroup row1_group = new IterableViewGroup(row1_list, findViewById(R.id.ll_row1), this);

        //Row 2 group
        List<View> row2_list = new ArrayList<>();
        row2_list.add(ib_contacts);
        row2_list.add(ib_media_player);

        IterableGroup row2_group = new IterableViewGroup(row2_list, findViewById(R.id.ll_row2), this);

        //Row 3 group
        List<View> row3_list = new ArrayList<>();
        row3_list.add(ib_snake);
        row3_list.add(ib_camera);

        IterableGroup row3_group = new IterableViewGroup(row3_list, findViewById(R.id.ll_row3), this);

        //Row 4 group
        List<View> row4_list = new ArrayList<>();
        row4_list.add(ib_painter);
        row4_list.add(ib_browser);

        IterableGroup row4_group = new IterableViewGroup(row4_list, findViewById(R.id.ll_row4), this);

        //The general group
        List<IterableGroup> all_rows_list = new ArrayList<>();
        all_rows_list.add(row1_group);
        all_rows_list.add(row2_group);
        all_rows_list.add(row3_group);
        all_rows_list.add(row4_group);

        group = new IterableViewGroups(all_rows_list, findViewById(R.id.ll_all_rows), this);
        group.start();
    }

    @Override
    public void onClick(View v) {
        //For the navigation buttons
        super.onClick(v);

        Intent intent;
        switch(v.getId()) {
            case R.id.ib_messages:
                intent = new Intent(getApplicationContext(), SmsActivity.class);
                startActivity(intent);
                break;
            case R.id.ib_phone:
                intent = new Intent(getApplicationContext(), PhoneActivity.class);
                startActivity(intent);
                break;
            case R.id.ib_contacts:
                intent = new Intent(getApplicationContext(), ContactsActivity.class);
                startActivity(intent);
                break;
            case R.id.ib_media_player:
                intent = new Intent(getApplicationContext(), AudioActivity.class);
                startActivity(intent);
                break;
            case R.id.ib_snake:
                //Trying to open the snake app
                intent = new Intent(getApplicationContext(), SnakeActivity.class);
                startActivity(intent);
                break;
            case R.id.ib_camera:
                intent = new Intent(getApplicationContext(), CameraActivity.class);
                startActivity(intent);
                break;
            case R.id.ib_painter:
                intent = new Intent(getApplicationContext(), PainterActivity.class);
                startActivity(intent);
                break;
            case R.id.ib_browser:
                //Trying to open the bluetooth settings app
                try {
                    intent = getPackageManager().getLaunchIntentForPackage("yearlyproject.bluetooth");
                    startActivity(intent);
                } catch(ActivityNotFoundException anfe) {
                    Toast.makeText(this, "Can't find the application on your device", Toast.LENGTH_LONG).show();
                } catch(Exception e) {
                    Toast.makeText(this, "Can't open the application", Toast.LENGTH_LONG).show();
                }
                break;
        }
    }
}
