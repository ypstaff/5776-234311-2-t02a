package com.example.keyboard.navigationlibrary;

import android.view.View;

import java.util.List;

/**
 * Created by sRoySvin on 11/7/2015.
 *
 * @author Roy Svinik
 */

public interface IterableGroup {

    /* starts the iteration, marks first element in the group,
    return false if there is a single element in the group, otherwise returns true */
    public boolean start();

    public void stop();
    /* returns true if iteration is supported, otherwise returns false */
    public boolean iterate();
    /* enter a sub group or invokes selected element */
    public void select();
    /* returns true if control is passed to the father group, otherwise returns false */
    public boolean back();
    /* marks all elements in the group */
    public void markGroup();
    /* unmarks all elements in the group */
    public void unmarkGroup();
    /* marks all elements in the group */
    public void markAllChilds();
    /* unmarks all elements in the group */
    public void unmarkAllChilds();
    /* circles the entire group */
    public void circleGroup();
    /* uncircles the entire group */
    public void uncircleGroup();
    /* circles all the elements in the group */
    public void circleAllChilds();
    /* uncircles all the elements in the group */
    public void uncircleAllChilds();
    /* return the views currently selected in the group */
    List<View> getSelectedViews();
    /* return the view containing the group */
    View getGroupView();

}
