package com.example.keyboard.activities;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.keyboard.R;

public class SmsActivity extends Activity {

    EditText et_number, et_message;
    private Button greenButton;
    private Button redButton;
    private Button blueButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sms);
        et_number=(EditText)findViewById(R.id.et_number);
        et_message=(EditText)findViewById(R.id.et_message);

        // if was started from contacts
        if (this.getIntent().hasExtra("phone number") == true) {
            Bundle b = getIntent().getExtras();
            et_number.setText(b.getString("phone number"));

            // open the keyboard on the message edit-text
            et_message.setFocusable(true);
            et_message.setFocusableInTouchMode(true);
            et_message.requestFocus();

            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.toggleSoftInput(InputMethodManager.SHOW_IMPLICIT, 0);

        }else{
            et_number.requestFocus();

            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.toggleSoftInput(InputMethodManager.SHOW_IMPLICIT, 0);
        }
        initButtons();
        initTextViewDoneListener();
    }

    private void initTextViewDoneListener() {
        TextView.OnEditorActionListener editTextListener = new TextView.OnEditorActionListener() {

            @Override
            public boolean onEditorAction(TextView v, int actionId,
                                          KeyEvent event) {
                if (event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) {
                    InputMethodManager in = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);

                    in.hideSoftInputFromWindow(v.getApplicationWindowToken(),
                            InputMethodManager.HIDE_NOT_ALWAYS);

                    // show the buttons when the keyboard is closed
                    redButton.setVisibility(View.VISIBLE);
                    greenButton.setVisibility(View.VISIBLE);
                    blueButton.setVisibility(View.VISIBLE);

                    // Must return true here to consume event
                    return true;
                }
                return false;
            }
        };
        et_number.setOnEditorActionListener(editTextListener);
        et_message.setOnEditorActionListener(editTextListener);
    }

    protected void sendSms() {
        String number=et_number.getText().toString();
        String message=et_message.getText().toString();

        Log.d("size number",Integer.toString(number.length()));
        Log.d("size message", Integer.toString(message.length()));
        String regex = "[0-9]+";

        if (this.getIntent().hasExtra("phone number") == false) {
            // if wasn't stated from contacts - do some checks on validity of the input
            if(!number.matches(regex)){
                Toast.makeText(getApplicationContext(), "number must contain only digits", Toast.LENGTH_LONG).show();
                return;
            }
            if(number.length() != 10){
                Toast.makeText(getApplicationContext(), "number must contain 10 digits", Toast.LENGTH_LONG).show();
                return;
            }
            if(message.length() == 0){
                Toast.makeText(getApplicationContext(), "message can not be empty", Toast.LENGTH_LONG).show();
                return;
            }
        }

        SmsManager manager=SmsManager.getDefault();
        manager.sendTextMessage(number, null, message, null, null);
        Toast.makeText(getApplicationContext(), "sent succesfully", Toast.LENGTH_LONG).show();
        et_number.setText("");
        et_message.setText("");
    }

    /**
     * Initialize the 3 buttons with two options (Arrows or Write a Name) to select a contact,
     * and a "Back" button.
     */
    private void initButtons() {
        greenButton = (Button) findViewById(R.id.b_green);
        redButton = (Button) findViewById(R.id.b_red);
        blueButton = (Button) findViewById(R.id.b_blue);

        greenButton.setTextSize(20);
        greenButton.setTextColor(Color.BLACK);

        redButton.setTextSize(20);
        redButton.setTextColor(Color.BLACK);
        // if there are two contacts with the same name, let the user choose between them

        blueButton.setTextSize(20);
        blueButton.setTextColor(Color.BLACK);

        setButtonsToDefault();
    }

    private void setButtonsToDefault() {

        greenButton.setText("Back");
        redButton.setText("Send");
        blueButton.setText("Change Details");

        // "Back" button - select the currently checked item in the contacts list
        greenButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("sms", "finish");
                finish();
            }
        });

        // "Go Down" button - scrolls down in the list of contacts
        redButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendSms();
            }
        });

        blueButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setButtonsToDetails();
            }
        });
    }

    private void setButtonsToDetails() {

        blueButton.setText("Change Number");
        redButton.setText("Change Msg");

        // "Back" button - select the currently checked item in the contacts list
        blueButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("sms", "Change Number");
                et_number.requestFocus();
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.toggleSoftInput(InputMethodManager.SHOW_IMPLICIT, 0);
                setButtonsToDefault();
            }
        });

        // "Go Down" button - scrolls down in the list of contacts
        redButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("sms", "Change Msg");
                et_message.requestFocus();
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.toggleSoftInput(InputMethodManager.SHOW_IMPLICIT, 0);
                setButtonsToDefault();
            }
        });
    }

}
