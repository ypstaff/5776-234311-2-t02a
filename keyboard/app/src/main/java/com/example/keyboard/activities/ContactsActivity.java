package com.example.keyboard.activities;

import java.util.ArrayList;
import java.util.List;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.app.Activity;
import android.provider.ContactsContract;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.keyboard.R;
import com.example.keyboard.classes.CommunicationAction;
import com.example.keyboard.classes.PhoneBook;
import com.example.keyboard.classes.PhoneBookAdapter;

/**
 * @author Or Mauda
 */
public class ContactsActivity extends Activity {

	private ListView lvPhone;
    private ListView listDialogOptions;
    private EditText etSearch;

    private PhoneBook selectedContact;

    private FrameLayout historyContainer;
    private ViewStub viewStub;
    private final List<PhoneBook> historyList = new ArrayList<PhoneBook>();
    private static final List<PhoneBook> historyData = new ArrayList<PhoneBook>();

    // 3 navigation buttons
    private Button greenButton, redButton, blueButton;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_contacts);
		
		lvPhone = (ListView)findViewById(R.id.listPhone);
        etSearch = (EditText) findViewById(R.id.xEt);

        fillLvPhoneWithContacts();

        etSearch.setFocusable(true);
        etSearch.setFocusableInTouchMode(true);
        etSearch.requestFocus();

        initButtons();

        // hide the buttons when the keyboard is open
        redButton.setVisibility(View.GONE);
        greenButton.setVisibility(View.GONE);
        blueButton.setVisibility(View.GONE);

        etSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {

            @Override
            public boolean onEditorAction(TextView v, int actionId,
                                          KeyEvent event) {
                if (event != null&& (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) {
                    InputMethodManager in = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);

                    in.hideSoftInputFromWindow(etSearch.getApplicationWindowToken(),
                            InputMethodManager.HIDE_NOT_ALWAYS);

                    // show the buttons when the keyboard is closed
                    redButton.setVisibility(View.VISIBLE);
                    greenButton.setVisibility(View.VISIBLE);
                    blueButton.setVisibility(View.VISIBLE);

                    // Must return true here to consume event
                    return true;

                }
                return false;
            }
        });
        // open the keyboard
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_IMPLICIT, 0);

        setFilterList();
	}

    /**
     * Initialize the 3 buttons with two options (Arrows or Write a Name) to select a contact,
     * and a "Back" button.
     */
    private void initButtons() {
        greenButton = (Button) findViewById(R.id.b_green);
        redButton = (Button) findViewById(R.id.b_red);
        blueButton = (Button) findViewById(R.id.b_blue);

        greenButton.setTextSize(20);
        greenButton.setTextColor(Color.BLACK);
        greenButton.setText("Select");

        redButton.setTextSize(20);
        redButton.setTextColor(Color.BLACK);
        // if there are two contacts with the same name, let the user choose between them
        redButton.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, R.drawable.arrow_down);

        blueButton.setTextSize(20);
        blueButton.setTextColor(Color.BLACK);
        blueButton.setText("Back");

        // "Go Down" button - scrolls down in the list of contacts
        redButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                scrollOneDown();
            }
        });

        // "Select" button - select the currently checked item in the contacts list
        greenButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectChecked();
            }
        });

        blueButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void selectChecked() {
        int currentCheckedPosition = lvPhone.getCheckedItemPosition();
        PhoneBook checkedPhone = (PhoneBook) lvPhone.getItemAtPosition(currentCheckedPosition);
        etSearch.setText(checkedPhone.getmName());

        // set the selectedContact variable
        selectedContact = new PhoneBook(checkedPhone);

        // open a dialog to allow the user to choose: sms or phone call
        openDialog();
    }

    /**
     * opens a custom dialog, to allow the user to choose the proper action (sms\phone call...)
     */
    private void openDialog() {

        // open custom dialog
        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.contacts_dialog);
        dialog.setTitle("Select an Action For " + selectedContact.getmName());
        listDialogOptions = (ListView) dialog.findViewById(R.id.listOptions);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.contacts_select_dialog_item, CommunicationAction.getActionOptions());

        listDialogOptions.setAdapter(adapter);

        listDialogOptions.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        listDialogOptions.setItemChecked(0, true);

        initDialogButtons(dialog);

        dialog.show();
    }

    /**
     * initialize the dialog buttons.
     * @param dialog - the dialog
     */
    private void initDialogButtons(final Dialog dialog) {
        Button greenButtonDialog, redButtonDialog, blueButtonDialog;
        greenButtonDialog = (Button) dialog.findViewById(R.id.b_green);
        redButtonDialog = (Button) dialog.findViewById(R.id.b_red);
        blueButtonDialog = (Button) dialog.findViewById(R.id.b_blue);

        greenButtonDialog.setTextSize(20);
        greenButtonDialog.setTextColor(Color.BLACK);
        greenButtonDialog.setText("Select");

        redButtonDialog.setTextSize(20);
        redButtonDialog.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, R.drawable.arrow_down);

        blueButtonDialog.setTextSize(20);
        blueButtonDialog.setTextColor(Color.BLACK);
        blueButtonDialog.setText("Cancel");

        // "Go Down" button - scrolls down in the list of contacts
        redButtonDialog.setOnClickListener(new View.OnClickListener() {
            // scroll one down in dialog actions
            @Override
            public void onClick(View v) {
                int index = listDialogOptions.getCheckedItemPosition();
                if (index + 1 == listDialogOptions.getCount()) {
                    listDialogOptions.setItemChecked(0, true);
                    listDialogOptions.smoothScrollToPositionFromTop(0, 0, 0);
                    listDialogOptions.deferNotifyDataSetChanged();
                    return;
                }

                index++;
                listDialogOptions.setItemChecked(index, true);
                listDialogOptions.smoothScrollToPositionFromTop(index, listDialogOptions.getHeight() / 2);
                listDialogOptions.deferNotifyDataSetChanged();
            }
        });

        // "Select" button - select the currently checked item in the options list
        greenButtonDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int currentCheckedPosition = listDialogOptions.getCheckedItemPosition();
                CommunicationAction action = CommunicationAction.values()[currentCheckedPosition];

                Bundle b = new Bundle();
                b.putString("phone number", selectedContact.getmPhone());

                Intent intent = action.getCommunicationIntent(b, getApplicationContext());
                startActivity(intent);
            }
        });

        blueButtonDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                // let the user have another try to select a different contact
                etSearch.setSelection(etSearch.getText().length());

                etSearch.requestFocus();

                // hide the buttons when the keyboard is open
                redButton.setVisibility(View.GONE);
                greenButton.setVisibility(View.GONE);
                blueButton.setVisibility(View.GONE);

                // open the keyboard
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.toggleSoftInput(InputMethodManager.SHOW_IMPLICIT, 0);

            }
        });
    }

    private void scrollOneDown() {
        int index = lvPhone.getCheckedItemPosition();
        if (index + 1 == lvPhone.getCount()) {
            lvPhone.setItemChecked(0, true);
            lvPhone.smoothScrollToPositionFromTop(0, 0, 0);
            lvPhone.deferNotifyDataSetChanged();
            return;
        }

        index++;
        lvPhone.setItemChecked(index, true);
        lvPhone.smoothScrollToPositionFromTop(index, lvPhone.getHeight() / 2);
        lvPhone.deferNotifyDataSetChanged();
    }


    /**
     * Fills the list view with the contacts, and sets its adapter.
     */
    private void fillLvPhoneWithContacts() {
        List<PhoneBook> listPhoneBook = new ArrayList<PhoneBook>();

        Cursor phones = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,null,null, null);
        while (phones.moveToNext())
        {
            String name=phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
            String phoneNumber = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NORMALIZED_NUMBER));
            listPhoneBook.add(new PhoneBook(name, phoneNumber, ""/*email*/));
        }
        phones.close();

        PhoneBookAdapter adapter = new PhoneBookAdapter(this, listPhoneBook);
        lvPhone.setAdapter(adapter);

        lvPhone.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        lvPhone.setItemChecked(0, true);
    }

    @Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.

		//getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

    protected void setFilterList() {

        historyContainer = (FrameLayout) findViewById(R.id.history_container_layout);
        //EditText filterEditText = (EditText) findViewById(R.id.filter_text);
        etSearch.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                historyContainer.removeAllViews();
                final List<PhoneBook> tempHistoryList = new ArrayList<PhoneBook>();
                tempHistoryList.addAll(historyList);

                for (PhoneBook aPhone : historyList) {
                    if (aPhone.getmName().toLowerCase().indexOf(s.toString().toLowerCase()) == -1) {
                        tempHistoryList.remove(aPhone);
                    }
                }

                viewStub = new ViewStub(ContactsActivity.this, R.layout.history_schedule);

                // place the new view in place
                RelativeLayout.LayoutParams p = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT);
                p.addRule(RelativeLayout.BELOW, R.id.xLinearEt);
                p.addRule(RelativeLayout.ABOVE, R.id.threeButtons);

                viewStub.setLayoutParams(p);

                viewStub.setOnInflateListener(new ViewStub.OnInflateListener() {

                    @Override
                    public void onInflate(ViewStub stub, View inflated) {
                        setUIElements(inflated, tempHistoryList);
                    }
                });

                historyContainer.addView(viewStub);
                viewStub.inflate();
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

            }
        });
        setViewStub();
        lvPhone.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        lvPhone.setItemChecked(0, true);
    }

    private void setViewStub() {

        Cursor phones = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,null,null, null);
        while (phones.moveToNext()) {
            String name=phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
            String phoneNumber = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
            historyList.add(new PhoneBook(name, phoneNumber,""/*email*/));
        }
        phones.close();

        viewStub = new ViewStub(ContactsActivity.this, R.layout.history_schedule);
        viewStub.setOnInflateListener(new ViewStub.OnInflateListener() {

            @Override
            public void onInflate(ViewStub stub, View inflated) {
                setUIElements(inflated, historyList);
            }
        });

        historyContainer.addView(viewStub);
        viewStub.inflate();
    }

    private void setUIElements(View v, List<PhoneBook> historyLists) {
        if(v != null) {
            historyData.clear();

            historyData.addAll(historyLists);

            lvPhone = (ListView)findViewById(R.id.listPhone);
            lvPhone.setAdapter(new PhoneBookAdapter(this, historyData));

            lvPhone.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
            lvPhone.setItemChecked(0, true);

            registerForContextMenu(lvPhone);
        }
    }
}
