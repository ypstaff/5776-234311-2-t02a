package com.example.keyboard.classes;

import android.graphics.Point;


import java.util.LinkedList;
import java.util.Random;

/**
 * @author Inbar Donag
 */
public class SnakeGame {

    // scores & level
    private int score = 0;
    private int highScore = 0;
    private boolean newHighScore = false;
    private int level = 1;

    // snake
    private long sleepTime = 0; // time in ms between 2 snake moves. the higher the sleep time, the easier the game
    private int currentDirection;
    private LinkedList<Cell> snakeCells;
    private boolean gotFood = false;
    private boolean isDead = false;

    // the board of cells
    private Cell[][] board;
    private Point boardSize;

    // props
    private int numOfMovesTillLevelUpProp = 100;

    ////////////////////////////////////////////////////////////////////////////////////////////////

    public SnakeGame() {}

    public void increaseScore(int inc) {
        score += inc;
    }

    public int getScore(){
        return score;
    }

    public int getHighScore() {
        return highScore;
    }

    public void setHighScore(int s) {
        highScore = s;
    }

    public void updateHighScore() {
        if (score > highScore) {
            highScore = score;
            newHighScore = true;
        } else {
            newHighScore = false;
        }
    }

    public boolean isNewHighScore() {
        return newHighScore;
    }

    public void reset() {
        score = 0;
        level = 1;
    }

    public void levelUp() {
        level++;
        makeSnakeFaster();
    }

    public int getLevel() {
        return level;
    }

    ///////////////////////////////////////// board ////////////////////////////////////////////////

    public static class Cell {

        public  enum  Type {SNAKE_BODY, SNAKE_HEAD, BOMB, LEVEL_UP, FOOD, STAR, EXPLOSION, EMPTY}

        public  Cell up, upperRight, right, lowerRight, down, lowerLeft, left, upperLeft; // neighbor cells
        private Type type;
        public int rowIndex = 0, colIndex = 0;     // indexes on board

        public Cell(int i, int j) {
            up = null;
            upperRight = null;
            right = null;
            lowerRight = null;
            down = null;
            lowerLeft = null;
            left = null;
            upperLeft = null;
            rowIndex = i;
            colIndex = j;

            clear();
        }

        public void clear() {
            type = Type.EMPTY;
        }

        public void setHead() {
            type = Type.SNAKE_HEAD;
        }

        public void setBody()
        {
            type = Type.SNAKE_BODY;
        }

        public void setExplosion() {
            type = Type.EXPLOSION;
        }

        public void setFood() {
            type = Type.FOOD;
        }

        public void setStar() {
            type = Type.STAR;
        }

        public void setBomb() {
            type = Type.BOMB;
        }

        public void setLevelUp() {
            type = Type.LEVEL_UP;
        }

        Type getType() {
            return type;
        }

        boolean isOnSnake() {
            return  type == Type.SNAKE_HEAD || type == Type.SNAKE_BODY;
        }

    }

    public void initBoard(int rowsCount, int colsCount) {
        boardSize = new Point(rowsCount, colsCount);
        board = new Cell[rowsCount][colsCount];

        for (int i=0; i < rowsCount; i++) {

            for (int j = 0; j < colsCount; j++) {

                board[i][j] = new Cell(i, j);
            }
        }

        for (int i=0; i < rowsCount; i++) {
            for (int j = 0; j < colsCount; j++) {


                if (j-1 >= 0) {
                    board[i][j].left = board[i][j-1];
                }
                if (i-1 >= 0) {
                    board[i][j].up = board[i-1][j];
                }
                if (i-1 >= 0 && j-1 >= 0) {
                    board[i][j].upperLeft = board[i-1][j-1];
                }
                if (j+1 < colsCount) {
                    board[i][j].right = board[i][j+1];
                }
                if (i+1 < rowsCount) {
                    board[i][j].down = board[i+1][j];
                }
                if (i+1 < rowsCount && j+1 < colsCount) {
                    board[i][j].lowerRight = board[i+1][j+1];
                }
                if (i-1 >= 0 && j+1 < colsCount) {
                    board[i][j].upperRight = board[i-1][j+1];
                }
                if (i+1 < rowsCount && j-1 >= 0) {
                    board[i][j].lowerLeft = board[i+1][j-1];
                }
            }
        }
    }

    public Cell getCell(int i, int j) {
        return board[i][j];
    }

    ////////////////////////////////////// snake ///////////////////////////////////////////////////

    // directions
    public static final int LEFT = 0, UPPER_LEFT = 1, UP = 2, UPPER_RIGHT = 3,
            RIGHT = 4, LOWER_RIGHT = 5, DOWN = 6, LOWER_LEFT = 7;

    public enum Difficulty {EASY, MEDIUM, HARD};

    public void initSnake(Difficulty difficulty) {
        // put the snake head in the center
        int i = boardSize.x /2;
        int j = boardSize.y /2;
        Cell center = getCell(i,j);
        center.setHead();
        // and its little body
        center.left.setBody();
        center.left.left.setBody();

        snakeCells = new LinkedList<>();
        snakeCells.addFirst(center.left.left);
        snakeCells.addFirst(center.left);
        snakeCells.addFirst(center);

        currentDirection = RIGHT;

        if (difficulty == Difficulty.EASY)
            sleepTime = 500;
        else if (difficulty == Difficulty.MEDIUM)
            sleepTime = 300;
        else if (difficulty == Difficulty.HARD)
            sleepTime = 200;
        else
            assert(false);
    }


    public void advanceSnake(Cell cell) {
        if (cell == null) {
            return;
        }
        snakeCells.getFirst().setBody();
        cell.setHead();
        snakeCells.addFirst(cell);

        if (gotFood) {
            gotFood = false;
            return;  // if the snake just got food it's tail won't be removed, so it grows
        }
        snakeCells.getLast().clear();
        snakeCells.removeLast();

    }

    private void makeSnakeFaster() {
        if (sleepTime - 20 < 0 )
            return;
        sleepTime -= 20;
    }


    public void turnLeft() {
        // change direction of the snake counterclockwise
        currentDirection--;
        if (currentDirection < 0) {
            currentDirection = 7;
        }
    }

    public void turnRight() {
        // change direction of the snake clockwise
        currentDirection++;
        if (currentDirection > 7) {
            currentDirection = 0;
        }
    }

    private void handleCollision(Cell c) {

        if (c == null ) { // collision with wall
            isDead = true;

        } else if (c.getType() == Cell.Type.BOMB) {
            advanceSnake(c);
            c.setExplosion();
            isDead = true;

        } else if (c.getType() == Cell.Type.SNAKE_BODY) {
            isDead = true;

        } else if (c.getType() == Cell.Type.EMPTY) {
            return;

        } else if (c.getType() == Cell.Type.FOOD) {
            increaseScore(20 * getLevel());
            gotFood = true;
            c.clear();
            getNewPropCell().setFood();

        } else if (c.getType() == Cell.Type.STAR) {
            increaseScore(50 * getLevel());
            c.clear();
            getNewPropCell().setStar();

        } else if (c.getType() == Cell.Type.LEVEL_UP) {
            levelUp();
            increaseScore(100 * getLevel());
            c.clear();
            numOfMovesTillLevelUpProp = 100;
        }
    }

    public void move() {

        // move the snake in the current direction after sleepTime
        Cell head = null;

        switch (currentDirection){
            case LEFT:
                head = snakeCells.getFirst().left;
                break;
            case UPPER_LEFT:
                head = snakeCells.getFirst().upperLeft;
                break;
            case UP:
                head = snakeCells.getFirst().up;
                break;
            case UPPER_RIGHT:
                head = snakeCells.getFirst().upperRight;
                break;
            case RIGHT:
                head = snakeCells.getFirst().right;
                break;
            case LOWER_RIGHT:
                head = snakeCells.getFirst().lowerRight;
                break;
            case DOWN:
                head = snakeCells.getFirst().down;
                break;
            case LOWER_LEFT:
                head = snakeCells.getFirst().lowerLeft;
                break;
        }

        handleCollision(head);
        if (!isDead) {
            advanceSnake(head);
            if (numOfMovesTillLevelUpProp > 0) {
                numOfMovesTillLevelUpProp--;
                if (numOfMovesTillLevelUpProp == 0)
                    getNewPropCell().setLevelUp();
            }
        }
    }

    public boolean isSnakeDead() {
        return isDead;
    }

    public long getSnakeSleepTime() {
        return sleepTime;
    }

    public Cell getCellUnderSnakeHead() {
        return snakeCells.getFirst();
    }

    public int getSnakeLength() {
        return snakeCells.size();
    }

    public Cell getCellUnderSnakeNode(int index) {
        return snakeCells.get(index);
    }

    public int getSnakeCurrDir() {
        return currentDirection;
    }

    //////////////////////////////////////// props /////////////////////////////////////////////////

    // for scattering props on board
    private Random random = new Random();

    // generate a cell on board that is not on the snake or a level up prop for placing a new prop inside
    private SnakeGame.Cell getNewPropCell() {
        boolean onSnake = true;
        boolean onLevelUp = true;
        int i = 0, j = 0;
        while (onSnake || onLevelUp) {
            i = random.nextInt(boardSize.x);
            j = random.nextInt(boardSize.y);
            onSnake = getCell(i, j).isOnSnake();
            onLevelUp = getCell(i, j).getType() == SnakeGame.Cell.Type.LEVEL_UP;
        }
        return getCell(i, j);
    }

    public void scatterPropsOnBoard() {

        int propsAmount = (boardSize.x * boardSize.y) / 10;
        for (int propCount = 0; propCount < propsAmount; propCount++ ) {
            int i = random.nextInt(boardSize.x);
            int j = random.nextInt(boardSize.y);
            int k = random.nextInt(3);

            if (k==0) {
                getCell(i,j).setFood();
            } else if (k==1) {
                getCell(i,j).setStar();
            } else {
                getCell(i,j).setBomb();
            }
        }
    }



}
