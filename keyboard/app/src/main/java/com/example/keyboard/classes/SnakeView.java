package com.example.keyboard.classes;


import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.CornerPathEffect;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.Shader;
import android.os.Handler;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.View;

import com.example.keyboard.R;
import com.example.keyboard.activities.SnakeActivity;

/**
 * @author Inbar Donag
 */
public class SnakeView extends View
{
    private Context context;
    private SnakeGame game;
    private Handler handler = new Handler();

    public SnakeGame getGame() {
        return game;
    }

    /////////////////////////////////////// graphics ///////////////////////////////////////////////
    // styles
    private Paint paintBackground;
    private Paint paintBorder;
    private Paint paintSnake;

    // bitmaps
    private BitmapFactory bitmapFactory; // used to create bitmap out of png file
    private Resources resources = getResources();
    private Bitmap snakeHeadPic = bitmapFactory.decodeResource(resources, R.drawable.snake_head, null);
    private Bitmap explosionPic = bitmapFactory.decodeResource(resources, R.drawable.explosion, null);
    private Bitmap foodPic = bitmapFactory.decodeResource(resources, R.drawable.food, null);
    private Bitmap starPic = bitmapFactory.decodeResource(resources, R.drawable.star, null);
    private Bitmap bombPic = bitmapFactory.decodeResource(resources, R.drawable.bomb, null);
    private Bitmap levelUpPic = bitmapFactory.decodeResource(resources, R.drawable.level_up, null);

    private Bitmap getBitmapFromCellType(SnakeGame.Cell c) {

        switch (c.getType()) {

            case SNAKE_HEAD:
                return snakeHeadPic;
            case BOMB:
                return bombPic;
            case LEVEL_UP:
                return levelUpPic;
            case FOOD:
                return foodPic;
            case STAR:
                return starPic;
            case EXPLOSION:
                return explosionPic;
            case EMPTY:
                return null;
        }
        return null;

    }

    ///////////////////////////////////// load & save //////////////////////////////////////////////


    private void saveHighScore() {

        //setting preferences
        SharedPreferences prefs = context.getSharedPreferences("prefsKey", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putInt("key", game.getHighScore());
        editor.apply();
    }

    private void loadHighScore() {

        //getting preferences
        SharedPreferences prefs = context.getSharedPreferences("prefsKey", Context.MODE_PRIVATE);
        int highScore = prefs.getInt("key", 0); //0 is the default value
        game.setHighScore(highScore);
    }

    ///////////////////////////////////////// sizes ////////////////////////////////////////////////

    private int rowsCount = 0;
    private int colsCount = 0;
    private int picSize = 0;
    private int viewWidth = 0;
    private int viewHeight = 0;
    private int verticalBorderWidth = 0;
    private int horizontalBorderWidth = 0;


    ///////////////////////////////////////// snake ////////////////////////////////////////////////

    public Runnable move = new Runnable()
    {
        @Override
        public void run() {
            game.move();
            invalidate();
            ((SnakeActivity)context).updateGUI();

            if (game.isSnakeDead()) {
                getHandler().post(endGameHandler);
            }
            handler.postDelayed(move, game.getSnakeSleepTime());
        }
    };

    //////////////////////////////////////// init //////////////////////////////////////////////////

    private int difficulty = R.string.easy;

    public SnakeView(Context context) {
        super(context);
        init(context);
    }

    public SnakeView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public SnakeView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context);
    }

    public void setDifficulty(int d) {
        difficulty = d;
    }

    @Override
    protected void onSizeChanged(int xNew, int yNew, int xOld, int yOld){
        super.onSizeChanged(xNew, yNew, xOld, yOld);

        viewWidth = xNew;
        viewHeight = yNew;

        DisplayMetrics metrics = context.getResources().getDisplayMetrics();
        picSize = (int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_MM, 4, metrics);
        paintSnake.setStrokeWidth(picSize);

        rowsCount = viewHeight/picSize;
        colsCount = viewWidth/picSize;

        int boardHeight = rowsCount * picSize;
        int boardWidth = colsCount * picSize;

        verticalBorderWidth = (viewWidth - boardWidth) / 2;
        horizontalBorderWidth = (viewHeight - boardHeight) / 2;
    }

    private void init(Context c) {
        context = c;
        paintBackground = new Paint();
        bitmapFactory = new BitmapFactory();

        // set background style
        Bitmap background = bitmapFactory.decodeResource(resources, R.drawable.background, null);
        BitmapShader shader = new BitmapShader(background, Shader.TileMode.REPEAT, Shader.TileMode.REPEAT);
        paintBackground.setShader(shader);

        // set border style
        paintBorder = new Paint();
        paintBorder.setColor(Color.LTGRAY);

        // set snake style
        paintSnake = new Paint();
        paintSnake.setColor(Color.argb(255, 150, 211, 96));
        paintSnake.setStyle(Paint.Style.STROKE);
        paintSnake.setPathEffect(new CornerPathEffect(50));
        paintSnake.setStrokeJoin(Paint.Join.ROUND);
        paintSnake.setStrokeCap(Paint.Cap.ROUND);
        paintSnake.setAntiAlias(true);

    }

    //////////////////////////////////// game states ///////////////////////////////////////////////


    private Runnable endGameHandler = new Runnable() {
        @Override
        public void run() {
            endGame();
        }
    };


    public enum GameState {
        INITIAL, // user just opened the activity
        GAME_RUNNING,
        GAME_PAUSED,
        GAME_OVER,
    }

    public GameState gameState = GameState.INITIAL;


    public void startGame()
    {
        if (gameState != GameState.GAME_PAUSED) {
            // start new game
            game = new SnakeGame();
            loadHighScore();
            game.initBoard(rowsCount, colsCount);
            game.scatterPropsOnBoard();
            switch (difficulty) {
                case R.string.easy:
                    game.initSnake(SnakeGame.Difficulty.EASY);
                    break;
                case R.string.medium:
                    game.initSnake(SnakeGame.Difficulty.MEDIUM);
                    break;
                case R.string.hard:
                    game.initSnake(SnakeGame.Difficulty.HARD);
                    break;

            }
            // draw the board
            invalidate();

        }
        handler.post(move);
        gameState = GameState.GAME_RUNNING;
    }

    public void pauseGame()
    {
        if (gameState == GameState.GAME_RUNNING) {
            gameState = GameState.GAME_PAUSED;
        }
        handler.removeCallbacks(move);
    }

    public void endGame()
    {
        game.updateHighScore();
        if(game.isNewHighScore())
            saveHighScore();

        game.reset();

        gameState = GameState.GAME_OVER;
        ((SnakeActivity)context).updateGUI();

        handler.removeCallbacks(move);
    }

    //////////////////////////////////////// drawing ///////////////////////////////////////////////

    // converts indexes on board to the (x,y) position on board
    private Point convertIndexesToPosition(SnakeGame.Cell c) {

        if (c == null)
            return null;

        int i = c.rowIndex;
        int j = c.colIndex;

        int x = verticalBorderWidth + j * picSize + picSize/2;
        int y = horizontalBorderWidth + i * picSize + picSize/2;

        Point p = new Point();
        p.set(x, y);
        return p;
    }

    private boolean isOrdered(int a, int b, int c) {
        return (a > b && b > c) || (a < b && b < c);
    }

    private boolean isStraightLine(Point a, Point b, Point c) {


        if (a == null || b == null || c == null)
            return true;

        if (a.x == b.x && b.x == c.x)
            return true;

        if (a.y == b.y && b.y == c.y)
            return true;

        if (isOrdered(a.x,b.x,c.x) && isOrdered(a.y,b.y,c.y))
            return true;

        return false;

    }

    private Bitmap rotate(Bitmap bmp, int angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(bmp, 0, 0, bmp.getWidth(), bmp.getHeight(), matrix, true);
    }

    @Override
    protected void onDraw(Canvas canvas)
    {
        super.onDraw(canvas);

        // draw background
        canvas.drawRect(0, 0, viewWidth, viewHeight, paintBackground);

        // draw border
        canvas.drawRect(0,0,verticalBorderWidth-1,viewHeight,paintBorder);
        canvas.drawRect(viewWidth-verticalBorderWidth,0,viewWidth,viewHeight,paintBorder);

        canvas.drawRect(0,0,viewWidth,horizontalBorderWidth,paintBorder);
        canvas.drawRect(0,viewHeight-horizontalBorderWidth,viewWidth,viewHeight,paintBorder);

        if (game == null)
            return;

        SnakeGame.Cell headCell = null;
        // draw game board
        for (int i = 0; i < rowsCount; i++) {
            for (int j = 0; j < colsCount; j++) {
                SnakeGame.Cell curr = game.getCell(i,j);

                Bitmap bmp = getBitmapFromCellType(curr);
                if (bmp != null) {

                    if (curr.getType() == SnakeGame.Cell.Type.SNAKE_HEAD || curr.getType() == SnakeGame.Cell.Type.EXPLOSION) {
                        headCell = curr;
                        continue;
                    }
                    Point pos = convertIndexesToPosition(curr);
                    Rect r = new Rect(pos.x - picSize/2, pos.y - picSize/2, pos.x + picSize/2, pos.y + picSize/2);
                    canvas.drawBitmap(bmp, null, r, null);
                }

            }
        }



        // draw snake
        Path path = new Path();
        SnakeGame.Cell head = game.getCellUnderSnakeHead();
        Point pos = convertIndexesToPosition(head);
        path.moveTo(pos.x, pos.y);
        int size = game.getSnakeLength();


        for (int i = 0; i < size-1 ;)
        {
            Point p1 = convertIndexesToPosition(game.getCellUnderSnakeNode(i));
            Point p2 = convertIndexesToPosition(game.getCellUnderSnakeNode(i + 1));
            Point p3 = convertIndexesToPosition(i+2 <  size ? game.getCellUnderSnakeNode(i + 2) : null);

            if (isStraightLine(p1,p2,p3)) {
                path.lineTo(p2.x,p2.y);
                i++;
            } else {
                path.quadTo(p2.x, p2.y, p3.x, p3.y);
                i+=2;
            }
        }

        canvas.drawPath(path, paintSnake);

        Bitmap headPic = getBitmapFromCellType(headCell);
        int d = game.getSnakeCurrDir();
        if (d == SnakeGame.RIGHT) {
            headPic = rotate(headPic, 270);
        } else if (d == SnakeGame.LEFT) {
            headPic = rotate(headPic, 90);
        } else if (d == SnakeGame.UP || d == SnakeGame.UPPER_LEFT || d == SnakeGame.UPPER_RIGHT) {
            headPic = rotate(headPic, 180);
        }

        Point headPos = convertIndexesToPosition(headCell);
        Rect r =  new Rect(headPos.x -picSize/2, headPos.y - picSize/2, headPos.x + picSize/2, headPos.y + picSize/2);
        canvas.drawBitmap(headPic, null, r, null);
    }
}

