package com.example.keyboard.classes;

import android.inputmethodservice.InputMethodService;
import android.inputmethodservice.Keyboard;
import android.inputmethodservice.KeyboardView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.InputConnection;

import com.example.keyboard.R;

/**
 * Created by Matan Revivo on 26/12/2015.
 */
public class MyKeyboard extends InputMethodService
        implements KeyboardView.OnKeyboardActionListener {

    //main buttons
    public static final int GREEN_CODE = -11;
    public static final int RED_CODE = -22;
    public static final int BLUE_CODE = -33;

    private MyKeyboardView m_keyboardView;
    private Keyboard m_keyboard;
    private boolean caps = false;

    @Override
    public View onCreateInputView() {
        m_keyboardView = (MyKeyboardView) getLayoutInflater().inflate(R.layout.keyboard, null);
        m_keyboard = new Keyboard(this, R.xml.qwerty);
        m_keyboardView.setKeyboard(m_keyboard);
        m_keyboardView.setOnKeyboardActionListener(this);
        m_keyboardView.resetKeys();

        return m_keyboardView;
    }


    @Override
    public void onPress(int primaryCode) {

    }

    @Override
    public void onRelease(int primaryCode) {

    }

    @Override
    public void onKey(int primaryCode, int[] keyCodes) {

        boolean bCommit = false;
        switch (primaryCode) {
            case GREEN_CODE:
                //TODO autocomplete, for now reset keys
                m_keyboardView.resetKeys();
                break;
            case RED_CODE:
                bCommit = m_keyboardView.chooseColor(RED_CODE);
                break;
            case BLUE_CODE:
                bCommit = m_keyboardView.chooseColor(BLUE_CODE);
                break;
            default:
                return;
        }
        if (bCommit){
            keyCommit(m_keyboardView.getCodeToCommit());
            m_keyboardView.resetKeys();
        }
        else {
            m_keyboardView.updateColor();
        }
        m_keyboardView.postInvalidate();  //invalidate the view to force a draw.


    }

    /**
     * simulates a normal key press after a key was chose through our 3 button input method
     * @param primaryCode the code of key being committed
     */
    private void keyCommit(int primaryCode) {
        Log.d("keyCommit", String.valueOf(primaryCode));
        InputConnection ic = getCurrentInputConnection();
        switch (primaryCode) {
            case Keyboard.KEYCODE_DELETE:
                ic.deleteSurroundingText(1, 0);
                break;
            case Keyboard.KEYCODE_SHIFT:
                caps = !caps;
                m_keyboard.setShifted(caps);
                m_keyboardView.invalidateAllKeys();
                break;
            case Keyboard.KEYCODE_DONE:
                ic.sendKeyEvent(new KeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_ENTER));
                ic.sendKeyEvent(new KeyEvent(KeyEvent.ACTION_UP, KeyEvent.KEYCODE_ENTER));
                break;
            default:
                char code = (char) primaryCode;
                if (Character.isLetter(code) && caps) {
                    code = Character.toUpperCase(code);
                }
                ic.commitText(String.valueOf(code), 1);
        }
    }

    @Override
    public void onText(CharSequence text) {

    }

    @Override
    public void swipeLeft() {

    }

    @Override
    public void swipeRight() {

    }

    @Override
    public void swipeDown() {

    }

    @Override
    public void swipeUp() {

    }




    public Keyboard getKeyboard() {
        return m_keyboard;
    }
}
