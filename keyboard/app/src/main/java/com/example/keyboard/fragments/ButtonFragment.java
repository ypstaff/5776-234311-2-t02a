package com.example.keyboard.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.keyboard.R;

/**
 * @author Matan Revivo
 */

public class ButtonFragment extends android.support.v4.app.Fragment{
    public Button b_blue, b_red, b_green;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    enum OpCode {
        GREEN(1), RED(2), BLUE(3);
        private final int code;
        private OpCode(final int c) {
            code = c;
        }
        public int getCode() {
            return code;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.buttons_fragment, container);
        b_blue = (Button)view.findViewById(R.id.b_blue);
        b_red = (Button)view.findViewById(R.id.b_red);
        b_green = (Button)view.findViewById(R.id.b_green);
        return view;
    }
}
