package com.example.keyboard.classes;

/**
 * @author Or mauda
 * This class contains the data needed for a single contact.
 */
public class PhoneBook {

	private String mName;
	private String mPhone;
	private String mEmail;
	public PhoneBook(String mName, String mPhone, String mEmail) {
		if (null == mName) {
			throw new NullPointerException("mName must not be null");
		}
		if (mEmail != "" && false == mEmail.contains("@")) {
			throw new IllegalArgumentException("Email address must contain a '@'.");
		}
		this.mName = mName;
		this.mPhone = mPhone;
		this.mEmail = mEmail;
	}

    public PhoneBook(PhoneBook other) {
        this.mName = other.getmName();
        this.mPhone = other.getmPhone();
        this.mEmail = other.getmEmail();
    }

	public String getmName() {
		return mName;
	}
	public String getmPhone() {
		return mPhone;
	}
	public String getmEmail() {
		return mEmail;
	}
	public void setmName(String mName) {
		this.mName = mName;
	}
	public void setmPhone(String mPhone) {
		this.mPhone = mPhone;
	}
	public void setmEmail(String mEmail) {
		this.mEmail = mEmail;
	}
	
	
	
}
