package com.example.keyboard.activities;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;

import com.example.keyboard.R;
import com.example.keyboard.classes.MenuListAdapter;

import java.util.ArrayList;

/**
 * @author Or Mauda
 */
public class MainMenuActivity extends Activity {

    // main list
    private ListView menuListView;

    // 3 navigation buttons
    private Button greenButton, redButton, blueButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_menu);

        initMenuListView();
        initNavigationButtons();
    }

    /**
     * Initialize the menu List View and its adapter.
     */
    private void initMenuListView() {
        // initialize list's values
        ArrayList<String> values = new ArrayList<>();
        values.add("Camera");
        values.add("Snake");
        values.add("Phone");
        values.add("Sms");
        values.add("Contacts");
		values.add("Audio music player");
        values.add("Painter");


        menuListView = (ListView) findViewById(R.id.listViewMenu);
        // init the list's adapter
        MenuListAdapter adapter = new MenuListAdapter(this, values);
        menuListView.setAdapter(adapter);
        menuListView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        menuListView.setItemChecked(0, true);
    }

    /**
     * Initialize the 3 navigation buttons.
     */
    private void initNavigationButtons() {
        greenButton = (Button) findViewById(R.id.b_green);
        redButton = (Button) findViewById(R.id.b_red);
        blueButton = (Button) findViewById(R.id.b_blue);

        // add the arrows to the buttons
        greenButton.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, R.drawable.arrow_up);
        blueButton.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, R.drawable.arrow_down);

        redButton.setTextSize(20);
        redButton.setTextColor(Color.BLACK);
        redButton.setText("Select");

        greenButton.setNextFocusUpId(R.id.listViewMenu);
        blueButton.setNextFocusDownId(R.id.listViewMenu);

        // initialize buttons listeners
        greenButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // go up on the menu list
                int index = menuListView.getCheckedItemPosition();
                if (index == 0)
                    index = menuListView.getCount();

                index--;
                menuListView.setItemChecked(index, true);
                menuListView.smoothScrollToPositionFromTop(index, menuListView.getHeight() / 2);
                menuListView.deferNotifyDataSetChanged();
            }
        });

        blueButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // go down on the menu list
                int index = menuListView.getCheckedItemPosition();
                if (index + 1 == menuListView.getCount()) {
                    menuListView.setItemChecked(0, true);
                    menuListView.smoothScrollToPositionFromTop(0, 0, 0);
                    menuListView.deferNotifyDataSetChanged();
                    return;
                }

                index++;
                menuListView.setItemChecked(index, true);
                menuListView.smoothScrollToPositionFromTop(index, menuListView.getHeight() / 2);
                menuListView.deferNotifyDataSetChanged();
            }
        });


        redButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // enter the current activity
                int index = menuListView.getCheckedItemPosition();

                Intent intent;
                switch (index) {
                    case 0: {
                        // means we're on the camera
                        intent = new Intent(getApplicationContext(), CameraActivity.class);
                        startActivity(intent);
                        break;
                    }
                    case 1: {
                        // means we're on the snake
                        intent = new Intent(getApplicationContext(), SnakeActivity.class);
                        startActivity(intent);
                        break;
                    }
                    case 2: {
                        // means we're on the phone
                        intent = new Intent(getApplicationContext(), PhoneActivity.class);
                        startActivity(intent);
                        break;
                    }
                    case 3: {
                        // means we're on the audio
                        intent = new Intent(getApplicationContext(), SmsActivity.class);
                        startActivity(intent);
                        break;
                    }
                    case 4: {
                        // means we're on the contacts
                        intent = new Intent(getApplicationContext(), ContactsActivity.class);
                        startActivity(intent);
                        break;
                    }
                    case 5: {
                        // means we're on the audio
                        intent = new Intent(getApplicationContext(), AudioActivity.class);
                        startActivity(intent);
                        break;
                    }
                    case 6: {
                        // means we're on the painter
                        intent = new Intent(getApplicationContext(), PainterActivity.class);
                        startActivity(intent);
                        break;
                    }
                }
            }
        });
    }
}
