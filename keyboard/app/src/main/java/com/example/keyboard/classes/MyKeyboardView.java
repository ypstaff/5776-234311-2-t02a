package com.example.keyboard.classes;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.inputmethodservice.Keyboard;
import android.inputmethodservice.KeyboardView;
import android.util.AttributeSet;
import android.util.Log;
import android.view.KeyEvent;
import android.view.inputmethod.InputConnection;

import com.example.keyboard.R;

import java.security.Key;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Revivo Yehuda on 30/12/2015.
 */
public class MyKeyboardView extends KeyboardView {

    private final Context context;
    private List<Keyboard.Key> m_enabled;
    private List<Keyboard.Key> m_redKeys = new ArrayList<>();
    private List<Keyboard.Key> m_blueKeys = new ArrayList<>();
    private List<Keyboard.Key> m_mainKeys;

    public MyKeyboardView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        Log.d("KEY", "CONSTRUCT");
        setPreviewEnabled(false);
    }

    /**
     * recolors all of the keys, half of them red and half blue.
     */
    public void resetKeys(){
        m_enabled = new ArrayList<>(getKeyboard().getKeys());

        m_mainKeys = new ArrayList<>();
        // the main keys are the 3 last keys.
        m_mainKeys.add(m_enabled.get(m_enabled.size() - 3)); // green
        m_mainKeys.add(m_enabled.get(m_enabled.size() - 2)); // red
        m_mainKeys.add(m_enabled.get(m_enabled.size() - 1)); // blue

        m_enabled.removeAll(m_mainKeys);
        updateColor();
    }

    /**
     * this method is used to preprocess the colors of the keys for the method onDraw()
     */
    public void updateColor() {
        int i = 0;
        m_blueKeys = new ArrayList<>();
        m_redKeys = new ArrayList<>();
        for(Keyboard.Key k : m_enabled){
            if (i < m_enabled.size() / 2){
                m_redKeys.add(k);
            }
            else{
                m_blueKeys.add(k);
            }
            i++;
        }
    }

    @Override
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        List<Keyboard.Key> allKeys = new ArrayList<>(getKeyboard().getKeys());
        allKeys.removeAll(m_blueKeys);
        allKeys.removeAll(m_redKeys);
        allKeys.removeAll(m_mainKeys);
        for (Keyboard.Key key : allKeys) {
            myDraw(canvas, key, R.color.trans_grey);
        }
        for (Keyboard.Key key : m_blueKeys) {
            myDraw(canvas, key, R.color.trans_blue);
        }
        for (Keyboard.Key key : m_redKeys) {
            myDraw(canvas,key,R.color.trans_red);
        }
        myDraw(canvas, m_mainKeys.get(2), R.color.trans_blue);
        myDraw(canvas, m_mainKeys.get(1), R.color.trans_red);
        myDraw(canvas, m_mainKeys.get(0), R.color.trans_green);
    }

    /** colors a single key on the canvas
     *
     * @param canvas
     * @param key
     * @param color
     */
    private void myDraw(Canvas canvas, Keyboard.Key key, int color) {
        Drawable dr = (Drawable) context.getResources().getDrawable(color);
        dr.setBounds(key.x, key.y, key.x + key.width, key.y + key.height);
        dr.draw(canvas);
    }

    /** returns true if the key should be committed
     *
     * @param code
     * @return
     */
    public boolean chooseColor(int code) {
        if (code == MyKeyboard.RED_CODE){
            m_enabled = new ArrayList<>(m_redKeys);
        }
        else if (code == MyKeyboard.BLUE_CODE){
            m_enabled = new ArrayList<>(m_blueKeys);
        }
        if (m_enabled.size() == 1){
            return true;
        }
        return false;

    }

    /**
     *
     * @return the code of the key that was chosen by 3-button-input
     */
    public int getCodeToCommit() {
        return m_enabled.get(0).codes[0];
    }
}
