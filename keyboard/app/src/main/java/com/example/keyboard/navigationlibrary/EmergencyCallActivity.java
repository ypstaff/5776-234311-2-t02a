package com.example.keyboard.navigationlibrary;

import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.support.design.widget.FloatingActionButton;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;

import java.lang.Override;

/**
 * Created by sRoySvin on 12/8/2015.
 *
 * @author Roy Svinik
 */
public abstract class EmergencyCallActivity extends AppCompatActivity
        implements View.OnClickListener, View.OnLongClickListener {

    Intent dialIntent;
    final int PERMISSIONS_REQUEST_CALL_PHONE = 1;


    //add your activity global group here
    protected IterableGroup group;
    protected FloatingActionButton fab_left, fab_right, fab_middle;
    EmergencyCallDetector detector = new EmergencyCallDetector();

    @Override
    public void onClick(View v) {
        detector.init();

        if (v.getId() == yearlyproject.navigationlibrary.R.id.fab_left) {
            group.iterate();
        } else if (v.getId() == yearlyproject.navigationlibrary.R.id.fab_middle) {
            group.select();
        } else if (v.getId() == yearlyproject.navigationlibrary.R.id.fab_right) {
            if (group.back())
                onBackPressed();
        }
    }

    @Override
    public boolean onLongClick(View v) {
        if (v.getId() == yearlyproject.navigationlibrary.R.id.fab_left) {
            detector.next();
        } else if (v.getId() == yearlyproject.navigationlibrary.R.id.fab_middle) {
            detector.select();
        } else if (v.getId() == yearlyproject.navigationlibrary.R.id.fab_right) {
            if (detector.back()) {
                //Starting emergency call
                try {
                    Intent intent = new Intent("yearlyproject.phonedialer.ACTION_EMERGENCY");
                    startActivity(intent);
                } catch (ActivityNotFoundException anfe) {
                    Toast.makeText(this, "Can't find the custom phone app on your device. So, it is impossible to make the emergency call.", Toast.LENGTH_LONG).show();
                } catch (Exception e) {
                    Toast.makeText(this, "Can't make the emergency call.", Toast.LENGTH_SHORT).show();
                }
            }
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSIONS_REQUEST_CALL_PHONE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    startActivity(dialIntent);
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Toast.makeText(this, "Can't make a phone call without the requested permission", Toast.LENGTH_SHORT).show();
                }
                return;
            }
        }
    }

    //bluetooth messages receiver
    private final BroadcastReceiver btMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if ("BluetoothConnector.BT_MESSAGE_RECEIVED".equals(action)) {
                //String msg = intent.getData().toString();
                String msg = intent.getStringExtra("msg");
                performAction(msg.split("\\u0000")[0]);
            }
        }
    };

    public void performAction(final String s) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                //Toast.makeText(EmergencyCallActivity.this, s, Toast.LENGTH_SHORT).show();
                if(s.equals("next"))
                    fab_left.callOnClick();
                if(s.equals("select"))
                    fab_middle.callOnClick();
                if(s.equals("back"))
                    fab_right.callOnClick();
                if(s.equals("long next"))
                    fab_left.performLongClick();
                if(s.equals("long select"))
                    fab_middle.performLongClick();
                if(s.equals("long back"))
                    fab_right.performLongClick();
            }
        });
    }

    protected void unregisterBTReceiver() {
        try {
            unregisterReceiver(btMessageReceiver);
        }catch (IllegalArgumentException e) {}
    }

    protected void registerBTReceiver() {
        registerReceiver(btMessageReceiver, new IntentFilter("BluetoothConnector.BT_MESSAGE_RECEIVED"));
    }
    @Override
    protected void onPause() {
        unregisterBTReceiver();
        super.onPause();
    }

    @Override
    protected void onResume(){
        super.onResume();
        registerBTReceiver();
    }


    protected void showSoftKey(EditText et){
        unregisterBTReceiver();
        final InputMethodManager inputMethodManager = (InputMethodManager)
                this.getSystemService(this.INPUT_METHOD_SERVICE);
        inputMethodManager.showSoftInput(et, InputMethodManager.SHOW_IMPLICIT);


    }

    protected void hideSoftKey(){
        registerBTReceiver();
    }
}
