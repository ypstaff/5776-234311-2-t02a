package com.example.keyboard.activities;

import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.view.MotionEvent;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.example.keyboard.R;
import com.example.keyboard.fragments.ButtonFragment;

import java.util.ArrayList;

/**
 * @author Matan Revivo & Tomer Ben Altabe
 */
public class KeyboardActivity extends FragmentActivity implements OnTouchListener, OnClickListener {
    private EditText mEt; // Edit Text boxes
    private Button mBSpace, mNum;
    private ImageButton mBack, mCapslock, mEnter;
    private String mUpper = "upper", mLower = "lower";
    private LinearLayout mLayout;
    private RelativeLayout mKLayout;
    private int mWindowWidth;
    private ArrayList<View> m_CurrentButtons = new ArrayList<>();
    private String sL[] = {"q","w","e","r","t","y","u","i","o","p","a","s","d","f","g","h",
            "j","k","l","ç", "à","z","x","c","v",
            "b","n","m","!", ",", ".", "?"};
    private String cL[] = {"Q","W","E","R","T","Y","U","I","O","P","A","S","D","F","G","H",
            "J","K","L","ç", "à", "Z","X","C","V",
            "B","N","M","!", ",", ".", "?"};
    private String nS[] = {"1", "2", "3", "4", "5", "6", "7", "8", "9", "0",
            "!", "@", "#", "$", "%", "&", "*", "?", "/", "=", "[", "_", "\"",
            "'", "(", ")", "-", "+", "]", ".", ","};
    private Button mB[] = new Button[32];

    private ButtonFragment mBFragment;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_keyboard);
        // adjusting key regarding window sizes
        setKeys();
        enableAllButtons();
        setKeysSize();
        mEt = (EditText) findViewById(R.id.xEt);
        mEt.setOnTouchListener(this);
        mEt.setOnClickListener(this);
        mLayout = (LinearLayout) findViewById(R.id.xK1);
        mKLayout = (RelativeLayout) findViewById(R.id.xKeyBoard);

    }

    private void colorKeys() {
        for (int i = 0; i < 32; i++){
            mB[i].setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.grey));
        }
        mBSpace.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.grey));
        mBack.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.grey));
        mEnter.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.grey));
        mCapslock.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.grey));
        mNum.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.grey));

        for (int i = 0; i < m_CurrentButtons.size(); i++) {
            if (i < m_CurrentButtons.size() / 2)
                m_CurrentButtons.get(i).setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.red));
            else
                m_CurrentButtons.get(i).setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.blue));
        }

    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        if (v == mEt) {
            hideDefaultKeyboard();
            enableKeyboard();
        }
        return true;
    }

    @Override
    public void onClick(View v) {
        if (v == mCapslock) { // capslock
            if (mCapslock.getTag().equals(mUpper)) {
                changeSmallLetters();
                changeSmallTags();
                mCapslock.setImageResource(R.drawable.caps_lock);
            } else if (mCapslock.getTag().equals(mLower)) {
                changeCapitalLetters();
                changeCapitalTags();
                mCapslock.setImageResource(R.drawable.caps_lock_pressed);
            }
        } else if (v == mEnter) {
            addText(v);
        } else if (v == mBack) {
            backspace(v);
        } else if (v == mNum) {
            String nTag = (String) mNum.getTag();
            if (nTag.equals("num")) {
                changeSyNuLetters();
                changeSyNuTags();
                mCapslock.setVisibility(Button.INVISIBLE);

            }
            if (nTag.equals("ABC")) {
                changeCapitalLetters();
                changeCapitalTags();
            }
        } else if (v == mBFragment.b_blue || v == mBFragment.b_red){
            chooseColor(v);
        } else if (v == mBFragment.b_green){
            enableAllButtons();
        } else {
            addText(v);
        }
    }

    private void chooseColor(View v) {
        ArrayList<View> oldButtons = m_CurrentButtons;
        m_CurrentButtons = new ArrayList<>();
        for(int i = 0; i < oldButtons.size(); i++){
            ColorDrawable oldColor = (ColorDrawable) oldButtons.get(i).getBackground();
            ColorDrawable pressedColor = (ColorDrawable) v.getBackground();
            if (oldColor.getColor() == pressedColor.getColor()) {
                m_CurrentButtons.add(oldButtons.get(i));
            }
        }
        if (m_CurrentButtons.size() == 1){
            onClick(m_CurrentButtons.get(0));
            enableAllButtons();
            return;
        }
        colorKeys();
    }

    private void addText(View v) {
        String b = "";
        b = (String) v.getTag();
        if (b != null) {
            // adding text in Edittext
            mEt.append(b);

        }
    }

    private void backspace(View v) {
        CharSequence cc = mEt.getText();
        if (cc != null && cc.length() > 0) {
            {
                mEt.setText("");
                mEt.append(cc.subSequence(0, cc.length() - 1));
            }

        }
    }

    private void changeSmallLetters() {
        mCapslock.setVisibility(Button.VISIBLE);
        for (int i = 0; i < sL.length; i++)
            mB[i].setText(sL[i]);
        mNum.setTag("12#");
    }

    private void changeSmallTags() {
        for (int i = 0; i < sL.length; i++)
            mB[i].setTag(sL[i]);
        mCapslock.setTag("lower");
        mNum.setTag("num");
    }

    private void changeCapitalLetters() {
        mCapslock.setVisibility(Button.VISIBLE);
        for (int i = 0; i < cL.length; i++)
            mB[i].setText(cL[i]);
        mCapslock.setTag("upper");
        mNum.setText("12#");

    }

    private void changeCapitalTags() {
        for (int i = 0; i < cL.length; i++)
            mB[i].setTag(cL[i]);
        mNum.setTag("num");

    }

    private void changeSyNuLetters() {

        for (int i = 0; i < nS.length; i++)
            mB[i].setText(nS[i]);
        mNum.setText("ABC");
    }

    private void changeSyNuTags() {
        for (int i = 0; i < nS.length; i++)
            mB[i].setTag(nS[i]);
        mNum.setTag("ABC");
    }

    // enabling customized keyboard
    private void enableKeyboard() {

        mLayout.setVisibility(RelativeLayout.VISIBLE);
        mKLayout.setVisibility(RelativeLayout.VISIBLE);

    }

    // Disable customized keyboard
    private void disableKeyboard() {
        mLayout.setVisibility(RelativeLayout.INVISIBLE);
        mKLayout.setVisibility(RelativeLayout.INVISIBLE);

    }

    private void hideDefaultKeyboard() {
        getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

    }

    private void setKeysSize() {
        int w = (mWindowWidth / 10);
        int h = mB[16].getLayoutParams().height;
        //set size of letters keys
        for (int i = 0; i < mB.length; i++) {
            mB[i].setLayoutParams(new LinearLayout.LayoutParams(w, h));
        }
        //set size of backspace
        mBack.setLayoutParams(new LinearLayout.LayoutParams(w, h));
        mEnter.setScaleType(ImageView.ScaleType.FIT_CENTER);
        //set size last key line
        mCapslock.setLayoutParams(new LinearLayout.LayoutParams(w, h));
        mCapslock.setScaleType(ImageView.ScaleType.FIT_CENTER);
        mBSpace.setLayoutParams(new LinearLayout.LayoutParams(3 * w, h));
        mNum.setLayoutParams(new LinearLayout.LayoutParams(2*w, h));
        mEnter.setLayoutParams(new LinearLayout.LayoutParams(w, h));
        mEnter.setScaleType(ImageView.ScaleType.FIT_CENTER);

    }

    private void setKeys() {
        mWindowWidth = getWindowManager().getDefaultDisplay().getWidth(); // getting window heigh
        // getting ids from xml files
        mB[0] = (Button) findViewById(R.id.xQ);
        mB[1] = (Button) findViewById(R.id.xW);
        mB[2] = (Button) findViewById(R.id.xE);
        mB[3] = (Button) findViewById(R.id.xR);
        mB[4] = (Button) findViewById(R.id.xT);
        mB[5] = (Button) findViewById(R.id.xY);
        mB[6] = (Button) findViewById(R.id.xU);
        mB[7] = (Button) findViewById(R.id.xI);
        mB[8] = (Button) findViewById(R.id.xO);
        mB[9] = (Button) findViewById(R.id.xP);
        mB[10] = (Button) findViewById(R.id.xA);
        mB[11] = (Button) findViewById(R.id.xS);
        mB[12] = (Button) findViewById(R.id.xD);
        mB[13] = (Button) findViewById(R.id.xF);
        mB[14] = (Button) findViewById(R.id.xG);
        mB[15] = (Button) findViewById(R.id.xH);
        mB[16] = (Button) findViewById(R.id.xJ);
        mB[17] = (Button) findViewById(R.id.xK);
        mB[18] = (Button) findViewById(R.id.xL);
        mB[19] = (Button) findViewById(R.id.xS1);
        mB[20] = (Button) findViewById(R.id.xS2);
        mB[21] = (Button) findViewById(R.id.xZ);
        mB[22] = (Button) findViewById(R.id.xX);
        mB[23] = (Button) findViewById(R.id.xC);
        mB[24] = (Button) findViewById(R.id.xV);
        mB[25] = (Button) findViewById(R.id.xB);
        mB[26] = (Button) findViewById(R.id.xN);
        mB[27] = (Button) findViewById(R.id.xM);
        mB[28] = (Button) findViewById(R.id.xS3);
        mB[29] = (Button) findViewById(R.id.xS4);
        mB[30] = (Button) findViewById(R.id.xS5);
        mB[31] = (Button) findViewById(R.id.xS6);
        mBSpace = (Button) findViewById(R.id.xSpace);
        mEnter = (ImageButton) findViewById(R.id.xEnter);
        mCapslock = (ImageButton) findViewById(R.id.xCapslock);
        mBack = (ImageButton) findViewById(R.id.xBack);
        mNum = (Button) findViewById(R.id.xNum);
        for (int i = 0; i < mB.length; i++) {
            mB[i].setOnClickListener(this);
            mB[i].setTransformationMethod(null);
        }
        mBSpace.setOnClickListener(this);
        mEnter.setOnClickListener(this);
        mBack.setOnClickListener(this);
        mCapslock.setOnClickListener(this);
        mNum.setOnClickListener(this);
        mBFragment = (ButtonFragment) getSupportFragmentManager().findFragmentById(R.id.f_buttons);
        mBFragment.b_blue.setOnClickListener(this);
        mBFragment.b_green.setOnClickListener(this);
        mBFragment.b_red.setOnClickListener(this);
    }

    private void enableAllButtons() {
        m_CurrentButtons = new ArrayList<>();
        for (int i = 0; i < 32; i++){
            m_CurrentButtons.add(mB[i]);
            if (i == 28) {
                m_CurrentButtons.add(mBack);
                m_CurrentButtons.add(mCapslock);
            }
            else if (i == 29){
                m_CurrentButtons.add(mBSpace);
            }
        }
        m_CurrentButtons.add(mNum);
        m_CurrentButtons.add(mEnter);
        colorKeys();
    }
}
