package com.yp;

import android.test.ActivityInstrumentationTestCase2;
import android.test.TouchUtils;
import android.widget.Button;
import android.widget.EditText;

import com.yp.activities.SmsActivity;
import com.yp.cursor.R;

import org.junit.Before;

/**
 * * @author Matan Revivo
 */

public class SmsTest extends ActivityInstrumentationTestCase2<SmsActivity> {
    public SmsTest() {
        super(SmsActivity.class);
    }

    SmsActivity activity;

    @Before
    public void setUp() {
        activity = (SmsActivity)getActivity();
        activity.setTestMode(true);
    }


    public void testPhoneNumberNotOnlyDigits() {
        // set number to "123456789a"
        final EditText numberEditText =
                (EditText) activity.findViewById(R.id.et_number);

        // Send string input value
        getInstrumentation().runOnMainSync(new Runnable() {
            @Override
            public void run() {
                numberEditText.requestFocus();
            }
        });

        getInstrumentation().waitForIdleSync();
        getInstrumentation().sendStringSync("123456789a");
        getInstrumentation().waitForIdleSync();

        Button greetButton =
                (Button) activity.findViewById(R.id.b_send);

        TouchUtils.clickView(this, greetButton);

        assertEquals("number must contain only digits", activity.getToast());
    }

    public void testPhoneNumberLessThan10Digits() {
        // set number to "123456789"
        final EditText numberEditText =
                (EditText) activity.findViewById(R.id.et_number);

        // Send string input value
        getInstrumentation().runOnMainSync(new Runnable() {
            @Override
            public void run() {
                numberEditText.requestFocus();
            }
        });

        getInstrumentation().waitForIdleSync();
        getInstrumentation().sendStringSync("123456789");
        getInstrumentation().waitForIdleSync();

        Button greetButton =
                (Button) activity.findViewById(R.id.b_send);

        TouchUtils.clickView(this, greetButton);

        assertEquals("number must contain 10 digits", activity.getToast());
    }

    public void testPhoneNumberMoreThan10Digits() {
        // set number to "12345678901"
        final EditText numberEditText =
                (EditText) activity.findViewById(R.id.et_number);

        // Send string input value
        getInstrumentation().runOnMainSync(new Runnable() {
            @Override
            public void run() {
                numberEditText.requestFocus();
            }
        });

        getInstrumentation().waitForIdleSync();
        getInstrumentation().sendStringSync("12345678901");
        getInstrumentation().waitForIdleSync();

        Button greetButton =
                (Button) activity.findViewById(R.id.b_send);

        TouchUtils.clickView(this, greetButton);

        assertEquals("number must contain 10 digits", activity.getToast());
    }

    public void testEmptyMessage() throws InterruptedException {
        Thread.sleep(2000); // without this the tests fails because of
                            //  permissions when we run all the tests together

        // set number to "1234567890"
        final EditText numberEditText =
                (EditText) activity.findViewById(R.id.et_number);

        // Send string input value
        getInstrumentation().runOnMainSync(new Runnable() {
            @Override
            public void run() {
                numberEditText.requestFocus();
            }
        });

        getInstrumentation().waitForIdleSync();
        getInstrumentation().sendStringSync("1234567890");
        getInstrumentation().waitForIdleSync();

        Button greetButton =
                (Button) activity.findViewById(R.id.b_send);

        TouchUtils.clickView(this, greetButton);

        assertEquals("message can not be empty", activity.getToast());
    }

    public void testSmsSentSuccesfully() {
        // set number to "0500000000"
        final EditText numberEditText =
                (EditText) activity.findViewById(R.id.et_number);

        // Send string input value
        getInstrumentation().runOnMainSync(new Runnable() {
            @Override
            public void run() {
                numberEditText.requestFocus();
            }
        });

        getInstrumentation().waitForIdleSync();
        getInstrumentation().sendStringSync("0500000000");
        getInstrumentation().waitForIdleSync();

        // set message to "hi!"
        final EditText messageEditText =
                (EditText) activity.findViewById(R.id.et_message);

        // Send string input value
        getInstrumentation().runOnMainSync(new Runnable() {
            @Override
            public void run() {
                messageEditText.requestFocus();
            }
        });

        getInstrumentation().waitForIdleSync();
        getInstrumentation().sendStringSync("hi!");
        getInstrumentation().waitForIdleSync();

        // IMPORTANT !! - uncomment the next line only for demos
//        activity.setTestMode(false);

        Button greetButton =
                (Button) activity.findViewById(R.id.b_send);

        TouchUtils.clickView(this, greetButton);

        assertEquals("sent succesfully", activity.getToast());
    }

}


