package com.yp.cursor.facedetect;

import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.PointF;
import android.support.annotation.NonNull;
import android.support.test.runner.AndroidJUnit4;

import com.google.android.gms.vision.CameraSource;
import com.google.android.gms.vision.face.Face;
import com.yp.classes.Constants;
import com.yp.cursor.CursorApplication;
import com.yp.cursor.R;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertTrue;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @author Avner Elizarov
 */
@RunWith(AndroidJUnit4.class)
public class FaceCursorTest  {

    private static final int SCREEN_DIMENSIONS_Y = 1000;
    public static final int CENTERED_FACE_Y = SCREEN_DIMENSIONS_Y / 2;
    private static final int SCREEN_DIMENSIONS_X = 300;
    public static final int CENTERED_FACE_X = SCREEN_DIMENSIONS_X / 2;
    private static final float FACE_WIDTH = 20;
    private static final float FACE_HEIGHT = 20;
    public static final int VALID_FACE_MOVEMENT = FaceGraphic.MINIMUM_FACE_MOVEMENT * 2;

    FaceGraphic faceCursor;
    private GraphicOverlay overlay;
    private Point screenDimensions;
    private Paint paint;
    private Canvas canvas;

    @Before
    public void setUp() {
        paint = mock(Paint.class);
        overlay = mock(GraphicOverlay.class);
        overlay.mFacing = CameraSource.CAMERA_FACING_BACK;
        overlay.mWidthScaleFactor = 1f;
        overlay.mHeightScaleFactor = 1f;
        screenDimensions = mock(Point.class);
        screenDimensions.x = SCREEN_DIMENSIONS_X;
        screenDimensions.y = SCREEN_DIMENSIONS_Y;
        final SharedPreferences sharedPref = mock(SharedPreferences.class);
        when(sharedPref.getInt(CursorApplication.getInstance().getString(R.string.pref_key_x_sensitivity), Constants.FaceCursor.DEFAULT_X_MOVEMENT_PORTRAIT))
                .thenReturn(Constants.FaceCursor.DEFAULT_X_MOVEMENT_PORTRAIT);
        when(sharedPref.getInt(CursorApplication.getInstance().getString(R.string.pref_key_y_sensitivity), Constants.FaceCursor.DEFAULT_Y_MOVEMENT_PORTRAIT))
                .thenReturn(Constants.FaceCursor.DEFAULT_Y_MOVEMENT_PORTRAIT);
        faceCursor = new FaceGraphic(overlay, screenDimensions, paint, Configuration.ORIENTATION_PORTRAIT, sharedPref);
        canvas = mock(Canvas.class);
    }

    @Test
    public void testCursorShouldntShowUntilFaceIsCenteredInXAxis() {

        PointF position = mockPosition(10f, 10f);
        sendFace(position);

        assertFalse(faceCursor.shouldShowCursor());
    }

    @Test
    public void testCursorShouldShowWhenFaceIsCenteredInXAxis() {

        PointF position = mockPosition(CENTERED_FACE_X, CENTERED_FACE_Y);
        sendFace(position);
        assertTrue(faceCursor.shouldShowCursor());
    }

    @Test
    public void testCursorShouldntMoveWhenFaceNotMoved() {

        PointF initial_position = mockPosition(CENTERED_FACE_X, CENTERED_FACE_Y);
        sendFace(initial_position);
        PointF oldCursorLocation = faceCursor.getCursorLocation();

        PointF new_position = mockPosition(CENTERED_FACE_X, CENTERED_FACE_Y);
        sendFace(new_position);
        PointF newCursorLocation = faceCursor.getCursorLocation();
        assertEquals(oldCursorLocation, newCursorLocation);
    }

    @Test
    public void testCursorShouldntMoveWhenFaceMovesLessThenMinimum() {

        PointF initial_position = mockPosition(CENTERED_FACE_X, CENTERED_FACE_Y);
        sendFace(initial_position);
        PointF oldCursorLocation = faceCursor.getCursorLocation();

        PointF new_position = mockPosition(CENTERED_FACE_X + FaceGraphic.MINIMUM_FACE_MOVEMENT - 1
                                , CENTERED_FACE_Y + FaceGraphic.MINIMUM_FACE_MOVEMENT - 1);
        sendFace(new_position);
        PointF newCursorLocation = faceCursor.getCursorLocation();
        assertEquals(oldCursorLocation, newCursorLocation);
    }


    @Test
    public void testCursorShouldMoveWhenFaceMovedMoreThenMinimumHorizontally() {

        PointF initial_position = mockPosition(CENTERED_FACE_X, CENTERED_FACE_Y);
        sendFace(initial_position);
        PointF oldCursorLocation = new PointF(faceCursor.getCursorLocation().x, faceCursor.getCursorLocation().y);

        PointF new_position = mockPosition(CENTERED_FACE_X+  FaceGraphic.MINIMUM_FACE_MOVEMENT * 2 , CENTERED_FACE_Y);
        sendFace(new_position);
        PointF newCursorLocation = faceCursor.getCursorLocation();
        assertNotEquals(oldCursorLocation, newCursorLocation);
    }

    @Test
    public void testCursorShouldMoveWhenFaceMovedMoreThenMinimumVertically() {

        PointF initial_position = mockPosition(CENTERED_FACE_X, CENTERED_FACE_Y);
        sendFace(initial_position);
        PointF oldCursorLocation = new PointF(faceCursor.getCursorLocation().x, faceCursor.getCursorLocation().y);

        PointF new_position = mockPosition(CENTERED_FACE_X, CENTERED_FACE_Y+  FaceGraphic.MINIMUM_FACE_MOVEMENT * 2 );
        sendFace(new_position);
        PointF newCursorLocation = faceCursor.getCursorLocation();
        assertNotEquals(oldCursorLocation, newCursorLocation);
    }

    @Test
    public void testCursorHorizontalMovementDiff() {

        PointF initialFacePosition = mockPosition(CENTERED_FACE_X, CENTERED_FACE_Y);
        sendFace(initialFacePosition);
        PointF oldCursorLocation = new PointF(faceCursor.getCursorLocation().x, faceCursor.getCursorLocation().y);

        PointF newFacePosition = mockPosition(CENTERED_FACE_X + VALID_FACE_MOVEMENT, CENTERED_FACE_Y);
        sendFace(newFacePosition);
        PointF newCursorLocation = faceCursor.getCursorLocation();
        assertEquals(oldCursorLocation.y, newCursorLocation.y);

        float stableFacePositionX = (newFacePosition.x + FaceGraphic.PRIOR_CENTER_COEFFICIENT * initialFacePosition.x) / FaceGraphic.COEFFICIENT_SUM;
        final float faceDiffX = stableFacePositionX - initialFacePosition.x;
        final float speed = faceCursor.getMinXMovement() + faceCursor.getXSpeedDiff();
        float expectedCursorLocationX = oldCursorLocation.x + faceDiffX * speed;
        assertEquals(expectedCursorLocationX, newCursorLocation.x);
    }

    @Test
    public void testCursorVerticalMovementDiff() {

        PointF initialFacePosition = mockPosition(CENTERED_FACE_X, CENTERED_FACE_Y);
        sendFace(initialFacePosition);
        PointF oldCursorLocation = new PointF(faceCursor.getCursorLocation().x, faceCursor.getCursorLocation().y);

        PointF newFacePosition = mockPosition(CENTERED_FACE_X, CENTERED_FACE_Y + VALID_FACE_MOVEMENT);
        sendFace(newFacePosition);
        PointF newCursorLocation = faceCursor.getCursorLocation();
        assertEquals(oldCursorLocation.x, newCursorLocation.x);

        float stableFacePositionY = (newFacePosition.y + FaceGraphic.PRIOR_CENTER_COEFFICIENT * initialFacePosition.y) / FaceGraphic.COEFFICIENT_SUM;
        final float faceDiffY = stableFacePositionY - initialFacePosition.y;
        final float speed = faceCursor.getMinYMovement() + faceCursor.getYSpeedDiff();
        float expectedCursorLocationY = oldCursorLocation.y + faceDiffY * speed;
        assertEquals(expectedCursorLocationY, newCursorLocation.y);
    }


    @Test
    public void testCursorShouldntMoveBeyondScreenDimensionsHigherBoundX() {

        PointF initial_position = mockPosition(CENTERED_FACE_X, CENTERED_FACE_Y);
        sendFace(initial_position);

        PointF new_position = mockPosition(CENTERED_FACE_X + 2000, CENTERED_FACE_Y);
        sendFace(new_position);
        PointF newCursorLocation = faceCursor.getCursorLocation();
        float expectedLocationX = screenDimensions.x - FaceGraphic.MARGIN_FROM_RIGHT;
        assertEquals(expectedLocationX, newCursorLocation.x);
    }

    @Test
    public void testCursorShouldntMoveBeyondScreenDimensionsLowerBoundX() {

        PointF initial_position = mockPosition(CENTERED_FACE_X, CENTERED_FACE_Y);
        sendFace(initial_position);

        PointF new_position = mockPosition(0, CENTERED_FACE_Y);
        sendFace(new_position);
        PointF newCursorLocation = faceCursor.getCursorLocation();
        assertEquals((float) FaceGraphic.MIN_SCREEN_X, newCursorLocation.x);
    }


    @Test
    public void testCursorShouldntMoveBeyondScreenDimensionsHigherBoundY() {

        PointF initial_position = mockPosition(CENTERED_FACE_X, CENTERED_FACE_Y);
        sendFace(initial_position);

        PointF new_position = mockPosition(CENTERED_FACE_X
                , CENTERED_FACE_Y + 2000);
        sendFace(new_position);
        PointF newCursorLocation = faceCursor.getCursorLocation();
        float expectedLocationY = screenDimensions.y - FaceGraphic.MARGIN_FROM_BOTTOM;
        assertEquals(expectedLocationY, newCursorLocation.y);
    }

    @Test
    public void testCursorShouldntMoveBeyondScreenDimensionsLowerBoundY() {

        PointF initial_position = mockPosition(CENTERED_FACE_X, CENTERED_FACE_Y);
        sendFace(initial_position);

        PointF new_position = mockPosition(CENTERED_FACE_X, 0);
        sendFace(new_position);
        PointF newCursorLocation = faceCursor.getCursorLocation();
        assertEquals((float) FaceGraphic.MIN_SCREEN_Y, newCursorLocation.y);
    }



    private void sendFace(PointF initial_position) {
        Face face = mockFace(initial_position);
        faceCursor.updateFace(face);
        faceCursor.draw(canvas);
    }


    @NonNull
    private PointF mockPosition(float x, float y) {
        PointF position = mock(PointF.class);
        position.x = x;
        position.y = y;
        return position;
    }

    private Face mockFace(PointF position) {
        Face face = mock(Face.class);
        when(face.getIsLeftEyeOpenProbability()).thenReturn(-1f);
        when(face.getIsRightEyeOpenProbability()).thenReturn(-1f);
        when(face.getPosition()).thenReturn(position);
        when(face.getWidth()).thenReturn(FACE_WIDTH);
        when(face.getHeight()).thenReturn(FACE_HEIGHT);
        return face;
    }

}


