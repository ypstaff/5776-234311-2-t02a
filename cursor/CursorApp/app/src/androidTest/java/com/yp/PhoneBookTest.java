package com.yp;
import com.yp.classes.PhoneBook;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 * @author Or Mauda
 *
 * This test validates a PhoneBook used in ContactsActivity.
 */
public class PhoneBookTest {

    @Test(expected = IllegalArgumentException.class)
    public void mustHaveAStrudelInEmailAddress() throws Exception {
        PhoneBook contact = new PhoneBook("Or", "0123456789", "a");
    }

    @Test(expected = NullPointerException.class)
    public void nameMustNotBeNull() throws Exception {
        PhoneBook contact = new PhoneBook(null, "0123456789", "a@v");
    }

    @Test(expected = NullPointerException.class)
    public void phoneMustNotBeNull() throws Exception {
        PhoneBook contact = new PhoneBook("a", null, "a@v");
    }

    @Test
    public void TestEquals() {
        PhoneBook contact1 = new PhoneBook("a", "0123456789", "a@v");
        PhoneBook contact2 = new PhoneBook("a", "0123456789", "a@v");
        PhoneBook contact3 = new PhoneBook("a", "0123456789 ", "a@v");
        PhoneBook contact4 = new PhoneBook("a", "0123456780 ", "a@v");
        assertTrue(contact1.equals(contact2));
        assertTrue(contact2.equals(contact1));
        assertFalse(contact1.equals(contact3));
        assertFalse(contact1.equals(contact4));
    }
}
