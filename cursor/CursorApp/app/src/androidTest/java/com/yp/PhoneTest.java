package com.yp;

import android.support.test.espresso.ViewInteraction;
import android.support.test.espresso.matcher.ViewMatchers;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.widget.TextView;

import com.yp.activities.PhoneActivity;
import com.yp.cursor.R;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static org.junit.Assert.assertTrue;

/**
 * Created by tbenalta on 18-Apr-16.
 */
@RunWith(AndroidJUnit4.class)
public class PhoneTest {

    @Rule
    public final ActivityTestRule<PhoneActivity> main = new ActivityTestRule<>(PhoneActivity.class);

    private void clickGenerator(String button){
        ViewInteraction redClick = onView(ViewMatchers.withId(R.id.b_red));
        ViewInteraction blueClick = onView(withId(R.id.b_blue));
        switch (button) {
            case "1":
                redClick.perform(click());
                redClick.perform(click());
                redClick.perform(click());
                break;
            case "2":
                redClick.perform(click());
                redClick.perform(click());
                blueClick.perform(click());
                redClick.perform(click());
                break;
            case "3":
                redClick.perform(click());
                redClick.perform(click());
                blueClick.perform(click());
                blueClick.perform(click());
                break;
            case "4":
                redClick.perform(click());
                blueClick.perform(click());
                redClick.perform(click());
                redClick.perform(click());
                break;
            case "5":
                redClick.perform(click());
                blueClick.perform(click());
                redClick.perform(click());
                blueClick.perform(click());
                break;
            case "6":
                redClick.perform(click());
                blueClick.perform(click());
                blueClick.perform(click());
                redClick.perform(click());
                break;
            case "7":
                redClick.perform(click());
                blueClick.perform(click());
                blueClick.perform(click());
                blueClick.perform(click());
                break;
            case "8":
                blueClick.perform(click());
                redClick.perform(click());
                redClick.perform(click());
                redClick.perform(click());
                break;
            case "9":
                blueClick.perform(click());
                redClick.perform(click());
                redClick.perform(click());
                blueClick.perform(click());
                break;
            case "0":
                blueClick.perform(click());
                redClick.perform(click());
                blueClick.perform(click());
                blueClick.perform(click());
                break;
            case "*":
                blueClick.perform(click());
                redClick.perform(click());
                blueClick.perform(click());
                redClick.perform(click());
                break;
            case "#":
                blueClick.perform(click());
                blueClick.perform(click());
                redClick.perform(click());
                redClick.perform(click());
                break;
            case "DEL":
                blueClick.perform(click());
                blueClick.perform(click());
                blueClick.perform(click());
                blueClick.perform(click());
                break;
            case "CONTACTS":
                blueClick.perform(click());
                blueClick.perform(click());
                redClick.perform(click());
                blueClick.perform(click());
                break;
            case "CALL":
                blueClick.perform(click());
                blueClick.perform(click());
                blueClick.perform(click());
                redClick.perform(click());
                break;
            default:
                break;
        }
    }

    @Test
    public void numberTyping(){
        TextView tv = (TextView) main.getActivity().findViewById(R.id.phoneNumberText);
        clickGenerator("1");        assertTrue(tv.getText().toString().equals("1"));
        clickGenerator("2");        assertTrue(tv.getText().toString().equals("12"));
        clickGenerator("3");        assertTrue(tv.getText().toString().equals("123"));
        clickGenerator("4");        assertTrue(tv.getText().toString().equals("1234"));
        clickGenerator("5");        assertTrue(tv.getText().toString().equals("12345"));
        clickGenerator("6");        assertTrue(tv.getText().toString().equals("123456"));
        clickGenerator("7");        assertTrue(tv.getText().toString().equals("1234567"));
        clickGenerator("8");        assertTrue(tv.getText().toString().equals("12345678"));
        clickGenerator("9");        assertTrue(tv.getText().toString().equals("123456789"));
        clickGenerator("*");        assertTrue(tv.getText().toString().equals("123456789*"));
        for (int i = 0; i < 4; i++) {
            clickGenerator("DEL");
        }
        assertTrue(tv.getText().toString().equals("123456"));
        clickGenerator("0");        assertTrue(tv.getText().toString().equals("1234560"));
        clickGenerator("#");        assertTrue(tv.getText().toString().equals("1234560#"));
        clickGenerator("#");        assertTrue(tv.getText().toString().equals("1234560##"));
        clickGenerator("#");        assertTrue(tv.getText().toString().equals("1234560###"));
    }

    @Test
    public void openContacts() {
//        Activity currentActivity = ((Application)context.getApplicationContext()).getCurrentActivity();
        clickGenerator("CONTACTS");
        onView(withId(R.id.xEt)).check(matches(isDisplayed()));
        //the above means we are on the contacts activity
        //TODO find a cleaner way to check that
    }

    @Test
    public void call(){
        TextView tv = (TextView) main.getActivity().findViewById(R.id.phoneNumberText);
        String phoneNumber = "000000000";
        for (int i = 0; i < phoneNumber.length(); i++) {
            clickGenerator(phoneNumber.substring(i,i+1));
        }
        assertTrue(tv.getText().toString().equals(phoneNumber));
        // IMPORTANT !! - uncomment the next line only for demos
//        clickGenerator("CALL");
    }

}
