package com.yp;

import android.graphics.Point;

import com.yp.classes.SnakeGame;
import com.yp.classes.SnakeGame.Cell;
import com.yp.classes.SnakeGame.Prop;
import com.yp.classes.SnakeGame.Difficulty;

import junit.framework.TestCase;

import java.util.LinkedList;

/**
 *  @author Inbar Donag
 */

public class SnakeTest extends TestCase {

    SnakeGame game = new SnakeGame();

    protected void setUp() {
        game.initBoard(10, 10);
        game.initSnake(Difficulty.EASY);

    }

    public void testInitSnake() {

        // snake's head is in (5,5) and it has 2 more body nodes in the left
        assertTrue(game.getCellUnderSnakeHead().rowIndex == 5);
        assertTrue(game.getCellUnderSnakeHead().colIndex == 5);
        assertTrue(game.getSnakeLength() == 3);
    }

    public void testDeathByBomb() {

        // put a bomb 2 step away in front of the snake, and let it move in its default right direction
        LinkedList<Prop> props = new LinkedList<>();
        Prop bomb = new Prop(Cell.Type.BOMB, new Point(5, 7));
        props.addLast(bomb);
        game.scatterPropsOnBoard(props);

        assertTrue(SnakeGame.RIGHT == game.getSnakeCurrDir());  // default direction should be RIGHT
        game.move();
        assertFalse(game.isSnakeDead()); // after first step snake is still alive
        game.move();
        assertTrue(game.isSnakeDead()); // after second step snake is dead

    }

    public void testSnakeMovement() {

        // starting from (5,5) with curr direction: RIGHT
        game.turnLeft();
        assertTrue(game.getSnakeCurrDir() == SnakeGame.UPPER_RIGHT);
        game.move();
        assertTrue(game.getCellUnderSnakeHead().rowIndex == 4);
        assertTrue(game.getCellUnderSnakeHead().colIndex == 6);
        game.turnLeft();
        assertTrue(game.getSnakeCurrDir() == SnakeGame.UP);
        game.turnLeft();
        assertTrue(game.getSnakeCurrDir() == SnakeGame.UPPER_LEFT);
        game.turnLeft();
        assertTrue(game.getSnakeCurrDir() == SnakeGame.LEFT);
        game.move();
        assertTrue(game.getCellUnderSnakeHead().rowIndex == 4);
        assertTrue(game.getCellUnderSnakeHead().colIndex == 5);
        game.turnRight();
        assertTrue(game.getSnakeCurrDir() == SnakeGame.UPPER_LEFT);
        game.move();
        assertTrue(game.getCellUnderSnakeHead().rowIndex == 3);
        assertTrue(game.getCellUnderSnakeHead().colIndex == 4);
        game.turnRight();
        assertTrue(game.getSnakeCurrDir() == SnakeGame.UP);
        game.move();
        assertTrue(game.getCellUnderSnakeHead().rowIndex == 2);
        assertTrue(game.getCellUnderSnakeHead().colIndex == 4);
        game.turnLeft();
        assertTrue(game.getSnakeCurrDir() == SnakeGame.UPPER_LEFT);
        game.turnLeft();
        assertTrue(game.getSnakeCurrDir() == SnakeGame.LEFT);
        game.move();
        game.move();
        assertTrue(game.getCellUnderSnakeHead().rowIndex == 2);
        assertTrue(game.getCellUnderSnakeHead().colIndex == 2);

    }

    public void testGrowthAndDeathBySnakeBody() {

        // surround the snake with foods
        LinkedList<Prop> props = new LinkedList<>();
        Prop food1 = new Prop(Cell.Type.FOOD, new Point(5, 6));
        Prop food2 = new Prop(Cell.Type.FOOD, new Point(6, 6));
        Prop food3 = new Prop(Cell.Type.FOOD, new Point(6, 5));
        Prop food4 = new Prop(Cell.Type.FOOD, new Point(6, 4));
        Prop food5 = new Prop(Cell.Type.FOOD, new Point(6, 3));
        props.addLast(food1);
        props.addLast(food2);
        props.addLast(food3);
        props.addLast(food4);
        props.addLast(food5);
        game.scatterPropsOnBoard(props);

        // move it so it's eating everything and check growth
        game.move();
        assertTrue(game.getSnakeLength() == 4); // grown up by 1
        game.turnRight();
        game.turnRight();
        game.move();
        assertTrue(game.getSnakeLength() == 5); // grown up by 1
        game.turnRight();
        game.turnRight();
        game.move();
        assertTrue(game.getSnakeLength() == 6); // grown up by 1
        game.move();
        assertTrue(game.getSnakeLength() == 7); // grown up by 1
        game.move();
        assertTrue(game.getSnakeLength() == 8); // grown up by 1

        // now make it crash into its body
        game.turnRight();
        game.turnRight();
        game.move();
        assertTrue(game.isSnakeDead());

    }

    public void testScoreChanges() {

        assertTrue(game.getScore() == 0);
        assertTrue(game.getHighScore() == 0);

        // surround the snake with foods and stars
        LinkedList<Prop> props = new LinkedList<>();
        Prop food1 = new Prop(Cell.Type.FOOD, new Point(5, 6));
        Prop food2 = new Prop(Cell.Type.FOOD, new Point(6, 6));
        Prop star1 = new Prop(Cell.Type.STAR, new Point(6, 5));
        Prop star2 = new Prop(Cell.Type.STAR, new Point(6, 4));
        Prop star3 = new Prop(Cell.Type.STAR, new Point(6, 3));
        props.addLast(food1);
        props.addLast(food2);
        props.addLast(star1);
        props.addLast(star2);
        props.addLast(star3);
        game.scatterPropsOnBoard(props);

        // move it so it's collecting all the props and check score
        game.move();
        assertTrue(game.getScore() > 0);
        int foodPoints = game.getScore();
        game.turnRight();
        game.turnRight();
        game.move();
        assertTrue(game.getScore() == foodPoints * 2);
        game.turnRight();
        game.turnRight();
        game.move();
        assertTrue(game.getScore() > foodPoints * 2);
        int starPoints = game.getScore() - foodPoints*2;
        game.move();
        assertTrue(game.getScore() == 2 * starPoints + 2 * foodPoints);
        game.move();
        assertTrue(game.getScore() == 3 * starPoints + 2 * foodPoints);

    }

    public void testDeathByWall() {

        // move the snake in the upper right direction till it hits a wall
        game.turnLeft();
        for (int i = 0; i < 4 ; i++) {
            game.move();
        }
        // almost touching the wall, still alive
        assertFalse(game.isSnakeDead());
        game.move();
        // touched the wall
        assertTrue(game.isSnakeDead());

    }

    public void testLevelUpProp() {

        // every 100 steps level up prop should appear.
        for (int i = 0; i< 100; i++) {
            game.move();
            game.turnLeft();
        }

        // search board for this prop (randomly placed)
        boolean found = false;
        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {
                if (game.getCell(i,j).getType() == Cell.Type.LEVEL_UP) {
                    found = true;
                    break;
                }
            }
        }
        assertTrue(found);

    }


}
