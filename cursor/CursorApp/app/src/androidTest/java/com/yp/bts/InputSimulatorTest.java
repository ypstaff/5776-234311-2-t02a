package com.yp.bts;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.util.Log;

import com.yp.remoteInputSim.ISimulatorActivity;
import com.yp.remoteInputSim.InputSimulator;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.IOException;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertNotSame;
import static junit.framework.Assert.assertTrue;
import static junit.framework.Assert.fail;

/**
 * @author Oded
 * @version 11/06/2016
 */
public class InputSimulatorTest {

    private static final String INPUT_SIM_TAG = "INPUT_SIM_TESTER";
    private static final String INPUT_SIM_TESTET_TAG = "INPUT_SIM_TESTET_TAG";
    private static final String APP_UUID = "00001101-0000-1000-8000-00805F9B34FB";
    private static final long RECONECT_INTERVAL_MILLIS = 300L;
    private static final long DEFAULT_CONNECT_TIMEOUT_MILLIS = 5000L;
    private static final String INITIAL_SEND_STRING = "00000000";
    private static final long DEFAULT_SEND_DELAY = 500L;
    static BluetoothAdapter btAdapter = BluetoothAdapter.getDefaultAdapter();
    static InputSimulator simulator;
    static String devAddr;
    static ISimulatorActivity simActivity;
    private static String last_sent = "";
    static String simOutput = "";

    @BeforeClass
    public static void primarySetup() {
        if (null == btAdapter) {
            Log.d(INPUT_SIM_TESTET_TAG, "setup: CAN'T RUN TESTS! BT-ADAPTER WAS NULL");
            return;
        }

        btAdapter.enable();
        devAddr = "";
        for (BluetoothDevice dev : btAdapter.getBondedDevices()) {
            devAddr = dev.getAddress();
        }
    }

    @Before
    public void setup()  {
        simActivity = new SimActivityTester();
        simulator = InputSimulator.GetSimulator(simActivity, APP_UUID, devAddr);
        simulator.initBluetoothConnection();
    }

    @After
    public void cleanup() {
        if (null != simulator && simulator.isRunning()) {
            try {
                Thread.sleep(100L);
            } catch (InterruptedException e) {}

            simulator.terminateConnection();
        }

        if (null != simulator && !simulator.isRunning()) {
            simulator.initBluetoothConnection();
            connectWithTimeout(DEFAULT_CONNECT_TIMEOUT_MILLIS);
            simulator.terminateConnection();
        }

        simulator = null;
    }

    private boolean establishConnection() {
        if (null == btAdapter || !btAdapter.isEnabled()) {
            Log.d(INPUT_SIM_TAG, "sendUponConnection: NO BLUETOOTH AVAILABLE - SKIPPED TEST!");
            return false;
        }

        if (!connectWithTimeout(DEFAULT_CONNECT_TIMEOUT_MILLIS)) {
            Log.d(INPUT_SIM_TAG, "sendUponConnection: BLUETOOTH FAILED TO CONNECT - SKIPPED TEST!");
            return false;
        }
        return true;
    }

    private void setOutput(String s) {
        simOutput = s;
    }

    private String getLastSent(long delay) {
        synchronized (last_sent) {
            try {
                last_sent.wait(delay);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            return last_sent;
        }
    }

    private String getLastSent() {
        return getLastSent(DEFAULT_SEND_DELAY);
    }

    private boolean connectWithTimeout(long timeout) {
        long start = System.currentTimeMillis();

        while (!simulator.connect() && System.currentTimeMillis() - start < timeout) {
            try {
                Thread.sleep(RECONECT_INTERVAL_MILLIS);
            } catch (InterruptedException e) {
                e.printStackTrace();
                return false;
            }
        }

        return simulator.isRunning();
    }

    static final Object disconnectNotifier = new Object();
    static final Object connectNotifier = new Object();
    private boolean waitForDisconnect(long delay) {
        synchronized (disconnectNotifier) {
            try {
                disconnectNotifier.wait(delay);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        return simulator.isRunning();
    }

    @Test
    public void sendUponConnection() {
        if (!establishConnection()) return;

        setOutput(INITIAL_SEND_STRING);
        assertEquals("Should have sent the initial string", INITIAL_SEND_STRING, getLastSent());
    }

    @Test
    public void dontSendWhenPaused() throws InterruptedException {
        if (!establishConnection()) return;

        setOutput(INITIAL_SEND_STRING);

        simulator.pauseSending();
        String other = "OTHER";
        setOutput(other);

        Thread.sleep(10L);

        assertNotSame("Should not have sent while paused", other, getLastSent());
    }

    @Test
    public void continueSendingOnResume() throws InterruptedException {
        if (!establishConnection()) return;

        setOutput(INITIAL_SEND_STRING);

        simulator.pauseSending();
        String other = "OTHER";
        setOutput(other);
        Thread.sleep(simActivity.getSendInterval());

        simulator.resumeSending();
        Thread.sleep(simActivity.getSendInterval());
        assertEquals("Should have sent new message after resume", other, getLastSent());
    }

    @Test
    public void stopSendingOnRemoteDisconnect() throws IOException, InterruptedException {
        if (!establishConnection()) return;

        simulator.getSocket().close();
        if (waitForDisconnect(DEFAULT_SEND_DELAY)) {
            fail("Should have disconnected...");
        }

        String late = "LATE";
        setOutput(late);

        assertNotSame("Should not have sent after remote sock disconnect", late, getLastSent());
    }

    @Test
    public void canReconnectAfterDisconnect() throws InterruptedException {
        if (!establishConnection()) return;

        simulator.terminateConnection();
        simulator.initBluetoothConnection();

        Thread.sleep(50L);

        assertTrue("Should have been able to re-establish connection", establishConnection());
    }

    @Test
    public void sendTerminationOnIntentionalDisconnect() {
        if (!establishConnection()) return;

        simulator.terminateConnection();

        assertTrue("Should have sent out a termination message", simulator.wasIntentionallyTerminated());
    }

    @Test
    public void dontSendTerminationOnUnintentionalDisconnect() throws IOException {
        if (!establishConnection()) return;

        simulator.getSocket().close();

        assertFalse("Should not have sent out a termination message", simulator.wasIntentionallyTerminated());
    }


    private static class SimActivityTester implements ISimulatorActivity {

        @Override public long getSendInterval() { return 100L; }

        @Override public long getClickLength() { return 200L; }

        @Override
        public void onConnect() {
            Log.d(INPUT_SIM_TESTET_TAG, "onConnect: ");

            synchronized (connectNotifier) {
                connectNotifier.notifyAll();
            }
        }

        @Override public String getSimOutput() { return simOutput; }

        @Override public byte getSingleLevelOutput() { return 0; }

        @Override
        public void onValueSent(String s) {
            synchronized (last_sent) {
                Log.d(INPUT_SIM_TESTET_TAG, "onValueSent: SETTING LAST SENT TO: " + s);
                last_sent = s;
                last_sent.notifyAll();
            }
        }

        @Override
        public void showToast(String s) {
            Log.d(INPUT_SIM_TESTET_TAG, "showToast: " + s);
        }

        @Override
        public void onRemoteDisconnect() {
            Log.d(INPUT_SIM_TESTET_TAG, "onRemoteDisconnect: ");

            synchronized (disconnectNotifier) {
                disconnectNotifier.notifyAll();
            }
        }
    } // end private static class SimActivityTester
}
