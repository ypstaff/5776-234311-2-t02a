package com.yp.bts;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.util.Log;

import com.yp.SimpleInputEvent;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mockito;

import java.io.IOException;

import static junit.framework.Assert.fail;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.internal.verification.VerificationModeFactory.times;

/**
 * @author Oded
 * @version 13/04/2016
 */
public class BluetoothReceiverTest implements BluetoothEventReceiver {

    /* CONSTANTS */
    private static final String  BT_RECEIVER_TESTER_TAG  = "BT_Receiver Tester";
    private static final String  TESTER_UUID_STRING      = "00001101-0000-1000-8000-00805F9B34FB";
    private static final long    BT_STATE_POLL_INTERVAL  = 50L;
    private static final int     MAX_BT_STATE_POLL_TRIES = 10;
    private static final long    BT_CONNECT_TIMEOUT      = 20000L;
    private static final long    BT_DISCONNECT_TIMEOUT   = 2000L;
    private static       Context ctx;
    private static final byte[]  TERMINATION_MSG         = {(byte)0xFF};

    /* MEMBERS (HELPERS) */
    static BluetoothAdapter   btAdapter = BluetoothAdapter.getDefaultAdapter();
    static boolean            origBtState;
    static SimpleInputHandler stubHandler;
    static TranslatorGroup    transGrp;
    static BluetoothReceiver  defaultReceiver;
    private boolean           connected;
    private boolean           disconnected;


    @BeforeClass
    public static void prepareBluetooth() {
        Log.d(BT_RECEIVER_TESTER_TAG, "prepareBluetooth: RAN BEFORE CLASS");
        if (null == btAdapter) {
            return;
        }

        origBtState = btAdapter.isEnabled(); // used for restoring when tests are complete
        btAdapter.enable();
        ctx = null;
    }

    @Before
    public void setup() throws InterruptedException {
        defaultReceiver = new BluetoothReceiver(ctx, btAdapter, TESTER_UUID_STRING);
        stubHandler = mock(SimpleInputHandler.class);
        transGrp = mock(TranslatorGroup.class);
        if (null != btAdapter) {
            btAdapter.enable();
        }

        connected = false;
        disconnected = false;
    }

    @After
    public void after() {
        if (null != defaultReceiver) {
            defaultReceiver.stop(false);
        }

        defaultReceiver = null;
    }

    @AfterClass
    public static void cleanup() {
        restoreBtState();
    }

    private static void restoreBtState() {
        if (null == btAdapter) return;

        if (btAdapter.isEnabled() != origBtState) {
            if (origBtState)
                btAdapter.enable();
            else
                btAdapter.disable();
        }
    }

    /**
     * Disable the BT of the device running the tests.
     */
    public void disableBluetooth() {
        if (null != btAdapter && btAdapter.isEnabled()) {
            btAdapter.disable();
        }
    }

    /**
     * Wait for a change in BT state needed for test to be executed.
     *
     * @param state 'true' for enabled, 'false' for disabled.
     * @return 'true' if state changed within 0.5 seconds to given state, 'false' otherwise.
     * @throws InterruptedException
     */
    private boolean waitForBtState(boolean state) throws InterruptedException {
        int tries = MAX_BT_STATE_POLL_TRIES;
        while (tries > 0 && state != btAdapter.isEnabled()) {
            tries--;
            Thread.sleep(BT_STATE_POLL_INTERVAL);
        }

        return state == btAdapter.isEnabled();
    }

    @Test
    public void onConnectInvokedUponConnection() {
        defaultReceiver.start(this);
        if (!waitUntilConnected(BT_CONNECT_TIMEOUT)) {
            fail("COULD NOT CONNECT TO HELPER DEVICE");
        }

        assertTrue("onConnect should have been invoked", connected);
    }

    @Test
    public void onDisconnectInvokedUponIntentionalDisconnection() {
        defaultReceiver.start(this);
        if (!waitUntilConnected(BT_CONNECT_TIMEOUT)) {
            fail("COULD NOT CONNECT TO HELPER DEVICE");
        }

        defaultReceiver.stop(true);
        assertTrue("onConnect should have been invoked", disconnected);
    }

    @Test
    public void onDisconnectInvokedUponUnintentionalDisconnection() {
        defaultReceiver.start(this);
        if (!waitUntilConnected(BT_CONNECT_TIMEOUT)) {
            fail("COULD NOT CONNECT TO HELPER DEVICE");
        }

        defaultReceiver.stop(false);
        assertTrue("onConnect should have been invoked", disconnected);
    }


    @Test
    public void dontStartWhenBluetoothUnsupported() {
        BluetoothReceiver receiver = new BluetoothReceiver(ctx, null, TESTER_UUID_STRING);
        assertFalse(receiver.start(this));
    }

    @Test
    public void dontStartWhenBluetoothDisabled() throws InterruptedException {
        disableBluetooth();
        if (!waitForBtState(false)) {
            Log.d(BT_RECEIVER_TESTER_TAG, "dontStartWhenBluetoothDisabled: BT not ready on time - SKIPPING");
            return; // skip test
        }

        assertFalse(btAdapter.isEnabled());
        assertFalse(defaultReceiver.start(this));
    }

    @Test
    public void dontStartAgainWhenAlreadyReceiving() throws InterruptedException {
        if (!waitForBtState(true)) {
            Log.d(BT_RECEIVER_TESTER_TAG, "dontStartAgainWhenAlreadyReceiving: BT not ready on time - SKIPPING");
            return; // skip test
        }

        assertTrue(btAdapter.isEnabled());
        assertTrue(defaultReceiver.start(this));
        assertFalse(defaultReceiver.start(this));
    }

    @Test
    public void onlyFirstStopIsExecuted() {
        defaultReceiver.start(this);
        defaultReceiver.stop(true);
        assertTrue("Should have disconnected", disconnected);
        disconnected = false;
        defaultReceiver.stop(true);
        assertFalse("Should not have tried to disconnect again", disconnected);
    }

    @Test
    public void canRestartAfterStop() throws InterruptedException {
        if (!waitForBtState(true)) {
            Log.d(BT_RECEIVER_TESTER_TAG, "canRestartAfterStop: BT not ready on time - SKIPPING");
            return; // skip test
        }

        assertTrue(defaultReceiver.start(this));
        defaultReceiver.stop(false);
        assertTrue(defaultReceiver.start(this));
        defaultReceiver.stop(false);
        assertTrue(defaultReceiver.start(this));
    }

    @Test
    public void stopOnRegisteringNullHandler() {
        defaultReceiver.subscribe(stubHandler);
        defaultReceiver.start(this);

        defaultReceiver.subscribe(null);
        assertTrue(!defaultReceiver.isStarted());
    }

    @Test
    public void registeredHandlerCallbackInvokedOnClick() {
        // create stub handler
        transGrp = mock(TranslatorGroup.class);

        // subscribe first handler
        defaultReceiver.subscribe(stubHandler);
        defaultReceiver.setTranslatorGroup(transGrp);

        // generate event
        transGrp.dispatchEvent(0, SimpleInputEvent.CLICK);

        // assert (only) last subscribed handler was invoked
        Mockito.verify(stubHandler, times(0)).handleEvent(0, SimpleInputEvent.CLICK);
    }

    @Test
    public void registeredHandlerCallbackInvokedOnLongPressDown() {
        // create stub handler
        transGrp = mock(TranslatorGroup.class);

        // subscribe first handler
        defaultReceiver.subscribe(stubHandler);
        defaultReceiver.setTranslatorGroup(transGrp);

        // generate event
        transGrp.dispatchEvent(0, SimpleInputEvent.LONG_PRESS_DOWN);

        // assert (only) last subscribed handler was invoked
        Mockito.verify(stubHandler, times(0)).handleEvent(0, SimpleInputEvent.LONG_PRESS_DOWN);
    }

    @Test
    public void registeredHandlerCallbackInvokedOnLongPressUp() {
        // create stub handler
        transGrp = mock(TranslatorGroup.class);

        // subscribe first handler
        defaultReceiver.subscribe(stubHandler);
        defaultReceiver.setTranslatorGroup(transGrp);

        // generate event
        transGrp.dispatchEvent(0, SimpleInputEvent.LONG_PRESS_UP);

        // assert (only) last subscribed handler was invoked
        Mockito.verify(stubHandler, times(0)).handleEvent(0, SimpleInputEvent.LONG_PRESS_UP);
    }

    @Test
    public void onlyMostRecentSubscriberCallbackInvoked() {
        // create stub handler
        SimpleInputHandler stubHandler2 = mock(SimpleInputHandler.class);
        transGrp = new TranslatorGroup(2);

        // subscribe first handler
        defaultReceiver.subscribe(stubHandler);
        defaultReceiver.setTranslatorGroup(transGrp);

        // subscribe another handler
        defaultReceiver.subscribe(stubHandler2);

        // generate event
        transGrp.dispatchEvent(0, SimpleInputEvent.CLICK);

        // assert (only) last subscribed handler was invoked
        Mockito.verify(stubHandler2, times(1)).handleEvent(0, SimpleInputEvent.CLICK);
        Mockito.verify(stubHandler, times(0)).handleEvent(0, SimpleInputEvent.CLICK);
    }

    @Test
    public void intentionalConnectionTerminationIsIdentified() {
        // inject a termination message while receiver is running
        defaultReceiver.start(this);
        defaultReceiver.injectRawMessage(TERMINATION_MSG);
        assertTrue("Receiver should have stopped", disconnected);
    }

    @Test
    public void unexpectedConnectionTerminationIsIdentified() throws IOException {
        // forcibly disable receiving thread / BT while running
        defaultReceiver.start(this);
        if (!waitUntilConnected(BT_CONNECT_TIMEOUT)) return;
        BluetoothSocket sock = defaultReceiver.getDataSocket();
        sock.close();
        Thread.yield();
        waitUntilDisconncted(BT_DISCONNECT_TIMEOUT);
        assertTrue("Receiver should have stopped", disconnected);
    }

    @Test
    public void terminationMsgSentOnIntentionalClose() {
        // terminate connection and make sure termination message was sent
        defaultReceiver.start(this);
        System.out.println("CONNECT BT NOW!");
        if (!waitUntilConnected(BT_CONNECT_TIMEOUT)) {
            fail("COULD NOT CONNECT TO HELPER DEVICE");
        }

        boolean sentTermination = defaultReceiver.stop(true);
        assertTrue("Should have sent a termination message", sentTermination);
    }

    @Test
    public void terminationMsgNotSentOnIntentionalClose() {
        // terminate connection and make sure termination message was NOT sent
        defaultReceiver.start(this);
        System.out.println("CONNECT BT NOW!");
        if (!waitUntilConnected(BT_CONNECT_TIMEOUT)) {
            fail("COULD NOT CONNECT TO HELPER DEVICE");
        }

        boolean sentTermination = defaultReceiver.stop(false);
        assertFalse("Should not have sent a termination message", sentTermination);
    }

    @Override
    public void onConnect(boolean success) {
        synchronized (connectNotifier) {
            connectNotifier.notifyAll();
        }

        connected = true;
    }

    @Override
    public void onDisconnect() {
        synchronized (disconnectNotifier) {
            disconnectNotifier.notifyAll();
        }

        disconnected = true;
    }

    final Object connectNotifier = new Object();
    private boolean waitUntilConnected(long timeout) {
        if (connected) return true;

        synchronized (connectNotifier) {
            try {
                connectNotifier.wait(timeout);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        return connected;
    }

    final Object disconnectNotifier = new Object();
    private boolean waitUntilDisconncted(long timeout) {
        if (disconnected) return true;

        synchronized (disconnectNotifier) {
            try {
                disconnectNotifier.wait(timeout);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        return disconnected;
    }
}
