package com.yp;

import android.content.DialogInterface;
import android.support.test.InstrumentationRegistry;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.ActivityInstrumentationTestCase2;

import com.yp.activities.GalleryActivity;
import com.yp.cursor.R;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.matcher.ViewMatchers.withId;

@RunWith(AndroidJUnit4.class)
public class GalleryTest extends ActivityInstrumentationTestCase2<GalleryActivity> implements DialogInterface.OnShowListener {

    private static final int MAX_PAGES_TO_SCROLL = 5;
    private static final long DIALOG_DISPLAY_MAX_WAIT_MILLIS = 500L;

    GalleryActivity activity;
    private final Object dialogShowNotifier = new Object();

    public GalleryTest() {
        super(GalleryActivity.class);
    }

    @Before
    public void setUp() throws Exception {
        super.setUp();

        injectInstrumentation(InstrumentationRegistry.getInstrumentation());
        activity = getActivity();
        activity.setDialogListener(this);
    }

    @Rule
    public ActivityTestRule<GalleryActivity> activityRule = new ActivityTestRule<>(
            GalleryActivity.class,
            true,     // initialTouchMode
            false);   // launchActivity. False to customize the intent

    @Test
    public void returnUriWhenLaunchedFromOtherActivity() {
        activity.setFromAnotherActivity(true);
        onView(withId(R.id.iv_gallery_1_1)).perform(click());
        assertFalse("Dialog should NOT be displayed", activity.isShowingDialog());
    }

    @Test
    public void displayDialogWhenNotLaunchedFromOtherActivity() {
        onView(withId(R.id.iv_gallery_1_1)).perform(click());
        assertTrue("Dialog should be displayed", activity.isShowingDialog());
    }

    @Test
    public void leftFabClosesPreviewDialog() throws InterruptedException {
        onView(withId(R.id.iv_gallery_1_1)).perform(click());
        assertTrue("Dialog should be displayed", activity.isShowingDialog());
        activity.getLeftFab().callOnClick();
        waitForDialog();
        Thread.sleep(100L);
        assertFalse("Dialog should have been closed", activity.isShowingDialog());
    }

    private void waitForDialog() {
        synchronized (dialogShowNotifier) {
            try {
                dialogShowNotifier.wait(DIALOG_DISPLAY_MAX_WAIT_MILLIS);
            } catch (InterruptedException e) {
//                e.printStackTrace();
            }
        }
    }

    @Test
    public void middleFabClosesPreviewDialog() throws InterruptedException {
        onView(withId(R.id.iv_gallery_1_1)).perform(click());
        assertTrue("Dialog should be displayed", activity.isShowingDialog());

        activity.getMiddleFab().callOnClick();
        waitForDialog();
        Thread.sleep(100L);
        assertFalse("Dialog should have been closed", activity.isShowingDialog());
    }

    @Test
    public void rightFabClosesPreviewDialog() throws InterruptedException {
        onView(withId(R.id.iv_gallery_1_1)).perform(click());
        assertTrue("Dialog should be displayed", activity.isShowingDialog());

        activity.getRightFab().callOnClick();
        waitForDialog();

        Thread.sleep(100L);
        assertFalse("Dialog should have been closed", activity.isShowingDialog());
    }

    @Test
    public void pressingTopJumpsToPageOne() {
        onView(withId(R.id.gallery_scroll_next)).perform(click());

        onView(withId(R.id.gallery_scroll_top)).perform(click());
        assertEquals("Current page should be 1 (the first)", 1, activity.getCurrentPage());
    }

    @Test
    public void pressingEndJumpsToLastPage() {
        onView(withId(R.id.gallery_scroll_end)).perform(click());
        assertEquals("Current page should be the last", activity.getPageCount(), activity.getCurrentPage());
    }

    @Test
    public void pressingNextMovesToNextPage() {
        final int totalPages = activity.getPageCount();
        final int bound = MAX_PAGES_TO_SCROLL > totalPages ? totalPages : MAX_PAGES_TO_SCROLL;

        for (int i = 1; i < bound; ++i) {
            onView(withId(R.id.gallery_scroll_next)).perform(click());
            assertEquals("Current page should " + (i + 1), i + 1, activity.getCurrentPage());
        }
    }

    @Test
    public void pressingPrevMovesToPreviousPage() {
        onView(withId(R.id.gallery_scroll_end)).perform(click());

        final int totalPages = activity.getPageCount();
        final int bound = MAX_PAGES_TO_SCROLL > totalPages ? 1 : totalPages - MAX_PAGES_TO_SCROLL;

        for (int i = totalPages; i > bound; --i) {
            onView(withId(R.id.gallery_scroll_back)).perform(click());
            assertEquals("Current page should " + (i - 1), i - 1, activity.getCurrentPage());
        }
    }

    @Test
    public void pageDoesNotChangeWhenPressingNextOnLastPage() {
        onView(withId(R.id.gallery_scroll_end)).perform(click());
        int lastPage = activity.getCurrentPage();

        onView(withId(R.id.gallery_scroll_next)).perform(click());
        assertEquals("Should be still at last page", lastPage, activity.getCurrentPage());
    }

    @Test
    public void pageDoesNotChangeWhenPressingPrevOnFirstPage() {
        int lastDisplayedPage = activity.getCurrentPage();
        onView(withId(R.id.gallery_scroll_back)).perform(click());
        assertEquals("Should be still at first page", lastDisplayedPage, activity.getCurrentPage());

        if (activity.getPageCount() > 1) {
            onView(withId(R.id.gallery_scroll_next)).perform(click());
            onView(withId(R.id.gallery_scroll_back)).perform(click());
            assertEquals("Should be still at first page", lastDisplayedPage, activity.getCurrentPage());
        }
    }

    @Override
    public void onShow(DialogInterface dialog) {
        synchronized (dialogShowNotifier) {
            dialogShowNotifier.notifyAll();
        }
    }
}


