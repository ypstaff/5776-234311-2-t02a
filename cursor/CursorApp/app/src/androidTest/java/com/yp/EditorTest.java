package com.yp;

import android.content.Context;
import android.graphics.Typeface;
import android.support.test.InstrumentationRegistry;
import android.support.test.espresso.matcher.ViewMatchers;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.ActivityInstrumentationTestCase2;
import android.text.style.BulletSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.QuoteSpan;
import android.text.style.StrikethroughSpan;
import android.text.style.StyleSpan;
import android.text.style.URLSpan;
import android.text.style.UnderlineSpan;

import com.yp.activities.EditorActivity;
import com.yp.cursor.R;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import io.github.mthli.knife.KnifeText;

import static android.support.test.espresso.Espresso.onData;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.clearText;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isCompletelyDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withEffectiveVisibility;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static org.hamcrest.CoreMatchers.allOf;
import static org.hamcrest.Matchers.anything;


/**
 * @author Matan Revivo
 */
@RunWith(AndroidJUnit4.class)
public class EditorTest extends ActivityInstrumentationTestCase2<EditorActivity> {
    public EditorTest() {
        super(EditorActivity.class);
    }

    Context context;
    EditorActivity activity;
    KnifeText knife;

    @Before
    public void setUp() throws Exception {
        super.setUp();

        injectInstrumentation(InstrumentationRegistry.getInstrumentation());
        activity = getActivity();
        activity.setTestMode(true);
        knife= activity.getKnife();
    }


    @Rule
    public ActivityTestRule<EditorActivity> activityRule = new ActivityTestRule<>(
            EditorActivity.class,
            true,     // initialTouchMode
            false);   // launchActivity. False to customize the intent

    boolean contains(StyleSpan[] styleSpans, int style){
        for(StyleSpan s: styleSpans){
            if(s.getStyle() == style){
                return true;
            }
        }
        return false;
    }

    private void applyStyleThroughGUI(String s, int id) {
        onView(withId(R.id.knife)).perform(clearText(), typeText(s));
        onView(withId(R.id.selection)).perform(click());
        onView(withId(R.id.back)).perform(click());

        onView(withId(id)).perform(click());
    }

    @Test
    public void testBold(){
        applyStyleThroughGUI("Bold", R.id.bold);
        StyleSpan[] styleSpans = knife.getEditableText().getSpans(knife.getSelectionStart(), knife.getSelectionEnd(), StyleSpan.class);
        assertTrue(contains(styleSpans, Typeface.BOLD));
    }

    @Test
    public void testRemoveBold() {
        applyStyleThroughGUI("Bold", R.id.bold);
        onView(withId(R.id.bold)).perform(click());
        StyleSpan[] styleSpans = knife.getEditableText().getSpans(knife.getSelectionStart(), knife.getSelectionEnd(), StyleSpan.class);
        assertFalse(contains(styleSpans, Typeface.BOLD));
    }


    @Test
    public void testItalic(){
        applyStyleThroughGUI("Italic", R.id.italic);
        StyleSpan[] styleSpans = knife.getEditableText().getSpans(knife.getSelectionStart(), knife.getSelectionEnd(), StyleSpan.class);
        assertTrue(contains(styleSpans, Typeface.ITALIC));
    }

    @Test
    public void testRemoveItalic() {
        applyStyleThroughGUI("Italic", R.id.italic);
        onView(withId(R.id.italic)).perform(click());
        StyleSpan[] styleSpans = knife.getEditableText().getSpans(knife.getSelectionStart(), knife.getSelectionEnd(), StyleSpan.class);
        assertFalse(contains(styleSpans, Typeface.ITALIC));
    }

    @Test
    public void testUnderline(){
        applyStyleThroughGUI("Underline", R.id.underline);
        UnderlineSpan[] styleSpans = knife.getEditableText().getSpans(knife.getSelectionStart(), knife.getSelectionEnd(), UnderlineSpan.class);
        assertTrue(styleSpans.length>0);
    }

    @Test
    public void testRemoveUnderline() {
        applyStyleThroughGUI("Underline", R.id.underline);
        onView(withId(R.id.underline)).perform(click());
        UnderlineSpan[] styleSpans = knife.getEditableText().getSpans(knife.getSelectionStart(), knife.getSelectionEnd(), UnderlineSpan.class);
        assertTrue(styleSpans.length == 0);
    }

    @Test
    public void testStrikethrough(){
        applyStyleThroughGUI("Strikethrough", R.id.strikethrough);
        StrikethroughSpan[] styleSpans = knife.getEditableText().getSpans(knife.getSelectionStart(), knife.getSelectionEnd(), StrikethroughSpan.class);
        assertTrue(styleSpans.length>0);
    }

    @Test
    public void testRemoveStrikethrough(){
        applyStyleThroughGUI("Strikethrough", R.id.strikethrough);
        onView(withId(R.id.strikethrough)).perform(click());
        StrikethroughSpan[] styleSpans = knife.getEditableText().getSpans(knife.getSelectionStart(), knife.getSelectionEnd(), StrikethroughSpan.class);
        assertTrue(styleSpans.length == 0);
    }

    @Test
    public void testBullet(){
        onView(withId(R.id.knife)).perform(clearText(), typeText("This line will be with bullet"));
        onView(withId(R.id.bullet)).perform(click());

        BulletSpan[] styleSpans = knife.getEditableText().getSpans(knife.getSelectionStart(), knife.getSelectionEnd(), BulletSpan.class);
        assertTrue(styleSpans.length>0);
    }

    @Test
    public void testRemoveBullet(){
        onView(withId(R.id.knife)).perform(clearText(), typeText("This line will demonstrate bullet removal"));
        onView(withId(R.id.bullet)).perform(click());
        onView(withId(R.id.bullet)).perform(click());

        BulletSpan[] styleSpans = knife.getEditableText().getSpans(knife.getSelectionStart(), knife.getSelectionEnd(), BulletSpan.class);
        assertTrue(styleSpans.length == 0);
    }

    @Test
    public void testQuote(){
        onView(withId(R.id.knife)).perform(clearText(), typeText("This line will be with quote"));
        onView(withId(R.id.quote)).perform(click());

        QuoteSpan[] styleSpans = knife.getEditableText().getSpans(knife.getSelectionStart(), knife.getSelectionEnd(), QuoteSpan.class);
        assertTrue(styleSpans.length>0);
    }

    @Test
    public void testRemoveQuote(){
        onView(withId(R.id.knife)).perform(clearText(), typeText("This line will be with quote"));
        onView(withId(R.id.quote)).perform(click());
        onView(withId(R.id.quote)).perform(click());


        QuoteSpan[] styleSpans = knife.getEditableText().getSpans(knife.getSelectionStart(), knife.getSelectionEnd(), QuoteSpan.class);
        assertTrue(styleSpans.length==0);
    }

    @Test
    public void testLink(){
        applyStyleThroughGUI("Link", R.id.link);
        onView(withId(R.id.link_edittext)).perform(typeText("google.com"));
        onView(withId(R.id.ok_button)).perform(click());

        URLSpan[] styleSpans = knife.getEditableText().getSpans(knife.getSelectionStart(), knife.getSelectionEnd(), URLSpan.class);
        assertTrue(styleSpans.length>0);
        assertEquals(styleSpans[0].getURL().toLowerCase().replaceAll("\\s+",""), "http://www.google.com");
    }

    @Test
    public void testRemoveLink(){
        applyStyleThroughGUI("Link", R.id.link);
        onView(withId(R.id.link_edittext)).perform(typeText("google.com"));
        onView(withId(R.id.ok_button)).perform(click());
        onView(withId(R.id.link)).perform(click());


        URLSpan[] styleSpans = knife.getEditableText().getSpans(knife.getSelectionStart(), knife.getSelectionEnd(), URLSpan.class);
        assertTrue(styleSpans.length == 0);
    }

    @Test
    public void testTextColor(){
        applyStyleThroughGUI("red", R.id.textColor);
        onData(anything()).inAdapterView(withId(R.id.listFonts))
                .atPosition(1)
                .perform(click());

        onView(allOf(withId(R.id.fab_middle),
                withEffectiveVisibility(ViewMatchers.Visibility.VISIBLE)))
                .check(matches(isCompletelyDisplayed())).perform(click());

        ForegroundColorSpan[] styleSpans = knife.getEditableText().getSpans(knife.getSelectionStart(), knife.getSelectionEnd(), ForegroundColorSpan.class);
        assertEquals(styleSpans[0].getForegroundColor(),  activity.getRed());
    }


    @Test
    public void testCopyPaste(){
        onView(withId(R.id.knife)).perform(clearText(), typeText("Copy this"));
        onView(withId(R.id.selection)).perform(click());
        onView(withId(R.id.back)).perform(click());
        onView(withId(R.id.back)).perform(click());
        onView(withId(R.id.copy)).perform(click());
        assertEquals(knife.getText().toString(), "Copy this");

        onView(withId(R.id.selection)).perform(click());
        onView(withId(R.id.forward)).perform(click());
        onView(withId(R.id.paste)).perform(click());
        assertEquals(knife.getText().toString(), "Copy thisCopy this");

        onView(withId(R.id.paste)).perform(click());
        assertEquals(knife.getText().toString(), "Copy thisCopy thisCopy this");
    }

    @Test
    public void testCutPaste(){
        onView(withId(R.id.knife)).perform(clearText(), typeText("Cut this"));
        onView(withId(R.id.selection)).perform(click());
        onView(withId(R.id.back)).perform(click());
        onView(withId(R.id.back)).perform(click());
        onView(withId(R.id.cut)).perform(click());
        assertEquals(knife.getText().toString(), "");

        onView(withId(R.id.selection)).perform(click());
        onView(withId(R.id.forward)).perform(click());
        onView(withId(R.id.paste)).perform(click());
        assertEquals(knife.getText().toString(), "Cut this");

        onView(withId(R.id.paste)).perform(click());
        assertEquals(knife.getText().toString(), "Cut thisCut this");
    }

    @Test
    public void testStatistics(){
        onView(withId(R.id.knife)).perform(clearText());
       onView(withId(R.id.statistics)).perform(click());
        assertEquals(activity.getStatisticsMessage(), "0 words, " + "0 lines, " + "0 chars");
        onView(withId(R.id.knife)).perform(clearText(), typeText("one two three\none two three"));
        onView(withId(R.id.statistics)).perform(click());
        assertEquals(activity.getStatisticsMessage(), "6 words, " + "2 lines, " + "27 chars");
    }

    @Test
    public void testAllEffects() {
        onView(withId(R.id.knife)).perform(clearText(), typeText("all effects"));
        onView(withId(R.id.selection)).perform(click());
        onView(withId(R.id.back)).perform(click());
        onView(withId(R.id.back)).perform(click());
        onView(withId(R.id.link)).perform(click());
        onView(withId(R.id.link_edittext)).perform(typeText("google.com"));
        onView(withId(R.id.ok_button)).perform(click());
        onView(withId(R.id.bold)).perform(click());
        onView(withId(R.id.italic)).perform(click());
        onView(withId(R.id.underline)).perform(click());
        onView(withId(R.id.strikethrough)).perform(click());
        onView(withId(R.id.textColor)).perform(click());
        onData(anything()).inAdapterView(withId(R.id.listFonts))
                .atPosition(1)
                .perform(click());

        onView(allOf(withId(R.id.fab_middle),
                withEffectiveVisibility(ViewMatchers.Visibility.VISIBLE)))
                .check(matches(isCompletelyDisplayed())).perform(click());
        onView(withId(R.id.selection)).perform(click());
        onView(withId(R.id.forward)).perform(click());
        onView(withId(R.id.bullet)).perform(click());
        onView(withId(R.id.quote)).perform(click());

        StyleSpan[] styleSpans = knife.getEditableText().getSpans(knife.getSelectionStart(), knife.getSelectionEnd(), StyleSpan.class);
        assertTrue(contains(styleSpans, Typeface.BOLD));
        styleSpans = knife.getEditableText().getSpans(knife.getSelectionStart(), knife.getSelectionEnd(), StyleSpan.class);
        assertTrue(contains(styleSpans, Typeface.ITALIC));
        UnderlineSpan[] styleSpans2 = knife.getEditableText().getSpans(knife.getSelectionStart(), knife.getSelectionEnd(), UnderlineSpan.class);
        assertTrue(styleSpans2.length>0);
        StrikethroughSpan[] styleSpans3 = knife.getEditableText().getSpans(knife.getSelectionStart(), knife.getSelectionEnd(), StrikethroughSpan.class);
        assertTrue(styleSpans3.length>0);
        BulletSpan[] styleSpans4 = knife.getEditableText().getSpans(knife.getSelectionStart(), knife.getSelectionEnd(), BulletSpan.class);
        assertTrue(styleSpans4.length>0);
        QuoteSpan[] styleSpans5 = knife.getEditableText().getSpans(knife.getSelectionStart(), knife.getSelectionEnd(), QuoteSpan.class);
        assertTrue(styleSpans5.length>0);
        URLSpan[] styleSpans6 = knife.getEditableText().getSpans(knife.getSelectionStart(), knife.getSelectionEnd(), URLSpan.class);
        assertTrue(styleSpans6.length>0);
        ForegroundColorSpan[] styleSpans7 = knife.getEditableText().getSpans(knife.getSelectionStart(), knife.getSelectionEnd(), ForegroundColorSpan.class);
        assertEquals(styleSpans7[0].getForegroundColor(),  activity.getRed());
    }


}


