package com.yp.bts;

import com.yp.SimpleInputEvent;

import org.junit.Test;
import org.junit.Assert;

/**
 * Created by etayhn on 17-Apr-16.
 */
public class TranslatorStateMachineTest {

    @Test public void setThresholdsSetsTheClickThreshold(){
        TranslatorStateMachine.setThresholds(54,67);
        Assert.assertEquals(54, TranslatorStateMachine.getClickThreshold());

        TranslatorStateMachine.setThresholds(89,123);
        Assert.assertEquals(89, TranslatorStateMachine.getClickThreshold());
    }

    @Test public void setThresholdsSetsTheLongPressThreshold(){
        TranslatorStateMachine.setThresholds(54,67);
        Assert.assertEquals(67, TranslatorStateMachine.getLongPressThreshold());

        TranslatorStateMachine.setThresholds(89,123);
        Assert.assertEquals(123, TranslatorStateMachine.getLongPressThreshold());
    }

    @Test public void cannotSetClickThresholdLargerThanLongPressThreshold(){
        Assert.assertTrue(TranslatorStateMachine.setThresholds(54,67));
        Assert.assertEquals(54, TranslatorStateMachine.getClickThreshold());
        Assert.assertEquals(67, TranslatorStateMachine.getLongPressThreshold());

        Assert.assertFalse(TranslatorStateMachine.setThresholds(123, 9));
        Assert.assertEquals(54, TranslatorStateMachine.getClickThreshold());
        Assert.assertEquals(67, TranslatorStateMachine.getLongPressThreshold());
    }

    @Test public void clickIsNotGeneratedIfNumberOfOnesIsLessThanClickThreshold1(){
        TranslatorStateMachine tsm = new TranslatorStateMachine();
        TranslatorStateMachine.setThresholds(4, 7);

        Assert.assertEquals(SimpleInputEvent.NOP, tsm.changeState(true)); // first 1
        Assert.assertEquals(SimpleInputEvent.NOP, tsm.changeState(false)); // no second 1
    }

    @Test public void clickIsNotGeneratedIfNumberOfOnesIsLessThanClickThreshold2(){
        TranslatorStateMachine tsm = new TranslatorStateMachine();
        TranslatorStateMachine.setThresholds(4, 7);

        Assert.assertEquals(SimpleInputEvent.NOP, tsm.changeState(true)); // first 1
        Assert.assertEquals(SimpleInputEvent.NOP, tsm.changeState(true)); // second 1
        Assert.assertEquals(SimpleInputEvent.NOP, tsm.changeState(false)); // no third 1
    }

    @Test public void clickIsNotGeneratedIfNumberOfOnesIsLessThanClickThreshold3(){
        TranslatorStateMachine tsm = new TranslatorStateMachine();
        TranslatorStateMachine.setThresholds(4, 7);

        Assert.assertEquals(SimpleInputEvent.NOP, tsm.changeState(true)); // first 1
        Assert.assertEquals(SimpleInputEvent.NOP, tsm.changeState(true)); // second 1
        Assert.assertEquals(SimpleInputEvent.NOP, tsm.changeState(true)); // third 1
        Assert.assertEquals(SimpleInputEvent.NOP, tsm.changeState(false)); // no fourth 1
    }

    @Test public void clickIsGeneratedIfNumberOfOnesIsBetweenClickThresholdAndLongPressThreshold1(){
        TranslatorStateMachine tsm = new TranslatorStateMachine();
        TranslatorStateMachine.setThresholds(4, 7);

        Assert.assertEquals(SimpleInputEvent.NOP, tsm.changeState(true)); // first 1
        Assert.assertEquals(SimpleInputEvent.NOP, tsm.changeState(true)); // second 1
        Assert.assertEquals(SimpleInputEvent.NOP, tsm.changeState(true)); // third 1
        Assert.assertEquals(SimpleInputEvent.NOP, tsm.changeState(true)); // fourth 1
        Assert.assertEquals(SimpleInputEvent.CLICK, tsm.changeState(false)); // no fifth 1
    }

    @Test public void clickIsGeneratedIfNumberOfOnesIsBetweenClickThresholdAndLongPressThreshold2(){
        TranslatorStateMachine tsm = new TranslatorStateMachine();
        TranslatorStateMachine.setThresholds(4, 7);

        Assert.assertEquals(SimpleInputEvent.NOP, tsm.changeState(true)); // first 1
        Assert.assertEquals(SimpleInputEvent.NOP, tsm.changeState(true)); // second 1
        Assert.assertEquals(SimpleInputEvent.NOP, tsm.changeState(true)); // third 1
        Assert.assertEquals(SimpleInputEvent.NOP, tsm.changeState(true)); // fourth 1
        Assert.assertEquals(SimpleInputEvent.NOP, tsm.changeState(true)); // fifth 1
        Assert.assertEquals(SimpleInputEvent.CLICK, tsm.changeState(false)); // no sixth 1
    }

    @Test public void clickIsGeneratedIfNumberOfOnesIsBetweenClickThresholdAndLongPressThreshold3(){
        TranslatorStateMachine tsm = new TranslatorStateMachine();
        TranslatorStateMachine.setThresholds(4, 7);

        Assert.assertEquals(SimpleInputEvent.NOP, tsm.changeState(true)); // first 1
        Assert.assertEquals(SimpleInputEvent.NOP, tsm.changeState(true)); // second 1
        Assert.assertEquals(SimpleInputEvent.NOP, tsm.changeState(true)); // third 1
        Assert.assertEquals(SimpleInputEvent.NOP, tsm.changeState(true)); // fourth 1
        Assert.assertEquals(SimpleInputEvent.NOP, tsm.changeState(true)); // fifth 1
        Assert.assertEquals(SimpleInputEvent.NOP, tsm.changeState(true)); // sixth 1
        Assert.assertEquals(SimpleInputEvent.CLICK, tsm.changeState(false)); // no seventh 1
    }

    @Test public void clickIsGeneratedIfNumberOfOnesIsBetweenClickThresholdAndLongPressThreshold4(){
        TranslatorStateMachine tsm = new TranslatorStateMachine();
        TranslatorStateMachine.setThresholds(4, 7);

        Assert.assertEquals(SimpleInputEvent.NOP, tsm.changeState(true)); // first 1
        Assert.assertEquals(SimpleInputEvent.NOP, tsm.changeState(true)); // second 1
        Assert.assertEquals(SimpleInputEvent.NOP, tsm.changeState(true)); // third 1
        Assert.assertEquals(SimpleInputEvent.NOP, tsm.changeState(true)); // fourth 1
        Assert.assertEquals(SimpleInputEvent.NOP, tsm.changeState(true)); // fifth 1
        Assert.assertEquals(SimpleInputEvent.NOP, tsm.changeState(true)); // sixth 1
        SimpleInputEvent sie = tsm.changeState(true);                    // no seventh 1
        Assert.assertNotEquals(SimpleInputEvent.NOP, sie);
        Assert.assertNotEquals(SimpleInputEvent.CLICK, sie);
    }

    @Test public void longPressDownEventIsGeneratedAfterLongPressThresholdConsecutiveOnes(){
        TranslatorStateMachine tsm = new TranslatorStateMachine();
        TranslatorStateMachine.setThresholds(4, 7);

        Assert.assertEquals(SimpleInputEvent.NOP, tsm.changeState(true)); // first 1
        Assert.assertEquals(SimpleInputEvent.NOP, tsm.changeState(true)); // second 1
        Assert.assertEquals(SimpleInputEvent.NOP, tsm.changeState(true)); // third 1
        Assert.assertEquals(SimpleInputEvent.NOP, tsm.changeState(true)); // fourth 1
        Assert.assertEquals(SimpleInputEvent.NOP, tsm.changeState(true)); // fifth 1
        Assert.assertEquals(SimpleInputEvent.NOP, tsm.changeState(true)); // sixth 1
        Assert.assertEquals(SimpleInputEvent.LONG_PRESS_DOWN, tsm.changeState(true)); // seventh 1
    }

    @Test public void NOPIsGeneratedAfterLongPressDownWasTriggeredAndUntilLongPressUpIsGenerated(){
        TranslatorStateMachine tsm = new TranslatorStateMachine();
        TranslatorStateMachine.setThresholds(4, 7);

        Assert.assertEquals(SimpleInputEvent.NOP, tsm.changeState(true)); // first 1
        Assert.assertEquals(SimpleInputEvent.NOP, tsm.changeState(true)); // second 1
        Assert.assertEquals(SimpleInputEvent.NOP, tsm.changeState(true)); // third 1
        Assert.assertEquals(SimpleInputEvent.NOP, tsm.changeState(true)); // fourth 1
        Assert.assertEquals(SimpleInputEvent.NOP, tsm.changeState(true)); // fifth 1
        Assert.assertEquals(SimpleInputEvent.NOP, tsm.changeState(true)); // sixth 1
        Assert.assertEquals(SimpleInputEvent.LONG_PRESS_DOWN, tsm.changeState(true)); // seventh 1
        Assert.assertEquals(SimpleInputEvent.NOP, tsm.changeState(true));
        Assert.assertEquals(SimpleInputEvent.NOP, tsm.changeState(true));
        Assert.assertEquals(SimpleInputEvent.NOP, tsm.changeState(true));
        Assert.assertEquals(SimpleInputEvent.NOP, tsm.changeState(true));
    }

    @Test public void LongPressUpIsGeneratedAtTheFirstZeroAfterLongPressDownWasGenerated(){
        TranslatorStateMachine tsm = new TranslatorStateMachine();
        TranslatorStateMachine.setThresholds(4, 7);

        Assert.assertEquals(SimpleInputEvent.NOP, tsm.changeState(true)); // first 1
        Assert.assertEquals(SimpleInputEvent.NOP, tsm.changeState(true)); // second 1
        Assert.assertEquals(SimpleInputEvent.NOP, tsm.changeState(true)); // third 1
        Assert.assertEquals(SimpleInputEvent.NOP, tsm.changeState(true)); // fourth 1
        Assert.assertEquals(SimpleInputEvent.NOP, tsm.changeState(true)); // fifth 1
        Assert.assertEquals(SimpleInputEvent.NOP, tsm.changeState(true)); // sixth 1
        Assert.assertEquals(SimpleInputEvent.LONG_PRESS_DOWN, tsm.changeState(true)); // seventh 1
        Assert.assertEquals(SimpleInputEvent.NOP, tsm.changeState(true));
        Assert.assertEquals(SimpleInputEvent.NOP, tsm.changeState(true));
        Assert.assertEquals(SimpleInputEvent.NOP, tsm.changeState(true));
        Assert.assertEquals(SimpleInputEvent.NOP, tsm.changeState(true));
        Assert.assertEquals(SimpleInputEvent.LONG_PRESS_UP, tsm.changeState(false));
    }

    @Test public void resettingBeforeClickThresholdOnesWereReceivedGeneratesNOP1(){
        TranslatorStateMachine tsm = new TranslatorStateMachine();
        TranslatorStateMachine.setThresholds(4, 7);

        Assert.assertEquals(SimpleInputEvent.NOP, tsm.changeState(true)); // first 1
        Assert.assertEquals(SimpleInputEvent.NOP, tsm.reset());
    }

    @Test public void resettingBeforeClickThresholdOnesWereReceivedGeneratesNOP2(){
        TranslatorStateMachine tsm = new TranslatorStateMachine();
        TranslatorStateMachine.setThresholds(4, 7);

        Assert.assertEquals(SimpleInputEvent.NOP, tsm.changeState(true)); // first 1
        Assert.assertEquals(SimpleInputEvent.NOP, tsm.changeState(true)); // second 1
        Assert.assertEquals(SimpleInputEvent.NOP, tsm.reset());
    }

    @Test public void resettingBeforeClickThresholdOnesWereReceivedGeneratesNOP3(){
        TranslatorStateMachine tsm = new TranslatorStateMachine();
        TranslatorStateMachine.setThresholds(4, 7);

        Assert.assertEquals(SimpleInputEvent.NOP, tsm.changeState(true)); // first 1
        Assert.assertEquals(SimpleInputEvent.NOP, tsm.changeState(true)); // second 1
        Assert.assertEquals(SimpleInputEvent.NOP, tsm.changeState(true)); // third 1
        Assert.assertEquals(SimpleInputEvent.NOP, tsm.reset());
    }

    @Test public void resettingBetweenClickThresholdAndLongPressThresholdGeneratesNOP1(){
        TranslatorStateMachine tsm = new TranslatorStateMachine();
        TranslatorStateMachine.setThresholds(4, 7);

        Assert.assertEquals(SimpleInputEvent.NOP, tsm.changeState(true)); // first 1
        Assert.assertEquals(SimpleInputEvent.NOP, tsm.changeState(true)); // second 1
        Assert.assertEquals(SimpleInputEvent.NOP, tsm.changeState(true)); // third 1
        Assert.assertEquals(SimpleInputEvent.NOP, tsm.changeState(true)); // fourth 1
        Assert.assertEquals(SimpleInputEvent.NOP, tsm.reset());
    }

    @Test public void resettingBetweenClickThresholdAndLongPressThresholdGeneratesNOP2(){
        TranslatorStateMachine tsm = new TranslatorStateMachine();
        TranslatorStateMachine.setThresholds(4, 7);

        Assert.assertEquals(SimpleInputEvent.NOP, tsm.changeState(true)); // first 1
        Assert.assertEquals(SimpleInputEvent.NOP, tsm.changeState(true)); // second 1
        Assert.assertEquals(SimpleInputEvent.NOP, tsm.changeState(true)); // third 1
        Assert.assertEquals(SimpleInputEvent.NOP, tsm.changeState(true)); // fourth 1
        Assert.assertEquals(SimpleInputEvent.NOP, tsm.changeState(true)); // fifth 1
        Assert.assertEquals(SimpleInputEvent.NOP, tsm.changeState(true)); // sixth 1
        Assert.assertEquals(SimpleInputEvent.NOP, tsm.reset());
    }

    @Test public void resettingAfterLongPressThresholdGeneratesLongPressUp1(){
        TranslatorStateMachine tsm = new TranslatorStateMachine();
        TranslatorStateMachine.setThresholds(4, 7);

        Assert.assertEquals(SimpleInputEvent.NOP, tsm.changeState(true)); // first 1
        Assert.assertEquals(SimpleInputEvent.NOP, tsm.changeState(true)); // second 1
        Assert.assertEquals(SimpleInputEvent.NOP, tsm.changeState(true)); // third 1
        Assert.assertEquals(SimpleInputEvent.NOP, tsm.changeState(true)); // fourth 1
        Assert.assertEquals(SimpleInputEvent.NOP, tsm.changeState(true)); // fifth 1
        Assert.assertEquals(SimpleInputEvent.NOP, tsm.changeState(true)); // sixth 1
        Assert.assertEquals(SimpleInputEvent.LONG_PRESS_DOWN, tsm.changeState(true)); // seventh 1
        Assert.assertEquals(SimpleInputEvent.LONG_PRESS_UP, tsm.reset());
    }

    @Test public void resettingAfterLongPressThresholdGeneratesLongPressUp2(){
        TranslatorStateMachine tsm = new TranslatorStateMachine();
        TranslatorStateMachine.setThresholds(4, 7);

        Assert.assertEquals(SimpleInputEvent.NOP, tsm.changeState(true)); // first 1
        Assert.assertEquals(SimpleInputEvent.NOP, tsm.changeState(true)); // second 1
        Assert.assertEquals(SimpleInputEvent.NOP, tsm.changeState(true)); // third 1
        Assert.assertEquals(SimpleInputEvent.NOP, tsm.changeState(true)); // fourth 1
        Assert.assertEquals(SimpleInputEvent.NOP, tsm.changeState(true)); // fifth 1
        Assert.assertEquals(SimpleInputEvent.NOP, tsm.changeState(true)); // sixth 1
        Assert.assertEquals(SimpleInputEvent.LONG_PRESS_DOWN, tsm.changeState(true)); // seventh 1
        Assert.assertEquals(SimpleInputEvent.NOP, tsm.changeState(true));
        Assert.assertEquals(SimpleInputEvent.LONG_PRESS_UP, tsm.reset());
    }

    @Test public void resettingAfterLongPressThresholdGeneratesLongPressUp3(){
        TranslatorStateMachine tsm = new TranslatorStateMachine();
        TranslatorStateMachine.setThresholds(4, 7);

        Assert.assertEquals(SimpleInputEvent.NOP, tsm.changeState(true)); // first 1
        Assert.assertEquals(SimpleInputEvent.NOP, tsm.changeState(true)); // second 1
        Assert.assertEquals(SimpleInputEvent.NOP, tsm.changeState(true)); // third 1
        Assert.assertEquals(SimpleInputEvent.NOP, tsm.changeState(true)); // fourth 1
        Assert.assertEquals(SimpleInputEvent.NOP, tsm.changeState(true)); // fifth 1
        Assert.assertEquals(SimpleInputEvent.NOP, tsm.changeState(true)); // sixth 1
        Assert.assertEquals(SimpleInputEvent.LONG_PRESS_DOWN, tsm.changeState(true)); // seventh 1
        Assert.assertEquals(SimpleInputEvent.NOP, tsm.changeState(true));
        Assert.assertEquals(SimpleInputEvent.NOP, tsm.changeState(true));
        Assert.assertEquals(SimpleInputEvent.LONG_PRESS_UP, tsm.reset());
    }

}

