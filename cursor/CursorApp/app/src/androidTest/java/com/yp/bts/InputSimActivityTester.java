package com.yp.bts;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.support.test.InstrumentationRegistry;
import android.support.test.espresso.UiController;
import android.support.test.espresso.ViewAction;
import android.support.test.espresso.action.GeneralLocation;
import android.support.test.espresso.action.MotionEvents;
import android.support.test.espresso.action.Press;
import android.support.test.runner.AndroidJUnit4;
import android.test.ActivityInstrumentationTestCase2;
import android.test.suitebuilder.annotation.LargeTest;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ToggleButton;

import com.yp.cursor.R;
import com.yp.remoteInputSim.InputSimActivity;
import com.yp.remoteInputSim.InputSimulator;

import org.hamcrest.Matcher;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.Timeout;
import org.junit.runner.RunWith;
import org.mockito.Mockito;

import java.util.HashMap;
import java.util.Map;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.longClick;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayingAtLeast;
import static android.support.test.espresso.matcher.ViewMatchers.withId;

/**
 * @author Oded
 * @version 13/06/2016
 */
@RunWith(AndroidJUnit4.class)
@LargeTest
public class InputSimActivityTester extends ActivityInstrumentationTestCase2<InputSimActivity> {

    private InputSimActivity activity;

    ViewAction hold = LowLevelActions.pressAndHold();
    ViewAction release = LowLevelActions.release();

    public InputSimActivityTester() {
        super(InputSimActivity.class);
    }

    //global timeout rule
//    @Rule public Timeout globalTimeout = Timeout.seconds(5);


    public InputSimulator getStartableMockSimulator() {
        InputSimulator $ = Mockito.mock(InputSimulator.class);

        // set simulator to return the tested device's own name
        Mockito.when($.getRemoteDevName()).thenReturn(BluetoothAdapter.getDefaultAdapter().getName());

        // set on call simulator.initBluetoothConnection() return true
        Mockito.when($.initBluetoothConnection()).thenReturn(true);

        // set on call simulator.connect() return true
        Mockito.when($.connect()).thenReturn(true);

        // set on call simulator.resumeSending() do nothing
        Mockito.doNothing().when($).resumeSending();

        // set on call simulator.pauseSending() do nothing
        Mockito.doNothing().when($).pauseSending();

        return $;
    }


    @Override
    @Before
    public void setUp() throws Exception {
        super.setUp();

        injectInstrumentation(InstrumentationRegistry.getInstrumentation());
        setActivityIntent(new Intent().putExtra(BluetoothDevice.EXTRA_DEVICE, selfAddress()));
        activity = getActivity();
    }

    private String selfAddress() {
        return BluetoothAdapter.getDefaultAdapter().getAddress();
    }


    @After
    public void cleanup() {
        LowLevelActions.tearDown();
    }

    public void clickButton(int id) {
        onView(withId(id)).perform(click());
    }

    static int[] btnIds = {
            R.id.tbtn_input1,
            R.id.tbtn_input2,
            R.id.tbtn_input3,
            R.id.tbtn_input4,
            R.id.tbtn_input5,
            R.id.tbtn_input6,
            R.id.tbtn_input7,
            R.id.tbtn_input8,
    };

    static int[] fabIds = {
            R.id.fbtn1,
            R.id.fbtn2,
            R.id.fbtn3,
            R.id.fbtn4,
    };

    private void clickButtonAtIndex(int i) {
        clickButton(btnIds[i]);
    }

    private void holdButtonAtIndex(int i) {
        onView(withId(btnIds[i])).perform(hold);
    }

    private void releaseButtonAtIndex(int i) {
        onView(withId(btnIds[i])).perform(release);
    }

    private void toggleStartButton() {
        onView(withId(R.id.tbtn_toggleSending)).perform(click());
    }

    @Test
    public void sanityCheck() {
        onView(withId(R.id.tbtn_input1)).perform(longClick());
    }

    @Test
    public void eachButtonChangesMatchingOutputIndex() {
        // go over all buttons, hold each down, validate correct bit was set and release it
        for (int i = 0; i < btnIds.length; ++i) {
            holdButtonAtIndex(i);
            assertEquals((byte) (1 << i), activity.getSingleLevelOutput());
            releaseButtonAtIndex(i);
        }
    }

    @Test
    public void generatesCorrectOutputForSending() {
        // "press" a combo and make sure generated output is returned by getSingleLevelOutput
        int[] holdIndices = {1, 3, 5};
        byte expectedOutput = (byte) 0;
        for (int i : holdIndices) {
            holdButtonAtIndex(i);
            expectedOutput |= (1 << i);
        }

        assertEquals(expectedOutput, activity.getSingleLevelOutput());
    }

    @Test
    public void pauseAndResumeChangeStatusText() throws InterruptedException {
        InputSimulator sim = getStartableMockSimulator();
        activity.setSimulator(sim);
        ToggleButton startBtn = (ToggleButton) activity.findViewById(R.id.tbtn_toggleSending);

        final String pause = activity.getString(R.string.label_pause_sending);
        final String resume = activity.getString(R.string.label_resume_sending);

        toggleStartButton(); // start sending
        assertEquals(pause, startBtn.getText());

        toggleStartButton(); // pause
        Thread.sleep(100L);
        assertEquals(resume, startBtn.getText());

        toggleStartButton(); // resume
        Thread.sleep(100L);
        assertEquals(pause, startBtn.getText());

        toggleStartButton(); // pause
        Thread.sleep(100L);
        assertEquals(resume, startBtn.getText());

        toggleStartButton(); // resume
        Thread.sleep(100L);
        assertEquals(pause, startBtn.getText());
    }

    @Test
    public void outputDoesntChangeWhenPaused() throws InterruptedException {
        InputSimulator sim = getStartableMockSimulator();
        activity.setSimulator(sim);
        toggleStartButton(); // start sending
        toggleStartButton(); // pause sending

        final String before = activity.getSimOutput();
        holdButtonAtIndex(1);
        Thread.sleep(activity.getSendInterval()); // wait for at least one send cycle
        final String after = activity.getSimOutput();

        assertNotSame("Should not have changed output while paused", before, after);
    }

    @Test
    public void specializedFabButtonsOnlyClickAndStops() throws InterruptedException {
        InputSimulator sim = getStartableMockSimulator();
        activity.setSimulator(sim);

        toggleStartButton(); // start
        Thread.sleep(50L);

        for (int i = 0; i < fabIds.length; ++i) {
            clickButton(fabIds[i]);
            Thread.sleep(2 * activity.getClickLength());
            assertEquals("Value should have returned to 0", (byte) 0, activity.getSingleLevelOutput());
        }
    }
}

class LowLevelActions {
    static Map<View, MotionEvent> heldViews = new HashMap<>();

    public static PressAndHoldAction pressAndHold() {
        return new PressAndHoldAction();
    }

    public static ReleaseAction release() {
        return new ReleaseAction();
    }

    public static void tearDown() {
        heldViews.clear();
    }

    static class PressAndHoldAction implements ViewAction {

        @Override
        public Matcher<View> getConstraints() {
            return isDisplayingAtLeast(90); // Like GeneralClickAction
        }

        @Override
        public String getDescription() {
            return "Press and hold action";
        }

        @Override
        public void perform(final UiController uiController, final View view) {
            float[] precision = Press.FINGER.describePrecision();
            float[] coords = GeneralLocation.CENTER.calculateCoordinates(view);
            heldViews.put(view, MotionEvents.sendDown(uiController, coords, precision).down);
        }
    }

    static class ReleaseAction implements ViewAction {
        @Override
        public Matcher<View> getConstraints() {
            return isDisplayingAtLeast(90);  // Like GeneralClickAction
        }

        @Override
        public String getDescription() {
            return "Release action";
        }

        @Override
        public void perform(final UiController uiController, final View view) {
            MotionEvent event = heldViews.get(view);
            if (event == null) {
                throw new AssertionError("Before calling release(), you must call pressAndHold() on a view");
            }

            float[] coords = GeneralLocation.CENTER.calculateCoordinates(view);
            MotionEvents.sendUp(uiController, event, coords);
        }
    }
}
