package com.yp;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Point;
import android.support.test.runner.AndroidJUnit4;

import com.yp.classes.Painter;
import com.yp.classes.PainterCursor;

import junit.framework.TestCase;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import static junit.framework.Assert.assertTrue;

/**
 * @author Inbar Donag
 */
@RunWith(AndroidJUnit4.class)
public class PainterTest
{
    Painter painter;
    Bitmap output;
    Canvas canvas;
    PainterCursor cursor;
    Bitmap cursorBitmap;
    static final int canvasSize = 100;



    @Before
    public void setUp()
    {
        painter = new Painter(canvasSize,canvasSize);
        output = Bitmap.createBitmap(canvasSize, canvasSize, Bitmap.Config.ARGB_8888);
        canvas = new Canvas(output);
        cursorBitmap = Bitmap.createBitmap(30, 30, Bitmap.Config.ARGB_8888);
        cursor = new PainterCursor(cursorBitmap, 0, canvasSize, canvasSize);
        cursor.setCursorPos(new Point(5,5));
    }

    @Test
    public void testCursor()
    {
        // starting from (5,5) with curr direction: RIGHT
        cursor.turnLeft();
        assertTrue(cursor.getCurrentDirection() == PainterCursor.UPPER_RIGHT);
        cursor.advanceCursor();
        assertTrue(cursor.getCursorPos().x == 6);
        assertTrue(cursor.getCursorPos().y == 4);
        cursor.turnLeft();
        assertTrue(cursor.getCurrentDirection() == PainterCursor.UP);
        cursor.turnLeft();
        assertTrue(cursor.getCurrentDirection() == PainterCursor.UPPER_LEFT);
        cursor.turnLeft();
        assertTrue(cursor.getCurrentDirection() == PainterCursor.LEFT);
        cursor.advanceCursor();
        assertTrue(cursor.getCursorPos().x == 5);
        assertTrue(cursor.getCursorPos().y == 4);
        cursor.turnRight();
        assertTrue(cursor.getCurrentDirection() == PainterCursor.UPPER_LEFT);
        cursor.advanceCursor();
        assertTrue(cursor.getCursorPos().x == 4);
        assertTrue(cursor.getCursorPos().y == 3);
        cursor.turnRight();
        assertTrue(cursor.getCurrentDirection() == PainterCursor.UP);
        cursor.advanceCursor();
        assertTrue(cursor.getCursorPos().x == 4);
        assertTrue(cursor.getCursorPos().y == 2);
        cursor.turnLeft();
        assertTrue(cursor.getCurrentDirection() == PainterCursor.UPPER_LEFT);
        cursor.turnLeft();
        assertTrue(cursor.getCurrentDirection() == PainterCursor.LEFT);
        cursor.advanceCursor();
        cursor.advanceCursor();
        assertTrue(cursor.getCursorPos().x == 2);
        assertTrue(cursor.getCursorPos().y == 2);
    }

    @Test
    public void testBrush()
    {
        assertTrue(painter.getCurrColor() == Color.BLACK);

        // draw a diagonal
        painter.startNewStroke(new Point(0,0));
        painter.advanceStroke(new Point(10,10));

        painter.render(canvas);

        for (int i = 0 ; i <= 10; i++)
            assertTrue(output.getPixel(i,i) == Color.BLACK);

        assertTrue(output.getPixel(0,70) == Color.WHITE);
        assertTrue(output.getPixel(2,64) == Color.WHITE);
        assertTrue(output.getPixel(36,26) == Color.WHITE);
        assertTrue(output.getPixel(12,12) == Color.WHITE);
        assertTrue(output.getPixel(13,0) == Color.WHITE);

    }

    @Test
    public void testEraser()
    {
        // draw a diagonal
        painter.startNewStroke(new Point(0,0));
        painter.advanceStroke(new Point(10,10));

        // erase the diagonal
        painter.startEraser(new Point(0,0));
        painter.advanceStroke(new Point(10,10));

        painter.render(canvas);

        for (int i = 0 ; i <= 10; i++)
            assertTrue(output.getPixel(i,i) == Color.WHITE);
    }

    @Test
    public void testUndoRedo()
    {
        // draw 2 strokes
        painter.startNewStroke(new Point(0,0));
        painter.advanceStroke(new Point(5,5));
        painter.startNewStroke(new Point(7,7));
        painter.advanceStroke(new Point(10,10));

        painter.undo();
        painter.render(canvas);

        for (int i = 0 ; i <= 5; i++)
            assertTrue(output.getPixel(i,i) == Color.BLACK);
        for (int i = 7 ; i <= 10; i++)
            assertTrue(output.getPixel(i,i) == Color.WHITE);

        painter.redo();
        painter.render(canvas);

        for (int i = 0 ; i <= 5; i++)
            assertTrue(output.getPixel(i,i) == Color.BLACK);
        for (int i = 7 ; i <= 10; i++)
            assertTrue(output.getPixel(i,i) == Color.BLACK);

        painter.undo();
        painter.undo();
        painter.render(canvas);

        for (int i = 0 ; i <= 5; i++)
            assertTrue(output.getPixel(i,i) == Color.WHITE);
        for (int i = 7 ; i <= 10; i++)
            assertTrue(output.getPixel(i,i) == Color.WHITE);

        painter.redo();
        painter.redo();
        painter.render(canvas);

        for (int i = 0 ; i <= 5; i++)
            assertTrue(output.getPixel(i,i) == Color.BLACK);
        for (int i = 7 ; i <= 10; i++)
            assertTrue(output.getPixel(i,i) == Color.BLACK);
    }

    @Test
    public void testColors()
    {
        painter.setColor(Color.BLUE);
        painter.startNewStroke(new Point(0,0));
        painter.advanceStroke(new Point(10,10));
        painter.render(canvas);

        for (int i = 0 ; i <= 10; i++)
            assertTrue(output.getPixel(i,i) == Color.BLUE);

        painter.setColor(Color.WHITE);
        painter.startNewStroke(new Point(0,0));
        painter.advanceStroke(new Point(10,10));
        painter.render(canvas);

        for (int i = 0 ; i <= 10; i++)
            assertTrue(output.getPixel(i,i) == Color.WHITE);

    }

    @Test
    public void testShowBitmap()
    {
        Bitmap tempBitmap = Bitmap.createBitmap(canvasSize, canvasSize, Bitmap.Config.ARGB_8888);
        Canvas tempCanvas = new Canvas(tempBitmap);
        painter.startNewStroke(new Point(0,0));
        painter.advanceStroke(new Point(10,10));
        painter.render(tempCanvas);

        painter.showBitmap(tempBitmap);
        painter.render(canvas);

        assertTrue(output.sameAs(tempBitmap));

    }

}
