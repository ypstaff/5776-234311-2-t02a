package com.yp.cursor;

import android.graphics.Rect;

import com.yp.cursor.onebutton.Background;
import com.yp.cursor.onebutton.AxisStateMachine;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import static com.yp.cursor.onebutton.AxisStateMachine.SingleState.DONE;
import static com.yp.cursor.onebutton.AxisStateMachine.SingleState.SET_X;
import static com.yp.cursor.onebutton.AxisStateMachine.SingleState.SET_Y;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.*;
/**
 * Created by avner on 13/04/2016.
 */
@RunWith(MockitoJUnitRunner.class)
public class AxisStateMachineTest {

    AxisStateMachine sm;
    private final Rect canvasBounds = new Rect(1,1,1,1);;

    @Before
    public void setUp() {
        sm = new AxisStateMachine(SET_X);
    }

    @Test
    public void testInitialState() {
        assertEquals(SET_X, sm.currentState());
    }

    @Test
    public void testAdvanceState() {
        sm.advanceState();
        assertEquals(SET_Y, sm.currentState());
        sm.advanceState();
        assertEquals(DONE, sm.currentState());
        sm.advanceState();
        assertEquals(SET_X, sm.currentState());
    }

    @Test
    public void testDoStepSetX() {
        Background bgMock = Mockito.mock(Background.class);
        sm.setCanvasBounds(canvasBounds);
        sm.setBackground(bgMock);
        sm.doStep();
        verify(bgMock, times(1)).moveHorizontalAxis(canvasBounds.width());
    }

    @Test
    public void testDoStepSetY() {
        sm = new AxisStateMachine(SET_Y);
        Background bgMock = Mockito.mock(Background.class);
        sm.setCanvasBounds(canvasBounds);
        sm.setBackground(bgMock);
        sm.doStep();
        verify(bgMock, times(1)).moveVerticalAxis(canvasBounds.height());
    }

    @Test
    public void testDoStepSetDone() {
        sm = new AxisStateMachine(DONE);
        Background bgMock = Mockito.mock(Background.class);
        sm.setCanvasBounds(canvasBounds);
        sm.setBackground(bgMock);
        assertTrue(sm.currentState().shouldExecute());
        sm.doStep();
        verify(bgMock, times(1)).putMarker();
        verify(bgMock, times(1)).forwardTouchEvent();
        verify(bgMock, times(1)).resetAxisLocation();
        assertFalse(sm.currentState().shouldExecute());
    }
}
