package com.yp.cursor.facedetect;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PointF;

import com.yp.classes.Constants;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
/**
 * @author Avner Elizarov
 */
@RunWith(MockitoJUnitRunner.class)
public class EyeStateMachineTest {

    EyeStateMachine eyeSM;
    private PointF cursorLocation;
    private Canvas canvas;
    private Paint paint;

    @Before
    public void setUp() {
        paint = Mockito.mock(Paint.class);
        canvas = Mockito.mock(Canvas.class);
        cursorLocation = new PointF(10,10);
        eyeSM = new EyeStateMachine(EyeStateMachine.Eye.LEFT, paint, paint);
    }

    @Test
    public void testInitialState() {
        assertEquals(EyeStateMachine.IdleState.class, eyeSM.getCurrentState().getClass());
    }

    @Test
    public void shouldMoveToStartedBlinkStateWhenEyeIsClosed() {
        sendClosedEyeProbability();
        assertEquals(EyeStateMachine.StartedBlinkState.class, eyeSM.getCurrentState().getClass());

    }

    @Test
    public void shouldMoveToClickStateWhenEyeWasClosedLongEnough() {
        moveToClickState();
        assertEquals(EyeStateMachine.ClickState.class, eyeSM.getCurrentState().getClass());

    }

    @Test
    public void shouldReturnToIdleStateIfEyeIsOpenDuringStartBlinkState() {
        // move to start blink state
        sendClosedEyeProbability();
        // should move back to idle
        sendOpenEyeProbability();
        assertEquals(EyeStateMachine.IdleState.class, eyeSM.getCurrentState().getClass());

    }

    @Test
    public void shouldStayInStartBlinkStateIfEyeIsntOpenOrClosedDuringStartBlinkState() {
        // move to start blink state
        sendClosedEyeProbability();
        // should move back to idle
        sendNotOpenAndNotClosedEyeProbability();
        assertEquals(EyeStateMachine.StartedBlinkState.class, eyeSM.getCurrentState().getClass());

    }

    @Test
    public void shouldStayInClickStateUntilEyeIsOpen() {
        moveToClickState();
        sendClosedEyeProbability();
        assertEquals(EyeStateMachine.ClickState.class, eyeSM.getCurrentState().getClass());
        sendOpenEyeProbability();
        assertEquals(EyeStateMachine.IdleState.class, eyeSM.getCurrentState().getClass());
    }

    @Test
    public void shouldDrawToCanvasWhenClickIsMade() {
        moveToClickState();
        Mockito.verify(canvas, Mockito.times(1)).drawCircle(cursorLocation.x, cursorLocation.y, EyeStateMachine.MARKER_RADIUS, paint);
    }

    @Test
    public void shouldDrawToCanvasWhenClickIsMadeForInitialCursorLocation() {
        moveToClickStateWithDifferentCursor();
        Mockito.verify(canvas, Mockito.times(1)).drawCircle(cursorLocation.x, cursorLocation.y, EyeStateMachine.MARKER_RADIUS, paint);
    }

    private void moveToClickStateWithDifferentCursor() {
        for(int i=0 ; i< Constants.FaceCursor.DEFAULT_EYE_BLINK_THRESHOLD + 1; i++){
            PointF newCursorLocation = new PointF(cursorLocation.x + i, cursorLocation.y + i);
            sendClosedEyeProbability(newCursorLocation);
        }
    }


    private void sendOpenEyeProbability() {
        eyeSM.onNewProbability(EyeStateMachine.EyeState.OPEN_THRESHOLD + 0.1f, cursorLocation, canvas);
    }

    private void sendClosedEyeProbability() {
        eyeSM.onNewProbability(EyeStateMachine.EyeState.CLOSED_THRESHOLD - 0.1f, cursorLocation, canvas);
    }

    private void sendClosedEyeProbability(PointF cursorLocation) {
        eyeSM.onNewProbability(EyeStateMachine.EyeState.CLOSED_THRESHOLD - 0.1f, cursorLocation, canvas);
    }

    private void sendNotOpenAndNotClosedEyeProbability() {
        eyeSM.onNewProbability(EyeStateMachine.EyeState.CLOSED_THRESHOLD + 0.1f, cursorLocation, canvas);
    }

    private void moveToClickState() {
        for(int i=0 ; i< Constants.FaceCursor.DEFAULT_EYE_BLINK_THRESHOLD + 1; i++){
            sendClosedEyeProbability();
        }
    }


}
