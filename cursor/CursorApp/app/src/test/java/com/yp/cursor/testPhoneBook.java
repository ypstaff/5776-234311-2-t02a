package com.yp.cursor;

import com.yp.classes.PhoneBook;

import org.junit.Test;

/**
 * @author Or Mauda
 *
 * This test validates that the parameters of a contact in the PhoneBook are legal.
 */
public class testPhoneBook {

    @Test(expected = IllegalArgumentException.class)
    public void mustHaveAStrudelInEmailAddress() throws Exception {
        PhoneBook contact = new PhoneBook("Or", "0123456789", "a");
    }

    @Test(expected = NullPointerException.class)
    public void nameMustNotBeNull() throws Exception {
        PhoneBook contact = new PhoneBook(null, "0123456789", "a@v");
    }
}
