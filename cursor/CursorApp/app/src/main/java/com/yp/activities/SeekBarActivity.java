package com.yp.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.yp.cursor.R;

/**
 * @author Or Mauda
 */
public class SeekBarActivity extends SideMenuActivity {

    private TextView seekTxt;
    private SeekBar seekBar;

    public enum Type { // the type of the seek bar
        OPACITY("opacity", 255) {
            public String getText(int progress, int barMax) {
                return Integer.toString(progress) + "%";
            }
        },
        SIZE("size", 50) {
            public String getText(int progress, int barMax) {
                return Integer.toString(progress) + "p";
            }
        },
        SPEED("speed", 100) {
            private int progressToValue(int progress, int barMax) {
                return getMaxValue() -
                        Math.round((float) progress / barMax * getMaxValue());
            }

            public int calcReturnValue(SeekBar seekBar, int barMax) {
                return progressToValue(seekBar.getProgress(), barMax);
            }

            String[] text= {"", "quite slow", "quite fast", "fast"};
            public String getText(int progress, int barMax) {
                int speed = progress;
                if (progress == 0)
                    speed = 1;
                return speed + "%";
            }

            public int getInitialProgress(int initialValue, final int barMax) {
                return (int)((double) progressToValue(initialValue, barMax) / getMaxValue() * barMax);
            }
        };

        private final String name;
        private final int maxVal; // the real value when the seek bar is at 100%


        Type(String name, int maxVal) {
            this.name = name;
            this.maxVal = maxVal;
        }

        public String getName() {return name;}
        public int getMaxValue() {return maxVal;}

        public int getInitialProgress(int initialValue, final int barMax) {
            return (int)((double) initialValue / maxVal * barMax);
        }

        /**
         * default method for calculating the returned value
         */
        public int calcReturnValue(SeekBar seekBar, int barMax) {
            return Math.round((float) seekBar.getProgress() / barMax * getMaxValue());
        }
        public abstract String getText(int progress, int barMax);
    }

    private Type type;

    private final int barMax = 100;
    private final int barMin = 0;
    private final int barStep = 10; // the amount of %'s to step in each click

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_seek_bar);

        Bundle b = getIntent().getExtras();
        String name = "";
        int initialValue = barMax;
        if (b != null) {
            name = b.getString("type");
            initialValue = b.getInt("initial_value");
        }
        if (name.equals(Type.OPACITY.getName())) {
            type = Type.OPACITY;
        } else if (name.equals(Type.SIZE.getName())) {
            type = Type.SIZE;
        } else if (name.equals(Type.SPEED.getName())) {
            type = Type.SPEED;
        }

        seekTxt = (TextView)findViewById(R.id.opq_txt);
        seekBar = (SeekBar)findViewById(R.id.seekBar);

        seekBar.setMax(barMax);

        Toast.makeText(this, Integer.toString(initialValue), Toast.LENGTH_SHORT).show();
        seekBar.setProgress(type.getInitialProgress(initialValue, barMax));
        seekTxt.setText(type.getText(seekBar.getProgress(), barMax));

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                seekTxt.setText(type.getText(progress, barMax));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });

    }

    @Override
    public void onUpClick(View v) {
        int currVal = seekBar.getProgress();
        if (currVal == barMax)
            return;
        currVal+= barStep;
        seekBar.setProgress(currVal);
    }

    @Override
    public void onDownClick(View v) {
        int currVal = seekBar.getProgress();
        if (currVal == barMin) return;
        currVal-= barStep;
        seekBar.setProgress(currVal);
    }

    @Override
    public void onSelectClick(View v) {
        Intent i = new Intent();
        i.putExtra(type.getName(), type.calcReturnValue(seekBar, barMax));
        setResult(RESULT_OK, i);
        finish();
    }

}
