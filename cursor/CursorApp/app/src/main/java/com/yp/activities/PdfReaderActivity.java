package com.yp.activities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.pdf.PdfRenderer;
import android.net.Uri;
import android.os.ParcelFileDescriptor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.Toast;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.tool.xml.ElementList;
import com.itextpdf.tool.xml.XMLWorkerHelper;
import com.yp.cursor.R;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import static android.os.Environment.getExternalStorageDirectory;

public class PdfReaderActivity extends AppCompatActivity {

    private ImageView iv;
    private String strPath;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pdf_reader);

        iv = (ImageView) findViewById(R.id.iv_pdfViewer);
        strPath = getExternalFilesDir(null) + "/test.pdf";

        pdfCreate();
//        try {
//            cssCreatePDF(getFilesDir() + "/test.pdf");
//        } catch (IOException e) {
//            e.printStackTrace();
//        } catch (DocumentException e) {
//            e.printStackTrace();
//        }
        sendPDF();
        displayPDF();

    }

    private void sendPDF() {
        Intent shareIntent = new Intent(Intent.ACTION_SEND);
        shareIntent.setType("application/pdf");
        String emailString = ""; // change to desired email;
        shareIntent.putExtra(Intent.EXTRA_EMAIL, new String[] { emailString }); //TODO
        shareIntent.putExtra(Intent.EXTRA_SUBJECT, "3Boom");
        shareIntent.putExtra(Intent.EXTRA_TEXT, "test");
        shareIntent.putExtra(Intent.EXTRA_STREAM, Uri.parse("file://" + strPath));
        startActivity(shareIntent);
    }

    public void pdfCreate() {
        try {
            String k = "<html><body> This is <span style=\"font-size: 30px;\">big</span> <span style=\"color: red;\">red</span> Project </body></html>";
            Toast.makeText(PdfReaderActivity.this, strPath, Toast.LENGTH_LONG).show();
            Log.d("PATHTAG", strPath);
            OutputStream file = new FileOutputStream(new File(strPath));
            Document document = new Document();
            PdfWriter writer = PdfWriter.getInstance(document, file);
            document.open();
            InputStream is = new ByteArrayInputStream(k.getBytes());
            XMLWorkerHelper.getInstance().parseXHtml(writer, document, is);
            document.close();
            file.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //POC
    private void displayPDF() {
        // filePath represent path of Pdf document on storage
        File file = new File(strPath);
// FileDescriptor for file, it allows you to close file when you are
// done with it
        ParcelFileDescriptor mFileDescriptor = null;
        try {
            mFileDescriptor = ParcelFileDescriptor.open(file,
                    ParcelFileDescriptor.MODE_READ_ONLY);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
// PdfRenderer enables rendering a PDF document
        PdfRenderer mPdfRenderer = null;
        try {
            mPdfRenderer = new PdfRenderer(mFileDescriptor);
        } catch (IOException e) {
            e.printStackTrace();
        }

// Open page with specified index
        PdfRenderer.Page mCurrentPage = mPdfRenderer.openPage(0);
        Bitmap bitmap = Bitmap.createBitmap(mCurrentPage.getWidth(),
                mCurrentPage.getHeight(), Bitmap.Config.ARGB_8888);

// Pdf page is rendered on Bitmap
        mCurrentPage.render(bitmap, null, null,
                PdfRenderer.Page.RENDER_MODE_FOR_DISPLAY);
// Set rendered bitmap to ImageView (pdfView in my case)
        iv.setImageBitmap(bitmap);

        mCurrentPage.close();
        mPdfRenderer.close();
        try {
            mFileDescriptor.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
