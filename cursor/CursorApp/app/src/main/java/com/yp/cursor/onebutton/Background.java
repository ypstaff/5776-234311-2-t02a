package com.yp.cursor.onebutton;

import android.app.Instrumentation;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Looper;
import android.os.SystemClock;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Toast;

// Avner wrote forwardTouchEvent()
/**
 * Created by ItayHazan on 09/11/2015.
 *
 * @author Itay
 */
public class Background extends View {

    private static final int MIN_DELTA = 1;
    private static final int MAX_DELTA = 10;
    private Paint lightGray = null;
    private Paint darkGray = null;
    private Paint blue = null;
    private float movementDelta = 10f;

    private float y = 0f;
    private float x = 0f;
    private final float axisWidth = 15f;
    private final float markerRadius = 10f;

    private float lastCursorXPosition = -axisWidth;
    private float lastCursorYPosition = -axisWidth;


    /**
     * Constructor.
     *
     * @param context
     */
    public Background(Context context) {
        super(context);

        lightGray = new Paint();
        lightGray.setColor(Color.rgb(180, 180, 180));
        darkGray = new Paint();
        darkGray.setColor(Color.rgb(120, 120, 120));
        blue = new Paint();
        blue.setColor(Color.rgb(0, 50, 150));
    }

    /**
     * Effectively set the movement speed of the axes.
     *
     * @param delta value in range [1,10], 10 is the fastest. Values out of range are ignored.
     */
    public void setDelta(int delta) {
        if (delta < MIN_DELTA || delta > MAX_DELTA) return;
        
        this.movementDelta = delta;
    }


    public void resetAxisLocation() {
        y = x = 0;
    }

    private float verticalFactor = 1.0f;
    public void moveVerticalAxis(int height) {
        final float dy = movementDelta * verticalFactor;
        y += dy;

        if (y + axisWidth > height || y < 0) {
            verticalFactor *= -1f;
            y -= (2f * dy);
        }
    }

    private float horizontalFactor = 1.0f;
    public void moveHorizontalAxis(int width) {
        final float dx = movementDelta * horizontalFactor;
        x += dx;

        if (x + axisWidth > width || x < 0) {
            horizontalFactor *= -1f;
            x -= (2f * dx);
        }
    }

    public void drawToCanvas(Canvas canvas) {
        canvas.drawColor(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);
        //              l  t  r           b
        canvas.drawRect(0, y, canvas.getWidth(), y + axisWidth, lightGray); // horizontal
        canvas.drawRect(x, 0, x + axisWidth, canvas.getHeight(), lightGray); // vertical
        canvas.drawRect(x, y, x + axisWidth, y + axisWidth, darkGray); // dark intersection

        canvas.drawCircle(lastCursorXPosition, lastCursorYPosition, markerRadius, blue); // marker
    }


    public void putMarker() {
        lastCursorXPosition = x;
        lastCursorYPosition = y;
    }

    // Avner
    public void forwardTouchEvent() {
        lastCursorXPosition = x;
        lastCursorYPosition = y;

            new AsyncTask<Void, Void, Void>(){

                @Override
                protected Void doInBackground(Void... params) {
                    Instrumentation instrumentation = new Instrumentation();
                    try{

                        instrumentation.sendPointerSync(MotionEvent.obtain(
                                SystemClock.uptimeMillis(),
                                SystemClock.uptimeMillis(),
                                MotionEvent.ACTION_DOWN, lastCursorXPosition, lastCursorYPosition, 0));

                        instrumentation.sendPointerSync(MotionEvent.obtain(
                                SystemClock.uptimeMillis(),
                                SystemClock.uptimeMillis(),
                                MotionEvent.ACTION_UP, lastCursorXPosition, lastCursorYPosition, 0));
                        Log.d("MarkerService", "performed click in y:" + y);
                    }catch(Exception e){
                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(getContext(),"can't send key event to other apps",Toast.LENGTH_SHORT).show();
                            }
                        });

                    }
                    return null;
                }
            }.execute();
    }
}
