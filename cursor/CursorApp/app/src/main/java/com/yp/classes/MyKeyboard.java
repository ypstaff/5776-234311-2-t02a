package com.yp.classes;

import android.inputmethodservice.InputMethodService;
import android.inputmethodservice.Keyboard;
import android.inputmethodservice.KeyboardView;
import android.os.Handler;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputConnection;

import com.yp.cursor.CursorApplication;
import com.yp.cursor.R;
import com.yp.cursor.SingleInput;

import java.util.List;


/**
 * Created by Matan Revivo on 26/12/2015.
 */
public class MyKeyboard extends InputMethodService
        implements KeyboardView.OnKeyboardActionListener {

    private static final String LOG_TAG = "KEYBOARD_TAG";
    public static final int GREEN_CODE = -11;
    public static final int RED_CODE = -22;
    public static final int BLUE_CODE = -33;
    public static final int TO_NUMERIC = -44;
    public static final int TO_ALPHABET = -55;
    public static final int HIDE_CODE = -100;
    public static final int COMPLETE_FIRST = -444;
    public static final int COMPLETE_SECOND = -555;
    public static final int COMPLETE_THIRD = -666;
    private static final int NUMBER_OF_INPUTS = 3;
    public static final int MAX_WORD_LENGTH = 20;


    private MyKeyboardView m_keyboardView;
    private Keyboard m_keyboard;
    private Keyboard m_numericKeyboard;
    private boolean caps = false;
    private Handler handler;

    private SingleInput[] oldInputs;
    private int[] buttonCodes = new int[]{GREEN_CODE, RED_CODE, BLUE_CODE};

    @Override
    public View onCreateInputView() {
        m_keyboardView = (MyKeyboardView) getLayoutInflater().inflate(R.layout.keyboard, null);
        m_keyboard = new Keyboard(this, R.xml.qwerty);
        m_numericKeyboard = new Keyboard(this, R.xml.numkey);
        m_keyboardView.setKeyboard(m_keyboard);
        m_keyboardView.setOnKeyboardActionListener(this);
        m_keyboardView.resetKeys();

        return m_keyboardView;
    }

    @Override
    public void onPress(int primaryCode) {

    }

    @Override
    public void onRelease(int primaryCode) {

    }

    @Override
    public void onKey(int primaryCode, int[] keyCodes) {
        Log.v(LOG_TAG, "primary code: " + primaryCode);
        //direct click on completion buttons
        switch(primaryCode) {
            case COMPLETE_FIRST:
                completeAt(0);
                return;
            case COMPLETE_SECOND:
                completeAt(1);
                return;
            case COMPLETE_THIRD:
                completeAt(2);
                return;
            default:
                break;
        }

        if (m_keyboardView.isAutoComplete())
            autoCompleteMode(primaryCode);
        else
            normalMode(primaryCode);
        m_keyboardView.invalidateAllKeys();
    }

    private void completeAt(int location) {
        final CharSequence label = m_keyboardView.getKeyboard().getKeys().get(location).label;
        if(label == null){
            commitWord("");
        }else{
            commitWord(String.valueOf(label));
        }
    }

    private void autoCompleteMode(int primaryCode){
        switch (primaryCode) {
            case HIDE_CODE:
                InputConnection ic = getCurrentInputConnection();
                ic.performEditorAction(EditorInfo.IME_ACTION_DONE);
                break;
            case GREEN_CODE:
                m_keyboardView.stopAutoComplete();
                break;
            case RED_CODE: // the button will actually be yellow if we are on completion mode
                m_keyboardView.completionNextWord();
                break;
            case BLUE_CODE:
                commitWord(m_keyboardView.getCompletionForCommit());
                break;
            default:
                keyCommit(primaryCode);
        }
    }

    void commitWord(String word){

        if(word.isEmpty()){
            return;
        }

        InputConnection ic = getCurrentInputConnection();
        CharSequence input =  ic.getTextBeforeCursor(MAX_WORD_LENGTH, 0); // read last characters from input to find last word
        String strInput = (input != null) ? input.toString() : "";

        int start = strInput.length();
        while (start > 0 && !Character.isWhitespace(strInput.charAt(start - 1))) start--;

        String lastWord = strInput.substring(start);
        if(! lastWord.isEmpty() && word.startsWith(lastWord) ){ // in the middle of a word
            word = word.substring(lastWord.length());
        }

        for (int i = 0; i < word.length(); ++i){
            int code = word.codePointAt(i);
            keyCommit(code);
        }
        keyCommit(32); //committing space character
        m_keyboardView.invalidateAllKeys();
    }

    private void normalMode(int primaryCode) {
        boolean bCommit = false;
        switch (primaryCode) {
            case HIDE_CODE:
                InputConnection ic = getCurrentInputConnection();
                ic.performEditorAction(EditorInfo.IME_ACTION_DONE);
                break;
            case GREEN_CODE:
                if (m_keyboardView.isReset())
                    m_keyboardView.startAutoComplete();
                else
                    m_keyboardView.resetKeys();
                break;
            case RED_CODE:
                bCommit = m_keyboardView.chooseColor(RED_CODE);
                break;
            case BLUE_CODE:
                bCommit = m_keyboardView.chooseColor(BLUE_CODE);
                break;
            default:
                keyCommit(primaryCode);
                return;
        }
        if (bCommit) {
            keyCommit(m_keyboardView.getCodeToCommit());
            m_keyboardView.resetKeys();
        } else {
            m_keyboardView.updateColor();
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        return super.onKeyUp(keyCode, event);
    }

    /**
     * simulates a normal key press after a key was chose through our 3 button input method
     *
     * @param primaryCode the code of key being committed
     */
    private void keyCommit(int primaryCode) {
        Log.v(LOG_TAG, String.valueOf(primaryCode));
        InputConnection ic = getCurrentInputConnection();
        switch (primaryCode) {
            case Keyboard.KEYCODE_DELETE:
                ic.deleteSurroundingText(1, 0);
                break;
            case Keyboard.KEYCODE_SHIFT:
                caps = !caps;
                m_keyboard.setShifted(caps);
                m_keyboardView.invalidateAllKeys();
                break;
            case Keyboard.KEYCODE_DONE:
                ic.sendKeyEvent(new KeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_ENTER));
                ic.sendKeyEvent(new KeyEvent(KeyEvent.ACTION_UP, KeyEvent.KEYCODE_ENTER));
                break;
            case TO_NUMERIC:
                m_keyboardView.setKeyboard(m_numericKeyboard);
                m_keyboardView.invalidateAllKeys();
                break;
            case TO_ALPHABET:
                m_keyboardView.setKeyboard(m_keyboard);
                m_keyboardView.invalidateAllKeys();
                break;
            default:
                char code = (char) primaryCode;
                if (Character.isLetter(code) && caps) {
                    code = Character.toUpperCase(code);
                }
                ic.commitText(String.valueOf(code), 1);
        }
        updateSuggestions();

    }

    private void updateSuggestions() {
        InputConnection ic = getCurrentInputConnection();
        CharSequence input =  ic.getTextBeforeCursor(100, 0); // read last 100 characters of the input
        String strInput = (input != null) ? input.toString() : "";
        String[] suggestions = ((CursorApplication) getApplication()).getNgramDatabaseSuggester().suggest(strInput);
        Log.v("MY_KEYBOARD", "keyCommit: suggestions are: {" + suggestions[0] + ", " + suggestions[1] + ", " + suggestions[2] + "}");
        List<Keyboard.Key> allKeys = m_keyboardView.getKeyboard().getKeys();
        for (int i=0; i<3; i++)
            allKeys.get(i).label = suggestions[i];
    }

    @Override
    public void onText(CharSequence text) {

    }

    @Override
    public void swipeLeft() {

    }

    @Override
    public void swipeRight() {

    }

    @Override
    public void swipeDown() {

    }

    @Override
    public void swipeUp() {

    }

    public Keyboard getKeyboard() {
        return m_keyboard;
    }

    private void unregisterFromBT() {
        // restore old bluetooth inputs
        for (int i = 0; i < NUMBER_OF_INPUTS; i++) {
            CursorApplication.getInstance().dispatcher.setInputToDispatch(i, oldInputs[i]);
        }
        oldInputs = null;
    }

    private void registerToBt() {
        // save old inputs
        if (oldInputs == null) {
            oldInputs = new SingleInput[NUMBER_OF_INPUTS];
            for (int i = 0; i < NUMBER_OF_INPUTS; i++) {
                oldInputs[i] = CursorApplication.getInstance().dispatcher.getInputToDispatch(i);
                registerButtonToBluetooth(i, buttonCodes[i]);
            }
        }

    }

    private void registerButtonToBluetooth(final int buttonIndex, final int buttonCode) {
        CursorApplication.getInstance().dispatcher.setInputToDispatch(buttonIndex, new SingleInput() {
            @Override
            public void onClick() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        onKey(buttonCode, new int[]{buttonCode});
                    }
                });
            }

            @Override
            public void onLongPressStart() {
            }

            @Override
            public void onLongPressStop() {

                if(buttonCode == GREEN_CODE){
                    InputConnection ic = getCurrentInputConnection();
                    ic.performEditorAction(EditorInfo.IME_ACTION_DONE);
                }
            }

            @Override
            public void onDoubleClick() {
            }

            @Override
            public boolean isDoubleClickEnabled() {
                return false;
            }
        });
    }

    @Override
    public void onCreate() {
        // Handler will get associated with the current thread,
        // which is the main thread.
        handler = new Handler();
        super.onCreate();
    }

    private void runOnUiThread(Runnable runnable) {
        handler.post(runnable);
    }

    @Override
    public boolean onShowInputRequested(int flags, boolean configChange) {
        CmdDispatcher dispatcher = CursorApplication.getInstance().dispatcher;
        if (dispatcher != null) {
            Log.d(LOG_TAG, "registering for bt");
            registerToBt();
        }
        return super.onShowInputRequested(flags, configChange);
    }

    @Override
    public void onWindowHidden() {
        CmdDispatcher dispatcher = CursorApplication.getInstance().dispatcher;
        if (dispatcher != null) {
            Log.d(LOG_TAG, "unregistering from bt");
            unregisterFromBT();
        }
        super.onWindowHidden();
    }
}
