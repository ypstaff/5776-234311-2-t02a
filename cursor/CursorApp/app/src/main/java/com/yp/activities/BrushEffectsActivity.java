package com.yp.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;

import com.yp.cursor.R;

/**
 * @author Inbar Donag
 */

public class BrushEffectsActivity extends SideMenuActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_brush_effects);

        // add all the Image Button effects to the list
        LinearLayout paintLayout = (LinearLayout)findViewById(R.id.effectsLayout);
        for (int i = 0; i < paintLayout.getChildCount(); i++){
            btnList.addLast((ImageButton)paintLayout.getChildAt(i));
        }

        currBtn = (ImageButton)paintLayout.getChildAt(0);
        markCurrentButtonPressed();
    }


    public void onSelectClick(View v) {
        Intent i = new Intent();
        i.putExtra("effect", currBtn.getTag().toString());
        setResult(RESULT_OK, i);
        finish();
    }
}
