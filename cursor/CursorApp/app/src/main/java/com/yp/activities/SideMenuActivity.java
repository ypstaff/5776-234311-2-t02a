package com.yp.activities;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

import com.yp.classes.ClickOnlyView;
import com.yp.cursor.CursorApplication;
import com.yp.cursor.R;
import com.yp.cursor.SingleInput;

import java.util.LinkedList;

/**
 * @author Inbar Donag
 * @author Or Mauda
 *
 * Extend this class to implement your own Side-Menu.
 */
public abstract class SideMenuActivity extends Activity {

    protected ImageButton upBtn;
    protected ImageButton downBtn;
    protected Button selectBtn;

    protected LinkedList<ImageButton> btnList = new LinkedList<>();
    protected ImageButton currBtn;
    protected int index = 0;
    protected static final int green = Color.argb(255, 164, 198, 57);

    private SingleInput[] navSimpleInputs = new ClickOnlyView[3];

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_side_menu);

        upBtn = (ImageButton)findViewById(R.id.upBtn);
        downBtn = (ImageButton)findViewById(R.id.downBtn);
        selectBtn = (Button)findViewById(R.id.selectBtn);
    }

    protected void markCurrentButtonPressed() {
        currBtn.setColorFilter(green);
    }

    protected void markCurrentButtonUnpressed() {
        currBtn.clearColorFilter();
    }

    public void onUpClick(View v) {
        if (currBtn.equals(btnList.getFirst())) {
            markCurrentButtonUnpressed();
            index = btnList.size()-1;
            currBtn = btnList.get(index);
            markCurrentButtonPressed();
            return;
        }
        markCurrentButtonUnpressed();
        index--;
        currBtn = btnList.get(index);
        markCurrentButtonPressed();
    }

    public void onDownClick(View v) {
        if (currBtn.equals(btnList.getLast())) {
            markCurrentButtonUnpressed();
            index = 0;
            currBtn = btnList.get(index);
            markCurrentButtonPressed();
            return;
        }
        markCurrentButtonUnpressed();
        index++;
        currBtn = btnList.get(index);
        markCurrentButtonPressed();
    }

    public abstract void onSelectClick(View v);

    private void unregisterFromBT() {
        CursorApplication.getInstance().dispatcher.setInputToDispatch(0, null);
        CursorApplication.getInstance().dispatcher.setInputToDispatch(1, null);
        CursorApplication.getInstance().dispatcher.setInputToDispatch(2, null);
    }

    private void registerToBt() {

        upBtn = (ImageButton)findViewById(R.id.upBtn);
        downBtn = (ImageButton)findViewById(R.id.downBtn);
        selectBtn = (Button)findViewById(R.id.selectBtn);

        navSimpleInputs[0] = new ClickOnlyView(this, upBtn);
        navSimpleInputs[1] = new ClickOnlyView(this, selectBtn);
        navSimpleInputs[2] = new ClickOnlyView(this, downBtn);

        for (int i = 0; i < navSimpleInputs.length; ++i) {
            if(CursorApplication.getInstance().dispatcher != null)
                CursorApplication.getInstance().dispatcher.setInputToDispatch(i, navSimpleInputs[i]);
        }

    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterFromBT();
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerToBt();
    }
}
