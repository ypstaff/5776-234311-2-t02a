package com.yp.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ScrollView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.yp.classes.ClickOnlyView;
import com.yp.classes.CmdDispatcher;
import com.yp.classes.Constants;
import com.yp.cursor.CursorApplication;
import com.yp.cursor.R;
import com.yp.cursor.SingleInput;
import com.yp.navigationlibrary.EmergencyCallActivity;
import com.yp.navigationlibrary.IterableGroup;
import com.yp.navigationlibrary.IterableSingleView;
import com.yp.navigationlibrary.IterableViewGroups;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Settings activity for the 3-Boom application.
 * Holds global settings, relevant to general behaviour of the application.
 *
 * @author Oded
 * @version 10/06/2016
 */
public class SettingsActivity extends EmergencyCallActivity implements CompoundButton.OnCheckedChangeListener {

    private static final int MAX_EYE_SENSITIVITY = 30;
    private static final int MIN_EYE_SENSITIVITY = 5;
    private static final int MIN_X_SENSITIVITY = 1;
    private static final int MAX_X_SENSITIVITY = 15;
    private static final int MIN_Y_SENSITIVITY = 1;
    private static final int MAX_Y_SENSITIVITY = 30;
    private static final int GET_DB_FILE_CODE = 45145;
    private static final int DEFAULT_ONE_BUTTON_CURSOR_SPEED = 5;
    private static final int MIN_ONE_BUTTON_SPEED = 1;
    private static final int MAX_ONE_BUTTON_SPEED = 10;
    private static final int MIN_BT_EVENT_SENSITIVITY = 1;
    private static final int DEFAULT_BT_EVENT_SENSITIVITY = 5;
    private static final int MAX_BT_EVENT_SENSITIVITY = 10;
    private SharedPreferences.Editor editor;
    private CmdDispatcher dispatcher;
    private SingleInput[] navSimpleInputs = new ClickOnlyView[3];
    private EditText et_eye_sensitivity;
    private EditText et_x_sensitivity;
    private EditText et_y_sensitivity;
    private ScrollView mScrollView;
    private ImageButton ib_upload_text_file;
    private Animation fabPressAnimation;
    private EditText et_one_button_cursor_speed;
    private ToggleButton defaultCursor;
    private EditText et_bt_event_sensitivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);
        dispatcher = CursorApplication.getInstance().dispatcher;

        // get reference of shared preferences
        PreferenceManager.setDefaultValues(this, R.xml.preferences, false);
        final SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        editor = sharedPref.edit(); // ignore studio's comment, we need this w/o commit
        mScrollView = (ScrollView) findViewById(R.id.sv_settings);

        // build and initialize the IterableGroup for NAV system + get previous preferences values
        initNavButtons();
        List<IterableGroup> navGroup = new ArrayList<>();

        addCheckBoxes(sharedPref, navGroup);
        addEyeSensitivityButtons(sharedPref, navGroup);
        addXAxisSensitivityButtons(sharedPref, navGroup);
        addYAxisSensitivityButtons(sharedPref, navGroup);
        addAutoCompleteFileUpload(navGroup);
        addOneButtonCursorSpeed(sharedPref, navGroup);
        addChangeDefaultCursor(sharedPref, navGroup);
        addBtEventGenerationSensitivity(sharedPref, navGroup);
        group = new IterableViewGroups(navGroup, findViewById(R.id.ll_all_pref_rows), this);
        group.start();

        fabPressAnimation = AnimationUtils.loadAnimation(getApplication(), R.anim.fab_main_press);
    }

    private void addBtEventGenerationSensitivity(SharedPreferences pref, List<IterableGroup> group) {
        final Button minus = (Button) findViewById(R.id.b_bt_event_sensitivity_minus);
        final Button plus = (Button) findViewById(R.id.b_bt_event_sensitivity_plus);
        et_bt_event_sensitivity = (EditText) findViewById(R.id.et_bt_event_sensitivity);
        et_bt_event_sensitivity.setText(String.format("%d", pref.getInt(getString(R.string.pref_key_bt_event_sensitivity), DEFAULT_BT_EVENT_SENSITIVITY)));
        minus.setOnClickListener(this);
        plus.setOnClickListener(this);
        group.add(new IterableSingleView(minus, this));
        group.add(new IterableSingleView(plus, this));
    }

    private void addChangeDefaultCursor(SharedPreferences sharedPref, List<IterableGroup> navGroup) {
        boolean isFaceCursorDefault = sharedPref.getBoolean(getString(R.string.pref_is_face_cursor_default), false);
        defaultCursor = (ToggleButton) findViewById(R.id.tb_default_cursor);
        defaultCursor.setChecked(isFaceCursorDefault);
        navGroup.add(new IterableSingleView(defaultCursor, this));
    }

    private void addOneButtonCursorSpeed(SharedPreferences sharedPref, List<IterableGroup> group) {
        final Button minus = (Button) findViewById(R.id.b_one_button_cursor_speed_minus);
        final Button plus = (Button) findViewById(R.id.b_one_button_cursor_speed_plus);
        et_one_button_cursor_speed = (EditText) findViewById(R.id.et_one_button_cursor_speed);
        et_one_button_cursor_speed.setText(String.format("%d", sharedPref.getInt(getString(R.string.pref_one_button_cursor_speed), DEFAULT_ONE_BUTTON_CURSOR_SPEED)));
        minus.setOnClickListener(this);
        plus.setOnClickListener(this);
        group.add(new IterableSingleView(minus, this));
        group.add(new IterableSingleView(plus, this));
    }

    private void addAutoCompleteFileUpload(List<IterableGroup> navGroup) {
        ib_upload_text_file = (ImageButton) findViewById(R.id.ib_upload_text_file);
        navGroup.add(new IterableSingleView(ib_upload_text_file, this));
    }

    private void addCheckBoxes(SharedPreferences sharedPref, List<IterableGroup> navGroup) {
        CheckBox cb_bt_auto_connect = (CheckBox) findViewById(R.id.cb_pref_auto_bt_value);
        CheckBox cb_bt_auto_reconnect = (CheckBox) findViewById(R.id.cb_pref_auto_bt_reconnect_value);
        CheckBox cb_show_developer = (CheckBox) findViewById(R.id.cb_pref_show_developer_value);
        cb_bt_auto_connect.setChecked(sharedPref.getBoolean(getString(R.string.pref_key_bt_auto_connect), false));
        cb_bt_auto_reconnect.setChecked(sharedPref.getBoolean(getString(R.string.pref_key_bt_auto_reconnect), false));
        cb_show_developer.setChecked(sharedPref.getBoolean(getString(R.string.pref_key_show_developer), false));

        // set this activity as state change listener
        cb_bt_auto_connect.setOnCheckedChangeListener(this);
        cb_bt_auto_reconnect.setOnCheckedChangeListener(this);
        cb_show_developer.setOnCheckedChangeListener(this);

        navGroup.add(new IterableSingleView(cb_bt_auto_connect, this));
        navGroup.add(new IterableSingleView(cb_bt_auto_reconnect, this));
        navGroup.add(new IterableSingleView(cb_show_developer, this));
    }

    private void addEyeSensitivityButtons(SharedPreferences sharedPref, List<IterableGroup> navGroup) {
        Button b_eye_sensitivity_minus = (Button) findViewById(R.id.b_eye_sensitivity_minus);
        Button b_eye_sensitivity_plus = (Button) findViewById(R.id.b_eye_sensitivity_plus);
        et_eye_sensitivity = (EditText) findViewById(R.id.et_eye_sensitivity);
        et_eye_sensitivity.setText(String.format("%d", sharedPref.getInt(getString(R.string.pref_key_blink_sensitivity), Constants.FaceCursor.DEFAULT_EYE_BLINK_THRESHOLD)));
        b_eye_sensitivity_minus.setOnClickListener(this);
        b_eye_sensitivity_plus.setOnClickListener(this);
        navGroup.add(new IterableSingleView(b_eye_sensitivity_minus, this));
        navGroup.add(new IterableSingleView(b_eye_sensitivity_plus, this));
    }

    private void addXAxisSensitivityButtons(SharedPreferences sharedPref, List<IterableGroup> navGroup) {
        Button b_x_sensitivity_minus = (Button) findViewById(R.id.b_x_sensitivity_minus);
        Button b_x_sensitivity_plus = (Button) findViewById(R.id.b_x_sensitivity_plus);
        et_x_sensitivity = (EditText) findViewById(R.id.et_x_sensitivity);
        et_x_sensitivity.setText(String.format("%d", sharedPref.getInt(getString(R.string.pref_key_x_sensitivity), Constants.FaceCursor.DEFAULT_X_MOVEMENT_PORTRAIT)));
        b_x_sensitivity_minus.setOnClickListener(this);
        b_x_sensitivity_plus.setOnClickListener(this);
        navGroup.add(new IterableSingleView(b_x_sensitivity_minus, this));
        navGroup.add(new IterableSingleView(b_x_sensitivity_plus, this));
    }

    private void addYAxisSensitivityButtons(SharedPreferences sharedPref, List<IterableGroup> navGroup) {
        Button b_y_sensitivity_minus = (Button) findViewById(R.id.b_y_sensitivity_minus);
        Button b_y_sensitivity_plus = (Button) findViewById(R.id.b_y_sensitivity_plus);
        et_y_sensitivity = (EditText) findViewById(R.id.et_y_sensitivity);
        et_y_sensitivity.setText(String.format("%d", sharedPref.getInt(getString(R.string.pref_key_y_sensitivity), Constants.FaceCursor.DEFAULT_Y_MOVEMENT_PORTRAIT)));
        b_y_sensitivity_minus.setOnClickListener(this);
        b_y_sensitivity_plus.setOnClickListener(this);
        navGroup.add(new IterableSingleView(b_y_sensitivity_minus, this));
        navGroup.add(new IterableSingleView(b_y_sensitivity_plus, this));
    }


    private void initNavButtons() {
        //Setting the 3 buttons navigation system for the activity
        fab_left = (FloatingActionButton) findViewById(R.id.fab_left);
        fab_right = (FloatingActionButton) findViewById(R.id.fab_right);
        fab_middle = (FloatingActionButton) findViewById(R.id.fab_middle);

        // Setting the listeners for the navigation buttons, and make them semi-transparent
        for (FloatingActionButton fab : Arrays.asList(fab_left, fab_right, fab_middle)) {
            fab.setOnClickListener(this);
            fab.setOnLongClickListener(this);
            fab.setAlpha((float) 0.5);
        }

        navSimpleInputs[0] = new ClickOnlyView(this, fab_right);
        navSimpleInputs[1] = new ClickOnlyView(this, fab_middle);
        navSimpleInputs[2] = new ClickOnlyView(this, fab_left);
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        switch (buttonView.getId()) {
            case R.id.cb_pref_auto_bt_value:
                editor.putBoolean(getString(R.string.pref_key_bt_auto_connect), isChecked);
                break;
            case R.id.cb_pref_auto_bt_reconnect_value:
                editor.putBoolean(getString(R.string.pref_key_bt_auto_reconnect), isChecked);
                break;
            case R.id.cb_pref_show_developer_value:
                editor.putBoolean(getString(R.string.pref_key_show_developer), isChecked);
                break;
        }
        editor.commit();
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);

        switch(v.getId()) {
            case R.id.fab_left:
                checkScroll();
            case R.id.fab_middle:
            case R.id.fab_right:
                v.startAnimation(fabPressAnimation);
                break;
        }

        handleEyeSensitivity(v);
        handleXAxisSensitivity(v);
        handleYAxisSensitivity(v);
        handleOneButtonCursorSpeed(v);
        handleBtEventGenerationSensitivity(v);
    }

    private void handleBtEventGenerationSensitivity(View v) {
        handlePlusMinusChange(v.getId(), et_bt_event_sensitivity, R.string.pref_key_bt_event_sensitivity,
                R.id.b_bt_event_sensitivity_minus, R.id.b_bt_event_sensitivity_plus,
                MIN_BT_EVENT_SENSITIVITY, MAX_BT_EVENT_SENSITIVITY);
    }

    private void checkScroll() {
        final List<View> selectedViews = group.getSelectedViews();
        for (View view : selectedViews) {
            scroll(view);
        }
    }

    private void handleOneButtonCursorSpeed(View v) {
        handlePlusMinusChange(v.getId(), et_one_button_cursor_speed, R.string.pref_one_button_cursor_speed,
                R.id.b_one_button_cursor_speed_minus, R.id.b_one_button_cursor_speed_plus,
                MIN_ONE_BUTTON_SPEED, MAX_ONE_BUTTON_SPEED);
    }

    private void scroll(final View view) {
        mScrollView.post(new Runnable() {
            @Override
            public void run() {
                if (view.getId() == R.id.cb_pref_auto_bt_value) {
                    mScrollView.fullScroll(View.FOCUS_UP);
                } else {
                    mScrollView.scrollBy(0, findViewById(R.id.ll_perf_row1).getHeight() / 2);
                }
            }
        });
    }

    private void handleEyeSensitivity(View v) {
        handlePlusMinusChange(v.getId(), et_eye_sensitivity, R.string.pref_key_blink_sensitivity,
                R.id.b_eye_sensitivity_minus, R.id.b_eye_sensitivity_plus,
                MIN_EYE_SENSITIVITY, MAX_EYE_SENSITIVITY);
    }

    private void handleXAxisSensitivity(View v) {
        handlePlusMinusChange(v.getId(), et_x_sensitivity, R.string.pref_key_x_sensitivity,
                R.id.b_x_sensitivity_minus, R.id.b_x_sensitivity_plus,
                MIN_X_SENSITIVITY, MAX_X_SENSITIVITY);
    }

    private void handleYAxisSensitivity(View v) {
        handlePlusMinusChange(v.getId(), et_y_sensitivity, R.string.pref_key_y_sensitivity,
                R.id.b_y_sensitivity_minus, R.id.b_y_sensitivity_plus,
                MIN_Y_SENSITIVITY, MAX_Y_SENSITIVITY);
    }

    private void handlePlusMinusChange(int vId, EditText et, int perfId, int b_min_id, int b_max_id, int min, int max) {
        int delta = 0, sensitivity = Integer.parseInt(et.getText().toString());

        if (vId == b_min_id) {
            delta = (sensitivity > min) ? -1 : 0;
        } else if (vId == b_max_id) {
            delta = (sensitivity < max) ? 1 : 0;
        }

        if (0 != delta) {
            sensitivity += delta;
            editor.putInt(getString(perfId), sensitivity).commit();
            et.setText(String.format("%d", sensitivity));
        }
    }

    public void generateDbFromFile(View view) {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        Uri uri = Uri.parse(Environment.getExternalStorageDirectory().getPath() + "/");
        intent.setDataAndType(uri, "text/plain"); //text/plain
        try {
            startActivityForResult(Intent.createChooser(intent, "Open folder"), GET_DB_FILE_CODE);
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(this, "Please install a File Manager.", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case GET_DB_FILE_CODE: {
                if (data == null)
                    return;
                try {
                    Uri uri = data.getData();
                    ((CursorApplication) getApplication()).getNgramDatabaseSuggester().loadDB(uri);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerToBt();
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterFromBT();
    }

    private void registerToBt() {
        for (int i = 0; i < navSimpleInputs.length; ++i) {
            dispatcher.setInputToDispatch(i, navSimpleInputs[i]);
        }
    }

    private void unregisterFromBT() {
        for (int i = 0; i < navSimpleInputs.length; ++i) {
            dispatcher.setInputToDispatch(i, null);
        }
    }

    public void changeDefaultCursor(View view) {
        editor.putBoolean(getString(R.string.pref_is_face_cursor_default), defaultCursor.isChecked());
        editor.commit();
    }
}
