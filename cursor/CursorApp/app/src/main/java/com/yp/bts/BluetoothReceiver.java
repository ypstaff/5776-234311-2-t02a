package com.yp.bts;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;
import android.widget.Toast;

import java.io.IOException;
import java.util.UUID;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

/**
 * Receiver of remotely generated input, over a bluetooth connection.
 * <p/>
 * Once instantiated, in order to actually start receiving input, the {@link #start(BluetoothEventReceiver)}
 * method should be invoked, as well as the {@link #subscribe(SimpleInputHandler)} method for
 * registering a handler to which the generated input events will be dispatched.
 * <p/>
 * The Receiver can be stopped by calling the {@link #stop(boolean)} method, and restarted again by
 * invoking the {@link #start(BluetoothEventReceiver)} once more.
 */
public class BluetoothReceiver {

    /* CONSTANTS */
    private static final String BT_RECEIVER_TAG = "BLUETOOTH_SERVER";
    private static final int    MAX_INPUT_SIZE  = 8;

    /* MEMBERS */
    public  final UUID                   btUUID;
    public  final BluetoothAdapter       mBtAdapter;
    private final Context                ctx;
    private       Executor               executor;
    private       AcceptThread           acceptThread;
    private       TranslatorGroup        translatorGroup;
    private       SimpleInputHandler     handler;
    private final Object                 acceptThreadLocker = new Object();
    private       BluetoothEventReceiver mInitiator;

    /**
     * Constructor. Create a new BluetoothReceiver, using a custom BluetoothAdapter and a custom
     * UUID for identifying the application.
     * <p/>
     * It is strongly suggested to use overloaded constructor
     * {@link #BluetoothReceiver(Context, String)} which uses the device's default BluetoothAdapter.
     *
     * @param ctx     context in which this receiver is running, mostly for enabling UI feedback such as
     *                toasts, etc. If ctx is null, UI feedback will be reduced, but receiver will still
     *                be functional.
     * @param adapter BluetoothAdapter with which this receiver interacts. Used for establishing the
     *                remote connections.
     * @param uuid    Unique ID for this receiver to be used to identify this application over
     *                Bluetooth.
     */
    public BluetoothReceiver(@NonNull Context ctx, BluetoothAdapter adapter, @NonNull String uuid) {
        this.mBtAdapter      = adapter;
        this.executor        = Executors.newSingleThreadExecutor();
        this.ctx             = ctx;
        this.btUUID          = UUID.fromString(uuid);
        this.translatorGroup = new TranslatorGroup(MAX_INPUT_SIZE);
    }


    /**
     * Constructor. Create a new BluetoothReceiver, using a custom BluetoothAdapter and a custom
     * UUID for identifying the application.
     *
     * @param ctx  context in which this receiver is running, mostly for enabling UI feedback such as
     *             toasts, etc. If ctx is null, UI feedback will be reduced, but receiver will still
     *             be functional.
     * @param uuid Unique ID for this receiver to be used to identify this application over
     *             Bluetooth.
     */
    public BluetoothReceiver(@NonNull Context ctx, @NonNull String uuid) {
        this(ctx, BluetoothAdapter.getDefaultAdapter(), uuid);
    }

    /**
     * Replace this receiver's TranslationGroup, which handles the logic and interpretation of incoming
     * raw input over Bluetooth.
     * <p/>
     * Notice: replacing the TranslationGroup retains the currently subscribed handler. Also, if this
     * receiver is actively receiving and handling input at the time of invocation, the new TranslationGroup
     * is immediately activated and used.
     *
     * @param tGroup TranslatorGroup to replace current one.
     */
    public void setTranslatorGroup(@NonNull TranslatorGroup tGroup) {
        if (translatorGroup == tGroup) {
            return; // ignore consecutive calls with same Translator
        }

        translatorGroup.stop();
        translatorGroup = tGroup;

        if (null != this.handler) {
            subscribe(this.handler);
        }

        if (isStarted()) {
            translatorGroup.start();
        }
    }


    /**
     * Check whether Bluetooth is supported on the device this receiver is currently running.
     *
     * @return 'true' if Bluetooth is supported, 'false' otherwise.
     */
    private boolean isBluetoothSupported() {
        return (mBtAdapter != null);
    }


    /**
     * Check whether Bluetooth is currently enabled (and supported) on the device this receiver is
     * currently running.
     *
     * @return 'true' if Bluetooth is enabled, 'false' otherwise (disabled / not supported).
     */
    public boolean isBluetoothEnabled() {
        return (isBluetoothSupported() && mBtAdapter.isEnabled()); // don't change order!
    }

    /**
     * Try to accept connections to start receiving input from a remote device.
     *
     * @return 'true' if successful connection was established, 'false' if was already connected,
     * or failed to establish connection.
     *
     * @param initiator The receiver who initiated the connection, and is will get connection state
     *                  change updates.
     */
    public boolean start(BluetoothEventReceiver initiator) {
        if (!isBluetoothEnabled()) {
            showToast("Bluetooth disabled or unsupported");
            return false;
        }

        if (isStarted()) {
            showToast("Already connected via Bluetooth");
            return false;
        }

        try {
            synchronized (acceptThreadLocker) {
                acceptThread = new AcceptThread(this, initiator);
                translatorGroup.start();
                executor.execute(acceptThread);
            }
        } catch (IOException e) {
            Log.d(BT_RECEIVER_TAG, "start: Failed to attach to listening socket - " +
                    "NOT RECEIVING. Reason: " + e.getLocalizedMessage());

            showToast("Failed to connect through BT");
            return false;
        }

        mInitiator = initiator;
        return true;
    }

    /**
     * Wrapper for displaying toasts - only displayed when this receiver has a valid context.
     *
     * @param s      String to be displayed in toast.
     * @param length period for toast to be displayed.
     */
    public void showToast(String s, int length) {
        if (null != ctx) { // don't believe studio's comment about always being not null!
            Toast.makeText(ctx, s, length).show();
        }
    }

    /**
     * Wrapper for displaying toasts - only displayed when this receiver has a valid context.
     * <br><br>
     * Equivalent to calling <code>showToast(s, Toast.SHORT)</code>.
     *
     * @param s String to be displayed in toast.
     */
    public void showToast(String s) {
        showToast(s, Toast.LENGTH_SHORT);
    }

    /**
     * Stop receiving input from a remote device via Bluetooth. <br><br>
     * <p/>
     * Has no effect if this BluetoothReceiver wasn't actively receiving remote input.
     *
     * @param intentionalClose indicate whether closing was intentionally initiated.
     *
     * @return 'true' if a termination message was sent, 'false' otherwise.
     */
    public boolean stop(boolean intentionalClose) {
        Log.d(BT_RECEIVER_TAG, "Stopped listening for inputs via BT");
        if (!isStarted()) {
            return false;
        }

        translatorGroup.stop();
        boolean sentTermination = false;
        synchronized (acceptThreadLocker) {
            if (null != acceptThread) {
                sentTermination = acceptThread.close(intentionalClose);
                acceptThread = null;
            }
        }

        if (null != mInitiator) {
            mInitiator.onDisconnect();
        }

        return sentTermination;
    }


    /**
     * Subscribe a handler for inputs received from remote device via Bluetooth.<br><br>
     * <p/>
     * Each call to this method replaces any previously subscribed handler, so that the previous
     * one is effectively un-subscribed. And so, it would be good practice to invoke this upon
     * resuming to a paused activity (e.g: call this method from onResume(...) ).
     *
     * @param newHandler to be used and invoked upon receiving an input from a remote device. Use null
     *                   for effectively un-subscribing to stop receiving remote inputs.
     */
    public void subscribe(SimpleInputHandler newHandler) {
        if (null == newHandler) {
            Log.d(BT_RECEIVER_TAG, "subscribe: Got a null handler - stopping...");
            stop(true);
            return;
        }

        this.handler = newHandler;
        translatorGroup.setHandler(newHandler);
        Log.d(BT_RECEIVER_TAG, "subscribe: Got new handler: " + newHandler.getClass().getSimpleName());
    }


    /**
     * Check if this Receiver is currently actively receiving and handling remote input.
     *
     * @return 'true' if this handler is connected via Bluetooth to a remote device which is sending
     * input, and there is an active handler for the input. 'false' otherwise.
     */
    public boolean isStarted() {
        synchronized (acceptThreadLocker) {
            return (null != acceptThread);
        }
    }

    /**
     * Process a single complete input vector.
     *
     * @param bitVector vector of input current state (pressed or not, for matching bit)
     */
    public void processSingleLevelInput(byte bitVector) {
        translatorGroup.processInput(bitVector);
    }

    /**
     * Inject a raw message (as byte[]) to this receiver for handling.
     *
     * @param payload raw message contents (including header).
     */
    public void injectRawMessage(byte[] payload) {
        BtMessage.of(payload[0]).handle(payload, this);
    }

    /**
     * Get the bluetooth socket currently being used to receive data.
     *
     * @return bluetooth socket currently being used to receive data, 'null' if not running.
     */
    public BluetoothSocket getDataSocket() {
        if (!isStarted()) return null;
        return acceptThread.getDataSocket();
    }

    /**
     * Set the sensitivity of events generation, in the scale of 1 to 10. Higher values mean more
     * sensitive, which will cause events to be generated in higher frequencies ("shorter" input
     * events are recognized and dispatched, but long presses also start "sooner").
     *
     * Note: invoking this method resets the translator group.
     *
     * @param n sensitivity level, in the range [1,10]. default is 5.
     * @return 'true' if new sensitivity was successfully set, 'false' otherwise (e.g: for invalid
     * parameter value).
     */
    public boolean setEventGenerationSensitivity(int n) {
        if (n < 1 || n > 10) return false;

        translatorGroup.setTickLengthMillis(250 - (20 * n));

        return true;
    }

    public int getEventGenerationSensitivity() {
        return (translatorGroup.getTickLengthMillis() - 250) / 20;
    }
} // end class BluetoothReceiver
