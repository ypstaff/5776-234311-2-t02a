package com.yp.cursor;

import android.content.Context;
import android.graphics.Point;
import android.view.SurfaceView;

import java.util.List;

/**
 * Created by avner on 01/12/2015.
 */
public abstract class ViewInputHolder extends SurfaceView implements Runnable{

    protected ViewInputHolder(Context context) {
        super(context);
    }

    /**
     *
     * @return a list of single inputs to be activated when input from the user is received.
     */
    public abstract List<SingleInput> getInputs();

    /**
     * stop the current view. the view will stop rendering itself.
     */
    public abstract void stop();

    /**
     * start the current view. the view will render itself.
     */
    public abstract void start();

    /**
     * set the new orientation of the screen.
     * @param orientation - new orientation.
     * @param screenDimensions - new screen dimensions.
     */
    public abstract void setOrientation(int orientation, Point screenDimensions);
}
