package com.yp.activities;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.itextpdf.text.Document;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.tool.xml.XMLWorkerHelper;
import com.yp.autocomplete.NgramDatabaseSuggester;
import com.yp.classes.ClickOnlyView;
import com.yp.classes.SendEmailAsyncTask;
import com.yp.cursor.CursorApplication;
import com.yp.cursor.R;
import com.yp.cursor.SingleInput;
import com.yp.navigationlibrary.EmergencyCallActivity;
import com.yp.navigationlibrary.IterableGroup;
import com.yp.navigationlibrary.IterableViewGroup;
import com.yp.navigationlibrary.IterableViewGroups;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import io.github.mthli.knife.CustomTypefaceSpan;
import io.github.mthli.knife.KnifeText;

public class EditorActivity extends EmergencyCallActivity implements View.OnClickListener {
    private final String LOG_TAG = "EDITOR_TAG";

    private KnifeText knife;
    private Context context;
    private ListView listDialogFonts;
    protected FloatingActionButton fab_left, fab_right, fab_middle;
    private ArrayList<String> fonts;
    private ArrayList<String> fontSizes;

    private ImageButton bold, textColor, font, undo, redo, italic, underline, strikethrough, bullet,
            quote, link, statistics, back, forward, paste, copy, cut, font_size, export, keyboard, save, open, delete;
    private ToggleButton selection;
    int startSelection, endSelection, selectionPoint;
    private Animation fabPressAnimation;

    private Button[] suggestionButtons;
    private NgramDatabaseSuggester dbSuggester;
    private int numSuggestions;
    private View.OnClickListener listScrollListener;
    private String dirPath;
    private String openFileName = null;
    private boolean exit = false;
    int focused_widget_num;
    //Tell if we arrive to the save dialog through the exit button
    private boolean isExiting = false;
    private boolean testMode = false;
    private int red;
    private String statistics_message;
    private SingleInput[] navSimpleInputs = new ClickOnlyView[3];


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editor);
        context = this;

        //avoid keyboard automatically open
        hideKeyboardAndSuggestions();

        initEditorEditText();
        initClassVariables();

        //setting up UI functionality
        setupBold();
        setupItalic();
        setupUnderline();
        setupStrikethrough();
        setupBullet();
        setupQuote();
        setupLink();
        setupStatistics();
        setupUndo();
        setupRedo();
        setupKeyboard();
        setupSuggestions();
        setupFonts();
        setupTextColor();
        setupCopy();
        setupCut();
        setupPaste();
        setupBack();
        setupForward();
        setupSelection();
        setupFontSize();
        setupExport();
        setupSave();
        setupOpen();
        setupDelete();
        setupNavButtons();
        setupNavigation();
    }

    private void initClassVariables() {
        dirPath = getExternalFilesDir(null) + "/ThreeBoomEditor";

        //general list scrolling listener
        listScrollListener = new View.OnClickListener() {
            // scroll one down in dialog actions
            @Override
            public void onClick(View v) {
                v.startAnimation(fabPressAnimation);
                int index = listDialogFonts.getCheckedItemPosition();
                if (index + 1 == listDialogFonts.getCount()) {
                    listDialogFonts.setItemChecked(0, true);
                    listDialogFonts.smoothScrollToPositionFromTop(0, 0, 0);
                    listDialogFonts.deferNotifyDataSetChanged();
                    return;
                }

                index++;
                listDialogFonts.setItemChecked(index, true);
                listDialogFonts.smoothScrollToPositionFromTop(index, listDialogFonts.getHeight() / 2);
                listDialogFonts.deferNotifyDataSetChanged();
            }
        };
        //for testing
        red = ContextCompat.getColor(context, R.color.text_red);

    }

    private void initEditorEditText() {
        knife = (KnifeText) findViewById(R.id.knife);
        knife.setBackgroundColor(ContextCompat.getColor(context, R.color.myTileColor));
//        generateExample();

        knife.setSelection(knife.getEditableText().length());

        knife.setOnEditorActionListener(new TextView.OnEditorActionListener() {

            @Override
            public boolean onEditorAction(TextView v, int actionId,
                                          KeyEvent event) {
                Log.d(LOG_TAG, "on editor action. action id: " + actionId);
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    // this is the callback when keyboard closes
                    hideKeyboardAndSuggestions();
                    showTools();
                }
                return false;
            }
        });

        // Autocomplete suggestions
        dbSuggester = ((CursorApplication) getApplication()).getNgramDatabaseSuggester();
        numSuggestions = ((CursorApplication) getApplication()).getNumSuggestions();
        knife.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                String[] suggestions = dbSuggester.suggest(knife.getText().toString().substring(0, knife.getSelectionStart()));
                if (suggestions.length != numSuggestions) {
                    Log.d(LOG_TAG, "afterTextChanged: not enough suggestions given!");
                    return;
                }

                for (int i = 0; i < numSuggestions; i++)
                    suggestionButtons[i].setText(suggestions[i]);
            }
        });

        knife.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                Log.d(LOG_TAG, "onKey()");
                return false;
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerToBt();
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterFromBT();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fab_left:
            case R.id.fab_middle:
                v.startAnimation(fabPressAnimation);
                break;
            case R.id.fab_right:
                v.startAnimation(fabPressAnimation);
                if (group.back())
                    onBackClick();
                return;
        }
        super.onClick(v);
    }

    private void onBackClick() {
        final Dialog dialog = new Dialog(context);
        dialog.setContentView(R.layout.back_dialog);
        dialog.show();

        final Button b_do_not_save = (Button) dialog.findViewById(R.id.b_not_save);
        final Button b_save = (Button) dialog.findViewById(R.id.b_save);
        b_do_not_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        b_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isExiting = true;
                dismissDialog(dialog);
                save.performClick();
            }
        });


        final View[] views = new View[2];
        views[0] = b_do_not_save;
        views[1] = b_save;

        //initial navigate
        focused_widget_num = 0;
        b_do_not_save.setBackgroundColor(ContextCompat.getColor(context, R.color.yellow));
        for (int i = 1; i < views.length; i++)
            views[i].setBackgroundColor(ContextCompat.getColor(context, R.color.white));

        // "Go Down" button - scrolls down in the list of contacts
        View.OnClickListener leftListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                views[focused_widget_num].setBackgroundColor(ContextCompat.getColor(context, R.color.white));
                focused_widget_num = (focused_widget_num + 1) % views.length;
                views[focused_widget_num].setBackgroundColor(ContextCompat.getColor(context, R.color.yellow));
            }
        };

        // "Select" button - select the currently checked item in the options list
        View.OnClickListener middleListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                views[focused_widget_num].performClick();
            }
        };

        View.OnClickListener rightListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismissDialog(dialog);
            }
        };
        initDialogButtons(dialog, leftListener, middleListener, rightListener);
    }

    private void dismissDialog(Dialog dialog) {
        dialog.dismiss();
        fab_left = (FloatingActionButton) findViewById(R.id.fab_left);
        fab_right = (FloatingActionButton) findViewById(R.id.fab_right);
        fab_middle = (FloatingActionButton) findViewById(R.id.fab_middle);
        registerToBt();
    }

    private void setupKeyboard() {
        Log.d("EDITOR_TAG", "listener set");
        keyboard = (ImageButton) findViewById(R.id.keyboard);

        keyboard.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                showKeyboardAndSuggestions();
                hideTools();
            }
        });
    }

    private void hideTools() {
        findViewById(R.id.tools).setVisibility(LinearLayout.GONE);
        findViewById(R.id.threeButtons).setVisibility(LinearLayout.GONE);
    }

    private void showTools() {
        findViewById(R.id.tools).setVisibility(LinearLayout.VISIBLE);
        findViewById(R.id.threeButtons).setVisibility(LinearLayout.VISIBLE);
    }

    private void hideKeyboardAndSuggestions() {
        findViewById(R.id.suggestions).setVisibility(LinearLayout.GONE);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
    }

    private void showKeyboardAndSuggestions() {
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.toggleSoftInputFromWindow(knife.getApplicationWindowToken(), InputMethodManager.SHOW_FORCED, 0);
        knife.requestFocus();
    }

    /**
     * This method creates an html or plaintext file containing inlined css that produces the text
     * styles in the editor. The html file can be easily used to create a pdf.
     * (for example, by using Google Chrome Browser)
     *
     * @param fileName
     * @param htmlFlag if set to true, creating html file instead of plaintext
     */
    private void saveFile(String fileName, boolean htmlFlag) {
        String content;
        String fileExtension;
        if (htmlFlag) {
            content = knife.toHtml();
            fileExtension = ".html";
        } else {
            content = String.valueOf(knife.getText());
            fileExtension = ".txt";
        }

        String fileNameWithExtension = fileName + fileExtension;
        Log.d(LOG_TAG, "saving to path: \n" + dirPath + "/" + fileNameWithExtension);
        try {
            File root = new File(dirPath);
            if (!root.exists()) {
                root.mkdirs();
            }
            FileWriter writer = new FileWriter(new File(dirPath, fileNameWithExtension));
            writer.append(content);
            writer.flush();
            writer.close();
            openFileName = fileName;
        } catch (FileNotFoundException e) {
            Log.e(LOG_TAG, "file not found");
            e.printStackTrace();
        } catch (IOException e) {
            Log.e(LOG_TAG, "IO_EXCEPT");
            e.printStackTrace();
        }
    }

    /**
     * This function is used for debugging.
     * It replaces the text of the editor with the HTML generated by fromHtml()
     */
    private void showHTML() {
        String html = knife.toHtml();
        knife.setText(html);
    }

    /**
     * This method creates a pdf from an html with css using the itext library.
     * The library is bugged so for now we send the html file itself.
     * see saveFile() for more details.
     */
    public void pdfCreate() {
        try {
            String content = knife.toHtml();
            Log.d(LOG_TAG, dirPath);
            OutputStream file = new FileOutputStream(new File(dirPath + "/3BoomPDF.pdf"));
            Document document = new Document();
            PdfWriter writer = PdfWriter.getInstance(document, file);
            document.open();
            InputStream is = new ByteArrayInputStream(content.getBytes());
            XMLWorkerHelper.getInstance().parseXHtml(writer, document, is);
            document.close();
            file.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void sendNoIntent(String fileName, String receiverEMailAddress) {
        new SendEmailAsyncTask().execute(dirPath, fileName, receiverEMailAddress);
    }

    private void generateExample() {
        int start = 0;
        int end;
        String line;

        line = "This line is bold.\n";
        end = line.length();
        knife.append(line);
        knife.bold(true, start, end);

        line = "This line is italic.\n";
        start = end;
        end += line.length();
        knife.append(line);
        knife.italic(true, start, end);

        line = "This line is with bullet.\n";
        start = end;
        end += line.length();
        knife.append(line);
        knife.bullet(true, start, end);

        line = "This line is strike through.\n";
        start = end;
        end += line.length();
        knife.append(line);
        knife.strikethrough(true, start, end);

        line = "This line is with quote.\n";
        start = end;
        end += line.length();
        knife.append(line);
        knife.quote(true, start, end);


        line = "This line is orange.\n";
        start = end;
        end += line.length();
        knife.append(line);
        knife.color(ContextCompat.getColor(context, R.color.text_orange), start, end);

        line = "This line is link.\n";
        start = end;
        end += line.length();
        knife.append(line);
        knife.link("google.com", start, end);

        line = "This line is of font david\n";
        start = end;
        end += line.length();
        knife.append(line);
        Typeface face = Typeface.createFromAsset(getAssets(), "david.ttf");
        CustomTypefaceSpan cFace = new CustomTypefaceSpan("", face, "david");
        knife.font(cFace, start, end);

        line = "This line is of font impact\n";
        start = end;
        end += line.length();
        knife.append(line);
        face = Typeface.createFromAsset(getAssets(), "impact.ttf");
        cFace = new CustomTypefaceSpan("", face, "impact");
        knife.font(cFace, start, end);

        line = "This line with a font size of 100.\n";
        start = end;
        end += line.length();
        knife.append(line);
        knife.fontSize(100, start, end);
    }

    private void generateExampleOld() {
        int start = 0;
        int end;

        String line1 = "This line is bold.\n";
        end = line1.length();
        knife.append(line1);
        knife.bold(true, start, end);

        String line2 = "This line is of font david with underline.\n";
        start = end;
        end += line2.length();
        knife.append(line2);
        knife.underline(true, start, end);
        Typeface face = Typeface.createFromAsset(getAssets(), "david.ttf");
        CustomTypefaceSpan cFace = new CustomTypefaceSpan("", face, "david");
        knife.font(cFace, start, end);

        String line3 = "This line is of font journal with strike through.\n";
        start = end;
        end += line3.length();
        knife.append(line3);
        knife.strikethrough(true, start, end);
        face = Typeface.createFromAsset(getAssets(), "journal.ttf");
        cFace = new CustomTypefaceSpan("", face, "journal");
        knife.font(cFace, start, end);

        String line4 = "This line is of font pacifico with a font size of 100.\n";
        start = end;
        end += line4.length();
        knife.append(line4);
        knife.fontSize(100, start, end);
        face = Typeface.createFromAsset(getAssets(), "pacifico.ttf");
        cFace = new CustomTypefaceSpan("", face, "pacifico");
        knife.font(cFace, start, end);
    }

    private void setupSuggestions() {
        suggestionButtons = new Button[numSuggestions];
        suggestionButtons[0] = (Button) findViewById(R.id.suggestion1);
        suggestionButtons[1] = (Button) findViewById(R.id.suggestion2);
        suggestionButtons[2] = (Button) findViewById(R.id.suggestion3);

        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String currentWord = getCurrentWord();
                String textToAdd = ((Button) v).getText().toString().substring(currentWord.length());
                knife.getText().replace(knife.getSelectionStart(), knife.getSelectionEnd(), textToAdd + " ");
            }
        };
        for (int i = 0; i < numSuggestions; i++)
            suggestionButtons[i].setOnClickListener(listener);
    }

    private String getCurrentWord() {
        String text = knife.getText().toString().substring(0, knife.getSelectionStart());
        if (Character.isWhitespace(text.charAt(text.length() - 1)))
            return "";
        String[] words = text.split("\\s+");
        return words[words.length - 1];
    }

    private void setupNavigation() {
        List<IterableGroup> $ = new ArrayList<>();

        //Creating the groups for the navigation
        //Row 1 group
        List<View> row1_list = new ArrayList<>();
        row1_list.addAll(Arrays.asList(back, forward, bold, italic, underline, strikethrough, bullet, quote));
        $.add(new IterableViewGroup(row1_list, findViewById(R.id.ll_row1), this));

        //Row 2 group
        List<View> row2_list = new ArrayList<>();
        row2_list.addAll(Arrays.asList(undo, redo, link, textColor, font, font_size, statistics, keyboard));
        $.add(new IterableViewGroup(row2_list, findViewById(R.id.ll_row2), this));

        //Row 3 group
        List<View> row3_list = new ArrayList<>();
        row3_list.addAll(Arrays.asList(cut, copy, paste, selection, export, save, open, delete));
        $.add(new IterableViewGroup(row3_list, findViewById(R.id.ll_row3), this));

        group = new IterableViewGroups($, findViewById(R.id.tools), this);
        group.start();
    }

    private void setupNavButtons() {
        //Setting the 3 buttons navigation system for the activity
        fab_left = (FloatingActionButton) findViewById(R.id.fab_left);
        fab_right = (FloatingActionButton) findViewById(R.id.fab_right);
        fab_middle = (FloatingActionButton) findViewById(R.id.fab_middle);

        // Setting the listeners for the navigation buttons, and make them semi-transparent
        for (FloatingActionButton fab : Arrays.asList(fab_left, fab_right, fab_middle)) {
            fab.setOnClickListener(this);
            fab.setOnLongClickListener(this);
            fab.setAlpha((float) 0.5);
        }

        fabPressAnimation = AnimationUtils.loadAnimation(getApplication(), R.anim.fab_main_press);
    }

    private void setupTextColor() {
        textColor = (ImageButton) findViewById(R.id.textColor);

        final ArrayList<String> colors = new ArrayList<>();
        colors.add("Black");
        colors.add("Red");
        colors.add("Yellow");
        colors.add("Orange");
        colors.add("Green");
        colors.add("Blue");
        colors.add("Navy");
        colors.add("Gray");
        colors.add("Brown");
        colors.add("Purple");
        colors.add("Pink");
        colors.add("Gold");

        final ArrayList<Integer> colors_values = new ArrayList<>();
        colors_values.add(ContextCompat.getColor(context, R.color.text_black));
        colors_values.add(ContextCompat.getColor(context, R.color.text_red));
        colors_values.add(ContextCompat.getColor(context, R.color.text_yellow));
        colors_values.add(ContextCompat.getColor(context, R.color.text_orange));
        colors_values.add(ContextCompat.getColor(context, R.color.text_green));
        colors_values.add(ContextCompat.getColor(context, R.color.text_blue));
        colors_values.add(ContextCompat.getColor(context, R.color.text_navy));
        colors_values.add(ContextCompat.getColor(context, R.color.text_gray));
        colors_values.add(ContextCompat.getColor(context, R.color.text_brown));
        colors_values.add(ContextCompat.getColor(context, R.color.text_purple));
        colors_values.add(ContextCompat.getColor(context, R.color.text_pink));
        colors_values.add(ContextCompat.getColor(context, R.color.text_gold));

        textColor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog = new Dialog(context);
                dialog.setContentView(R.layout.font_dialog);
                dialog.setTitle("Select color");
                dialog.show();

                listDialogFonts = (ListView) dialog.findViewById(R.id.listFonts);
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(context, R.layout.contacts_select_dialog_item, colors){
                    @Override
                    public View getView(int position, View convertView, ViewGroup parent) {
                        View view = super.getView(position, convertView, parent);
                        TextView text = (TextView) view.findViewById(android.R.id.text1);
                        text.setTextColor(colors_values.get(position));
                        return view;
                    }
                };

                listDialogFonts.setAdapter(adapter);
                listDialogFonts.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
                listDialogFonts.setItemChecked(0, true);

                // "Go Down" button - scrolls down in the list of contacts
                View.OnClickListener leftListener = listScrollListener;

                // "Select" button - select the currently checked item in the options list
                View.OnClickListener middleListener = new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        int currentCheckedPosition = listDialogFonts.getCheckedItemPosition();
                        int color = colors_values.get(currentCheckedPosition);
                        knife.color(color, knife.getSelectionStart(), knife.getSelectionEnd());
                        dismissDialog(dialog);
                    }
                };

                View.OnClickListener rightListener = new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dismissDialog(dialog);
                    }
                };

                initDialogButtons(dialog, leftListener, middleListener, rightListener);
            }
        });
    }

    private void setupFontSize() {
        font_size = (ImageButton) findViewById(R.id.font_size);
        fontSizes = new ArrayList<>();
        fontSizes.add("30");
        fontSizes.add("40");
        fontSizes.add("50");
        fontSizes.add("60");
        fontSizes.add("70");
        fontSizes.add("80");
        fontSizes.add("100");
        fontSizes.add("120");

        font_size.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog = new Dialog(context);
                dialog.setContentView(R.layout.font_dialog);
                dialog.setTitle("Select font size");

                listDialogFonts = (ListView) dialog.findViewById(R.id.listFonts);
                ArrayAdapter<String> adapter = new ArrayAdapter<>(context, R.layout.contacts_select_dialog_item, fontSizes);

                listDialogFonts.setAdapter(adapter);
                listDialogFonts.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
                listDialogFonts.setItemChecked(0, true);

                // "Go Down" button - scrolls down in the list of contacts
                View.OnClickListener leftListener = listScrollListener;

                // "Select" button - select the currently checked item in the options list
                View.OnClickListener middleListener = new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        int currentCheckedPosition = listDialogFonts.getCheckedItemPosition();
                        int size = Integer.parseInt(fontSizes.get(currentCheckedPosition));
                        knife.fontSize(size, knife.getSelectionStart(), knife.getSelectionEnd());
                        dismissDialog(dialog);
                    }
                };

                View.OnClickListener rightListener = new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dismissDialog(dialog);
                    }
                };

                initDialogButtons(dialog, leftListener, middleListener, rightListener);
                dialog.show();
            }
        });
    }

    private void copyToClipboard() {
        String selectedText = knife.getText().toString().substring(knife.getSelectionStart(), knife.getSelectionEnd());
        android.content.ClipboardManager clipboard = (android.content.ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
        android.content.ClipData clip = android.content.ClipData.newPlainText("WordKeeper", selectedText);
        clipboard.setPrimaryClip(clip);
    }

    private void setupCut() {
        cut = (ImageButton) findViewById(R.id.cut);
        cut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                copyToClipboard();
                knife.getText().replace(knife.getSelectionStart(), knife.getSelectionEnd(), "");
                selection.setChecked(false);
            }
        });
    }

    private void setupCopy() {
        copy = (ImageButton) findViewById(R.id.copy);
        copy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                copyToClipboard();
            }
        });
    }

    private void setupPaste() {
        paste = (ImageButton) findViewById(R.id.paste);
        paste.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                android.content.ClipboardManager clipboard = (android.content.ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
                android.content.ClipData.Item item = clipboard.getPrimaryClip().getItemAt(0);
                if (item.getText() != null) {
                    knife.getText().insert(knife.getSelectionStart(), item.getText());
                }
            }
        });
    }

    private void setupSelection() {
        selection = (ToggleButton) findViewById(R.id.selection);

        selection.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (selection.isChecked()) {
                    startSelection = knife.getSelectionStart();
                    endSelection = knife.getSelectionStart();
                    selectionPoint = knife.getSelectionStart();
                }
            }
        });
    }

    private int getNextWhiteSpace(int start) {
        int i;
        for (i = start + 1; i < knife.getText().length(); i++) {
            if (Character.isWhitespace(knife.getText().toString().charAt(i))) {
                break;
            }
        }
        if (i > knife.getText().length())
            i = knife.getText().length();
        return i;
    }

    private int getPreviousWhiteSpace(int start) {
        int i;
        for (i = start - 1; i >= 0; i--) {
            if (Character.isWhitespace(knife.getText().toString().charAt(i))) {
                break;
            }
        }
        if (i < 0)
            i = 0;
        return i;
    }

    private void setupForward() {
        forward = (ImageButton) findViewById(R.id.forward);
        forward.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (selection.isChecked()) {
                    if (startSelection != selectionPoint)
                        startSelection = getNextWhiteSpace(startSelection);
                    else
                        endSelection = getNextWhiteSpace(endSelection);
                    knife.setSelection(startSelection, endSelection);
                } else {
                    if(knife.getSelectionStart() != knife.getSelectionEnd()) //selection is just canceled
                        knife.setSelection(knife.getSelectionEnd());
                    else
                        knife.setSelection(getNextWhiteSpace(knife.getSelectionStart()));
                }
            }
        });
    }

    private void setupBack() {
        back = (ImageButton) findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (selection.isChecked()) {
                    if (endSelection != selectionPoint)
                        endSelection = getPreviousWhiteSpace(endSelection);
                    else
                        startSelection = getPreviousWhiteSpace(startSelection);
                    knife.setSelection(startSelection, endSelection);
                } else {
                    if(knife.getSelectionStart() != knife.getSelectionEnd()) //selection is just canceled
                        knife.setSelection(knife.getSelectionStart());
                    else
                     knife.setSelection(getPreviousWhiteSpace(knife.getSelectionStart()));
                }
            }
        });
    }

    private void setupFonts() {
        font = (ImageButton) findViewById(R.id.font);

        //available fonts of our editor
        fonts = new ArrayList<>();
        fonts.add("Android Default");
        fonts.add("David");
//        fonts.add("Arial");
//        fonts.add("Comic Sans MS");
        fonts.add("Courier New");
        fonts.add("Impact");
        fonts.add("Lucida Sans");

        font.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog = new Dialog(context);
                dialog.setContentView(R.layout.font_dialog);
                dialog.setTitle("Select font");

                listDialogFonts = (ListView) dialog.findViewById(R.id.listFonts);
                ArrayAdapter<String> adapter = new ArrayAdapter<>(context, R.layout.contacts_select_dialog_item, fonts);

                listDialogFonts.setAdapter(adapter);
                listDialogFonts.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
                listDialogFonts.setItemChecked(0, true);

                // "Go Down" button - scrolls down in the list of contacts
                View.OnClickListener leftListener = listScrollListener;

                // "Select" button - select the currently checked item in the options list
                View.OnClickListener middleListener = new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        int currentCheckedPosition = listDialogFonts.getCheckedItemPosition();
                        Typeface face;
                        String fontName = null;
                        if (currentCheckedPosition == 0) {
                            fontName = "sans serif";
                            face = Typeface.SANS_SERIF;
                        } else {
                            fontName = fonts.get(currentCheckedPosition);
                            String ttfName = fontName.toLowerCase().replaceAll("\\s+", "") + ".ttf";
                            face = Typeface.createFromAsset(getAssets(), ttfName);
                        }
                        CustomTypefaceSpan cFace = new CustomTypefaceSpan("", face, fontName);
                        knife.font(cFace, knife.getSelectionStart(), knife.getSelectionEnd());

                        dismissDialog(dialog);
                    }
                };

                View.OnClickListener rightListener = new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dismissDialog(dialog);

                    }
                };

                initDialogButtons(dialog, leftListener, middleListener, rightListener);
                dialog.show();
            }
        });
    }

    private void initDialogButtons(final Dialog dialog,
                                   View.OnClickListener leftListener,
                                   View.OnClickListener middleListener,
                                   View.OnClickListener rightListener) {

        fab_left = (FloatingActionButton) dialog.findViewById(R.id.fab_left);
        fab_right = (FloatingActionButton) dialog.findViewById(R.id.fab_right);
        fab_middle = (FloatingActionButton) dialog.findViewById(R.id.fab_middle);

        //make the the navigation buttons semi-transparent
        for (FloatingActionButton fab : Arrays.asList(fab_left, fab_right, fab_middle)) {
            fab.setAlpha((float) 0.5);
        }

        fab_left.setOnClickListener(leftListener);
        fab_middle.setOnClickListener(middleListener);
        fab_right.setOnClickListener(rightListener);

        registerToBt();
    }

//    private void setNightMode() {
//        nightMode = (ImageButton) findViewById(R.id.day_night_mode);
//
//        nightMode.setOnClickListener(new View.OnClickListener() {
//            public void onClick(View v) {
//                if (!nightMOde) {
//                    //set up night mode
//                    knife.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.relaxedDark));
//                    knife.setTextColor(Color.WHITE);
//                    nightMode.setImageResource(R.drawable.ic_day);
//                    nightMOde = true;
//                } else {
//                    //set up night mode
//                    knife.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.myTileColor));
//                    knife.setTextColor(Color.BLACK);
//                    nightMode.setImageResource(R.drawable.ic_night);
//                    nightMOde = false;
//                }
//                knife.undo();
//                showKeyboardSuggestions();
//                hideTools();
//            }
//        });
//    }

    private void setupUndo() {
        undo = (ImageButton) findViewById(R.id.undo);

        undo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                knife.undo();
            }
        });

    }

    private void setupRedo() {
        redo = (ImageButton) findViewById(R.id.redo);

        redo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                knife.redo();
            }
        });

    }

    private void setupBold() {
        bold = (ImageButton) findViewById(R.id.bold);

        bold.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                knife.bold(!knife.contains(KnifeText.FORMAT_BOLD), knife.getSelectionStart(), knife.getSelectionEnd());
            }
        });

    }

    private void setupItalic() {
        italic = (ImageButton) findViewById(R.id.italic);

        italic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                knife.italic(!knife.contains(KnifeText.FORMAT_ITALIC), knife.getSelectionStart(), knife.getSelectionEnd());
            }
        });

    }

    private void setupUnderline() {
        underline = (ImageButton) findViewById(R.id.underline);

        underline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                knife.underline(!knife.contains(KnifeText.FORMAT_UNDERLINED), knife.getSelectionStart(), knife.getSelectionEnd());
            }
        });
    }

    private void setupStrikethrough() {
        strikethrough = (ImageButton) findViewById(R.id.strikethrough);

        strikethrough.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                knife.strikethrough(!knife.contains(KnifeText.FORMAT_STRIKETHROUGH), knife.getSelectionStart(), knife.getSelectionEnd());
            }
        });

    }

    private void setupBullet() {
        bullet = (ImageButton) findViewById(R.id.bullet);

        bullet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                knife.bullet(!knife.contains(KnifeText.FORMAT_BULLET), knife.getSelectionStart(), knife.getSelectionEnd());
            }
        });


    }

    private void setupQuote() {
        quote = (ImageButton) findViewById(R.id.quote);

        quote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                knife.quote(!knife.contains(KnifeText.FORMAT_QUOTE), knife.getSelectionStart(), knife.getSelectionEnd());
            }
        });

    }

    private void setupExport() {
        export = (ImageButton) findViewById(R.id.export);

        export.setOnClickListener(
                new View.OnClickListener() {


                    @Override
                    public void onClick(View v) {
                        final Dialog dialog = new Dialog(context);
                        dialog.setTitle("export");
                        dialog.setContentView(R.layout.export_dialog);

                        final Button sendButton = (Button) dialog.findViewById(R.id.send_button);
                        final RadioButton txtButton = (RadioButton) dialog.findViewById(R.id.txt_button);
                        final RadioButton htmlButton = (RadioButton) dialog.findViewById(R.id.html_button);
                        final EditText email = (EditText) dialog.findViewById(R.id.email_text);
                        final EditText filename = (EditText) dialog.findViewById(R.id.filename_text);
                        if (openFileName != null)
                            filename.setText(openFileName);

                        //prevent focus move on enter
                        email.setOnEditorActionListener(new TextView.OnEditorActionListener() {

                            @Override
                            public boolean onEditorAction(TextView v, int actionId,
                                                          KeyEvent event) {
                                if (event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) {
                                    return true;
                                }
                                return false;
                            }
                        });
                        filename.setOnEditorActionListener(new TextView.OnEditorActionListener() {

                            @Override
                            public boolean onEditorAction(TextView v, int actionId,
                                                          KeyEvent event) {
                                if (event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) {
                                    return true;
                                }
                                return false;
                            }
                        });


                        final View[] views = new View[5];
                        views[0] = htmlButton;
                        views[1] = txtButton;
                        views[2] = email;
                        views[3] = filename;
                        views[4] = sendButton;

                        //initial colors
                        htmlButton.setBackgroundColor(ContextCompat.getColor(context, R.color.white));
                        txtButton.setBackgroundColor(ContextCompat.getColor(context, R.color.yellow));
                        for (int i = 2; i < views.length; i++)
                            views[i].setBackgroundColor(ContextCompat.getColor(context, R.color.white));

                        sendButton.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
                                String strEmail = String.valueOf(email.getText());

                                Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
                                Matcher matcher = pattern.matcher(strEmail);
                                if (matcher.matches()) {
                                    String fileExtension = htmlButton.isChecked() ? ".html" : ".txt";
                                    String strFileName = String.valueOf(filename.getText());
                                    saveFile(strFileName, htmlButton.isChecked());
                                    sendNoIntent(strFileName + fileExtension, strEmail);
                                    Toast.makeText(context, "email sent", Toast.LENGTH_SHORT).show();
                                    dismissDialog(dialog);
                                } else {
                                    Toast.makeText(context, "illegal email address", Toast.LENGTH_SHORT).show();
                                }
                            }
                        });

                        focused_widget_num = 1;

                        View.OnClickListener leftListener = new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                views[focused_widget_num].setBackgroundColor(ContextCompat.getColor(context, R.color.white));
                                focused_widget_num = (focused_widget_num + 1) % views.length;
                                views[focused_widget_num].setBackgroundColor(ContextCompat.getColor(context, R.color.yellow));
                            }
                        };

                        View.OnClickListener middleListener = new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if (focused_widget_num < 2) {
                                    //Radio buttons
                                    ((RadioButton) views[focused_widget_num]).setChecked(true);
                                } else if (focused_widget_num < 4) {
                                    //Edit Texts
                                    InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                                    inputMethodManager.toggleSoftInputFromWindow(views[focused_widget_num].getApplicationWindowToken(), InputMethodManager.SHOW_FORCED, 0);
                                    views[focused_widget_num].requestFocus();
                                    getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
                                } else {
                                    sendButton.performClick();
                                }
                            }
                        };

                        View.OnClickListener rightListener = new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                hideKeyboardAndSuggestions();
                                dismissDialog(dialog);
                            }
                        };

                        initDialogButtons(dialog, leftListener, middleListener, rightListener);

                        dialog.show();

                    }
                }

        );
    }

    private void setupSave() {
        save = (ImageButton) findViewById(R.id.save);

        save.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final Dialog dialog = new Dialog(context);
                        dialog.setContentView(R.layout.save_dialog);
                        dialog.setTitle("Enter file name:");
                        dialog.show();

                        final RadioButton rb_html = (RadioButton) dialog.findViewById(R.id.rb_html);
                        final RadioButton rb_txt = (RadioButton) dialog.findViewById(R.id.rb_txt);
                        final EditText filename = (EditText) dialog.findViewById(R.id.filename_text);
                        if (openFileName != null) {
                            filename.setText(openFileName);
                            filename.setSelection(filename.getText().length());
                        }
                        final Button b_save = (Button) dialog.findViewById(R.id.save_button);

                        //prevent focus move on enter
                        filename.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                            @Override
                            public boolean onEditorAction(TextView v, int actionId,
                                                          KeyEvent event) {
                                if (event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) {
                                    return true;
                                }
                                return false;
                            }
                        });

                        final View[] views = new View[4];
                        views[0] = rb_html;
                        views[1] = rb_txt;
                        views[2] = filename;
                        views[3] = b_save;

                        //initial colors
                        rb_html.setBackgroundColor(ContextCompat.getColor(context, R.color.white));
                        rb_txt.setBackgroundColor(ContextCompat.getColor(context, R.color.yellow));
                        for (int i = 2; i < views.length; i++)
                            views[i].setBackgroundColor(ContextCompat.getColor(context, R.color.white));

                        b_save.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if (filename.getText().length() == 0) {
                                    Toast.makeText(context, "File name cannot be empty", Toast.LENGTH_LONG).show();
                                    return;
                                }
                                saveFile(String.valueOf(filename.getText()), rb_html.isChecked());
                                Toast.makeText(context, "File saved", Toast.LENGTH_LONG).show();
                                if (isExiting)
                                    onBackPressed();
                                else {
                                    hideKeyboardAndSuggestions();
                                    dismissDialog(dialog);
                                }
                            }
                        });

                        focused_widget_num = 1;

                        View.OnClickListener leftListener = new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                views[focused_widget_num].setBackgroundColor(ContextCompat.getColor(context, R.color.white));
                                focused_widget_num = (focused_widget_num + 1) % views.length;
                                views[focused_widget_num].setBackgroundColor(ContextCompat.getColor(context, R.color.yellow));
                            }
                        };

                        View.OnClickListener middleListener = new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if (focused_widget_num < 2) {
                                    //Radio buttons
                                    ((RadioButton) views[focused_widget_num]).setChecked(true);
                                } else if (focused_widget_num < 3) {
                                    //Edit Text
                                    InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                                    inputMethodManager.toggleSoftInputFromWindow(views[focused_widget_num].getApplicationWindowToken(), InputMethodManager.SHOW_FORCED, 0);
                                    views[focused_widget_num].requestFocus();
                                    getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
                                } else {
                                    b_save.performClick();
                                }
                            }
                        };

                        View.OnClickListener rightListener = new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                isExiting = false;
                                hideKeyboardAndSuggestions();
                                dismissDialog(dialog);
                            }
                        };

                        initDialogButtons(dialog, leftListener, middleListener, rightListener);

                    }
                }

        );
    }

    private static int countLines(String str) {
        if (str.length() == 0)
            return 0;
        String[] lines = str.split("\r\n|\r|\n");
        return lines.length;
    }

    public static int countWords(String s) {
        String trim = s.trim();
        if (trim.isEmpty())
            return 0;
        return trim.split("\\W+").length;
    }

    private void setupStatistics() {
        statistics = (ImageButton) findViewById(R.id.statistics);

        statistics.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String chars = String.valueOf(knife.getText().length());
                String lines = String.valueOf(countLines(knife.getText().toString()));
                String words = String.valueOf(countWords(knife.getText().toString()));

                //for testing
                statistics_message = words + " words, " + lines + " lines, " + chars + " chars";

                Toast.makeText(context, words + " words, " + lines + " lines, " + chars + " chars", Toast.LENGTH_LONG).show();
            }
        });
    }


    public ArrayList<String> getFilesInDirPath() {
        ArrayList<String> $ = new ArrayList<>();
        File folder = new File(dirPath);
        File[] listOfFiles = folder.listFiles();

        for (int i = 0; i < listOfFiles.length; i++) {
            if (listOfFiles[i].isFile()) {
                $.add(listOfFiles[i].getName());
            }
        }

        return $;
    }

    String getExtension(String fileName) {
        String extension = "";

        int i = fileName.lastIndexOf('.');
        if (i > 0) {
            extension = fileName.substring(i + 1);
        }
        return extension;
    }

    private String readFile(String filePath) {
        String ret = "";

        try {
            InputStream inputStream = new FileInputStream(filePath);

            if (inputStream != null) {
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String receiveString = "";
                StringBuilder stringBuilder = new StringBuilder();

                while ((receiveString = bufferedReader.readLine()) != null) {
                    stringBuilder.append(receiveString + '\n');
                }

                inputStream.close();
                ret = stringBuilder.toString();
            }
        } catch (FileNotFoundException e) {
            Log.e("login activity", "File not found: " + e.toString());
        } catch (IOException e) {
            Log.e("login activity", "Can not read file: " + e.toString());
        }
        return ret;
    }

    private void setupOpen() {
        open = (ImageButton) findViewById(R.id.open);


        open.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog = new Dialog(context);
                dialog.setContentView(R.layout.font_dialog);
                dialog.setTitle("Choose file");

                listDialogFonts = (ListView) dialog.findViewById(R.id.listFonts);
                final ArrayList<String> files = getFilesInDirPath();
                ArrayAdapter<String> adapter = new ArrayAdapter<>(context, R.layout.contacts_select_dialog_item, files);

                listDialogFonts.setAdapter(adapter);
                listDialogFonts.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
                listDialogFonts.setItemChecked(0, true);

                // "Go Down" button - scrolls down in the list of contacts
                View.OnClickListener leftListener = listScrollListener;

                // "Select" button - select the currently checked item in the options list
                View.OnClickListener middleListener = new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        int currentCheckedPosition = listDialogFonts.getCheckedItemPosition();
                        String fileName = files.get(currentCheckedPosition);
                        String filePath = dirPath + "/" + fileName;
                        if (getExtension(fileName).equals("txt")) {
                            knife.setText(readFile(filePath));
                        } else {
                            knife.fromHtml(readFile(filePath));
                        }
                        dismissDialog(dialog);
                    }
                };

                View.OnClickListener rightListener = new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dismissDialog(dialog);

                    }
                };

                initDialogButtons(dialog, leftListener, middleListener, rightListener);
                dialog.show();
            }
        });
    }

    private void setupDelete() {
        delete = (ImageButton) findViewById(R.id.delete);

        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog = new Dialog(context);
                dialog.setContentView(R.layout.font_dialog);
                dialog.setTitle("Choose file");

                listDialogFonts = (ListView) dialog.findViewById(R.id.listFonts);
                final ArrayList<String> files = getFilesInDirPath();
                ArrayAdapter<String> adapter = new ArrayAdapter<>(context, R.layout.contacts_select_dialog_item, files);

                listDialogFonts.setAdapter(adapter);
                listDialogFonts.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
                listDialogFonts.setItemChecked(0, true);

                // "Go Down" button - scrolls down in the list of contacts
                View.OnClickListener leftListener = listScrollListener;

                // "Select" button - select the currently checked item in the options list
                View.OnClickListener middleListener = new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        int currentCheckedPosition = listDialogFonts.getCheckedItemPosition();
                        String filePath = dirPath + "/" + files.get(currentCheckedPosition);
                        File f = new File(filePath);
                        if (f.delete()) {
                            Toast.makeText(context, files.get(currentCheckedPosition) + " is deleted", Toast.LENGTH_LONG).show();
                            dismissDialog(dialog);
                        } else {
                            Toast.makeText(context, "file is not deleted", Toast.LENGTH_LONG).show();
                        }

                    }
                };

                View.OnClickListener rightListener = new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dismissDialog(dialog);

                    }
                };

                initDialogButtons(dialog, leftListener, middleListener, rightListener);
                dialog.show();
            }
        });
    }

    private void setupLink() {
        link = (ImageButton) findViewById(R.id.link);

        link.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!knife.containLink(knife.getSelectionStart(), knife.getSelectionEnd()))
                    showLinkDialog(knife.getSelectionStart(), knife.getSelectionEnd());
                else
                    knife.link(null , knife.getSelectionStart(), knife.getSelectionEnd());
            }
        });

    }

    private void showLinkDialog(final int start, final int end) {
        final Dialog dialog = new Dialog(context);
        dialog.setContentView(R.layout.dialog_link);
        dialog.setTitle("Insert link:");
        dialog.show();

        final EditText link = (EditText) dialog.findViewById(R.id.link_edittext);
        final Button b_ok = (Button) dialog.findViewById(R.id.ok_button);

        link.setImeOptions(EditorInfo.IME_ACTION_DONE);
        //prevent focus move on enter
        link.setOnEditorActionListener(new TextView.OnEditorActionListener() {

            @Override
            public boolean onEditorAction(TextView v, int actionId,
                                          KeyEvent event) {
                if (event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) {
                    return true;
                }
                return false;
            }
        });

        b_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String linktxt = link.getText().toString().trim();
                if (TextUtils.isEmpty(linktxt)) {
                    Toast.makeText(context, "link cannot be empty", Toast.LENGTH_LONG).show();
                    return;
                }
                if(!(linktxt.startsWith("http://") || linktxt.startsWith("https://"))){
                    if(linktxt.startsWith("www."))
                        linktxt = "http://"+linktxt;
                    else
                        linktxt = "http://www."+linktxt;

                }
                // When KnifeText lose focus, use this method
                knife.link(linktxt, start, end);
                dismissDialog(dialog);
            }
        });

        final View[] views = new View[2];
        views[0] = link;
        views[1] = b_ok;

        link.setBackgroundColor(ContextCompat.getColor(context, R.color.yellow));
        b_ok.setBackgroundColor(ContextCompat.getColor(context, R.color.white));
        focused_widget_num = 0;


        // "Go Down" button - scrolls down in the list of contacts
        View.OnClickListener leftListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                views[focused_widget_num].setBackgroundColor(ContextCompat.getColor(context, R.color.white));
                focused_widget_num = (focused_widget_num + 1) % views.length;
                views[focused_widget_num].setBackgroundColor(ContextCompat.getColor(context, R.color.yellow));

            }
        };

        // "Select" button - select the currently checked item in the options list
        View.OnClickListener middleListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (focused_widget_num == 0) {
                    InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    inputMethodManager.toggleSoftInputFromWindow(link.getApplicationWindowToken(), InputMethodManager.SHOW_FORCED, 0);
                    link.requestFocus();
                    getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
                } else {
                    b_ok.performClick();
                }
            }
        };

        View.OnClickListener rightListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboardAndSuggestions();
                dismissDialog(dialog);
            }
        };

        initDialogButtons(dialog, leftListener, middleListener, rightListener);

    }

    private void unregisterFromBT() {
        for (int i = 0; i < navSimpleInputs.length; ++i) {
            if(CursorApplication.getInstance().dispatcher != null)
                CursorApplication.getInstance().dispatcher.setInputToDispatch(i, null);
        }
    }

    private void registerToBt() {

        navSimpleInputs[0] = new ClickOnlyView(this, fab_right);
        navSimpleInputs[1] = new ClickOnlyView(this, fab_middle);
        navSimpleInputs[2] = new ClickOnlyView(this, fab_left);

        for (int i = 0; i < navSimpleInputs.length; ++i) {
            if(CursorApplication.getInstance().dispatcher != null)
                CursorApplication.getInstance().dispatcher.setInputToDispatch(i, navSimpleInputs[i]);
        }
    }

    public void setTestMode(boolean testMode) {
        this.testMode = testMode;
    }

    public KnifeText getKnife() {
        return knife;
    }

    public int getRed() {
        return red;
    }

    public String getStatisticsMessage(){
        return statistics_message;
    }
}