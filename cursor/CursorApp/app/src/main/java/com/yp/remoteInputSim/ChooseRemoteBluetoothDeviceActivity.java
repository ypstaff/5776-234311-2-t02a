package com.yp.remoteInputSim;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.yp.cursor.R;

import java.util.ArrayList;
import java.util.List;

public class ChooseRemoteBluetoothDeviceActivity extends AppCompatActivity {

    private BluetoothAdapter myBTAdapter;
    private BluetoothDevice remoteDevice;
    private List<BluetoothDevice> pairedDevices;
    private ListView lv_btDevices;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_choose_remote_bluetooth_device);

        myBTAdapter = BluetoothAdapter.getDefaultAdapter();
        if (myBTAdapter == null) {
            Toast.makeText(getApplicationContext(), "Oops.. Something went terribly wrong..", Toast.LENGTH_SHORT).show();
            finish();
        }

        pairedDevices = new ArrayList<>();
        this.lv_btDevices = (ListView) findViewById(R.id.lv_btDevices);
        list();
    }

    public void list() {

        ArrayList<String> list = new ArrayList<>();
        for (BluetoothDevice bt : myBTAdapter.getBondedDevices()) {
            list.add(bt.getName());
            pairedDevices.add(bt);
        }

        final ArrayAdapter<String> adapter = new ArrayAdapter<>
                (this, android.R.layout.simple_list_item_1, list);
        lv_btDevices.setAdapter(adapter);
        lv_btDevices.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                Toast.makeText(ChooseRemoteBluetoothDeviceActivity.this, pairedDevices.get(position).getName() + "", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(getApplicationContext(), InputSimActivity.class);
                intent.putExtra(BluetoothDevice.EXTRA_DEVICE, pairedDevices.get(position).getAddress());
                startActivity(intent);
            }
        });
    }

}
