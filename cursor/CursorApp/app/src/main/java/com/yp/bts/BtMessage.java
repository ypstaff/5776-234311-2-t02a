package com.yp.bts;

import android.support.annotation.NonNull;
import android.util.Log;

import java.security.InvalidParameterException;

/**
 * Represent types of incoming messages containing inputs or connection controls.
 *
 * @author Oded
 * @version 10/06/2016
 */
public enum BtMessage {
    SingleLevelInput(0) {
        @Override
        public void handle(@NonNull byte[] buffer, @NonNull BluetoothReceiver receiver) {
            receiver.processSingleLevelInput(buffer[1]);
        }
    },

    MultiLevelInput(1) {
        @Override
        public void handle(@NonNull byte[] buffer, @NonNull BluetoothReceiver receiver) {
            Log.d(BT_MSG_TAG, "handle: Handling Multilevel Input - NOT IMPLEMENTED: ignoring");
        }
    },

    Terminate(-1) {
        @Override
        public void handle(@NonNull byte[] buffer, @NonNull BluetoothReceiver receiver) {
            Log.d(BT_MSG_TAG, "handle: Handling Terminate connection");
            receiver.stop(false);
        }
    };

    /* CONSTANTS */
    public final String BT_MSG_TAG = "BT MSG";

    /* MEMBERS */
    public final int opcode;

    /**
     * Private constructor.
     *
     * @param opcode of the matching message represented by the object.
     */
    BtMessage(int opcode) {
        this.opcode = opcode;
    }

    /**
     * Get the instance of an BtMessage that matches the given opcode.
     *
     * @param opcode by which to look for a matching BtMessage
     * @return BtMessage object with matching opcode - if exists.
     */
    public static BtMessage of(int opcode) {
        for (BtMessage msg : BtMessage.values()) {
            if (msg.opcode == opcode) return msg;
        }

        throw new InvalidParameterException("No such opcode: " + opcode);
    }


    /**
     * Specific handling method for a message of this opcode's type.
     *
     * @param buffer   as received from socket, containing message to be handled, including the
     *                 header (opcode field).
     * @param receiver to delegate any specific input handling.
     */
    abstract public void handle(@NonNull byte[] buffer, @NonNull BluetoothReceiver receiver);
} // end enum BtMessage
