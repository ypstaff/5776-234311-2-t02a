/*
 * Copyright (C) The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.yp.cursor.facedetect;

import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.PointF;
import android.preference.PreferenceManager;
import android.util.Log;

import com.google.android.gms.vision.face.Face;
import com.yp.classes.Constants;
import com.yp.cursor.CursorApplication;
import com.yp.cursor.R;

/**
 * Graphic instance for rendering face position, orientation, and landmarks within an associated
 * graphic overlay view.
 * @author Avner Elizarov
 */
class FaceGraphic extends GraphicOverlay.Graphic {
    private static final float FACE_POSITION_RADIUS = 10.0f;
    private static final float ID_TEXT_SIZE = 40.0f;
    private static final float ID_Y_OFFSET = 50.0f;
    private static final float ID_X_OFFSET = -50.0f;
    private static final float BOX_STROKE_WIDTH = 5.0f;


    public static final int MIN_SCREEN_X = 0;
    public static final int MIN_SCREEN_Y = 0;

    private static final int COLOR_CHOICES[] = {
            Color.RED,
            Color.GREEN,
    };
    public static final int MARGIN_FROM_BOTTOM = 20;
    public static final int MARGIN_FROM_RIGHT = 20;
    public static final int Y_MIDDLE_OFFSET = 100;
    public static final int X_MIDDLE_OFFSET = 50;
    public static final int MOVEMENT_TO_DIFF_DIVIDER = 15;
    public static final int MINIMUM_FACE_MOVEMENT = 10;
    public static final int PRIOR_CENTER_COEFFICIENT = 3;
    public static final int COEFFICIENT_SUM = 4;
    private SharedPreferences sharedPref;
    private static final String TAG = "Face Graphic";

    private Paint mFacePositionPaint;
    private Paint mIdPaint;
    private Paint mBoxPaint;

    private volatile Face mFace;

    /**
     * previous location of the center of the user's face.
     */
    private PointF priorFaceCenter;

    /* cursor speed parameters */
    private float xMovement;
    private float yMovement;

    /**
     * previous location of the cursor.
     */
    private PointF priorCursorLocation;

    private Bitmap cursorImage;

    private EyeStateMachine leftEye;
    private EyeStateMachine rightEye;
    private PointF initialFaceCenter;
    private Point screenDimensions;

    private boolean shouldMoveCursor = false;
    private int orientation;

    FaceGraphic(GraphicOverlay overlay, Point screenDimensions) {
        super(overlay);
        this.screenDimensions = screenDimensions;
        sharedPref = PreferenceManager.getDefaultSharedPreferences(CursorApplication.getInstance());
        leftEye = new EyeStateMachine(EyeStateMachine.Eye.LEFT);
        rightEye = new EyeStateMachine(EyeStateMachine.Eye.RIGHT);
        BitmapFactory.Options options = new BitmapFactory.Options();
        cursorImage = BitmapFactory.decodeResource(overlay.getResources(), R.drawable.face_cursor, options);
        priorCursorLocation = new PointF(screenDimensions.x / 2, screenDimensions.y / 2);
        orientation = overlay.getResources().getConfiguration().orientation;

        xMovement = getMinXMovement();
        yMovement = getMinYMovement();
        initPaints();
    }

    private void initPaints() {
        mFacePositionPaint = new Paint();
        mIdPaint = new Paint();
        mIdPaint.setTextSize(ID_TEXT_SIZE);
        mBoxPaint = new Paint();
        mBoxPaint.setStyle(Paint.Style.STROKE);
        mBoxPaint.setStrokeWidth(BOX_STROKE_WIDTH);
        setPaintsColor(COLOR_CHOICES[0]);
    }

    // used for testing since paint class uses native and fucks up the test.
    FaceGraphic(GraphicOverlay overlay, Point screenDimensions, Paint paint, int orientation, SharedPreferences sharedPref) {
        super(overlay);
        this.screenDimensions = screenDimensions;
        this.orientation = orientation;
        this.sharedPref = sharedPref;
        leftEye = new EyeStateMachine(EyeStateMachine.Eye.LEFT, paint, paint);
        rightEye = new EyeStateMachine(EyeStateMachine.Eye.RIGHT, paint, paint);

        BitmapFactory.Options options = new BitmapFactory.Options();
        cursorImage = BitmapFactory.decodeResource(overlay.getResources(), R.drawable.face_cursor, options);
        xMovement = getMinXMovement();
        yMovement = getMinYMovement();

        priorCursorLocation = new PointF(screenDimensions.x / 2, screenDimensions.y / 2);

        mBoxPaint = paint;
        mIdPaint = paint;
        mFacePositionPaint = paint;
    }

    /**
     * Updates the face instance from the detection of the most recent frame.  Invalidates the
     * relevant portions of the overlay to trigger a redraw.
     */
    void updateFace(Face face) {
        mFace = face;

//        if(initialFaceCenter == null){
//
//            float xCenter = translateX(face.getPosition().x + face.getWidth() / 2);
//            float yCenter = translateY(face.getPosition().y + face.getHeight() / 2);
//            initialFaceCenter = new Point((int)xCenter,(int)yCenter);
//            priorFaceCenter = new Point(initialFaceCenter.x, initialFaceCenter.y);
//        }
        postInvalidate();
    }

    /**
     * Draws the face annotations for position on the supplied canvas.
     * Also draws the cursor.
     */
    @Override
    public void draw(Canvas canvas) {

        Face face = mFace;
        if (face == null) {
            return;
        }
        float xCenter = translateX(face.getPosition().x + face.getWidth() / 2);
        float yCenter = translateY(face.getPosition().y + face.getHeight() / 2);
        PointF center = new PointF(xCenter,yCenter);

        // draw more information abouts the face if developer mode is on
        drawMoreInformation(canvas, face, xCenter, yCenter);
        // Draws a bounding box around the face.
        drawFace(canvas, face, xCenter, yCenter);

        if(shouldMoveCursor){
            updateCursorLocation(canvas, face, center);
        }else{
            // initial face location should be in the middle of the screen
            if(faceInTheMiddle(xCenter, yCenter)){
                shouldMoveCursor = true;
                initialFaceCenter = new PointF((int)xCenter,(int)yCenter);
                priorFaceCenter = new PointF(initialFaceCenter.x, initialFaceCenter.y);
                setPaintsColor(COLOR_CHOICES[1]); // color is now green - everything is o.k
            }

        }
    }

    private void drawMoreInformation(Canvas canvas, Face face, float xCenter, float yCenter) {
        if(shouldShowMoreInformation()){

            // Draws a circle at the position of the detected face, with the face's track id below.
            canvas.drawCircle(xCenter, yCenter, FACE_POSITION_RADIUS, mFacePositionPaint);
//            canvas.drawText("id: " + mFaceId, xCenter + ID_X_OFFSET, yCenter + ID_Y_OFFSET, mIdPaint);
            canvas.drawText("x: " + String.format("%.2f", xCenter) + ",y: " + String.format("%.2f", yCenter), xCenter + ID_X_OFFSET, yCenter + ID_Y_OFFSET, mIdPaint);
            canvas.drawText("happiness: " + String.format("%.2f", face.getIsSmilingProbability()), xCenter - ID_X_OFFSET, yCenter - ID_Y_OFFSET, mIdPaint);
            canvas.drawText("right eye: " + String.format("%.2f", face.getIsRightEyeOpenProbability()), xCenter + ID_X_OFFSET * 2, yCenter + ID_Y_OFFSET * 2, mIdPaint);
            canvas.drawText("left eye: " + String.format("%.2f", face.getIsLeftEyeOpenProbability()), xCenter - ID_X_OFFSET * 2, yCenter - ID_Y_OFFSET * 2, mIdPaint);
        }
    }

    private boolean shouldShowMoreInformation() {
        return sharedPref.getBoolean(CursorApplication.getInstance().getString(R.string.pref_key_show_developer), false);
    }

    private void setPaintsColor(int colorChoice) {
        mBoxPaint.setColor(colorChoice);
        mIdPaint.setColor(colorChoice);
        mFacePositionPaint.setColor(colorChoice);
    }

    private void updateCursorLocation(Canvas canvas, Face face, PointF center) {
        // didn't move so much on x
        if(Math.abs(center.x - priorFaceCenter.x) < MINIMUM_FACE_MOVEMENT ){
            center.x = priorFaceCenter.x;
        }
        // didn't move so much on y
        if(Math.abs(center.y - priorFaceCenter.y) < MINIMUM_FACE_MOVEMENT){
            center.y = priorFaceCenter.y;
        }
        // Smooth new center compared to old center
        center.x = (center.x + PRIOR_CENTER_COEFFICIENT * priorFaceCenter.x) / COEFFICIENT_SUM;
        center.y = (center.y + PRIOR_CENTER_COEFFICIENT * priorFaceCenter.y) / COEFFICIENT_SUM;
        moveCursor(center, canvas);
        priorFaceCenter.set(center.x, center.y);
        leftEye.onNewProbability(face.getIsLeftEyeOpenProbability(), priorCursorLocation, canvas);
        rightEye.onNewProbability(face.getIsRightEyeOpenProbability(), priorCursorLocation, canvas);
    }

    private boolean faceInTheMiddle(float xCenter, float yCenter) {
        int screenMiddleX = screenDimensions.x / 2;
//        int screenMiddleY = screenDimensions.y / 2;
        if(xCenter < screenMiddleX  - X_MIDDLE_OFFSET || xCenter > screenMiddleX  + X_MIDDLE_OFFSET){
            return false;
        }
//        if(yCenter < screenMiddleY  - Y_MIDDLE_OFFSET || yCenter > screenMiddleY  + Y_MIDDLE_OFFSET){
//            return false;
//        }
        return true;
    }

    private void drawFace(Canvas canvas, Face face, float xCenter, float yCenter) {
        float xOffset = scaleX(face.getWidth() / 2.0f);
        float yOffset = scaleY(face.getHeight() / 2.0f);
        float left = xCenter - xOffset;
        float top = yCenter - yOffset;
        float right = xCenter + xOffset;
        float bottom = yCenter + yOffset;
        canvas.drawRect(left, top, right, bottom, mBoxPaint);
    }

    /**
     * move cursor according to new face center
     * @param newFaceCenter - new center of face
     * @param canvas - canvas to draw on
     */
    private void moveCursor(PointF newFaceCenter, Canvas canvas) {

        Log.d(TAG, "initial face center: " + initialFaceCenter);
        Log.d(TAG, "new face center: " + newFaceCenter);
        Log.d(TAG, "old face center: " + priorFaceCenter);

        float newCursorX = updateCursorX(newFaceCenter.x, priorCursorLocation.x); /* get new x coordinate */
        float newCursorY = updateCursorY(newFaceCenter.y, priorCursorLocation.y); /* get new y coordinate */

        Log.v("face recognition", "old point: x-" + priorCursorLocation.x + ", y-" + priorCursorLocation.y);
        Log.v("face recognition", "new point: x-" + newCursorX + ", y-" + newCursorY);

        canvas.drawBitmap(cursorImage, newCursorX, newCursorY, null);
        priorCursorLocation.set(newCursorX, newCursorY);
    }

    /**
     * update y coordinate of cursor.
     * @param newFaceCenterY - new y coordinate of face center.
     * @param oldCursorY - previous y coordinate of cursor.
     * @return new y coordinate of cursor.
     */
    private float updateCursorY(float newFaceCenterY, float oldCursorY) {

        // face moved vertically, so add some speed to cursor.
        if(newFaceCenterY == priorFaceCenter.y){
            yMovement = getMinYMovement();
            return oldCursorY;
        }else{
            yMovement += getYSpeedDiff();
        }
        boolean isMoveToSameDirection = Math.signum(newFaceCenterY - initialFaceCenter.y) == Math.signum(newFaceCenterY - priorFaceCenter.y);
        float cursorDiff = isMoveToSameDirection ? newFaceCenterY - priorFaceCenter.y : 0;
        float newCursorY = oldCursorY + cursorDiff* yMovement;

        // check cursor hasn't exceeded screen limits
        if (newCursorY < MIN_SCREEN_Y) {
            newCursorY = MIN_SCREEN_Y;
        }
        if (newCursorY > screenDimensions.y- MARGIN_FROM_BOTTOM) {
            newCursorY = screenDimensions.y- MARGIN_FROM_BOTTOM;
        }
        Log.v(TAG, "screenDimensions.y: " + screenDimensions.y);
        return newCursorY;
    }

    int getMinYMovement() {
        if(isOrientationPortrait()){
            return sharedPref.getInt(CursorApplication.getInstance().getString(R.string.pref_key_y_sensitivity), Constants.FaceCursor.DEFAULT_Y_MOVEMENT_PORTRAIT);
        }else{
            return sharedPref.getInt(CursorApplication.getInstance().getString(R.string.pref_key_x_sensitivity), Constants.FaceCursor.DEFAULT_X_MOVEMENT_PORTRAIT);
        }
    }

    float getYSpeedDiff() {
        return (float)getMinYMovement() / MOVEMENT_TO_DIFF_DIVIDER;
    }

    /**
     * update x coordinate of cursor.
     * @param newFaceCenterX - new x coordinate of face center.
     * @param oldCursorX - previous x coordinate of cursor.
     * @return new x coordinate of cursor.
     */
    private float updateCursorX(float newFaceCenterX, float oldCursorX) {

        // face moved horizontally, so add some speed to cursor.
        if(newFaceCenterX == priorFaceCenter.x){
            xMovement = getMinXMovement();
            return oldCursorX;
        }else{
            xMovement += getXSpeedDiff();
        }
        boolean isMoveToSameDirection = Math.signum(newFaceCenterX - initialFaceCenter.x) == Math.signum(newFaceCenterX - priorFaceCenter.x);

        // if we ar moving away from the initial face center them move the cursor. otherwise, leave it where it is.
        float cursorDiff = isMoveToSameDirection ? newFaceCenterX - priorFaceCenter.x : 0;
        // calculate x diff from face and consider the speed.
        float newCursorX = oldCursorX + cursorDiff * xMovement;

        // check cursor hasn't exceeded screen limits
        if (newCursorX < MIN_SCREEN_X) {
            newCursorX = MIN_SCREEN_X;
        }
        if (newCursorX > screenDimensions.x - MARGIN_FROM_RIGHT) {
            newCursorX = screenDimensions.x - MARGIN_FROM_RIGHT;
        }
        return newCursorX;
    }

    int getMinXMovement() {
        if(isOrientationPortrait()){
            return sharedPref.getInt(CursorApplication.getInstance().getString(R.string.pref_key_x_sensitivity), Constants.FaceCursor.DEFAULT_X_MOVEMENT_PORTRAIT);
        }else{
            return sharedPref.getInt(CursorApplication.getInstance().getString(R.string.pref_key_y_sensitivity), Constants.FaceCursor.DEFAULT_Y_MOVEMENT_PORTRAIT);
        }
    }

    private boolean isOrientationPortrait() {
        return orientation == Configuration.ORIENTATION_PORTRAIT;
    }

    float getXSpeedDiff() {
        return (float)getMinXMovement() / MOVEMENT_TO_DIFF_DIVIDER;
    }

    public boolean shouldShowCursor() {
        return shouldMoveCursor;
    }

    public PointF getCursorLocation() {
        return priorCursorLocation;
    }

}

