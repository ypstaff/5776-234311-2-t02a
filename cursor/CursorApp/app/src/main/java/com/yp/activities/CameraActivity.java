/*
*
* *@author Matan Revivo
*
 */

package com.yp.activities;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.hardware.Camera;
import android.hardware.Camera.CameraInfo;
import android.hardware.Camera.ErrorCallback;
import android.hardware.Camera.PictureCallback;
import android.hardware.Camera.ShutterCallback;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Surface;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.yp.classes.CameraPreview;
import com.yp.cursor.CursorApplication;
import com.yp.cursor.R;
import com.yp.cursor.SingleInput;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

/*
*
* *@author Matan Revivo
*
 */

public class CameraActivity extends Activity {

    ImageView image;
    Activity context;
    CameraPreview preview;
    Camera camera;
    ImageView fotoButton;
    ImageView back;
    LinearLayout progressLayout;
    String path = "/sdcard/DCIM/Camera/";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera);

        context = this;
        fotoButton = (ImageView) findViewById(R.id.imageView_foto);
        back = (ImageView) findViewById(R.id.iv_back);
        image = (ImageView) findViewById(R.id.imageView_photo);
        progressLayout = (LinearLayout) findViewById(R.id.progress_layout);

        preview = new CameraPreview(this,
                (SurfaceView) findViewById(R.id.KutCameraFragment));
        FrameLayout frame = (FrameLayout) findViewById(R.id.preview);
        frame.addView(preview);
        preview.setKeepScreenOn(true);
        fotoButton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                try {
                    takeFocusedPicture();
                } catch (Exception e) {

                }
                fotoButton.setClickable(false);
                progressLayout.setVisibility(View.VISIBLE);
            }
        });
        back.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (camera != null) {
                    camera = null;
                }
                finish();
            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (camera != null) {
            camera = null;
        }
        unregisterFromBT();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(camera==null){
            camera = Camera.open();
            camera.startPreview();
            camera.setErrorCallback(new ErrorCallback() {
                public void onError(int error, Camera mcamera) {

                    camera.release();
                    camera = Camera.open();
                    Log.d("Camera died", "error camera");

                }
            });
        }
        if (camera != null) {
            if (Build.VERSION.SDK_INT >= 14)
                setCameraDisplayOrientation(context,
                        CameraInfo.CAMERA_FACING_BACK, camera);
            preview.setCamera(camera);
        }
        registerToBt();
    }

    private void unregisterFromBT() {
        CursorApplication.getInstance().dispatcher.setInputToDispatch(0, null);
        CursorApplication.getInstance().dispatcher.setInputToDispatch(1, null);
    }

    private void registerToBt() {
        CursorApplication.getInstance().dispatcher.setInputToDispatch(0, new SingleInput() {
            @Override
            public void onClick() {
                runOnUiThread(
                        new Runnable() {
                            @Override
                            public void run() {
                                back.callOnClick();
                            }
                        }
                );
            }

            @Override
            public void onLongPressStart() {}

            @Override
            public void onLongPressStop() {}

            @Override
            public void onDoubleClick() {}

            @Override
            public boolean isDoubleClickEnabled() {
                return false;
            }
        });
        CursorApplication.getInstance().dispatcher.setInputToDispatch(1, new SingleInput() {
            @Override
            public void onClick() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        fotoButton.callOnClick();
                    }
                });
            }

            @Override
            public void onLongPressStart() {}

            @Override
            public void onLongPressStop() {}

            @Override
            public void onDoubleClick() {}

            @Override
            public boolean isDoubleClickEnabled() {
                return false;
            }
        });
    }

    /**
     * By default the camera is landscape so you have to rotate it
     * in order use portrait
     * @param activity
     * @param cameraId
     * @param camera
     */
    private void setCameraDisplayOrientation(Activity activity, int cameraId,
                                             Camera camera) {
        CameraInfo info = new CameraInfo();
        Camera.getCameraInfo(cameraId, info);
        int rotation = activity.getWindowManager().getDefaultDisplay()
                .getRotation();
        int degrees = 0;
        switch (rotation) {
            case Surface.ROTATION_0:
                degrees = 0;
                break;
            case Surface.ROTATION_90:
                degrees = 90;
                break;
            case Surface.ROTATION_180:
                degrees = 180;
                break;
            case Surface.ROTATION_270:
                degrees = 270;
                break;
        }

        int result;
        if (info.facing == CameraInfo.CAMERA_FACING_FRONT) {
            result = (info.orientation + degrees) % 360;
            result = (360 - result) % 360; // compensate the mirror
        } else { // back-facing
            result = (info.orientation - degrees + 360) % 360;
        }
        camera.setDisplayOrientation(result);
    }


    Camera.AutoFocusCallback mAutoFocusCallback = new Camera.AutoFocusCallback() {
        @Override
        public void onAutoFocus(boolean success, Camera camera) {

            try{
                camera.takePicture(mShutterCallback, null, jpegCallback);
            }catch(Exception e){

            }

        }
    };

    ShutterCallback mShutterCallback = new ShutterCallback() {

        @Override
        public void onShutter() {
            // TODO Auto-generated method stub

        }
    };
    public void takeFocusedPicture() {
        camera.autoFocus(mAutoFocusCallback);

    }

    PictureCallback rawCallback = new PictureCallback() {
        public void onPictureTaken(byte[] data, Camera camera) {
            // Log.d(TAG, "onPictureTaken - raw");
        }
    };

    private byte[] BitmapToByteArray(Bitmap bmap){
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
        byte[] byteArray = stream.toByteArray();
        return byteArray;
    }

    /**
     * The function which called when you capture
     */
    PictureCallback jpegCallback = new PictureCallback() {
        @SuppressWarnings("deprecation")
        public void onPictureTaken(byte[] data, Camera camera) {
            FileOutputStream outStream = null;
            File mediaStorageDir = new File(path);
            if (!mediaStorageDir.exists()) {
                mediaStorageDir.mkdirs();
            }
            String timestamp =new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
            String filePath = path + timestamp + ".jpg";
            Bitmap realImage;
            final BitmapFactory.Options options = new BitmapFactory.Options();
            options.inSampleSize = 5;

            options.inPurgeable=true;                   //Tell to gc that whether it needs free memory, the Bitmap can be cleared

            options.inInputShareable=true;              //Which kind of reference will be used to recover the Bitmap data after being clear, when it will be used in the future

            realImage = BitmapFactory.decodeByteArray(data,0,data.length,options);
            realImage = rotate(realImage, 90);
            image.setImageBitmap(realImage);

            try {
                // Write to SD Card
                outStream = new FileOutputStream(filePath);
                outStream.write(BitmapToByteArray(realImage));
                outStream.close();
                Toast toast = Toast.makeText(context, "Picture saved: " + timestamp + ".jpg", Toast.LENGTH_LONG);
                toast.show();
            } catch (FileNotFoundException e) {
                Log.d("saved","problem1");
                e.printStackTrace();
            } catch (IOException e) {
                Log.d("saved","problem2");
                e.printStackTrace();
            } finally {
            }

            fotoButton.setClickable(true);
            camera.startPreview();
            progressLayout.setVisibility(View.GONE);
        }
    };


    /**
     * rotate source in angle degrees
     * @param source
     * @param angle
     * @return
     */
    public static Bitmap rotate(Bitmap source, int angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(),
                source.getHeight(), matrix, false);
    }

}
