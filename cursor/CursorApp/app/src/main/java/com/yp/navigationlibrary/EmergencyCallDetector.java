package com.yp.navigationlibrary;

/**
 * Created by sRoySvin on 12/8/2015.
 *
 * @author Roy Svinik
 */
public class EmergencyCallDetector {

    private State state = State.I;

    /* returns true if emergency call has been detected */
    public boolean next() {
        if(state == State.I) {
            state = State.N;
        } else {
            state = State.I;
        }
        return false;
    }

    /* returns true if emergency call has been detected */
    public boolean select() {
        if(state == State.N) {
            state = State.NS;
        } else {
            state = State.I;
        }
        return false;
    }

    /* returns true if emergency call has been detected */
    public boolean back() {
        if(state == State.NS) {
            state = State.I;
            return true;
        } else {
            state = State.I;
        }
        return false;
    }

    /* returns true if emergency call has been detected */
    public void init() {
        state = State.I;
    }

    /* states of the detector:
     * I: initial state, no part of the combination occurred
     * N: Next was long pressed
     * NS: Next and select were long pressed*/
    private enum State {
        I, N, NS
    }

}
