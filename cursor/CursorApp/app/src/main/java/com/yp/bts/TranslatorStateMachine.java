package com.yp.bts;

import android.util.Log;

import com.yp.SimpleInputEvent;

/**
 * Created by etayhn on 17-Apr-16.
 */
public class TranslatorStateMachine {
    /**
     * if the number of consecutive one bits is between clickThreshold (inclusive) and
     * longPressThreshold (exclusive), then a click event is triggered.
     */
    private static int clickThreshold = 1; // default: wait for two consecutive ones to trigger a click

    /**
     * if the number of consecutive one bits is greater than longPressThreshold (inclusive), then a
     * long press event is triggered.
     */
    private static int longPressThreshold = 4; // default: wait for at least four consecutive ones to trigger a click

    private int count = 0;

    public TranslatorStateMachine() {
        count = 0;
    }


    /**
     * Sets the click and long press thresholds. The method succeeds only if both thresholds are
     * positive and clickThreshold is smaller than longPressThreshold, since we require
     * 0 < clickThreshold < longPressThreshold
     *
     * @param clickThreshold     the desired click threshold
     * @param longPressThreshold the desired long press threshold
     * @return true if the new values were set, false otherwise.
     */
    public static boolean setThresholds(int clickThreshold, int longPressThreshold) {
        if (clickThreshold <= 0 || clickThreshold >= longPressThreshold) {
            Log.d("BTS_TRANSLATORS", "trying to set invalid thresholds");
            return false;
        }
        TranslatorStateMachine.clickThreshold = clickThreshold;
        TranslatorStateMachine.longPressThreshold = longPressThreshold;
        return true;
    }

    /**
     * resets the state machine to its initial state.
     * <p/>
     * If a long press was held, but then the state machine was reset, there might still be
     * someone waiting for the LONG_PRESS_UP event, so we trigger it.
     *
     * @return the event that should be triggered (LONG_PRESS_UP or NOP)
     */
    public SimpleInputEvent reset() {
        //Log.d("BTS_TRANSLATORS", "resetting translator");
        SimpleInputEvent eventToReturn = SimpleInputEvent.NOP;
        if (count >= longPressThreshold)
            eventToReturn = SimpleInputEvent.LONG_PRESS_UP;

        count = 0;
        return eventToReturn;
    }

    /**
     * Receives a new input bit, and returns whether or not to trigger a click or a long press.
     *
     * @param bit the next input bit
     * @return the event that should be triggered
     */
    public SimpleInputEvent changeState(boolean bit) {
        if (bit) {
            count++;
            if (count == longPressThreshold)
                return SimpleInputEvent.LONG_PRESS_DOWN;

            return SimpleInputEvent.NOP;
        }

        // otherwise, bit == 0
        SimpleInputEvent eventToReturn = SimpleInputEvent.NOP;
        if (count >= clickThreshold && count < longPressThreshold)
            eventToReturn = SimpleInputEvent.CLICK;
        else if (count > longPressThreshold)
            eventToReturn = SimpleInputEvent.LONG_PRESS_UP;
        count = 0;
        return eventToReturn;
    }

    public static int getClickThreshold() {
        return clickThreshold;
    }

    public static int getLongPressThreshold() {
        return longPressThreshold;
    }

}
