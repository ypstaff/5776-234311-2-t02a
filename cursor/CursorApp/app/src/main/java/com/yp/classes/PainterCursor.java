package com.yp.classes;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Point;
import android.graphics.Rect;

/**
 * @author Inbar Donag
 */


//////////////////////////////////////// Cursor ////////////////////////////////////////////////
public class PainterCursor
{
    // directions
    public static final int LEFT = 0, UPPER_LEFT = 1, UP = 2, UPPER_RIGHT = 3,
            RIGHT = 4, LOWER_RIGHT = 5, DOWN = 6, LOWER_LEFT = 7;

    private int currentDirection = RIGHT;
    private Point cursorPos = new Point(0,0);

    public Point getCursorPos() {
        return cursorPos;
    }
    public void setCursorPos(Point pos) { cursorPos = pos; }
    public int getCurrentDirection() {
        return currentDirection;
    }

    private Bitmap arrowPic;
    private Bitmap rotatedArrowBitmap;
    private int rotatedArrowDir;

    private int width;
    private int height;

    public PainterCursor (Bitmap pic, int picDir, int width, int height)
    {
        arrowPic = pic;
        rotatedArrowDir = picDir;
        rotatedArrowBitmap = pic;

        this.width = width;
        this.height = height;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////

    public void render(Canvas canvas)
    {
        rotateArrow();
        canvas.drawBitmap(rotatedArrowBitmap, null, getRect(cursorPos,30, canvas.getWidth(),canvas.getHeight()), null);
    }

    public void turnLeft()
    {
        // change direction of the stroke counterclockwise
        currentDirection--;
        if (currentDirection < 0) {
            currentDirection = 7;
        }
    }

    public void turnRight()
    {
        // change direction of the stroke clockwise
        currentDirection++;
        if (currentDirection > 7) {
            currentDirection = 0;
        }
    }

    public void advanceCursor()
    {
        int x = cursorPos.x;
        int y = cursorPos.y;

        switch (currentDirection) {
            case LEFT:
                if (x-1 < 0) return;
                cursorPos = new Point(x-1, y);
                break;
            case UPPER_LEFT:
                if (x-1 < 0 || y-1 < 0) return;
                cursorPos = new Point(x-1,y-1);
                break;
            case UP:
                if (y-1 < 0) return;
                cursorPos = new Point(x,y-1);
                break;
            case UPPER_RIGHT:
                if (y-1 < 0 || x+1 > width) return;
                cursorPos = new Point(x+1,y-1);
                break;
            case RIGHT:
                if (x+1 > width) return;
                cursorPos = new Point(x+1,y);
                break;
            case LOWER_RIGHT:
                if (y+1 > height || x+1 > width) return;
                cursorPos = new Point(x+1,y+1);
                break;
            case DOWN:
                if (y+1 > height) return;
                cursorPos = new Point(x,y+1);
                break;
            case LOWER_LEFT:
                if (y+1 > height || x-1 < 0) return;
                cursorPos = new Point(x-1,y+1);
                break;
        }

    }

    private void rotateArrow()
    {
        int d = getCurrentDirection();
        if (rotatedArrowDir == d)
            return;
        rotatedArrowDir = d;
        rotatedArrowBitmap = rotate(arrowPic, d*45);
    }

    private Bitmap rotate(Bitmap bmp, int angle)
    {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        if (angle%90 != 0)
            matrix.postScale((float)Math.sqrt(2), (float)Math.sqrt(2));
        return Bitmap.createBitmap(bmp, 0, 0, bmp.getWidth(), bmp.getHeight(), matrix, true);
    }

    private Rect getRect(Point p, int size, int width, int height)
    {
        return new Rect(Math.max(p.x - size, 0), Math.max(p.y - size, 0), Math.min(p.x + size, width) , Math.min(p.y+size, height));
    }
}