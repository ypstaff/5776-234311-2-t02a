package com.yp.cursor.onebutton;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.PixelFormat;
import android.graphics.Point;
import android.graphics.Rect;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.SurfaceHolder;

import com.yp.cursor.R;
import com.yp.cursor.SingleInput;
import com.yp.cursor.ViewInputHolder;

import java.util.Collections;
import java.util.List;

/**
 * An "overlay" that covers the layout underneath, and displays horizontal and vertical axes,
 * each moving separately (vertical first) for positioning a marker on the frame. After stopping
 * both axes, a marker will appear at their intersection, marking the spot where a "click"
 * event should be generated on the "top-most" layout currently underneath.
 *
 * @author Itay + Oded
 */
public class AxisMoverView extends ViewInputHolder {

    // CONSTANTS
    private static final int DEFAULT_SPEED = 5;
    private static final String AXIS_MOVER_TAG = "AXIS_MOVER";
    private static final long UI_UPDATE_DELAY_IN_MILLIS = 2L;
    public final SimpleTouchListener touchListener;

    // MEMBERS
    private Thread renderThread = null;
    private final SurfaceHolder holder;
    volatile boolean isRunning = false;

    private final Background bg;
    private final AxisStateMachine sm;

    /**
     * Constructor. Create a covering overlay for the given View (that is directly covered by
     * this AvisMoverView), upon which the "click" events will be generated once the marker is
     * positioned.
     *
     * @param ctx       of execution
     * (Itay + Oded)
     */
    public AxisMoverView(Context ctx) {
        super(ctx);
        Log.d(AXIS_MOVER_TAG, "New MOVER CREATED!");
        this.holder = getHolder();

        this.bg = new Background(ctx);
        this.bg.setDelta(PreferenceManager.getDefaultSharedPreferences(ctx).getInt(ctx.getString(R.string.pref_one_button_cursor_speed), DEFAULT_SPEED));
        this.sm = new AxisStateMachine(AxisStateMachine.SingleState.SET_X);
        this.sm.start();
        touchListener = new SimpleTouchListener();

        touchListener.setStateMachine(this.sm);

        setZOrderOnTop(true);
        holder.setFormat(PixelFormat.TRANSPARENT);
    }

    @Override
    public List<SingleInput> getInputs() {
        return Collections.singletonList((SingleInput) touchListener);
    }

    @Override
    public void run() { // Oded

        while (isRunning) {
            if (!holder.getSurface().isValid()) continue;

            Canvas canvas = holder.lockCanvas();
            final Rect canvasBounds = canvas.getClipBounds();

            // Update UI
            this.sm.setBackground(bg);
            this.sm.setCanvasBounds(canvasBounds);
            this.sm.doStep();

            // Re-Draw UI
//                Log.d("AXIS_MOVER", "bounds: " + canvas.getClipBounds());
            bg.drawToCanvas(canvas);
            holder.unlockCanvasAndPost(canvas);

            try {
                Thread.sleep(UI_UPDATE_DELAY_IN_MILLIS);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        sm.stop();
    }

    /**
     * Resume this axis-mover - moving the axes on the screen.
     */
    @Override
    public void start() { // Oded
        Log.d(AXIS_MOVER_TAG, "Resumed");
        isRunning = true;
        renderThread = new Thread(this);
        renderThread.start();
    }

    @Override
    public void setOrientation(int orientation, Point screenDimensions) {}

    /**
     * Pause this axis-mover - free resources and stop trying to update UI.
     */
    @Override
    public void stop() { // Oded
        Log.d(AXIS_MOVER_TAG, "Paused");
        isRunning = false; // will cause renderThread to exit its main loop

        while (true) {
            try {
                renderThread.join();
            } catch (InterruptedException e) {
                // nothing to do - if interrupted - stops anyway
            }
            break;
        }
    }

}


