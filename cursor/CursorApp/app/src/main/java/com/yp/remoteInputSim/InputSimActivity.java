package com.yp.remoteInputSim;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.yp.cursor.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

public class InputSimActivity extends AppCompatActivity implements ISimulatorActivity {

    // Constants
    public static final String JUNIT_TEST = "JUNIT_TEST";
    private static final String INPUT_SIM_TAG = "REMOTE_INPUT_SIMULATOR";
    private static final int MAX_RECONNECTION_ATTEMPTS = 20;
    private static final long RECONNECTION_DELAY_MILLIS = 500L;
    private static long SINGLE_CLICK_LENGTH = 200L; // milliseconds
    private static long SEND_INTERVAL = 100L; // milliseconds
    private static final int[] btn_ids = {
            R.id.tbtn_input1,
            R.id.tbtn_input2,
            R.id.tbtn_input3,
            R.id.tbtn_input4,
            R.id.tbtn_input5,
            R.id.tbtn_input6,
            R.id.tbtn_input7,
            R.id.tbtn_input8
    };

    // Widgets
    private final List<Button> btns = new ArrayList<>();
    private ToggleButton tbtn_toggleSend;
    private TextView tv_output;
    private TextView tv_lastSentValue;
    private OnTouchListener checkedChangeListener;
    private TextView tv_status;
//    private CheckBox cb_autoReconnect;

    // State members
    private byte singleLevelOutput = 0;
    private String simOutput = "00000000";
    private final Map<Integer, Integer> ids = new HashMap<>();

    private AtomicBoolean started = new AtomicBoolean(false);
    private InputSimulator simulator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_input_sim);

        // setup activity action bar icon
        final ActionBar actionBar = getSupportActionBar();
        if (null != actionBar) {
            actionBar.setDisplayShowHomeEnabled(true);
            actionBar.setIcon(R.mipmap.app_icon);
        }

        // setup runtime constants from XML resources
        SEND_INTERVAL = getResources().getInteger(R.integer.sim_send_interval);
        SINGLE_CLICK_LENGTH = getResources().getInteger(R.integer.sim_click_length);
        ((TextView) findViewById(R.id.tv_send_interval_text)).setText(String.format("%s [ms]", String.valueOf(SEND_INTERVAL)));

        String devName = BluetoothAdapter.getDefaultAdapter()
                .getRemoteDevice(getIntent().getStringExtra(BluetoothDevice.EXTRA_DEVICE)).getName();

        if (JUNIT_TEST.equals(devName)) {
            devName = BluetoothAdapter.getDefaultAdapter().getName();
        }

        ((TextView) findViewById(R.id.tv_connectedDeviceContent)).setText(devName);

        initToggleCheckChangeListener();
        initButtons();
        initToggleSendButton();

        // init status text views
        this.tv_lastSentValue = (TextView) findViewById(R.id.tv_lastSentValue);
        this.tv_output = (TextView) findViewById(R.id.tv_simOutputValue);
        this.tv_status = (TextView) findViewById(R.id.tv_status_text);
    }

    @Override
    protected void onResume() {
        super.onResume();
        this.registerReceiver(mReceiver, new IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED));
    }

    @Override
    protected void onPause() {
        super.onPause();
        this.unregisterReceiver(mReceiver);
    }

    /**
     * Initialize the main toggle button, so it starts/stops sending simulated output.
     * Toggling states will open/close the bluetooth socket as needed.
     */
    private void initToggleSendButton() {
        this.tbtn_toggleSend = (ToggleButton) findViewById(R.id.tbtn_toggleSending);
        this.tbtn_toggleSend.setOnCheckedChangeListener(new ToggleSendingListener());
    }

    /**
     * Listener for each input toggle button to handle presses.
     */
    private void initToggleCheckChangeListener() {
        checkedChangeListener = new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                final Button button = (Button) v;
                final char buttonNumber = button.getText().charAt(0);

                switch (event.getAction()){
                    case MotionEvent.ACTION_DOWN:
                        Log.v(INPUT_SIM_TAG, "onButtonPress: " + buttonNumber);
                        setInputBit(ids.get(button.getId()), true);
                        setViewText(button, String.format("%c (ON)", buttonNumber));
                        break;
                    case MotionEvent.ACTION_UP:
                        Log.v(INPUT_SIM_TAG, "onButtonRelease: " + buttonNumber);
                        setInputBit(ids.get(button.getId()), false);
                        setViewText(button, String.format("%c (OFF)", buttonNumber));
                        break;
                }

                simOutput = String.format("%8s", Integer.toBinaryString(0xFF & singleLevelOutput)).replace(' ', '0');
                setViewText(tv_output, simOutput);

                return false;
            }

            private void setViewText(final TextView tv, final String s) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        tv.setText(s);
                    }
                });
            }
        };
    }

    /**
     * Set a single bit in the simulated singleLevelOutput, based on the index of the button.
     *
     * @param index of bit (input) to be set
     * @param isPressed state of the input
     */
    private void setInputBit(Integer index, boolean isPressed) {
        byte mask = (byte) (1 << (index - 1)); // mask is 0..010..0 - the 1 is at the index bit
        if (isPressed) {
            singleLevelOutput |= mask;
            Log.d(INPUT_SIM_TAG, "setInputBit: SET BIT " + (index-1));
        } else {
            mask = (byte) ~mask; // mask is 1..101..1 - the 0 is at the index bit
            singleLevelOutput &= mask;
            Log.d(INPUT_SIM_TAG, "setInputBit: CLEAR BIT " + (index-1));
        }
    }

    Toast mToast;
    @Override public void showToast(final String s) {
        if (null != mToast) mToast.cancel();

        runOnUiThread(new Runnable() {
            @Override public void run() {
                mToast = Toast.makeText(InputSimActivity.this, s, Toast.LENGTH_SHORT);
                mToast.show();
            }
        });
    }

    /**
     * Initialize all the toggle buttons, so they have their unique identifiers.
     */
    private void initButtons() {
        Button toggle;
        int i = 0;
        for (int id : btn_ids) {
            toggle = (Button) findViewById(id);
            toggle.setOnTouchListener(checkedChangeListener);
            btns.add(toggle);
            ids.put(toggle.getId(), ++i);
        }

        FloatingActionButton fbtn;
        fbtn = (FloatingActionButton) findViewById(R.id.fbtn1);
        fbtn.setOnTouchListener(new NavTouchListener(0));
        fbtn = (FloatingActionButton) findViewById(R.id.fbtn2);
        fbtn.setOnTouchListener(new NavTouchListener(1));
        fbtn = (FloatingActionButton) findViewById(R.id.fbtn3);
        fbtn.setOnTouchListener(new NavTouchListener(2));
        fbtn = (FloatingActionButton) findViewById(R.id.fbtn4);
        fbtn.setOnTouchListener(new NavTouchListener(3));

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (started.get()) {
            simulator.terminateConnection();
        }
    }


    int[] fabIds = {R.id.fbtn1, R.id.fbtn2, R.id.fbtn3, R.id.fbtn4};

    @Override
    public String getSimOutput() {
        return simOutput;
    }

    @Override
    public long getSendInterval() {
        return SEND_INTERVAL;
    }

    @Override
    public byte getSingleLevelOutput() {
        return singleLevelOutput;
    }

    @Override
    public void onValueSent(final String s) {
        runOnUiThread(new Runnable() {
            @Override public void run() { tv_lastSentValue.setText(s); }
        });

    }

    @Override
    public void onRemoteDisconnect() {
        Log.v(INPUT_SIM_TAG, "handling remote disconnection");
        showToast(getString(R.string.input_sim_disconnected_toast));
        handleDisconnection();
    }


    public void handleDisconnection() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                tbtn_toggleSend.setTextOff(getString(R.string.label_start_sending));
                tbtn_toggleSend.setChecked(false);
                tv_status.setText(R.string.status_disconnected);
                tv_status.setTextColor(Color.RED);
                started.set(false);
            }
        });
    }

    @Override
    public long getClickLength() {
        return SINGLE_CLICK_LENGTH;
    }

    @Override
    public void onConnect() {
        handleConnection();
    }

    public void setSimulator(InputSimulator simulator) {
        this.simulator = simulator;
    }

    private class NavTouchListener implements OnTouchListener {
        private Button tbtn;
        private FloatingActionButton fab;

        NavTouchListener(int tbIndex) {
            this.tbtn = btns.get(tbIndex);
            fab = (FloatingActionButton) findViewById(fabIds[tbIndex]);
        }

        @Override
        public boolean onTouch(View v, MotionEvent event) {
            fab.startAnimation(AnimationUtils.loadAnimation(getApplication(), R.anim.fab_sim_press));
            Log.d("FAB_TOUCH_TAG", "onTouch: event - " + event.toString());
            final MotionEvent ev = event;
            new Thread(new Runnable() {
                @Override
                public void run() {
                    checkedChangeListener.onTouch(tbtn, ev);
                    try {
                        Thread.sleep(SINGLE_CLICK_LENGTH);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    MotionEvent upEvent = MotionEvent.obtain(ev);
                    upEvent.setAction(MotionEvent.ACTION_UP);
                    checkedChangeListener.onTouch(tbtn, upEvent);
                }
            }).start();

            return false;
        }
    }

    public boolean isStarted() {
        return started.get();
    }

    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            switch (intent.getAction()) {
                case BluetoothAdapter.ACTION_STATE_CHANGED:
                    // It means the user has changed his bluetooth state.
                    BluetoothAdapter btAdapter = BluetoothAdapter.getDefaultAdapter();
                    switch (btAdapter.getState()) {
                        case BluetoothAdapter.STATE_TURNING_OFF:
                        case BluetoothAdapter.STATE_OFF:
                            Log.d(INPUT_SIM_TAG, "onReceive: BT TURNING OFF");
                            if (started.get()) {
                                simulator.terminateConnection();
                            }

                            handleDisconnection();
                            tbtn_toggleSend.setEnabled(false);
                            tv_status.setText(R.string.bt_off);
                            return;
                        case BluetoothAdapter.STATE_ON:
                            Log.d(INPUT_SIM_TAG, "onReceive: BT IS NOW ON");
                            tbtn_toggleSend.setEnabled(true);
                            tv_status.setText(R.string.status_disconnected);
                            return;
                    }
                    break;
            }
        }
    };

    private class ToggleSendingListener implements CompoundButton.OnCheckedChangeListener {

        @Override
        public void onCheckedChanged(android.widget.CompoundButton buttonView, boolean isChecked) {
            if (!tbtn_toggleSend.isChecked()) {
                Log.d(INPUT_SIM_TAG, "onCheckedChanged: PAUSED SENDING");
                simulator.pauseSending();
                tv_status.setText(R.string.status_paused);
                tv_status.setTextColor(Color.parseColor("#FFA500")); // orange
                return;
            }

            if (started.get()) {
                Log.d(INPUT_SIM_TAG, "onCheckedChanged: UN-PAUSED SENDING");
                simulator.resumeSending();
                tv_status.setText(R.string.status_sending);
                tv_status.setTextColor(Color.GREEN);
                return;
            }

            Log.d(INPUT_SIM_TAG, "onCheckedChanged: STARTED SENDING");

            if (null == simulator) {
                simulator = InputSimulator.GetSimulator(
                        InputSimActivity.this,
                        getResources().getString(R.string.BT_UUID),
                        getIntent().getStringExtra(BluetoothDevice.EXTRA_DEVICE));
            }

            if (null == simulator) {
                showToast("Failed to initialize simulator");
                Log.d(INPUT_SIM_TAG, "onCreate: Got NULL simulator from factory method");
                finish();
            }

            if (!simulator.initBluetoothConnection()) {
                finish();
            }

            new Thread(new Connector()).start();
            tbtn_toggleSend.setTextOff(getString(R.string.label_resume_sending));
        }
    }

    private class Connector implements Runnable {

        @Override
        public void run() {
            if (!simulator.connect()) {
                final String remoteDeviceName = simulator.getRemoteDevName();
                showToast(String.format(getString(R.string.input_sim_device_not_found_toast), remoteDeviceName));
                Log.d(INPUT_SIM_TAG, "onCheckedChanged: couldn't open socket with server (receiver) '" + remoteDeviceName + "'");
                runOnUiThread(new Runnable() {
                    @Override public void run() {
                        started.set(true);
                        tbtn_toggleSend.setChecked(false);
                        tv_status.setText(R.string.status_disconnected);
                        tv_status.setTextColor(Color.RED);
                    }
                });
            }
        }
    }


    private void handleConnection() {
        runOnUiThread(new Runnable() {
            @Override public void run() {
                tbtn_toggleSend.setTextOff(getString(R.string.label_resume_sending));
                tbtn_toggleSend.setEnabled(true);
                simulator.resumeSending();
                tv_status.setText(R.string.status_sending);
                tv_status.setTextColor(Color.GREEN);
                started.set(true);
            }
        });
    }
}
