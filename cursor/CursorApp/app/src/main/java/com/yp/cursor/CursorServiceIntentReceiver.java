package com.yp.cursor;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

/**
 * Created by avner on 24/11/2015.
 */
public class CursorServiceIntentReceiver extends android.content.BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {

        String action = intent.getAction();
        if (action == null) {
            Log.d("Broadcast receiver", "action is null");
            return;
        }
        switch (action) {
            case CursorService.CLOSE_ACTION:
                Log.d("Broadcast receiver", "stopping service");
                CursorService cursorService = CursorApplication.getInstance().cursorService;
                if (cursorService != null) {
                    cursorService.stop();
                } else {
                    Intent i = new Intent(context, CursorService.class);
                    context.stopService(i);
                }
                break;
        }
    }
}
