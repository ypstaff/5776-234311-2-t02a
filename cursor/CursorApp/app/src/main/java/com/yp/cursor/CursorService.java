package com.yp.cursor;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.PixelFormat;
import android.graphics.Point;
import android.os.IBinder;
import android.os.PowerManager;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;

import com.yp.activities.MainActivity;

import java.util.List;

public class CursorService extends Service {
    public static final String BUTTON_TEXT = "Click";
    public static final float BUTTON_ALPHA = 0.5f;
    private static final int CURSOR_BUTTONS_MIN_INDEX = 3;
    private ViewInputHolder view;
    private boolean isRunning = false;

    /**
     * used to simulate bluetooth commands.
     */
    private LinearLayout debugButtons;
    private Button demoButton;

    private static final int NOTIFICATION_INDEX = 1;
    public static final String CLOSE_ACTION = "close";

    @Nullable
    private NotificationManager mNotificationManager = null;
    private NotificationCompat.Builder mNotificationBuilder;

    // simulation
    private PowerManager.WakeLock wakeLock;

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        CursorApplication.getInstance().cursorService = this;
        Log.d("CursorService", "Service created");
    }

    private void setupNotifications() { //called in start()

        if (mNotificationManager == null) {
            mNotificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        }
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0,
                new Intent(this, MainActivity.class)
                        .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP),
                0);
        PendingIntent pendingCloseIntent = PendingIntent.getBroadcast(this, 0,
                new Intent(this, CursorServiceIntentReceiver.class)
                        .setAction(CLOSE_ACTION),
                0);
        mNotificationBuilder = new NotificationCompat.Builder(this);
        mNotificationBuilder
                .setSmallIcon(R.drawable.cursor)
                .setCategory(NotificationCompat.CATEGORY_SERVICE)
                .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
                .setContentTitle(getText(R.string.app_name))
                .setWhen(System.currentTimeMillis())
                .setContentIntent(pendingIntent)
                .addAction(android.R.drawable.ic_menu_close_clear_cancel,
                        getString(R.string.action_exit), pendingCloseIntent)
                .setOngoing(true);
    }

    private void showNotification() {
        mNotificationBuilder
                .setTicker(getText(R.string.service_connected))
                .setContentText(getText(R.string.service_connected));
        if (mNotificationManager != null) {
            mNotificationManager.notify(NOTIFICATION_INDEX, mNotificationBuilder.build());
        }
    }
    private void hideNotification() {
        if (mNotificationManager != null) {
            mNotificationManager.cancel(NOTIFICATION_INDEX);
        }
    }

    private void addButtonsForInputs(WindowManager wm, final List<SingleInput> inputs) {
        WindowManager.LayoutParams paramsForButton = new WindowManager.LayoutParams(
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.TYPE_SYSTEM_ALERT,
                WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE
                        | WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL
                        | WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH,
                PixelFormat.TRANSLUCENT);

        paramsForButton.gravity = Gravity.BOTTOM | Gravity.END;

        debugButtons = new LinearLayout(this);
        debugButtons.setOrientation(LinearLayout.HORIZONTAL);
        debugButtons.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 10));
        for(int i=0; i<inputs.size(); i++){

            ImageButton button = new ImageButton(this);
            button.setAlpha(BUTTON_ALPHA);
            button.setImageResource(R.drawable.face_cursor);
            final SingleInput currentInput = inputs.get(i);
            final int finalI = i;
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.d("CursorService", "button " + finalI + " clicked" );
                    currentInput.onClick();
                }
            });
            button.setOnTouchListener(new View.OnTouchListener() {

                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    if (event.getAction() == MotionEvent.ACTION_DOWN){
                        Log.d("CursorService", "button " + finalI + " pressed down");
                        currentInput.onLongPressStart();
                    } else if (event.getAction() == MotionEvent.ACTION_UP) {
                        Log.d("CursorService", "button " + finalI + " pressed up");
                        currentInput.onLongPressStop();
                    }
                    return false;
                }
            });
            debugButtons.addView(button, i);
            registerButtonToBlueTooth(inputs.get(i), i);
        }
        wm.addView(debugButtons, paramsForButton);
    }

    private void registerButtonToBlueTooth(SingleInput singleInput, int i) {
        CursorApplication.getInstance().dispatcher.setInputToDispatch(i + CURSOR_BUTTONS_MIN_INDEX, singleInput);
    }

    private void unregisterButtonFromBlueTooth(int i) {
        CursorApplication.getInstance().dispatcher.setInputToDispatch(i + CURSOR_BUTTONS_MIN_INDEX, null);
    }


    private void addView(WindowManager wm, ViewInputHolder viewToDisplay) {
        WindowManager.LayoutParams params = new WindowManager.LayoutParams(
                WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.TYPE_SYSTEM_OVERLAY,//TYPE_SYSTEM_ALERT,//TYPE_SYSTEM_OVERLAY,
                WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE | WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE |
                WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN, //will cover status bar as well!!!
                PixelFormat.TRANSLUCENT);
        params.gravity = Gravity.START | Gravity.TOP;
        //params.x = 100;
        //params.y = 100;
        params.setTitle("Cursor");
        wm.addView(viewToDisplay, params);
        viewToDisplay.start();
    }

    @Override
    public void onDestroy() {
        if(isRunning){
            stop();
        }
        CursorApplication.getInstance().cursorService = null;
        super.onDestroy();

        Log.d("CursorService", "Service destroyed");
    }

    public void stop() {
        if (!isRunning) {
            return;
        }


        if (view != null) {
            for(int i=0; i< view.getInputs().size(); i++){
                unregisterButtonFromBlueTooth(i);
            }
            view.stop();
            ((WindowManager) getSystemService(WINDOW_SERVICE)).removeView(view);
            view = null;
        }
        if (debugButtons != null) {
            ((WindowManager) getSystemService(WINDOW_SERVICE)).removeView(debugButtons);
            debugButtons = null;
        }

        if (demoButton != null) {
            ((WindowManager) getSystemService(WINDOW_SERVICE)).removeView(demoButton);
            demoButton = null;
        }


        hideNotification();
        wakeLock.release();
        isRunning = false;
    }

    public boolean isRunning() {
        return isRunning;
    }

    public void start(ViewInputHolder view) {

        if(isRunning){
            return;
        }

        isRunning = true;
        this.view = view;
        WindowManager wm = (WindowManager) getSystemService(WINDOW_SERVICE);
        addView(wm, view);
        addButtonsForInputs(wm, view.getInputs());
        setupNotifications();
        showNotification();

        final PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
        wakeLock = pm.newWakeLock(PowerManager.SCREEN_BRIGHT_WAKE_LOCK | PowerManager.ON_AFTER_RELEASE, "");
        wakeLock.acquire();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if(view != null && isRunning){
            Log.i("CursorService", "setting new orientation");
            view.stop();
            WindowManager window = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
            Display display = window.getDefaultDisplay();
            Point screenDimensions = new Point();
            screenDimensions.x = display.getWidth();
            screenDimensions.y = display.getHeight();
            view.setOrientation(newConfig.orientation, screenDimensions);
            view.start();
        }
    }
}
