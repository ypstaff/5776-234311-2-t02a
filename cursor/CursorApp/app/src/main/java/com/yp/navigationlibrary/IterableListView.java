package com.yp.navigationlibrary;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.support.v4.content.res.ResourcesCompat;
import android.util.Log;
import android.view.View;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sRoySvin on 11/7/2015.
 *
 * @author Roy Svinik
 */

/* Note: This class does not support lists with rows that can change location */
public class IterableListView  implements IterableGroup {

    ListView list;
    Integer localPos;
    Integer globalPos;
    Boolean groupMode;
    Drawable formerBackground;
    Context context;
    Drawable background;
    int markColor = Color.YELLOW;

    public IterableListView(ListView list, Context context) {
        this.list = list;
        formerBackground = list.getBackground();
        this.context = context;
        background = ResourcesCompat.getDrawable(context.getResources(), yearlyproject.navigationlibrary.R.drawable.border, null);
    }

    //customize drawable color
    public IterableListView(ListView list, Context context, Drawable d,
                             int markColor) {
        this(list, context);
        background = d;
        this.markColor = markColor;
    }

    @Override
    public boolean start() {
        groupMode = true;
        markAllChilds();
        localPos = 0;
        globalPos = 0;
		list.setSelection(localPos);
        /* if all rows in the list fits the screen, automatically go into selecting rows */
        if(list.getLastVisiblePosition() - list.getFirstVisiblePosition() + 1 == list.getCount() &&
                list.getCount() > 0 &&
                isViewVisible(list.getChildAt(0)) &&
                isViewVisible(list.getChildAt(list.getLastVisiblePosition()))) {
            select();
        }
        return true;
    }

    @Override
    public boolean iterate() {
        if(list.getCount() == 0) return false;
        if(groupMode) {
            globalPos = (globalPos + list.getLastVisiblePosition() - list.getFirstVisiblePosition());
            if(globalPos >= list.getCount())
                globalPos = 0;
            list.setSelection(globalPos);
        } else {
            circle(list.getChildAt(localPos));
            localPos = (localPos + 1) %
                    (list.getLastVisiblePosition() - list.getFirstVisiblePosition() + 1);
            while(!isViewVisible(list.getChildAt(localPos))) {
                localPos = (localPos + 1) %
                        (list.getLastVisiblePosition() - list.getFirstVisiblePosition() + 1);
            }
            mark(list.getChildAt(localPos));
        }
        return true;
    }

    @Override
    public void select() {
        if(groupMode) {
            if(list.getCount() == 0) return;
            unmarkAllChilds();
            localPos = 0;
            if(!isViewVisible(list.getChildAt(0)))
                localPos++;
            circleAllChilds();
            mark(list.getChildAt(localPos));
            groupMode = false;
        } else {
            int pos = list.getFirstVisiblePosition() + localPos;
            list.performItemClick(
                    list.getAdapter().getView(pos, null, null),
                    pos,
                    list.getAdapter().getItemId(pos));
        }
    }

    @Override
    public boolean back() {
        if(groupMode) {
            return true;
        } else {
            groupMode = true;
            unmark(list.getChildAt(localPos));
            localPos = 0;
            uncircleAllChilds();
            mark(list);
            return (list.getLastVisiblePosition() - list.getFirstVisiblePosition() + 1 == list.getCount() &&
                    list.getCount() > 0 &&
                    isViewVisible(list.getChildAt(0)) &&
                    isViewVisible(list.getChildAt(list.getLastVisiblePosition())));
        }
    }

    @Override
    public void markGroup() {
        mark(list);
    }

    @Override
    public void unmarkGroup() {
        unmark(list);
    }

    @Override
    public void markAllChilds() {
        mark(list);
    }

    @Override
    public void unmarkAllChilds() {
        unmark(list);
    }

    @Override
    public void circleGroup() {
        circle(list);
    }

    @Override
    public void uncircleGroup() {
        uncircle(list);
    }

    @Override
    public void circleAllChilds() {
        for(int i = 0; i<= list.getLastVisiblePosition() - list.getFirstVisiblePosition(); ++i) {
            if(isViewVisible(list.getChildAt(i))) {
                circle(list.getChildAt(i));
            }
        }
    }

    @Override
    public void uncircleAllChilds() {
        for(int i = 0; i<= list.getLastVisiblePosition() - list.getFirstVisiblePosition(); ++i) {
            uncircle(list.getChildAt(i));
        }
    }

    @Override
    public List<View> getSelectedViews() {
        List<View> views = new ArrayList<>();
        if(groupMode) {
            views.add(list);
        } else {
            views.add(list.getChildAt(localPos));
        }
        return views;
    }

    @Override
    public View getGroupView() {
        return list;
    }

    private void mark(View v) {
        v.setBackgroundColor(markColor);
    }

    private void unmark(View v) {
        v.setBackground(formerBackground);
    }

    private void circle(View v) {
        v.setBackground(background);
    }

    private void uncircle(View v) {
        unmark(v);
    }

    private boolean isViewVisible(View view) {
		if(view.getVisibility() != View.VISIBLE) {
			return false;
		}
        Rect scrollBounds = new Rect();
        list.getDrawingRect(scrollBounds);

        float top = view.getY();
        float bottom = top + view.getHeight();

        return (scrollBounds.top <= top && scrollBounds.bottom >= bottom);
    }


    @Override
    public void stop() {
        Log.e("IterableListView", "stop");
        unmarkGroup();
        uncircleGroup();
//        unmarkAllChilds();
//        uncircleAllChilds();
    }
}
