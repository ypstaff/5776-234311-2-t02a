package com.yp.activities;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ComposePathEffect;
import android.graphics.CornerPathEffect;
import android.graphics.DashPathEffect;
import android.graphics.PathEffect;
import android.graphics.Point;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.provider.MediaStore;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Toast;

import com.yp.classes.BitmapEffect;
import com.yp.classes.Painter;
import com.yp.classes.PainterView;
import com.yp.cursor.CursorApplication;
import com.yp.cursor.R;
import com.yp.cursor.SingleInput;

import java.io.File;
import java.util.ArrayList;
import java.util.UUID;

/**
 * @author Inbar Donag
 *
 * Revised by:
 * @author Or Mauda
 * (added support for different colors, sizes, opacity, speed )
 */
public class PainterActivity extends Activity
{
    public static final String PAINTER_TAG = "PAINTER";
    private ImageButton rightBtn;
    private ImageButton leftBtn;


    private enum RequestType {
        TOOL_REQUEST(1), COLOR_PALETTE_REQUEST(2), NEW_COLOR_REQUEST(3), BRUSH_SIZE_REQUEST(4), OPACITY(5), EFFECTS_REQUEST(6),
        SPEED(7), OPEN_REQUEST(8);
        private final int val;
        RequestType(int val) { this.val = val; }
        public int getValue() {return val; }
    }

    public enum Tool {CURSOR, BRUSH, ERASER, UNDO, REDO, COLOR_PICKER, DELETE, EXIT}

    private Tool currTool = Tool.CURSOR;
    private Tool prevTool = Tool.BRUSH;

    private ArrayList<String> lastUsedColors;
    private int oldestColorIndex;
    private int currDelay;
    private PainterView painterView;
    private ImageButton toolBtn;
    private Handler handler = new Handler();
    private boolean toolShouldStartAction = false; // true if the tool should start repeating its function (useTool runnable), false if should stop
    private static final int green = Color.argb(255, 164, 198, 57);
    private Resources resources;
    String currEffect = "REGULAR";

    Bitmap colorPickerBitmap;


    // happens every 'currDelay' msec after tool is started
    private Runnable useTool = new Runnable()
    {
        @Override
        public void run()
        {
            Point cursorPos = painterView.cursor.getCursorPos();
            painterView.cursor.advanceCursor();

            if (currTool == Tool.COLOR_PICKER)
            {
                 int pickedColor = colorPickerBitmap.getPixel(cursorPos.x,cursorPos.y);
                 toolBtn.setColorFilter(pickedColor);
                 painterView.painter.setColor(pickedColor);
            }
            else if  (currTool == Tool.BRUSH || currTool == Tool.ERASER)
            {
                painterView.painter.advanceStroke(cursorPos);
            }

            painterView.invalidate();
            handler.postDelayed(useTool, currDelay);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_painter);
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);

        resources = getResources();

        painterView = (PainterView) findViewById(R.id.painter);
        toolBtn = (ImageButton) findViewById(R.id.toolBtn);
        rightBtn = (ImageButton) findViewById(R.id.turnRightBtn);
        leftBtn = (ImageButton) findViewById(R.id.turnLeftBtn);

        lastUsedColors = new ArrayList<>();
        oldestColorIndex = 0;
        currDelay = 20;

        toolBtn.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                if (currTool == Tool.UNDO) {
                    undo();
                } else if (currTool == Tool.REDO) {
                    redo();
                } else if (currTool == Tool.EXIT) {
                    onBackPressed();
                    onBackPressed();
                } else {
                    toolShouldStartAction = !toolShouldStartAction;
                    if (toolShouldStartAction) {
                        startUsingTool();
                    } else {
                        stopUsingTool();
                    }
                }
            }
        });

        toolBtn.setOnLongClickListener(new View.OnLongClickListener()
        {
            @Override
            public boolean onLongClick(View v) {

                if (currTool == Tool.EXIT)
                    return false; // no need for long click in exit

                if (toolShouldStartAction) {
                    stopUsingTool();
                }
                openToolMenu();
                return true;
            }
        });

        rightBtn.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                if (currTool == Tool.DELETE || currTool == Tool.EXIT)
                    return false; // no need for long click in delete and exit
                swapToLatestToolUsed();
                return true;
            }
        });
    }

    private void swapToLatestToolUsed() {

        stopUsingTool();
        Tool temp = prevTool;
        prevTool = currTool;
        currTool = temp;
        updateToolBtn();
    }

    private void unregisterFromBT() {
        final CursorApplication cursorApp = CursorApplication.getInstance();
        if (null == cursorApp || null == cursorApp.dispatcher) return;

        cursorApp.dispatcher.setInputToDispatch(0, null);
        cursorApp.dispatcher.setInputToDispatch(1, null);
        cursorApp.dispatcher.setInputToDispatch(2, null);
    }

    private void registerToBt() {
        final CursorApplication cursorApp = CursorApplication.getInstance();
        if (null == cursorApp || null == cursorApp.dispatcher) return;

        cursorApp.dispatcher.setInputToDispatch(0, new SingleInput() {
            @Override
            public void onClick() {
                runOnUiThread(
                        new Runnable() {
                            @Override
                            public void run() {
                                leftBtn.callOnClick();
                            }
                        }
                );

            }

            @Override
            public void onLongPressStart() {
            }

            @Override
            public void onLongPressStop() {
            }

            @Override
            public void onDoubleClick() {
            }

            @Override
            public boolean isDoubleClickEnabled() {
                return false;
            }
        });

        cursorApp.dispatcher.setInputToDispatch(1, new SingleInput() {
            @Override
            public void onClick() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        toolBtn.callOnClick();
                    }
                });

            }

            @Override
            public void onLongPressStart() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        MotionEvent event = MotionEvent.obtain(SystemClock.uptimeMillis(), SystemClock.uptimeMillis(), MotionEvent.ACTION_DOWN, 0, 0, 0);
                        toolBtn.onTouchEvent(event);
                    }
                });

            }

            @Override
            public void onLongPressStop() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        MotionEvent event = MotionEvent.obtain(SystemClock.uptimeMillis(), SystemClock.uptimeMillis(), MotionEvent.ACTION_UP, 0, 0, 0);
                        toolBtn.onTouchEvent(event);
                    }
                });

            }

            @Override
            public void onDoubleClick() {
            }

            @Override
            public boolean isDoubleClickEnabled() {
                return false;
            }
        });

        cursorApp.dispatcher.setInputToDispatch(2, new SingleInput() {
            @Override
            public void onClick() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        rightBtn.callOnClick();
                    }
                });

            }

            @Override
            public void onLongPressStart() {
            }

            @Override
            public void onLongPressStop() {
            }

            @Override
            public void onDoubleClick() {
            }

            @Override
            public boolean isDoubleClickEnabled() {
                return false;
            }
        });

    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterFromBT();
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerToBt();
    }

    private void openToolMenu() {
        Intent intent = new Intent(getApplicationContext(), ToolMenuActivity.class);
        startActivityForResult(intent, RequestType.TOOL_REQUEST.getValue());
    }

    private void openPaintPalette() {
        Intent intent = new Intent(getApplicationContext(), PaintPalletActivity.class);
        intent.putStringArrayListExtra("last_used_colors", lastUsedColors);
        startActivityForResult(intent, RequestType.COLOR_PALETTE_REQUEST.getValue());
    }

    private void openCreateNewColor() {
        Intent intent = new Intent(getApplicationContext(), RgbColorActivity.class);
        Bundle b = new Bundle();
        b.putInt("initial_value", painterView.painter.getCurrColor());
        intent.putExtras(b);
        startActivityForResult(intent, RequestType.NEW_COLOR_REQUEST.getValue());
    }

    private void openBrushSizes() {
        Intent intent = new Intent(getApplicationContext(), SeekBarActivity.class);
        Bundle b = new Bundle();
        b.putString("type", "size");
        b.putInt("initial_value", painterView.painter.getCurrSize());
        intent.putExtras(b);
        startActivityForResult(intent, RequestType.BRUSH_SIZE_REQUEST.getValue());
    }

    private void openOpacityBar() {
        Intent intent = new Intent(getApplicationContext(), SeekBarActivity.class);
        Bundle b = new Bundle();
        b.putString("type", "opacity");
        b.putInt("initial_value", painterView.painter.getCurrOpacity());
        intent.putExtras(b);
        startActivityForResult(intent, RequestType.OPACITY.getValue());
	}

    private void openSpeedBar() {
        Intent intent = new Intent(getApplicationContext(), SeekBarActivity.class);
        Bundle b = new Bundle();
        b.putString("type", "speed");
        b.putInt("initial_value", currDelay);
        intent.putExtras(b);
        startActivityForResult(intent, RequestType.SPEED.getValue());
    }

    private void openEffects() {
        Intent intent = new Intent(getApplicationContext(), BrushEffectsActivity.class);
        startActivityForResult(intent, RequestType.EFFECTS_REQUEST.getValue());
    }

    private void saveDrawing()
    {

        Bitmap bitmap = Bitmap.createBitmap(painterView.getWidth(), painterView.getHeight(), Bitmap.Config.ARGB_8888); // this creates a MUTABLE bitmap
        Canvas canvas = new Canvas(bitmap);
        painterView.painter.render(canvas);

        final String newImagePath = UUID.randomUUID().toString()+".png";

        String imgSaved = MediaStore.Images.Media.insertImage(
                getContentResolver(), bitmap,
                newImagePath, "drawing");

        if (imgSaved != null) {
            Toast savedToast = Toast.makeText(getApplicationContext(),
                    "Drawing saved to Gallery!", Toast.LENGTH_SHORT);
            savedToast.show();
            MediaScannerConnection.scanFile(this, new String[]{newImagePath}, null, new MediaScannerConnection.MediaScannerConnectionClient() {
                @Override
                public void onMediaScannerConnected() {

                }

                @Override
                public void onScanCompleted(String path, Uri uri) {
                    Log.d(PAINTER_TAG, "onScanCompleted: Added image file: " + newImagePath);
                }
            });
        }
        else {
            Toast unsavedToast = Toast.makeText(getApplicationContext(),
                    "Oops! Image could not be saved.", Toast.LENGTH_SHORT);
            unsavedToast.show();
        }
    }

    private void openImageFile() {
        Intent intent = new Intent(getApplicationContext(), GalleryActivity.class);
        startActivityForResult(intent, RequestType.OPEN_REQUEST.getValue());
    }

    private void deleteDrawing() {
        toolBtn.setVisibility(View.GONE);
        leftBtn.setImageResource(R.drawable.no);
        rightBtn.setImageResource(R.drawable.check_mark);
        Toast.makeText(PainterActivity.this, "Are you sure?", Toast.LENGTH_SHORT).show();
        currTool = Tool.DELETE;
    }

    private void exit() {
        leftBtn.setImageResource(R.drawable.cancel);
        toolBtn.setImageResource(R.drawable.no);
        rightBtn.setImageResource(R.drawable.check_mark);
        Toast.makeText(PainterActivity.this, "Do you want to save before you quit?", Toast.LENGTH_LONG).show();
        currTool = Tool.EXIT;
    }

    private void undo()
    {
        if (painterView.painter.undo() == Painter.STATUS.FAILURE)
        {
            Toast.makeText(this,"nothing to undo",Toast.LENGTH_SHORT).show();
            swapToLatestToolUsed();
        } else
        {
            painterView.invalidate();
        }
    }

    private void redo()
    {
        if (painterView.painter.redo() == Painter.STATUS.FAILURE) {
            Toast.makeText(this,"nothing to redo",Toast.LENGTH_SHORT).show();
            swapToLatestToolUsed();
        } else {
            painterView.invalidate();
        }
    }

    private void pickTool(String tool)
    {
        Tool temp = currTool;

        switch (tool) {
            case "CURSOR":
                currTool = Tool.CURSOR;
                break;
            case "BRUSH":
                currTool = Tool.BRUSH;
                break;
            case "ERASER":
                currTool = Tool.ERASER;
                break;
            case "UNDO":
                currTool = Tool.UNDO;
                undo();
                break;
            case "REDO":
                currTool = Tool.REDO;
                redo();
                break;
            case "PALETTE":
                openPaintPalette();
                currTool = Tool.BRUSH;
                break;
            case "NEW_COLOR":
                openCreateNewColor();
                currTool = Tool.BRUSH;
                break;
            case "COLOR_PICKER":
                currTool = Tool.COLOR_PICKER;
                setupColorPicker();
                break;
            case "BRUSH_SIZE":
                openBrushSizes();
                break;
            case "OPACITY":
                openOpacityBar();
                currTool = Tool.BRUSH;
                break;
            case "EFFECTS":
                openEffects();
                currTool = Tool.BRUSH;
                break;
            case "SPEED":
                openSpeedBar();
                break;
            case "SAVE":
                saveDrawing();
                break;
            case "OPEN":
                openImageFile();
                break;
            case "DELETE":
                deleteDrawing();
                break;
            case "EXIT":
                exit();
                break;
        }
        updateToolBtn();

        if (currTool != temp)
            prevTool = temp;
    }


    private void pickEffect(String effect)
    {
        PathEffect pathEffect = null;
        switch (effect)
        {
            case "REGULAR":
                break;
            case "DASH":
                int currSize = painterView.painter.getCurrSize();
                PathEffect dashEffect = new DashPathEffect(new float[]{currSize * 2, currSize * 2}, 0);
                pathEffect = new ComposePathEffect(dashEffect, new CornerPathEffect(50));
                break;
            case "LEAFS":
                Bitmap leaf1 = BitmapFactory.decodeResource(resources, R.drawable.leaf1, null);
                Bitmap leaf2 = BitmapFactory.decodeResource(resources, R.drawable.leaf2, null);
                Bitmap leaf3 = BitmapFactory.decodeResource(resources, R.drawable.leaf3, null);
                Bitmap leaf4 = BitmapFactory.decodeResource(resources, R.drawable.leaf4, null);
                pathEffect = new BitmapEffect(2, leaf1, leaf2, leaf3, leaf4);
                break;
            case "FLOWERS":
                Bitmap flower1 = BitmapFactory.decodeResource(resources, R.drawable.flower1, null);
                Bitmap flower2 = BitmapFactory.decodeResource(resources, R.drawable.flower2, null);
                Bitmap flower3 = BitmapFactory.decodeResource(resources, R.drawable.flower3, null);
                Bitmap flower4 = BitmapFactory.decodeResource(resources, R.drawable.flower4, null);
                pathEffect = new BitmapEffect(2, flower1, flower2, flower3, flower4);
                break;
            case "CHARCOAL":
                Bitmap charcoal = BitmapFactory.decodeResource(resources, R.drawable.charcoal, null);
                pathEffect = new BitmapEffect(1, charcoal);
                break;
            case "WATERCOLORS":
                Bitmap watercolor1 = BitmapFactory.decodeResource(resources, R.drawable.watercolor1, null);
                Bitmap watercolor2 = BitmapFactory.decodeResource(resources, R.drawable.watercolor2, null);
                Bitmap watercolor3 = BitmapFactory.decodeResource(resources, R.drawable.watercolor3, null);
                Bitmap watercolor4 = BitmapFactory.decodeResource(resources, R.drawable.watercolor4, null);
                pathEffect = new BitmapEffect(2, watercolor1, watercolor2, watercolor3, watercolor4);
                break;
        }
        painterView.painter.setEffect(pathEffect);
        currEffect = effect;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if (resultCode != RESULT_OK)
            return;

        // Check which request we're responding to
        if (requestCode == RequestType.TOOL_REQUEST.getValue()) {
            // The user picked a tool
            String tool = data.getStringExtra("tool");
            pickTool(tool);

        } else if (requestCode == RequestType.COLOR_PALETTE_REQUEST.getValue())
        {
            // The user picked a color
            String colorString = data.getStringExtra("color");

            int currColor;

            if (colorString.contains("#")) {
            currColor = Color.parseColor(data.getStringExtra("color"));
            } else {
                currColor = Integer.parseInt(colorString);
            }
            painterView.painter.setColor(currColor);

        } else if (requestCode == RequestType.NEW_COLOR_REQUEST.getValue())
        {
            // The user picked a color
            int currColor = Integer.parseInt(data.getStringExtra("color"));

            // update the the last used colors queue  with the new color for the palette

            int maxUsedColorsAmount = 6;
            if (lastUsedColors.size() < maxUsedColorsAmount) {
                lastUsedColors.add(Integer.toString(currColor));
            }
            lastUsedColors.remove(oldestColorIndex);
            lastUsedColors.add(oldestColorIndex, Integer.toString(currColor));
            oldestColorIndex = (oldestColorIndex+1)% maxUsedColorsAmount;

            painterView.painter.setColor(currColor);

        }
        else if (requestCode == RequestType.BRUSH_SIZE_REQUEST.getValue())
        {
            // The user picked a size
            int currSize = data.getIntExtra("size", 0);
            painterView.painter.setSize(currSize);
            pickEffect(currEffect);

        } else if (requestCode == RequestType.EFFECTS_REQUEST.getValue()) {
            // The user picked an effect
            String effect = data.getStringExtra("effect");
            pickEffect(effect);

        } else if (requestCode == RequestType.OPACITY.getValue())
        {
            // The user picked an opacity
            int paintAlpha = data.getIntExtra("opacity", 0);
            painterView.painter.setAlpha(paintAlpha);

        } else if (requestCode == RequestType.SPEED.getValue()) {
            // The user picked a speed
            currDelay = data.getIntExtra("speed", 0);

        } else if (requestCode == RequestType.OPEN_REQUEST.getValue()) {
            // The user picked an image
            final String path = data.getStringExtra(getString(R.string.selected_image_path));
            if (null == path) {
                Log.d(PAINTER_TAG, "onActivityResult: Got null image path to open. Make sure right string is extracted from bundle!");
                return;
            }

            Log.d(PAINTER_TAG, "onActivityResult: got image path: " + path);
            if (!new File(path).exists()) {
                Toast.makeText(this, "Invalid image path", Toast.LENGTH_SHORT).show();
                return;
            }
            painterView.painter.showBitmap(BitmapFactory.decodeFile(path));
        }
    }

    public void rightBtnOnClick(View v)
    {
        if (currTool == Tool.DELETE) {
            // approve delete
            painterView.painter.clearCanvas();
            painterView.invalidate();
            toolBtn.setVisibility(View.VISIBLE);
            swapToLatestToolUsed();
            leftBtn.setImageResource(R.drawable.turn_left);
            rightBtn.setImageResource(R.drawable.turn_right);

        } else if (currTool == Tool.EXIT) {
            saveDrawing();
            onBackPressed();
        }
        else {
            painterView.cursor.turnRight();
            painterView.invalidate();
        }
    }

    public void leftBtnOnClick(View v)
    {
        if (currTool == Tool.DELETE) {
            // no delete
            toolBtn.setVisibility(View.VISIBLE);
            swapToLatestToolUsed();
            leftBtn.setImageResource(R.drawable.turn_left);
            rightBtn.setImageResource(R.drawable.turn_right);

        } else if (currTool == Tool.EXIT) {
            // cancel
            swapToLatestToolUsed();
            leftBtn.setImageResource(R.drawable.turn_left);
            rightBtn.setImageResource(R.drawable.turn_right);
        }
        else {
            painterView.cursor.turnLeft();
            painterView.invalidate();
        }
    }

    private void updateToolBtn()
    {
        switch (currTool) {
            case CURSOR:
                toolBtn.setImageResource(R.drawable.cursor);
                toolBtn.clearColorFilter();
                break;
            case BRUSH:
                toolBtn.setImageResource(R.drawable.brush);
                toolBtn.clearColorFilter();
                break;
            case ERASER:
                toolBtn.setImageResource(R.drawable.eraser);
                toolBtn.clearColorFilter();
                break;
            case UNDO:
                toolBtn.setImageResource(R.drawable.undo);
                toolBtn.clearColorFilter();
                break;
            case REDO:
                toolBtn.setImageResource(R.drawable.redo);
                toolBtn.clearColorFilter();
                break;
            case COLOR_PICKER:
                Point point = painterView.cursor.getCursorPos();
                toolBtn.setImageResource(R.drawable.color_picker);
                toolBtn.setColorFilter(colorPickerBitmap.getPixel(point.x,point.y));
                break;
        }
    }

    private void startUsingTool()
    {
        if (currTool == Tool.ERASER)
            painterView.painter.startEraser(painterView.cursor.getCursorPos());
        else if (currTool == Tool.BRUSH)
            painterView.painter.startNewStroke(painterView.cursor.getCursorPos());

        toolBtn.setColorFilter(green);
        handler.post(useTool);
    }

    private void stopUsingTool() {
        if (currTool != Tool.COLOR_PICKER)
            toolBtn.clearColorFilter();
        handler.removeCallbacks(useTool);
        toolShouldStartAction = false;
    }

    private void setupColorPicker()
    {
        colorPickerBitmap = Bitmap.createBitmap(painterView.getWidth(), painterView.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(colorPickerBitmap);
        painterView.painter.render(canvas);

    }
}