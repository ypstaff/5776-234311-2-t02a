package com.yp.classes;

import android.util.Log;

import com.yp.SimpleInputEvent;
import com.yp.bts.BluetoothReceiver;
import com.yp.bts.SimpleInputHandler;
import com.yp.cursor.CursorApplication;
import com.yp.cursor.SingleInput;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by avner on 01/12/2015.
 * received commands from BT server and dispatches them to relevant single inputs.
 */
public class CmdDispatcher implements SimpleInputHandler {

    public static final int MAX_INPUT_CAPACITY = 10;
    List<SingleInput> inputsToDispatchTo = new ArrayList<>(MAX_INPUT_CAPACITY);
    private BluetoothReceiver bluetoothReceiver;

    public CmdDispatcher() {
        for (int i = 0; i < MAX_INPUT_CAPACITY; i++) {
            inputsToDispatchTo.add(null);
        }
    }

    /**
     * registers this cmd dispatcher to receive input from bluetooth server.
     */
    public void registerToBTServer() {

        bluetoothReceiver = CursorApplication.getInstance().bluetoothReceiver;
        if (bluetoothReceiver != null) {
            bluetoothReceiver.subscribe(this);
        }
    }

    /**
     * sets the given single input to be dispatched when the input with the index given received
     * input.
     *
     * @param index
     * @param input
     */
    public void setInputToDispatch(int index, SingleInput input) {

        inputsToDispatchTo.set(index, input);
    }

    /**
     * return the input to dispatch in the given index.
     * @param index - index of input to return
     * @return the input to dispatch in the given index.
     */
    public SingleInput getInputToDispatch(int index) {

        return inputsToDispatchTo.get(index);
    }

    /**
     * unregisters this cmd dispatcher from receiving inputs from bluetooth server.
     */
    public void unregisterFromBTServer() {

        if (bluetoothReceiver != null) {
            bluetoothReceiver.subscribe(this);
        }
    }

    @Override
    public void handleEvent(int index, SimpleInputEvent ev) {

        if (index >= inputsToDispatchTo.size() || index < 0) {
            Log.d("CmdDispatcher", "no subscriber for index of event");
            return;
        }
        final SingleInput singleInput = inputsToDispatchTo.get(index);
        if (singleInput == null) {
            return;
        }
        switch (ev) {
            case CLICK:
                singleInput.onClick();
                break;
            case LONG_PRESS_DOWN:
                singleInput.onLongPressStart();
                break;
            case LONG_PRESS_UP:
                singleInput.onLongPressStop();
                break;
            case DOUBLE_CLICK:
                singleInput.onDoubleClick();
                break;
        }
    }
}
