package com.yp.cursor;

import android.app.Application;

import com.yp.autocomplete.NgramDatabaseSuggester;
import com.yp.bts.BluetoothReceiver;
import com.yp.classes.CmdDispatcher;

public class CursorApplication extends Application {

    private static final int N_FOR_NGRAM = 3;
    private static CursorApplication m_Instance;

	public CursorService cursorService = null;

	public CmdDispatcher dispatcher = null;
	public BluetoothReceiver bluetoothReceiver = null;

    public static int getNumSuggestions() {
        return NUM_SUGGESTIONS;
    }

    private static final int NUM_SUGGESTIONS = 3;
	private NgramDatabaseSuggester dbSuggester;

	/*---------------------------------------------------------------------------------------------
	 * Singleton Init instance
	 *--------------------------------------------------------------------------------------------*/
	public CursorApplication() {
		super();
		m_Instance = this;
	}

	// Double-checked singleton fetching
	public static CursorApplication getInstance() {
		if(m_Instance == null) {
			synchronized(CursorApplication.class) {
				if(m_Instance == null) new CursorApplication();
			}
		}
		return m_Instance;
	}

	public CursorService getCursorService(){
		return cursorService;
	}
	@Override
	public void onCreate()
	{
		super.onCreate();
        dbSuggester = new NgramDatabaseSuggester(this, N_FOR_NGRAM, NUM_SUGGESTIONS);
        dbSuggester.loadDB(R.raw.the_return_of_sherlok_holmes_short);

	}

	public NgramDatabaseSuggester getNgramDatabaseSuggester() {
		return dbSuggester;
	}
}

