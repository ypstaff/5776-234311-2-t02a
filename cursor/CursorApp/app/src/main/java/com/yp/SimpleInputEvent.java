package com.yp;

/**
 * Created by Oded on 08/12/2015.
 */
public enum SimpleInputEvent {
    CLICK,
    LONG_PRESS_DOWN,
    LONG_PRESS_UP,
    DOUBLE_CLICK,
    NOP
}
