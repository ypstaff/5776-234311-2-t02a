package com.yp.classes;
import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.CornerPathEffect;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PathEffect;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.Shader;
import java.util.LinkedList;

/**
 * @author Inbar Donag
 */
public class Painter
{
    private LinkedList<Stroke> redoStrokes = new LinkedList<>();
    private LinkedList<Stroke> strokes = new LinkedList<>();

    // sizes
    public int width = 0;
    public int height = 0;

    // styles
    private Paint paintBackground = new Paint();

    // bitmap cache
    private Bitmap strokesCacheBitmap;
    private int strokesCacheCount = 0; // equals the index in strokes list where all the strokes until that index are cached
    boolean cacheIsValid = false;

    // new stroke default settings
    private PathEffect currEffect = null;
    private int currColor = Color.BLACK;
    private int currSize = 5; // current stroke size

    public enum STATUS {
        SUCCESS,
        FAILURE
    }


    ///////////////////////////////////////// c'tors ///////////////////////////////////////////////

    public Painter(int width, int height)
    {
        this.width = width;
        this.height = height;
        strokesCacheBitmap = Bitmap.createBitmap(this.width, this.height, Bitmap.Config.ARGB_8888);

        clearCanvas();
    }

    ////////////////////////////////////// settings /////// /////////////////////////////////////

    public void setEffect(PathEffect effect)
    {
        currEffect = effect;
    }

    public void setColor(int c)
    {
        int red = Color.red(c);
        int green = Color.green(c);
        int blue = Color.blue(c);
        int alpha = Color.alpha(currColor);
        currColor = Color.argb(alpha,red,green,blue);
    }

    public void setAlpha(int alpha)
    {
        int red = Color.red(currColor);
        int green = Color.green(currColor);
        int blue = Color.blue(currColor);
        currColor = Color.argb(alpha,red,green,blue);
    }

    public void setSize(int size) {
        currSize = size;
    }

    public int getCurrSize() {
        return  currSize;
    }

    public int getCurrColor() {
        return currColor;
    }

    public int getCurrOpacity() { return Color.alpha(currColor); }

    ///////////////////////////////////////////// ///////////////////////////////////////////////

    public void startNewStroke(Point point)
    {
        redoStrokes.clear();

        Stroke s;
        if (currEffect instanceof BitmapEffect)
            s = new BitmapStroke(point, (BitmapEffect)currEffect);
        else
            s = new Stroke(point,currEffect);

        s.setStrokeColor(Color.red(currColor), Color.green(currColor), Color.blue(currColor), Color.alpha(currColor));
        s.setStrokeWidth(currSize);

        strokes.addLast(s);
    }

    public STATUS undo()
    {
        if (strokes.size() == 0 )
            return  STATUS.FAILURE;

        Stroke removedStroke = strokes.removeLast();
        redoStrokes.addLast(removedStroke);
        return STATUS.SUCCESS;
    }

    public STATUS redo()
    {
        if (redoStrokes.size() == 0 )
            return STATUS.FAILURE;

        Stroke restoredStroke = redoStrokes.removeLast();
        strokes.addLast(restoredStroke);
        return STATUS.SUCCESS;
    }

    public void startEraser(Point point)
    {
        Stroke s = new Stroke(point, null);
        s.setStrokeColor(255, 255, 255,255);
        s.setStrokeWidth(currSize*3);
        strokes.addLast(s);
    }

    public void advanceStroke(Point p)
    {
        if (strokes.size() == 0)
            return;
        strokes.getLast().advanceStroke(p);
    }

    public void showBitmap(Bitmap bitmap)
    {
        // set background
        redoStrokes.clear();
        strokes.clear();
        bitmap = reduceImageSize(bitmap);
        BitmapShader shader = new BitmapShader(bitmap, Shader.TileMode.REPEAT, Shader.TileMode.REPEAT);
        paintBackground.setShader(shader);

        // we need to invalidate the cache because we changed the background bitmap
        invalidateCache();
    }

    public void clearCanvas()
    {
        redoStrokes.clear();
        strokes.clear();
        paintBackground.setColor(Color.WHITE);
        paintBackground.setShader(null);

        // we need to invalidate the cache because we changed the background bitmap
        invalidateCache();
    }

    ///////////////////////////////////////////////////////////////////////////////////////////

    public void render(Canvas canvas)
    {
        updateStrokesCache();

        // draw cache into output bitmap
        canvas.drawBitmap(strokesCacheBitmap, 0, 0, null);

        // draw uncached strokes to the output bitmap
        if (strokes != null)
        {
            // draw uncached strokes
            for (int i = strokesCacheCount; i < strokes.size(); i++)
                strokes.get(i).render(canvas);
        }
    }

    public boolean isClear() {
        return  strokes.size() == 0;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////

    private void updateStrokesCache()
    {
        if (strokes == null)
            return;

        int uncachedStrokesCount = strokes.size() - strokesCacheCount;
        if (uncachedStrokesCount >= 0 && uncachedStrokesCount <= 5 && cacheIsValid)
            return;

        strokesCacheCount = strokes.size() - strokes.size() % 5;

        Canvas canvas = new Canvas(strokesCacheBitmap);

        /// draw background bitmap/background color onto our cache bitmap
        canvas.drawRect(0, 0, width, height, paintBackground);

        // draw strokes to the cache bitmap
        for (int i = 0; i < strokesCacheCount ; i++)
            strokes.get(i).render(canvas);

        cacheIsValid = true;
    }

    private void invalidateCache()
    {
        cacheIsValid = false;
        strokesCacheCount = 0;
    }

    private Bitmap reduceImageSize(Bitmap b)
    {
        double aspectRatio = (double) b.getWidth() / (double) b.getHeight();
        return Bitmap.createScaledBitmap(b, width,(int)((double) width /aspectRatio), false);
    }

    private static Rect getRect(Point p, int size, int width, int height)
    {
        return new Rect(Math.max(p.x - size, 0), Math.max(p.y - size, 0), Math.min(p.x + size, width) , Math.min(p.y+size, height));
    }

    ///////////////////////////////////////////////////////////////////////////////////////////

    public static class Stroke
    {
        public int r = 0,g = 0,b = 0,a = 255;
        public int width = 5;
        public LinkedList<Point> strokePoints = new LinkedList<>();


        private PathEffect effect = new CornerPathEffect(50);

        public Stroke(Point firstPos, PathEffect effect)
        {
            if (effect != null)
                this.effect = effect;
            strokePoints.addLast(firstPos);
        }

        public void setStrokeColor(int r, int g, int b, int a)
        {
            this.r = r;
            this.g = g;
            this.b = b;
            this.a = a;
        }

        public void setStrokeWidth(int width) { // in pixels
            this.width = width;
        }

        public void advanceStroke(Point newPos)
        {
            if (strokePoints.size() >= 2)
            {
                Point last = strokePoints.getLast();
                Point prevLast = strokePoints.get(strokePoints.size() - 2);

                if (isStraightLine(prevLast, last, newPos))
                {
                    strokePoints.removeLast();
                }
            }
            strokePoints.addLast(newPos);
        }

        protected boolean isStraightLine(Point a, Point b, Point c)
        {
            if (a == null || b == null || c == null)
                return true;

            if (a.x == b.x && b.x == c.x)
                return true;

            if (a.y == b.y && b.y == c.y)
                return true;

            if (isOrdered(a.x,b.x,c.x) && isOrdered(a.y,b.y,c.y))
                return true;

            return false;
        }

        public boolean isOrdered(int a, int b, int c) {
            return (a > b && b > c) || (a < b && b < c);
        }

        public void render(Canvas canvas)
        {
            Paint paintStroke = new Paint();
            paintStroke.setStrokeCap(Paint.Cap.ROUND);
            paintStroke.setAntiAlias(true);
            paintStroke.setStyle(Paint.Style.STROKE);
            paintStroke.setStrokeJoin(Paint.Join.BEVEL);

            Path path = new Path();
            Point firstPoint = strokePoints.get(0);
            path.moveTo(firstPoint.x, firstPoint.y);

            for (int i = 1 ; i < strokePoints.size() ; i++)
            {
                Point p = strokePoints.get(i);
                path.lineTo(p.x, p.y);
            }

            Point lastPoint = strokePoints.getLast();
            path.lineTo(lastPoint.x, lastPoint.y);

            paintStroke.setARGB(a, r, g, b);
            paintStroke.setStrokeWidth(width);
            paintStroke.setPathEffect(effect);
            canvas.drawPath(path, paintStroke);
        }
    }

    ///////////////////////////////////////////////////////////////////////////////////////////

    public static class BitmapStroke extends Stroke
    {
        LinkedList<Bitmap> bitmaps = new LinkedList<>();
        BitmapEffect effect;
        private int count = 0;
        protected int spacing;

        BitmapStroke(Point firstPos, BitmapEffect effect)
        {
            super(firstPos, null);
            this.effect = effect;
            this.spacing = effect.getSpace();
            bitmaps.addLast(effect.getNextBitmap());
        }


        public void advanceStroke(Point newPos)
        {
            boolean moveLastPoint = false;

            if (strokePoints.size() >= 2)
            {
                Point last = strokePoints.getLast();
                Point prevLast = strokePoints.get(strokePoints.size() - 2);

                if (isStraightLine(prevLast, last, newPos))
                {
                        if (count < width* effect.getSpace())
                        {
                            // if the line is straight we move the last point forward
                            moveLastPoint = true;
                            count++;
                        } else {
                            count = 0;
                        }
                }
            }

            if (moveLastPoint) {
                strokePoints.set(strokePoints.size()-1, newPos);
            }
            else {
                bitmaps.addLast(effect.getNextBitmap());
                strokePoints.addLast(newPos);
            }

        }


        @Override
        public void render(Canvas canvas)
        {
            for (int i = 0 ; i < strokePoints.size() ; i++)
            {
                Point p = strokePoints.get(i);
                Bitmap bmp = bitmaps.get(i);
                canvas.drawBitmap(bmp, null, Painter.getRect(p,width*2, canvas.getWidth(),canvas.getHeight()), null);
            }
        }
    }
}