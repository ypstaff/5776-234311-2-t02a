package com.yp.cursor.facedetect;

import android.app.Instrumentation;
import android.content.SharedPreferences;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PointF;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Looper;
import android.os.SystemClock;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.MotionEvent;
import android.widget.Toast;

import com.yp.classes.Constants;
import com.yp.cursor.CursorApplication;
import com.yp.cursor.R;


/**
 * @author Avner Elizarov
 */
public class EyeStateMachine {
    public static final float MARKER_RADIUS = 20f;

    private EyeState currentState;
    private EyeState[] states = new EyeState[]{new IdleState(), new StartedBlinkState(), new ClickState()};
    private Canvas canvas;
    private Eye eye;
    private Paint clickPaint;
    private Paint longPressPaint;
    private final SharedPreferences sharedPref;
    public EyeStateMachine(Eye eye){
        this.eye = eye;
        states = new EyeState[]{new IdleState(), new StartedBlinkState(), new ClickState()};
        currentState = states[0];

        clickPaint = new Paint();
        clickPaint.setColor(Color.BLUE);
        longPressPaint = new Paint();
        longPressPaint.setColor(Color.YELLOW);
        sharedPref = PreferenceManager.getDefaultSharedPreferences(CursorApplication.getInstance());


    }
    public EyeStateMachine(Eye eye, Paint clickPaint, Paint longPressPaint){
        this.eye = eye;
        this.clickPaint = clickPaint;
        this.longPressPaint = longPressPaint;
        currentState = states[0];
        sharedPref = PreferenceManager.getDefaultSharedPreferences(CursorApplication.getInstance());
    }

    /**
     * called when a new eye open probability is received. this can result in current state being changed.
     * @param isEyeOpenProbability - probability of eye being open
     * @param cursorLocation - location of cursor when this eye probability was received.
     */
    public void onNewProbability(float isEyeOpenProbability, PointF cursorLocation, Canvas canvas){
        this.canvas = canvas;
        Log.d("Eye State Machine", "new eye probability: " + isEyeOpenProbability);
        currentState.onNewProbability(isEyeOpenProbability, cursorLocation);
        EyeState newState = currentState.getNextState();
        if(newState != null){
            changeState(newState);
        }
    }

    private void changeState(EyeState newState){
        PointF cursorLocation = currentState.getCursorLocation();
        currentState.exit();
        currentState = newState;
        currentState.enter(cursorLocation);
    }

    public EyeState getCurrentState() {
        return currentState;
    }


    public abstract class EyeState{

        protected static final String TAG = "Eye State";
        public static final int EYE_OPEN_COUNTER_THRESHOLD = 0; // number of times an eye can be perceived as open while blinking
        public static final float OPEN_THRESHOLD = 0.85f;
        public static final float CLOSED_THRESHOLD = 0.35f;

        protected PointF cursorLocation;

        protected boolean isEyeOpen(float isEyeOpenProbability) {
            return isEyeOpenProbability > OPEN_THRESHOLD;
        }

        protected boolean isEyeClosed(float isEyeOpenProbability) {
            return isEyeOpenProbability > 0 && isEyeOpenProbability <CLOSED_THRESHOLD;
        }

        /**
         * called when a new eye open probability is received.
         * @param isEyeOpenProbability - probability of eye being open
         * @param cursorLocation - location of cursor when this eye probability was received.
         */
        public abstract void onNewProbability(float isEyeOpenProbability, PointF cursorLocation);

        /**
         * exit the current state.
         */
        public abstract void exit();

        /**
         * enter the current state with the given cursor location
         * @param cursorLocation - current cursor location
         */
        public abstract void enter(PointF cursorLocation);

        /**
         * @return next state to move into. if should stay in same state null is returned.
         */
        public abstract EyeState getNextState();

        public PointF getCursorLocation(){
            return cursorLocation;
        }
    }

    public class IdleState extends EyeState {
        private float isEyeOpenProbability;

        @Override
        public void onNewProbability(float isEyeOpenProbability, PointF cursorLocation) {
            this.isEyeOpenProbability = isEyeOpenProbability;
            this.cursorLocation = cursorLocation;
        }

        @Override
        public void exit() {
            Log.d(TAG,"exited idle state");
        }

        @Override
        public void enter(PointF cursorLocation) {
            Log.d(TAG,"entered idle state");
        }

        @Override
        public EyeState getNextState() {
            if(isEyeClosed(isEyeOpenProbability)) {
                return states[1];
            }
            return null;
        }
    }
    public class StartedBlinkState extends EyeState{

        private int eyeClosedCounter;
        private int eyeOpenCounter;
        @Override
        public void onNewProbability(float isEyeOpenProbability, PointF cursorLocation) {
            if(isEyeClosed(isEyeOpenProbability)){
                eyeClosedCounter++;
                Log.d(TAG,"StartBLinkState: increasing eye closed counter to: " + eyeClosedCounter);
            }
            if(isEyeOpen(isEyeOpenProbability)){
                eyeOpenCounter++;
                Log.d(TAG,"StartBLinkState: stopped blink detected");
            }
        }

        @Override
        public void exit() {
            Log.d(TAG,"exited start blink state");
        }

        @Override
        public void enter(PointF cursorLocation) {
            eyeClosedCounter = 0;
            eyeOpenCounter = 0;
            this.cursorLocation = cursorLocation;
            Log.d(TAG,"entered start blink state. cursor location: " + cursorLocation);
        }

        @Override
        public EyeState getNextState() {
            if(eyeOpenCounter > EYE_OPEN_COUNTER_THRESHOLD){
                return states[0];
            }
            int eyeClosedThreshold = getEyeClosedThreshold();
            Log.d(TAG, "eye closed threshold: " + eyeClosedThreshold);
            if(eyeClosedCounter >= eyeClosedThreshold){
                return states[2];
            }
            return null;
        }

        private int getEyeClosedThreshold() {
            if(sharedPref != null){
                return sharedPref.getInt(CursorApplication.getInstance().getString(R.string.pref_key_blink_sensitivity)
                        , Constants.FaceCursor.DEFAULT_EYE_BLINK_THRESHOLD);
            }else{
                return Constants.FaceCursor.DEFAULT_EYE_BLINK_THRESHOLD;
            }
        }
    }
    public class ClickState extends EyeState{
        private float isEyeOpenProbability;

        @Override
        public void onNewProbability(float isEyeOpenProbability, PointF cursorLocation) {
            //nothing to do
            this.isEyeOpenProbability = isEyeOpenProbability;
        }

        @Override
        public void exit() {
            Log.d(TAG, "exited click state");
        }

        @Override
        public void enter(final PointF cursorLocation) {
            Log.d(TAG, "entered click state. performing click in: " + cursorLocation);
            if(eye.equals(Eye.LEFT)){
                // draw circle on clicked area
                generateClick(cursorLocation);
                canvas.drawCircle(cursorLocation.x, cursorLocation.y, MARKER_RADIUS, clickPaint);

            }else{
                // draw circle on clicked area
                generateLongClick(cursorLocation);
                canvas.drawCircle(cursorLocation.x, cursorLocation.y, MARKER_RADIUS, longPressPaint);

            }
        }

        private void generateLongClick(final PointF cursorLocation) {
            new AsyncTask<Void, Void, Void>(){

                @Override
                protected Void doInBackground(Void... params) {
                    Instrumentation instrumentation = new Instrumentation();
                    try{

                        instrumentation.sendPointerSync(MotionEvent.obtain(
                                SystemClock.uptimeMillis(),
                                SystemClock.uptimeMillis(),
                                MotionEvent.ACTION_DOWN, cursorLocation.x, cursorLocation.y, 0));

                        Thread.sleep(750);
                        instrumentation.sendPointerSync(MotionEvent.obtain(
                                SystemClock.uptimeMillis(),
                                SystemClock.uptimeMillis(),
                                MotionEvent.ACTION_UP, cursorLocation.x, cursorLocation.y, 0));
                        Log.d(TAG, "performed click in (" + cursorLocation.x + ", " + cursorLocation.y + ")");
                    }catch(Exception e){
                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(CursorApplication.getInstance(), "can't send key event to other apps", Toast.LENGTH_SHORT).show();
                            }
                        });

                    }
                    return null;

                }
            }.execute();
        }

        private void generateClick(final PointF cursorLocation) {
            new AsyncTask<Void, Void, Void>(){

                @Override
                protected Void doInBackground(Void... params) {
                    Instrumentation instrumentation = new Instrumentation();
                    try{

                        instrumentation.sendPointerSync(MotionEvent.obtain(
                                SystemClock.uptimeMillis(),
                                SystemClock.uptimeMillis(),
                                MotionEvent.ACTION_DOWN, cursorLocation.x, cursorLocation.y, 0));

                        instrumentation.sendPointerSync(MotionEvent.obtain(
                                SystemClock.uptimeMillis(),
                                SystemClock.uptimeMillis(),
                                MotionEvent.ACTION_UP, cursorLocation.x, cursorLocation.y, 0));
                        Log.d(TAG, "performed click in (" + cursorLocation.x + ", " + cursorLocation.y + ")");
                    }catch(Exception e){
                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(CursorApplication.getInstance(), "can't send key event to other apps", Toast.LENGTH_SHORT).show();
                            }
                        });

                    }
                    return null;

                }
            }.execute();
        }

        @Override
        public EyeState getNextState() {

            // finish click only when eye reopens
            if(isEyeOpen(isEyeOpenProbability)) {
                return states[0];
            }
            return null;
        }
    }

    public enum Eye{
        LEFT,
        RIGHT
    }
}
