package com.yp.cursor.facedetect;

import android.content.Context;
import android.graphics.PixelFormat;
import android.graphics.Point;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;

import com.google.android.gms.vision.CameraSource;
import com.google.android.gms.vision.Tracker;
import com.google.android.gms.vision.face.Face;
import com.google.android.gms.vision.face.FaceDetector;
import com.google.android.gms.vision.face.LargestFaceFocusingProcessor;
import com.yp.cursor.SingleInput;
import com.yp.cursor.ViewInputHolder;

import java.io.IOException;
import java.util.Collections;
import java.util.List;


/**
 * @author Avner Elizarov
 */
public class FdView extends ViewInputHolder {
    private static final String TAG = "FdVIew";
    public static final String TITLE = "Cursor";
    private CameraSourcePreview cameraSourcePreview;
    private  GraphicOverlay graphicOverlay;
    private CameraSource cameraSource;
    private Context context;
    private Point screenDimensions;

    public FdView(Context context, Point screenDimensions) {
        super(context);
        this.screenDimensions = screenDimensions;
        this.context = context;
        cameraSourcePreview =  new CameraSourcePreview(context, this);
        setZOrderOnTop(true);
        getHolder().setFormat(PixelFormat.TRANSPARENT);
    }

    private void createCameraSource(Context context, Point screenDimensions) {
        graphicOverlay = new GraphicOverlay(context);
        Context appContext = context.getApplicationContext();
        FaceDetector detector = new FaceDetector.Builder(appContext)
                .setClassificationType(FaceDetector.ALL_CLASSIFICATIONS)
                .setProminentFaceOnly(true)
                .setMode(FaceDetector.ACCURATE_MODE)
                .build();

        GraphicFaceTracker faceTracker = new GraphicFaceTracker(graphicOverlay, screenDimensions);
        detector.setProcessor(
                new LargestFaceFocusingProcessor(detector, faceTracker));

        if (!detector.isOperational()) {
            // Note: The first time that an app using face API is installed on a device, GMS will
            // download a native library to the device in order to do detection.  Usually this
            // completes before the app is run for the first time.  But if that download has not yet
            // completed, then the above call will not detect any faces.
            //
            // isOperational() can be used to check if the required native library is currently
            // available.  The detector will automatically become operational once the library
            // download completes on device.
            Toast.makeText(appContext, "android vision dependencies aren't installed. face tracking won't work", Toast.LENGTH_SHORT).show();
            Log.w(TAG, "Face detector dependencies are not yet available.");
        }

        cameraSource = new CameraSource.Builder(appContext, detector)
                .setRequestedPreviewSize(320, 240)
                .setFacing(CameraSource.CAMERA_FACING_FRONT)
                .setRequestedFps(30.0f)
                .build();

    }


    @Override
    public List<SingleInput> getInputs() {
        return Collections.EMPTY_LIST;
    }

    @Override
    public void stop() {
        if(cameraSourcePreview != null){
            cameraSourcePreview.stop();
            cameraSourcePreview.release();
        }
        ((WindowManager) context.getSystemService(Context.WINDOW_SERVICE)).removeView(graphicOverlay);
        ((WindowManager) context.getSystemService(Context.WINDOW_SERVICE)).removeView(cameraSourcePreview);

    }

    @Override
    public void start() {

        createCameraSource(context, screenDimensions);
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        addView(wm, cameraSourcePreview);
        addView(wm, graphicOverlay);

        if (cameraSource != null) {
            try {
                cameraSourcePreview.start(cameraSource, graphicOverlay);
            } catch (IOException e) {
                Log.e(TAG, "Unable to start camera source.", e);
                cameraSource.release();
                cameraSource = null;
            }
        }

    }

    @Override
    public void setOrientation(int orientation, Point screenDimensions) {
        this.screenDimensions = screenDimensions;
        graphicOverlay.getResources().getConfiguration().orientation = orientation;
    }

    private void addView(WindowManager wm, View viewToDisplay) {
        WindowManager.LayoutParams params = new WindowManager.LayoutParams(
                WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.TYPE_SYSTEM_OVERLAY,
                WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE | WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE |
                        WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN, //will cover status bar as well!!!
                PixelFormat.TRANSPARENT);
        params.gravity = Gravity.START | Gravity.TOP;
        params.setTitle(TITLE);
        wm.addView(viewToDisplay, params);
    }

    @Override
    public void run() {
        start();
    }

    //==============================================================================================
    // Graphic Face Tracker
    //==============================================================================================

    /**
     * Face tracker for each detected individual. This maintains a face graphic within the app's
     * associated face overlay.
     */
    private class GraphicFaceTracker extends Tracker<Face> {
        private GraphicOverlay mOverlay;
        private FaceGraphic mFaceGraphic;

        GraphicFaceTracker(GraphicOverlay overlay, Point screenDimensions) {
            mOverlay = overlay;
            mFaceGraphic = new FaceGraphic(overlay,screenDimensions);
        }

        /**
         * Start tracking the detected face instance within the face overlay.
         */
        @Override
        public void onNewItem(int faceId, Face item) {
        }

        /**
         * Update the position/characteristics of the face within the overlay.
         */
        @Override
        public void onUpdate(FaceDetector.Detections<Face> detectionResults, Face face) {
            mOverlay.add(mFaceGraphic);
            mFaceGraphic.updateFace(face);
        }

        /**
         * Hide the graphic when the corresponding face was not detected.  This can happen for
         * intermediate frames temporarily (e.g., if the face was momentarily blocked from
         * view).
         */
        @Override
        public void onMissing(FaceDetector.Detections<Face> detectionResults) {
            mOverlay.remove(mFaceGraphic);
        }

        /**
         * Called when the face is assumed to be gone for good. Remove the graphic annotation from
         * the overlay.
         */
        @Override
        public void onDone() {
            mOverlay.remove(mFaceGraphic);
        }

    }

}
