package com.yp.activities;

import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.graphics.Point;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.PersistableBundle;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.ActionBar;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.yp.bts.BluetoothEventReceiver;
import com.yp.bts.BluetoothReceiver;
import com.yp.classes.ClickOnlyView;
import com.yp.classes.CmdDispatcher;
import com.yp.cursor.CursorApplication;
import com.yp.cursor.CursorService;
import com.yp.cursor.R;
import com.yp.cursor.SingleInput;
import com.yp.cursor.ViewInputHolder;
import com.yp.cursor.facedetect.FdView;
import com.yp.cursor.onebutton.AxisMoverView;
import com.yp.navigationlibrary.EmergencyCallActivity;
import com.yp.navigationlibrary.IterableGroup;
import com.yp.navigationlibrary.IterableViewGroup;
import com.yp.navigationlibrary.IterableViewGroups;
import com.yp.remoteInputSim.ChooseRemoteBluetoothDeviceActivity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * Main container activity for the 3-Boom application.
 *
 * Displays the main menu from which all other functionalities are accessed.
 *
 * @author Matan Revivo, Avner Elizarov, Oded Greenberg
 */
public class MainActivity extends EmergencyCallActivity implements View.OnClickListener, BluetoothEventReceiver {

    /* CONSTANTS */
    private static final String MAIN_ACTIVITY_TAG = "MAIN_ACTIVITY";
    private static final String MENU_TAG = "MAIN_MENU_TAG";
    private static final String FIRST_CREATE_FLAG = "FIRST_CREATION";
    private static final int RC_HANDLE_SYSTEM_ALERT_PERM = 13;
    private static final int REQUEST_ENABLE_BT = 1; // must be different than 0
    private static final int BT_REQUEST_TIMEOUT = 30; // in seconds
    public static final float NAV_ALPHA_FACTOR = (float)0.5;
    private static final int DEFAULT_BT_EVENT_SENSITIVITY = 5;

    /* WIDGETS */
    ImageButton ib_messages;
    ImageButton ib_phone;
    ImageButton ib_contacts;
    ImageButton ib_media_player;
    ImageButton ib_camera;
    ImageButton ib_snake;
    ImageButton ib_painter;
    ImageButton ib_browser;
    ToggleButton tb_bluetooth;
    ImageButton ib_settings;
    ImageButton ib_developer;
    ImageButton ib_one_button;
    ImageButton ib_face_detection;
    ImageButton ib_remove_cursor;
    ImageButton ib_editor;
    ImageButton ib_gallery;

    /* MEMBERS */
    private BluetoothReceiver btReceiver;
    private BluetoothAdapter btAdapter;
    private ProgressDialog spinner;
    private Context ctx;
    private SharedPreferences sharedPref;
    private CmdDispatcher dispatcher;
    private SingleInput[] navSimpleInputs = new ClickOnlyView[3];
    private Animation fabPressAnimation;
    private boolean autoReconnect;
    private boolean shouldAutoReconnect;
    private Toast mToast;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        PreferenceManager.setDefaultValues(this, R.xml.preferences, false);
        sharedPref = PreferenceManager.getDefaultSharedPreferences(this);

        setContentView(R.layout.activity_main);
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);

        // setup activity action bar icon
        final ActionBar actionBar = getSupportActionBar();
        if (null != actionBar) {
            actionBar.setDisplayShowHomeEnabled(true);
            actionBar.setTitle("Main Menu");
            actionBar.setIcon(R.mipmap.app_icon);
        }

        ctx = getApplicationContext();

        // if BT isn't supported - this won't take up resources
        btAdapter = BluetoothAdapter.getDefaultAdapter();
        btReceiver = new BluetoothReceiver(ctx, getString(R.string.BT_UUID));

        initImageButtonsWidgets();
        initProgressDialogSpinner();
        initNavButtons();
        restartGroup(sharedPref.getBoolean(getString(R.string.pref_key_show_developer), false));

        dispatcher = new CmdDispatcher();
        CursorApplication.getInstance().dispatcher = dispatcher;
        CursorApplication.getInstance().bluetoothReceiver = btReceiver;
        dispatcher.registerToBTServer();

        startService(new Intent(this, CursorService.class));

        this.registerReceiver(mReceiver, new IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED));

        autoReconnect = sharedPref.getBoolean(getString(R.string.pref_key_bt_auto_reconnect), false);

        if (sharedPref.getBoolean(getString(R.string.pref_key_bt_auto_connect), false)) {
            Log.d(MAIN_ACTIVITY_TAG, "onCreate: BT auto-connecting...");
            tb_bluetooth.toggle();
            handleBtToggle();
        }
    }

    /**
     * Create and initialize the spinner dialog that is displayed whenever trying to connect to a
     * bluetooth sender.
     */
    private void initProgressDialogSpinner() {
        spinner = new ProgressDialog(this, ProgressDialog.STYLE_SPINNER);
        spinner.setMessage(String.format(getString(R.string.bt_connecting_spinner_msg_format), BT_REQUEST_TIMEOUT));
        spinner.setTitle(getString(R.string.dialog_title_bt_connecting));
        spinner.setCancelable(true);
        spinner.setIndeterminate(true);
        spinner.setCanceledOnTouchOutside(true);
        spinner.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override public void onCancel(DialogInterface dialog) {
                Log.d(MAIN_ACTIVITY_TAG, "Cancelled BT connection");
                shouldAutoReconnect = false;
                btReceiver.stop(true);
                timer.shutdown();
                toggleBtOff();
            }
        });

        spinner.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                timer.shutdownNow();
            }
        });
    }

    @NonNull
    private List<IterableGroup> buildNavMenuGroups(boolean withDeveloper) {
        List<IterableGroup> $ = new ArrayList<>();

        //Creating the groups for the navigation
        //Row 1 group
        List<View> row1_list = new ArrayList<>();
        row1_list.addAll(Arrays.asList(ib_phone, ib_messages, ib_contacts, ib_camera));
        $.add(new IterableViewGroup(row1_list, findViewById(R.id.ll_row1), this));

        //Row 2 group
        List<View> row2_list = new ArrayList<>();
        row2_list.addAll(Arrays.asList(ib_media_player, ib_browser, ib_snake, ib_painter));
        $.add(new IterableViewGroup(row2_list, findViewById(R.id.ll_row2), this));

        //Row 3 group
        List<View> row3_list = new ArrayList<>();
        row3_list.addAll(Arrays.asList(ib_remove_cursor, ib_face_detection, ib_one_button, ib_editor));
        $.add(new IterableViewGroup(row3_list, findViewById(R.id.ll_row3), this));

        //Row 4 group
        List<View> row4_list = new ArrayList<>();
        row4_list.addAll(Arrays.asList(ib_gallery, ib_settings));
        if (withDeveloper) {
            row4_list.add(ib_developer);
        }

        if (isBluetoothSupported()) { row4_list.add(0, tb_bluetooth); }
        else { tb_bluetooth.setText(R.string.image_button_label_no_bt); }
        $.add(new IterableViewGroup(row4_list, findViewById(R.id.ll_row4), this));

        return $;
    }

    /**
     * Switch developer button to invisible, and remove it from the NAV system.
     */
    private void hideDeveloperButton() {
        ib_developer.setVisibility(View.INVISIBLE);
        ib_developer.setEnabled(false);
        findViewById(R.id.tv_developer).setVisibility(View.INVISIBLE);
        restartGroup(false);
    }

    /**
     * Restart NAV system, whenever a change in button visibility must be performed.
     *
     * @param withDeveloper flags whether to display the developer button or not.
     */
    private void restartGroup(boolean withDeveloper) {
        if (null != group) group.stop();
        group = new IterableViewGroups(buildNavMenuGroups(withDeveloper), findViewById(R.id.ll_all_rows), this);
        group.start();
    }

    /**
     * Switch developer button to visible, and add it to the NAV system.
     */
    private void displayDeveloperButton() {
        ib_developer.setVisibility(View.VISIBLE);
        ib_developer.setEnabled(true);
        findViewById(R.id.tv_developer).setVisibility(View.VISIBLE);
        restartGroup(true);
    }

    private void initNavButtons() {
        //Setting the 3 buttons navigation system for the activity
        fab_left   = (FloatingActionButton) findViewById(R.id.fab_left);
        fab_right  = (FloatingActionButton) findViewById(R.id.fab_right);
        fab_middle = (FloatingActionButton) findViewById(R.id.fab_middle);

        // Setting the listeners for the navigation buttons, and make them semi-transparent
        for (FloatingActionButton fab : Arrays.asList(fab_left, fab_right, fab_middle)) {
            fab.setOnClickListener(this);
            fab.setOnLongClickListener(this);
            fab.setAlpha(NAV_ALPHA_FACTOR);
        }

        navSimpleInputs[0] = new ClickOnlyView(this, fab_right);
        navSimpleInputs[1] = new ClickOnlyView(this, fab_middle);
        navSimpleInputs[2] = new ClickOnlyView(this, fab_left);

        fabPressAnimation = AnimationUtils.loadAnimation(getApplication(), R.anim.fab_main_press);
    }

    private void initImageButtonsWidgets() {
        //Getting all the image buttons
        ib_messages       = (ImageButton) findViewById(R.id.ib_messages);
        ib_phone          = (ImageButton) findViewById(R.id.ib_phone);
        ib_contacts       = (ImageButton) findViewById(R.id.ib_contacts);
        ib_media_player   = (ImageButton) findViewById(R.id.ib_media_player);
        ib_snake          = (ImageButton) findViewById(R.id.ib_snake);
        ib_camera         = (ImageButton) findViewById(R.id.ib_camera);
        ib_painter        = (ImageButton) findViewById(R.id.ib_painter);
        ib_browser        = (ImageButton) findViewById(R.id.ib_browser);
        ib_developer      = (ImageButton) findViewById(R.id.ib_developer);
        ib_settings       = (ImageButton) findViewById(R.id.ib_settings);
        ib_one_button     = (ImageButton) findViewById(R.id.ib_one_button);
        ib_face_detection = (ImageButton) findViewById(R.id.ib_face_detection);
        ib_remove_cursor  = (ImageButton) findViewById(R.id.ib_remove_cursor);
        tb_bluetooth      = (ToggleButton) findViewById(R.id.tb_bluetooth);
        ib_editor         = (ImageButton) findViewById(R.id.ib_editor);
        ib_gallery        = (ImageButton) findViewById(R.id.ib_gallery);

        final View[] buttons = {ib_messages, ib_phone, ib_contacts, ib_media_player, ib_camera,
                ib_snake, ib_painter, ib_browser, tb_bluetooth, ib_settings, ib_developer,
                ib_one_button, ib_face_detection, ib_remove_cursor, ib_editor, ib_gallery};

        //Setting this activity as the on click listener for all the image buttons
        for (View v : buttons) {
            v.setOnClickListener(this);
        }

        if (!isBluetoothSupported()) {
            tb_bluetooth.setEnabled(false);
            tb_bluetooth.setClickable(false);
        }
    }

    private boolean isBluetoothSupported() {
        return (btAdapter != null);
    }

    @Override
    public void onClick(View v) {
        //For the navigation buttons
        super.onClick(v);

        switch(v.getId()) {
            case R.id.ib_messages:
                menuPressedLog("SMS");
                startActivity(new Intent(ctx, SmsActivity.class));
                break;
            case R.id.ib_phone:
                menuPressedLog("Phone");
                startActivity(new Intent(ctx, PhoneActivity.class));
                break;
            case R.id.ib_contacts:
                menuPressedLog("Contacts");
                startActivity(new Intent(ctx, ContactsActivity.class));
                break;
            case R.id.ib_media_player:
                menuPressedLog("Media Player");
                startActivity(new Intent(ctx, AudioActivity.class));
                break;
            case R.id.ib_snake:
                menuPressedLog("Snake");
                startActivity(new Intent(ctx, SnakeActivity.class));
                break;
            case R.id.ib_camera:
                menuPressedLog("Camera");
                startActivity(new Intent(ctx, CameraActivity.class));
                break;
            case R.id.ib_painter:
                menuPressedLog("Painter");
                startActivity(new Intent(ctx, PainterActivity.class));
                break;
            case R.id.tb_bluetooth:
                menuPressedLog("Bluetooth");
                shouldAutoReconnect = false;
                handleBtToggle();
                break;
            case R.id.ib_browser:
                menuPressedLog("Browser");
                startActivity(new Intent(ctx, BrowserActivity.class));
                break;
            case R.id.ib_developer:
                menuPressedLog("Developer");
                openInputSimulator();
                break;
            case R.id.ib_settings:
                menuPressedLog("Settings");
                startActivity(new Intent(ctx, SettingsActivity.class));
                break;
            case R.id.ib_one_button:
                menuPressedLog("One Button Cursor");
                onOneButtonCursor(v);
                break;
            case R.id.ib_face_detection:
                menuPressedLog("Face Detection Cursor");
                onDetectionCursor(v);
                break;
            case R.id.ib_remove_cursor:
                menuPressedLog("Remove Cursor");
                stopCursorService();
                break;
            case R.id.ib_editor:
                menuPressedLog("Editor");
                startActivity(new Intent(ctx, EditorActivity.class));
                break;
            case R.id.ib_gallery:
                menuPressedLog("Gallery");
                startActivity(new Intent(ctx, GalleryActivity.class));
                break;
            case R.id.fab_left:
                menuPressedLog("Left FAB");
                fab_left.startAnimation(fabPressAnimation);
                break;
            case R.id.fab_middle:
                menuPressedLog("Middle FAB");
                fab_middle.startAnimation(fabPressAnimation);
                break;
            case R.id.fab_right:
                menuPressedLog("Right FAB");
                fab_right.startAnimation(fabPressAnimation);
                break;
            default:
                Log.d(MENU_TAG, "Unhandled view: " + v.toString());
                break;
        }
    }

    private void menuPressedLog(String s) {
        Log.d(MENU_TAG, s + " pressed");
    }

    /**
     * called when detection cursor button is pressed. starts the cursor service with a face detection cursor.
     * @param view - button pressed
     */
    public void onDetectionCursor(View view) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M || Settings.canDrawOverlays(this)) {
            startFdView();
            return;
        }

        requestSystemAlertPermission();
    }

    private void startFdView() {
        showToast(R.string.main_toast_cursor_shown);
        final Point screenDimensions = new Point();
        getWindowManager().getDefaultDisplay().getSize(screenDimensions);

        final ViewInputHolder fdView = new FdView(this, screenDimensions);
        stopCursorService();
        startCursorService(fdView);
    }

    private void requestSystemAlertPermission() {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                                       Uri.parse("package:" + getPackageName()));
            startActivityForResult(intent, RC_HANDLE_SYSTEM_ALERT_PERM);
        }
    }


    /**
     * called when detection cursor button is pressed. starts the cursor service with a face detection cursor.
     * @param v - button pressed
     */
    public void onOneButtonCursor(View v) {
        Log.i(MAIN_ACTIVITY_TAG, "onOneButtonCursor called");
        stopCursorService();
        startCursorService(new AxisMoverView(ctx));
    }

    private void startCursorService(ViewInputHolder view) {
        final CursorService cursorService = CursorApplication.getInstance().getCursorService();

        if (cursorService != null) {
            cursorService.start(view);
        } else {
            Log.e(MAIN_ACTIVITY_TAG, "cursor service is null when trying to start it");
        }
    }

    private void stopCursorService() {
        final CursorService cursorService = CursorApplication.getInstance().getCursorService();
        if (cursorService != null) {
            cursorService.stop();
        }
    }

    @Override
    protected void onDestroy() {
        Log.i(MAIN_ACTIVITY_TAG, "onDestroy");
        stopCursorService();
        super.onDestroy();
        dispatcher.unregisterFromBTServer();
        btReceiver.stop(true);
        unregisterReceiver(mReceiver);
    }

    ScheduledExecutorService timer;
    private void displaySpinner() {
        if (timer != null && !timer.isShutdown()) {
            Log.d(MAIN_ACTIVITY_TAG, "displaySpinner: TIMER ALREADY RUNNING!");
            timer.shutdownNow();
            timer = null;
        }

        spinner.setMessage(String.format(getString(R.string.bt_connecting_spinner_msg_format), BT_REQUEST_TIMEOUT));
        timer = Executors.newSingleThreadScheduledExecutor();
        timer.scheduleAtFixedRate(new Runnable() {
            int secs = BT_REQUEST_TIMEOUT;

            @Override
            public void run() {
                Log.d(MAIN_ACTIVITY_TAG, "(TIMER) run: iteration " + secs);
                if (0 >= secs) {
                    timer.shutdown();
                    Log.d(MAIN_ACTIVITY_TAG, "Connection timed-out!");
                    onDisconnect();
                    btReceiver.stop(false);
                    return;
                }

                secs--;

                if (null != spinner && spinner.isShowing()) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            spinner.setMessage(String.format(getString(R.string.bt_connecting_spinner_msg_format), secs));
                        }
                    });
                }
            }
        }, 1, 1, TimeUnit.SECONDS);
        spinner.show();
    }

    final Object spinnerLock = new Object();
    private void dismissSpinner() {
        Log.d(MAIN_ACTIVITY_TAG, "dismissSpinner: DISMISSING SPINNER");

        if (null == spinner) {
            Log.d(MAIN_ACTIVITY_TAG, "dismissSpinner: NO SPINNER TO DISMISS");
            return;
        }

        synchronized (spinnerLock) {
            if (!spinner.isShowing()) {
                Log.d(MAIN_ACTIVITY_TAG, "dismissSpinner: SPINNER IS NOT SHOWING");
                return;
            }

            new Thread(new Runnable() {
                @Override public void run() {
                    runOnUiThread(new Runnable() {
                        @Override public void run() { if (null != spinner) spinner.dismiss(); }
                    });
                }
            }).start();
        }
    }

    // state flags for handling BT de/activation via intents
    boolean waitingToOpenReceiver = false;
    boolean waitingToOpenSender = false;

    /**
     * Turn Bluetooth Receiver On/Off to start/stop receiving inputs via bluetooth.
     */
    private void handleBtToggle() {
        if (tb_bluetooth.isChecked()) {
            Log.d(MAIN_ACTIVITY_TAG, "handleBtToggle: BT TOGGLED ON");
            if (!isBluetoothSupported()) {
                Log.d(MAIN_ACTIVITY_TAG, "handleBtToggle: BT unsupported");
                return;
            }

            if (isBluetoothEnabled()) {
                displaySpinner();
                btReceiver.start(this);
                return;
            }

            Log.d(MAIN_ACTIVITY_TAG, "handleBtToggle: Trying to open BT receiver");
            waitingToOpenReceiver = true;
            tryEnableBluetooth();
        }
        else {
            Log.d(MAIN_ACTIVITY_TAG, "handleBtToggle: BT TOGGLED OFF");
            btReceiver.stop(true);
        }
    }

    private boolean isBluetoothEnabled() {
        return (isBluetoothSupported() && btAdapter.isEnabled());
    }

    private void openInputSimulator() {
        Log.d(MAIN_ACTIVITY_TAG, "openInputSimulator: Trying to open simulator...");
        if (!isBluetoothSupported()) {
            showToast(R.string.toast_bt_not_supported);
            return;
        }

        if (!isBluetoothEnabled()) {
            // ask user to enable BT in device
            waitingToOpenSender = true;
            Log.d(MAIN_ACTIVITY_TAG, "tryEnableBluetooth: asking user to enable BT");
            Intent discoverableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
            startActivityForResult(discoverableIntent, REQUEST_ENABLE_BT);
            return;
        }

        // already enabled
        Intent intent = new Intent(this, ChooseRemoteBluetoothDeviceActivity.class);
        startActivity(intent);
    }

    private void tryEnableBluetooth() {
        if (!isBluetoothSupported()) {
            showToast(R.string.toast_bt_not_supported);
            return;
        }

        if (!isBluetoothEnabled()) {
            // ask user to enable BT in device
            Log.d(MAIN_ACTIVITY_TAG, "tryEnableBluetooth: asking user to enable BT");
            Intent discoverableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
            startActivityForResult(discoverableIntent, REQUEST_ENABLE_BT);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RC_HANDLE_SYSTEM_ALERT_PERM) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (Settings.canDrawOverlays(this)) {
                    startFdView();
                    // SYSTEM_ALERT_WINDOW permission not granted...
                } else {
                    showToast(R.string.main_toast_please_provide_alert_perm);
                }
            }
        }

        if (REQUEST_ENABLE_BT == requestCode) {
            if (RESULT_CANCELED == resultCode) {
                Log.d(MAIN_ACTIVITY_TAG, "onActivityResult: Bluetooth disabled - can't receiver/simulate");
                cancelWaitingForBt();
                if (tb_bluetooth.isChecked()) {
                    tb_bluetooth.setChecked(false);
                }

                return;
            }

            Log.d(MAIN_ACTIVITY_TAG, "handleBtToggle: BT TOGGLED ON");

            if (waitingToOpenReceiver) {
                cancelWaitingForBt();
                displaySpinner();
                btReceiver.start(this);
            }

            if (waitingToOpenSender) { // open simulator activity
                cancelWaitingForBt();
                startActivity(new Intent(this, ChooseRemoteBluetoothDeviceActivity.class));
            }
        }
    }

    private void cancelWaitingForBt() {
        waitingToOpenReceiver = false;
        waitingToOpenSender = false;
    }

    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            switch (intent.getAction()) {
                case BluetoothAdapter.ACTION_STATE_CHANGED:
                    // It means the user has changed his bluetooth state.
                    switch (btAdapter.getState()) {
                        case BluetoothAdapter.STATE_TURNING_OFF:
                        case BluetoothAdapter.STATE_OFF:
                            Log.d(MAIN_ACTIVITY_TAG, "onReceive: BT TURNING OFF");
                            tb_bluetooth.setChecked(false);
                            return;
                        case BluetoothAdapter.STATE_ON:
                            Log.d(MAIN_ACTIVITY_TAG, "onReceive: BT IS NOW ON");
                            return;
                    }
                    break;
            }
        }
    };

    private void unregisterFromBT() {
        for (int i = 0; i < navSimpleInputs.length; ++i) {
            dispatcher.setInputToDispatch(i, null);
        }
    }

    private void registerToBt() {
        for (int i = 0; i < navSimpleInputs.length; ++i) {
            dispatcher.setInputToDispatch(i, navSimpleInputs[i]);
        }
    }

    private void showToast(String s) {
        if (mToast != null) mToast.cancel();
        mToast = Toast.makeText(this, s, Toast.LENGTH_SHORT);
        mToast.show();
    }

    private void showToast(int id) {
        showToast(getString(id));
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(MAIN_ACTIVITY_TAG, "onPause: MAIN_ACTIVITY_PAUSED");
        unregisterFromBT();
    }

    @Override
    protected void onResume() {
        Log.d(MAIN_ACTIVITY_TAG, "onPause: MAIN_ACTIVITY_RESUMED");
        super.onResume();
        registerToBt();
        handleSharedPrefUpdates();
    }

    /**
     * Apply changes in the global application's shared preferences, when resuming from the
     * settings activity.
     */
    private void handleSharedPrefUpdates() {
        // handle changes in 'developer' button visibility
        final boolean shouldShowDeveloper =
                sharedPref.getBoolean(getString(R.string.pref_key_show_developer), false);

        if (shouldShowDeveloper && View.INVISIBLE == ib_developer.getVisibility()) {
            displayDeveloperButton();
        }

        if (!shouldShowDeveloper && View.VISIBLE == ib_developer.getVisibility()) {
            hideDeveloperButton();
        }

        // handle changes in BT event generation sensitivity
        final int translatorSensitivity =
                sharedPref.getInt(getString(R.string.pref_key_bt_event_sensitivity),
                                  DEFAULT_BT_EVENT_SENSITIVITY);

        if (btReceiver.getEventGenerationSensitivity() != translatorSensitivity) {
            btReceiver.setEventGenerationSensitivity(translatorSensitivity);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
        outState.putBoolean(FIRST_CREATE_FLAG, false);
        super.onSaveInstanceState(outState, outPersistentState);
    }

    @Override
    public void onConnect(boolean success) {
        if (success) {
            Log.d(MAIN_ACTIVITY_TAG, "onConnect: SUCCESSFUL CONNECT");
            shouldAutoReconnect = true;
            dismissSpinner();
        }
    }

    /**
     * Toggle the Bluetooth off. For use by external classes, or in callbacks from non-UI threads.
     */
    public void toggleBtOff() {
        if (tb_bluetooth.isChecked()) {
            tb_bluetooth.toggle();
        }
    }

    @Override
    public void onDisconnect() {
        dismissSpinner();

        (new Handler(ctx.getMainLooper())).post(new Runnable() {
            @Override
            public void run() {
                Log.d(MAIN_ACTIVITY_TAG, "onDisconnect: BT disconnected");
                showToast(R.string.toast_bt_disconnected);
                toggleBtOff();
                if (autoReconnect && shouldAutoReconnect) {
//                    showToast("SHOULD RECONNECT");
                    Log.d(MAIN_ACTIVITY_TAG, "run: try to reconnect...");
                    tb_bluetooth.toggle();
                    handleBtToggle();
                }
            }
        });
    }
}
