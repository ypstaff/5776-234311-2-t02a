package com.yp.activities;

/*
*
* *@author Matan Revivo
*
 */

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

import com.yp.cursor.CursorApplication;
import com.yp.cursor.R;
import com.yp.cursor.SingleInput;

import java.io.IOException;
import java.math.BigDecimal;

public class AudioActivity extends Activity {

    public static final int PROGRESS = 0;
    public static final double MSTOSEC = 1000.0;
    public static final double SECTOMIN = 60.0;
    public static final int LASTPOSITION = -1;
    //The xml components
    private TextView selectedFile = null;
    private SeekBar seekbar = null;
    private ImageButton b_play = null;
    private ImageButton b_prev = null;
    private ImageButton b_next = null;
    private ListView l_audio = null;

    //constants
    private static final int UPDATE_FREQUENCY = 500;
    private static final int STEP_VALUE = 4000;

    //class components
    private MediaPlayer player = null;
    private MediaCursorAdapter mediaAdapter = null;
    private int lastPosition = -1;
    private String currentFile = "";
    private boolean isMovingSeekBar = false;
    private final Handler handler = new Handler();

    private final Runnable updatePositionRunnable = new Runnable() {
        public void run() {
            updatePosition();
        }
    };

    /**
     * init the class fields and adapters
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_audio);

        selectedFile = (TextView) findViewById(R.id.selectedfile);
        seekbar = (SeekBar) findViewById(R.id.seekbar);
        b_play = (ImageButton) findViewById(R.id.play);
        b_prev = (ImageButton) findViewById(R.id.prev);
        b_next = (ImageButton) findViewById(R.id.next);

        player = new MediaPlayer();
        player.setOnCompletionListener(onCompletion);
        player.setOnErrorListener(onError);
        seekbar.setOnSeekBarChangeListener(seekBarChanged);

        Cursor cursor = getContentResolver().query(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, null, null, null, null);
        if (null != cursor) {
            cursor.moveToFirst();

            mediaAdapter = new MediaCursorAdapter(this, R.layout.audio_listitem, cursor);

            l_audio = (ListView) findViewById(R.id.lv_audio);
            l_audio.setAdapter(mediaAdapter);
            l_audio.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
            l_audio.setItemChecked(0, true);
            l_audio.setClickable(false);

            b_play.setOnClickListener(onButtonClick);
            b_next.setOnClickListener(onButtonClick);
            b_prev.setOnClickListener(onButtonClick);
            b_prev.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    finish();
                    return false;
                }
            });
        }
    }


    @Override
    protected void onPause() {
        super.onPause();
        unregisterFromBT();
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerToBt();

    }

    private void unregisterFromBT() {
        CursorApplication.getInstance().dispatcher.setInputToDispatch(0, null);
        CursorApplication.getInstance().dispatcher.setInputToDispatch(1, null);
        CursorApplication.getInstance().dispatcher.setInputToDispatch(2, null);
    }

    private void registerToBt() {
        CursorApplication.getInstance().dispatcher.setInputToDispatch(0, new SingleInput() {
            @Override
            public void onClick() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        b_prev.callOnClick();
                    }
                });

            }

            @Override
            public void onLongPressStart() {
            }

            @Override
            public void onLongPressStop() {
                finish();
            }

            @Override
            public void onDoubleClick() {}

            @Override
            public boolean isDoubleClickEnabled() {
                return false;
            }
        });
        CursorApplication.getInstance().dispatcher.setInputToDispatch(1, new SingleInput() {
            @Override
            public void onClick() {
               runOnUiThread(new Runnable() {
                   @Override
                   public void run() {
                       b_play.callOnClick();
                   }
               });
            }

            @Override
            public void onLongPressStart() {
            }

            @Override
            public void onLongPressStop() {
            }

            @Override
            public void onDoubleClick() {
            }

            @Override
            public boolean isDoubleClickEnabled() {
                return false;
            }
        });
        CursorApplication.getInstance().dispatcher.setInputToDispatch(2, new SingleInput() {
            @Override
            public void onClick() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        b_next.callOnClick();
                    }
                });
            }

            @Override
            public void onLongPressStart() {
            }

            @Override
            public void onLongPressStop() {
            }

            @Override
            public void onDoubleClick() {
            }

            @Override
            public boolean isDoubleClickEnabled() {
                return false;
            }
        });

    }


    @Override
    protected void onDestroy() {
        super.onDestroy();

        handler.removeCallbacks(updatePositionRunnable);
        player.stop();
        player.reset();
        player.release();

        player = null;
    }

    /**
     * start play the file which is name is the param
     * @param file
     */
    private void startPlay(String file) {
        Log.i("Selected: ", file);

        selectedFile.setText(file);
        seekbar.setProgress(0);

        player.stop();
        player.reset();

        try {
            player.setDataSource(file);
            player.prepare();
            player.start();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        seekbar.setMax(player.getDuration());
        b_play.setImageResource(android.R.drawable.ic_media_pause);

        updatePosition();

    }

    /**
     * stop play the song is currently playing
     */
    private void stopPlay() {
        player.stop();
        player.reset();
        b_play.setImageResource(android.R.drawable.ic_media_play);
        handler.removeCallbacks(updatePositionRunnable);
        seekbar.setProgress(PROGRESS);

    }

    /**
     * update the progress of the song
     */
    private void updatePosition() {
        handler.removeCallbacks(updatePositionRunnable);
        seekbar.setProgress(player.getCurrentPosition());
        handler.postDelayed(updatePositionRunnable, UPDATE_FREQUENCY);
    }

    /**
     * adapter of the songs list
     */
    private class MediaCursorAdapter extends SimpleCursorAdapter {

        public MediaCursorAdapter(Context context, int layout, Cursor c) {
            super(context, layout, c,
                    new String[]{MediaStore.MediaColumns.DISPLAY_NAME,
                            MediaStore.MediaColumns.TITLE,
                            MediaStore.Audio.AudioColumns.DURATION},
                    new int[]{R.id.displayname,
                            R.id.title,
                            R.id.duration});
        }

        @Override
        public void bindView(View view, Context context, Cursor cursor) {
            TextView title = (TextView) view.findViewById(R.id.title);
            TextView name = (TextView) view.findViewById(R.id.displayname);
            TextView duration = (TextView) view.findViewById(R.id.duration);

            String title_content = cursor.getString(cursor.getColumnIndex(MediaStore.MediaColumns.TITLE));
            if(title_content != null)
                title.setText(title_content);
            else
                title.setText("unknown");

            String name_content = cursor.getString(cursor.getColumnIndex(MediaStore.MediaColumns.DISPLAY_NAME));
            if(name_content != null)
                name.setText(name_content);
            else
                name.setText("unknown");

            String duration_content = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.AudioColumns.DURATION));
            if(duration_content != null) {
                long durationInMs = Long.parseLong(duration_content);
                double durationInMin = ((double) durationInMs / MSTOSEC) / SECTOMIN;
                durationInMin = new BigDecimal(Double.toString(durationInMin)).setScale(2, BigDecimal.ROUND_UP).doubleValue();
                duration.setText("" + durationInMin);
            }
            else{
                duration.setText("" + "unknown");
            }
            view.setTag(cursor.getString(cursor.getColumnIndex(MediaStore.MediaColumns.DATA)));
        }

        @Override
        public View newView(Context context, Cursor cursor, ViewGroup parent) {
            LayoutInflater inflater = LayoutInflater.from(context);
            View v = inflater.inflate(R.layout.audio_listitem, parent, false);

            bindView(v, context, cursor);

            return v;
        }
    }

    /**
     *
     * @param pos
     * @param listView
     * @return The view in the pos position in the listView
     */
    public View getViewByPosition(int pos, ListView listView) {
        final int firstListItemPosition = listView.getFirstVisiblePosition();
        final int lastListItemPosition = firstListItemPosition + listView.getChildCount() - 1;

        if (pos < firstListItemPosition || pos > lastListItemPosition) {
            return listView.getAdapter().getView(pos, null, listView);
        } else {
            final int childIndex = pos - firstListItemPosition;
            return listView.getChildAt(childIndex);
        }
    }

    /**
     * handle onclick of all buttons in this activity
     */
    private OnClickListener onButtonClick = new OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.play: {
                    playHandler();
                    break;
                }
                case R.id.next: {
                    nextHandler();
                    break;
                }
                case R.id.prev: {
                    prevHandler();
                    break;
                }
            }
        }
    };

    private void prevHandler() {
        if (player.isPlaying()) {
            int seekto = player.getCurrentPosition() - STEP_VALUE;
            if (seekto < 0)
                seekto = 0;
            player.pause();
            player.seekTo(seekto);
            player.start();
        } else {
            lastPosition = LASTPOSITION;
            int index = l_audio.getCheckedItemPosition();
            if (index == 0)
                index = l_audio.getCount();

            index--;
            l_audio.setItemChecked(index, true);
            l_audio.smoothScrollToPositionFromTop(index, l_audio.getHeight() / 2);
            l_audio.deferNotifyDataSetChanged();
        }
        return;
    }

    private void nextHandler() {
        if (player.isPlaying()) {
            int seekto = player.getCurrentPosition() + STEP_VALUE;

            if (seekto > player.getDuration())
                seekto = player.getDuration();

            player.pause();
            player.seekTo(seekto);
            player.start();
        } else {
            lastPosition = -1;
            int index = l_audio.getCheckedItemPosition();
            if (index + 1 == l_audio.getCount()) {
                l_audio.setItemChecked(0, true);
                l_audio.smoothScrollToPositionFromTop(0, 0, 0);
                l_audio.deferNotifyDataSetChanged();
                return;
            }

            index++;
            l_audio.setItemChecked(index, true);
            l_audio.smoothScrollToPositionFromTop(index, l_audio.getHeight() / 2);
            l_audio.deferNotifyDataSetChanged();

        }
    }

    private void playHandler() {
        Log.d("player", Boolean.toString(player.isPlaying()));
        if (player.isPlaying()) {
            handler.removeCallbacks(updatePositionRunnable);
            player.pause();
            setIconsOnPause();
        } else {
            if (lastPosition == l_audio.getCheckedItemPosition()) {
                player.start();
                setIconsOnPlay();
                updatePosition();
            } else {
                lastPosition = l_audio.getCheckedItemPosition();
                currentFile = (String) getViewByPosition(l_audio.getCheckedItemPosition(), l_audio).getTag();
                startPlay(currentFile);
                setIconsOnPlay();
            }
        }
    }

    private void setIconsOnPause() {
        b_play.setImageResource(android.R.drawable.ic_media_play);
        b_next.setImageResource(android.R.drawable.ic_media_next);
        b_prev.setImageResource(android.R.drawable.ic_media_previous);
    }

    private void setIconsOnPlay(){
        b_play.setImageResource(android.R.drawable.ic_media_pause);
        b_next.setImageResource(android.R.drawable.ic_media_ff);
        b_prev.setImageResource(android.R.drawable.ic_media_rew);
    }


    private MediaPlayer.OnCompletionListener onCompletion = new MediaPlayer.OnCompletionListener() {
        @Override
        public void onCompletion(MediaPlayer mp) {
            lastPosition = -1;
            b_next.setImageResource(android.R.drawable.ic_media_next);
            b_prev.setImageResource(android.R.drawable.ic_media_previous);
            stopPlay();
        }
    };

    private MediaPlayer.OnErrorListener onError = new MediaPlayer.OnErrorListener() {
        @Override
        public boolean onError(MediaPlayer mp, int what, int extra) {
            return false;
        }
    };

    private SeekBar.OnSeekBarChangeListener seekBarChanged = new SeekBar.OnSeekBarChangeListener() {
        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {
            isMovingSeekBar = false;
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {
            isMovingSeekBar = true;
        }

        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            if (isMovingSeekBar) {
                player.seekTo(progress);
                Log.i("OnSeekBarChangeListener", "onProgressChanged");
            }
        }
    };
}






