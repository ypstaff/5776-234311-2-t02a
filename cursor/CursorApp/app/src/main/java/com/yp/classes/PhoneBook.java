package com.yp.classes;

/**
 * @author Or mauda
 * This class contains the data needed for a single contact.
 * A contact must have a phone number
 * Note: two contacts are equal iff their mPhone is equal
 */
public class PhoneBook {

	private String mName;
	private String mPhone;
	private String mEmail;
	public PhoneBook(String mName, String mPhone, String mEmail) {
		if (null == mName) {
			throw new NullPointerException("mName must not be null");
		}
		if (null == mPhone) {
			throw new NullPointerException("mPhone must not be null");
		}
		if (mEmail != "" && false == mEmail.contains("@")) {
			throw new IllegalArgumentException("Email address must contain a '@'.");
		}
		this.mName = mName;
		this.mPhone = mPhone;
		this.mEmail = mEmail;
	}

    public PhoneBook(PhoneBook other) {
        this.mName = other.getmName();
        this.mPhone = other.getmPhone();
        this.mEmail = other.getmEmail();
    }

	@Override
	public boolean equals(Object other) {
		boolean result;
		if((other == null) || (getClass() != other.getClass())){
			result = false;
		} else {
			PhoneBook otherPhoneBook = (PhoneBook) other;
			result = mPhone.equals(otherPhoneBook.mPhone);
		}
		return result;
	}

	@Override
	public int hashCode() {
		return 1;
	}

	public String getmName() {
		return mName;
	}
	public String getmPhone() {
		return mPhone;
	}
	public String getmEmail() {
		return mEmail;
	}
	public void setmName(String mName) {
		this.mName = mName;
	}
	public void setmPhone(String mPhone) {
		this.mPhone = mPhone;
	}
	public void setmEmail(String mEmail) {
		this.mEmail = mEmail;
	}
	
	
	
}
