package com.yp.activities;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.CursorLoader;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;
import com.yp.classes.ClickOnlyView;
import com.yp.cursor.CursorApplication;
import com.yp.cursor.R;
import com.yp.cursor.SingleInput;
import com.yp.navigationlibrary.EmergencyCallActivity;
import com.yp.navigationlibrary.IterableGroup;
import com.yp.navigationlibrary.IterableViewGroup;
import com.yp.navigationlibrary.IterableViewGroups;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Basic Gallery activity that allows a user to view photos on the phone.
 *
 * When opened from another activity, selecting a picture would return its {@link Uri} in the
 * returned intent, as an extra string. Otherwise, selecting an image would open a preview dialog
 * with the picture in full size.
 *
 * @author Oded (Was Inbar's activity but Oded changed it a lot)
 * @version 19/06/2016
 */
public class GalleryActivity extends EmergencyCallActivity implements View.OnClickListener {

    // CONSTANTS
    public static final String GALLERY_TAG = "GALLERY_TAG";
    private static final int ROW_COUNT = 3;
    private static final int IMAGES_PER_ROW = 3;
    private static int IMAGES_PER_PAGE = ROW_COUNT * IMAGES_PER_ROW;
    private static final Uri URI_PATH = MediaStore.Images.Thumbnails.EXTERNAL_CONTENT_URI;
    private int LAST_PAGE;

    // MEMBERS
    private Cursor thumbs;
    private int dataColumnIndex;
    private int idColumnIndex;
    private Map<String, Uri> idToUri = new HashMap<>();
    private SingleInput[] navSimpleInputs = new ClickOnlyView[3];
    private Animation fabPressAnimation;
    private Map<Integer, String> ivIds = new HashMap<>();
    private Context ctx;
    IvOnClickListener ivListener = new IvOnClickListener();
    private ImageView[] ivs;
    private int page = 1;
    private boolean dialogShowing = false;

    // WIDGETS
    // scrolling
    private Button btn_top;
    private Button btn_prev;
    private Button btn_next;
    private Button btn_end;

    // preview dialog
    private Dialog d;
    private ImageView iv_dialogImage;
    private Uri lastLoadedUri;
    private boolean fromAnotherActivity = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gallery);
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);
        ctx = getApplicationContext();

        loadImageThumbnails();
        mapImageIdToUri();

        // Get the column index of the Image DATA & ID
        dataColumnIndex = thumbs.getColumnIndexOrThrow(MediaStore.Images.Thumbnails.DATA);
        idColumnIndex = thumbs.getColumnIndexOrThrow(MediaStore.Images.Thumbnails.IMAGE_ID);
        LAST_PAGE = thumbs.getCount() / IMAGES_PER_PAGE;
        if (0 != thumbs.getCount() % IMAGES_PER_PAGE) {
            LAST_PAGE++;
        }
        TextView tv_page = (TextView) findViewById(R.id.tv_gallery_page);
        tv_page.setText(String.format("Page %d (out of %d)", this.page, LAST_PAGE));

        fromAnotherActivity = (getCallingActivity() != null);

        initScrollButtons();
        initImageViews();
        showThumbnailsFromIndex(0);
        initDialog();

        fabPressAnimation = AnimationUtils.loadAnimation(getApplication(), R.anim.fab_main_press);
        initNav();
    }

    public void setFromAnotherActivity(boolean val) {
        this.fromAnotherActivity = val;
    }

    public View getLeftFab() {
        return findViewById(R.id.fab_left);
    }

    public View getMiddleFab() {
        return findViewById(R.id.fab_middle);
    }

    public View getRightFab() {
        return findViewById(R.id.fab_right);
    }

    private void initDialog() {
        d = new Dialog(GalleryActivity.this);
        d.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        d.setContentView(R.layout.dialog_gallery_image_preview);
        d.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override public void onDismiss(DialogInterface dialog) { dialogShowing = false; } });

        iv_dialogImage = (ImageView) d.findViewById(R.id.iv_image);
    }

    private void showThumbnailsFromIndex(int i) {
        int lastIndex = i + IMAGES_PER_PAGE;
        Log.d(GALLERY_TAG, "showThumbnailsFromIndex: SHOWING THUMBS FROM INDEX " + i + " to " + (lastIndex - 1));

        int j;
        thumbs.moveToPosition(i);
        for (j = 0; j < IMAGES_PER_PAGE; ++j) {
            ivIds.put(ivs[j].getId(), thumbs.getString(idColumnIndex));
            Log.d(GALLERY_TAG, "showThumbnailsFromIndex: showing thumbnail #" + (i + j) + " j=" + j);
            Picasso.with(ctx)
                    .load(new File(thumbs.getString(dataColumnIndex)))
                    .placeholder(R.drawable.progress_anim)
                    .into(ivs[j]);

            if (!thumbs.moveToNext()) {
                break;
            }

            if (i + j >= thumbs.getCount()) break;
        }

        while (i + j < lastIndex) {
            Picasso.with(ctx)
                    .load(R.drawable.square)
                    .placeholder(R.drawable.progress_anim)
                    .into(ivs[j]);

            ++j;
        }
    }

    private void initImageViews() {
        int[] ivResIds = {R.id.iv_gallery_1_1, R.id.iv_gallery_1_2, R.id.iv_gallery_1_3,
                R.id.iv_gallery_2_1, R.id.iv_gallery_2_2, R.id.iv_gallery_2_3,
                R.id.iv_gallery_3_1, R.id.iv_gallery_3_2, R.id.iv_gallery_3_3
        };

        ivs = new ImageView[IMAGES_PER_PAGE];
        int i = 0;
        for (int vid : ivResIds) {
            ImageView iv = (ImageView) findViewById(vid);
            ivs[i++] = iv;
            iv.setOnClickListener(ivListener);
        }
    }

    public boolean isShowingDialog() {
        if (null == d) return false;
        return d.isShowing();
    }

    public int getCurrentPage() {
        return page;
    }

    public int getPageCount() {
        return LAST_PAGE;
    }

    public void setDialogListener(DialogInterface.OnShowListener listener) {
        d.setOnShowListener(listener);
    }


    private class IvOnClickListener implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            final String vid = ivIds.get(v.getId());
            final Uri uri = idToUri.get(vid);

            if (null == uri) {
                Log.d(GALLERY_TAG, "onClick: got null URI for image id " + vid);
                return;
            }

            if (fromAnotherActivity) { // PainterActivity requested an image
                Log.d(GALLERY_TAG, String.format("Returning uri (%s) to calling activity as extra string '%s'",
                                                 uri.toString(), getString(R.string.selected_image_path)));
                
                Intent result = new Intent();
                result.putExtra(getString(R.string.selected_image_path), uri.toString());
                setResult(Activity.RESULT_OK, result);
                finish();
                return;
            }

            if (lastLoadedUri != uri) {
                Log.d(GALLERY_TAG, "Setting dialog URI to " + uri.toString());
                lastLoadedUri = uri;

                Picasso.with(ctx)
                        .load(new File(uri.toString()))
                        .placeholder(R.drawable.progress_anim)
                        .into(iv_dialogImage);
            }

            dialogShowing = true;
            d.show();
            Toast.makeText(ctx, "Press any button to close", Toast.LENGTH_SHORT).show();
        }
    }

    private void initScrollButtons() {
        btn_top  = (Button) findViewById(R.id.gallery_scroll_top);
        btn_prev = (Button) findViewById(R.id.gallery_scroll_back);
        btn_next = (Button) findViewById(R.id.gallery_scroll_next);
        btn_end  = (Button) findViewById(R.id.gallery_scroll_end);

        btn_top.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) { scrollToPage(1); } });

        btn_prev.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) { scrollToPage(page - 1); } });

        btn_next.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) { scrollToPage(page + 1); } });

        btn_end.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) { scrollToPage(LAST_PAGE); } });
    }

    private void initNav() {
        //Setting the 3 buttons navigation system for the activity
        fab_left   = (FloatingActionButton) getLeftFab();
        fab_right  = (FloatingActionButton) getRightFab();
        fab_middle = (FloatingActionButton) getMiddleFab();

        //Setting the listeners for the navigation buttons
        fab_left.setOnClickListener(this);
        fab_right.setOnClickListener(this);
        fab_middle.setOnClickListener(this);

        group = new IterableViewGroups(buildNavGroups(), findViewById(R.id.ll_galery_activity), this);
        group.start();

        navSimpleInputs[0] = new ClickOnlyView(this, fab_right);
        navSimpleInputs[1] = new ClickOnlyView(this, fab_middle);
        navSimpleInputs[2] = new ClickOnlyView(this, fab_left);
    }

    @NonNull
    private List<IterableGroup> buildNavGroups() {
        // Creating the groups for the navigation

        // Row 1 group
        final List<View> row1_list = new ArrayList<>();
        row1_list.addAll(Arrays.asList(ivs[0], ivs[1], ivs[2]));

        // Row 2 group
        final List<View> row2_list = new ArrayList<>();
        row2_list.addAll(Arrays.asList(ivs[3], ivs[4], ivs[5]));

        // Row 3 group
        final List<View> row3_list = new ArrayList<>();
        row3_list.addAll(Arrays.asList(ivs[6], ivs[7], ivs[8]));

        // scrolling group
        final List<View> scroll_list = new ArrayList<>();
        scroll_list.addAll(Arrays.asList(btn_top, btn_prev, btn_next, btn_end));

        // create the top-level NAV group
        final List<IterableGroup> rootGroup = new ArrayList<>();
        rootGroup.add(new IterableViewGroup(scroll_list, findViewById(R.id.ll_scroll_buttons), this));
        rootGroup.add(new IterableViewGroup(row1_list, findViewById(R.id.ll_gallery_row1), this));
        rootGroup.add(new IterableViewGroup(row2_list, findViewById(R.id.ll_gallery_row2), this));
        rootGroup.add(new IterableViewGroup(row3_list, findViewById(R.id.ll_gallery_row3), this));

        return rootGroup;
    }

    private void unregisterFromBT() {
        final CursorApplication cursorApp = CursorApplication.getInstance();
        if (null == cursorApp || null == cursorApp.dispatcher) return;

        for (int i = 0; i < navSimpleInputs.length; ++i) {
            cursorApp.dispatcher.setInputToDispatch(i, null);
        }
    }

    private void registerToBt() {
        final CursorApplication cursorApp = CursorApplication.getInstance();
        if (null == cursorApp || null == cursorApp.dispatcher) return;

        for (int i = 0; i < navSimpleInputs.length; ++i) {
            cursorApp.dispatcher.setInputToDispatch(i, navSimpleInputs[i]);
        }
    }

    @Override
    public void onClick(View v) {
        if (dialogShowing) {
            d.dismiss();
            return;
        }

        super.onClick(v);

        switch(v.getId()) {
            case R.id.fab_left:
            case R.id.fab_middle:
            case R.id.fab_right:
                v.startAnimation(fabPressAnimation);
                break;
        }
    }

    private void scrollToPage(int page) {
        Log.d(GALLERY_TAG, "scroll: Trying to scroll to page " + page);
        if (page < 1 || page > LAST_PAGE) {
            Log.d(GALLERY_TAG, "scrollToPage: NOT SCROLLING! min page is 1, max page is " + LAST_PAGE);
            return;
        }

        this.page = page;
        TextView tv_page = (TextView) findViewById(R.id.tv_gallery_page);
        tv_page.setText(String.format("Page %d (out of %d)", this.page, LAST_PAGE));

        Log.d(GALLERY_TAG, "scroll: SCROLLING TO PAGE " + page);
        showThumbnailsFromIndex((page - 1) * IMAGES_PER_PAGE);
    }

    private void loadImageThumbnails() {
        // Set up an array of the Thumbnail Image ID column we want
        String[] projection = {MediaStore.Images.Thumbnails.DATA,
                               MediaStore.Images.Thumbnails.IMAGE_ID};

        // Create the cursor pointing to the SDCard
        CursorLoader thumbLoader = new CursorLoader(getApplicationContext(),
                                               URI_PATH,
                                               projection,
                                               null,
                                               null,
                                               MediaStore.Images.Thumbnails.DEFAULT_SORT_ORDER);

        thumbs = thumbLoader.loadInBackground();

        if (thumbs == null) {
            Log.d(GALLERY_TAG, "onCreate: GOT NULL CURSOR");
            return;
        }

        Log.d(GALLERY_TAG, "onCreate: Got " + thumbs.getCount() + " thumbnails");
    }

    private void mapImageIdToUri() {
        CursorLoader imageLoader = new CursorLoader(getApplicationContext(),
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                new String[]{ MediaStore.Images.Media._ID, MediaStore.Images.Media.DATA },
                null,
                null,
                null);

        Cursor images = imageLoader.loadInBackground();

        if (images == null) {
            Log.d(GALLERY_TAG, "onCreate: GOT NULL CURSOR");
            return;
        }

        Log.d(GALLERY_TAG, "onCreate: Got " + images.getCount() + " images");
        if (images.moveToFirst()) {
            String imageId, imagePath;
            final int idColIndex = images.getColumnIndex(MediaStore.Images.Media._ID);
            final int pathColIndex = images.getColumnIndex(MediaStore.Images.Media.DATA);
            do {
                imageId = images.getString(idColIndex);
                imagePath = images.getString(pathColIndex);
                idToUri.put(imageId, Uri.parse(imagePath));
            } while (images.moveToNext());
        }
        Log.d(GALLERY_TAG, "onCreate: populated id to uri map");
        images.close();
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterFromBT();
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerToBt();
    }

    @Override protected void onDestroy() {
        thumbs.close();
        super.onDestroy();
    }
}
