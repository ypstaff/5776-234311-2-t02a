package com.yp.bts;

import android.util.Log;

import com.yp.SimpleInputEvent;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * Created by Oded on 08/12/2015.
 */
public class TranslatorGroup {
    public static final String TRANSLATOR_GROUP_TAG = "TRANSLATOR_GROUP_TAG";
    private TranslatorStateMachine[] translators = null;
    private boolean wasReset = false; // allow for a single reset at start

    private SimpleInputHandler handler = new SimpleInputHandler() {
        @Override
        public void handleEvent(int index, SimpleInputEvent ev) {
//            Log.d("BTS_TRANSLATORS", "default handleEvent is NOP");
        }
    };

    private ScheduledExecutorService scheduler = null;

    private Runnable handlerThread = new Runnable() {
        @Override
        public void run() {
            if (System.currentTimeMillis() - lastBitVectorTime > tickLengthMillis) { // time from last input was large
                if (!wasReset) {
                    Log.d(TRANSLATOR_GROUP_TAG, "translators reset - no input in at least " + tickLengthMillis + " millis");
                    resetTranslators();
                    wasReset = true;
                }
                return;
            }

            // else, do transitions in state machines
            if (wasReset) {
                Log.d(TRANSLATOR_GROUP_TAG, "received more input...");
            }
            wasReset = false;

            for (int i = 0; i < translators.length; i++) {
                final boolean isPressed = getInputBit(TranslatorGroup.this.lastBitVectorValue, i);
                dispatchEvent(i, translators[i].changeState(isPressed));
            }
        }
    };

    /**
     * The value of the last bit vector received.
     */
    private byte lastBitVectorValue = 0;

    /**
     * The time (in milliseconds) in which that last bit vector was received.
     */
    private long lastBitVectorTime = System.currentTimeMillis();

    /**
     * the maximum time (in milliseconds) allowed between consecutive input bits. If the translator
     * doesn't receive an input bit in tickLengthMillis millis, it resets to its initial state
     * (may prevent translator errors due to connection problems).
     */
    private int tickLengthMillis;

    public TranslatorGroup(int numTranslators) {
        this(numTranslators, 150);
    }

    public TranslatorGroup(int numTranslators, int tickLengthMillis) {
        this.tickLengthMillis = tickLengthMillis;

        this.translators = new TranslatorStateMachine[numTranslators];
        for (int i = 0; i < translators.length; ++i) {
            translators[i] = new TranslatorStateMachine();
        }
    }

    public void setHandler(SimpleInputHandler handler) {
        this.handler = handler;
    }

    /**
     * Set the thresholds for generating "Click" and "LongPress" events.
     *
     * @param clickThreshold amount of "ticks" for "Click" event generation.
     * @param longPressThreshold minimal amount of "ticks" for "LongPress" event generation.
     */
    public void setEventThresholds(int clickThreshold, int longPressThreshold) {
        if (clickThreshold <= 0 || clickThreshold >= longPressThreshold) {
            Log.d(TRANSLATOR_GROUP_TAG, "trying to set invalid thresholds");
            return;
        }

        Log.d(TRANSLATOR_GROUP_TAG, "setting new (click|longPress) thresholds");
        stop();
        TranslatorStateMachine.setThresholds(clickThreshold, longPressThreshold);
        start();
    }

    /**
     * this method starts the state machines; every tickLengthMillis millis, it changes their state
     * according to the latest bit vector that was received as input. If no bit vector was received
     * in over tickLengthMillis, the state machines stop.
     */
    public void start() {
        if (scheduler != null){
            scheduler.shutdownNow();
        }

        Log.d(TRANSLATOR_GROUP_TAG, "start");
        scheduler = Executors.newScheduledThreadPool(1);
        scheduler.scheduleAtFixedRate(handlerThread, 0, tickLengthMillis, TimeUnit.MILLISECONDS);
    }

    public void stop() {
        if (scheduler == null) return;
        Log.d(TRANSLATOR_GROUP_TAG, "stop");

        scheduler.shutdown();
        scheduler = null;
    }

    /**
     * Check if the translator group is currently running (has been started, and not stopped).
     *
     * @return 'true' if translator group is running, 'false' otherwise.
     */
    public boolean isRunning() {
        return (scheduler != null);
    }

    /**
     * Set the interval between two ticks, in milliseconds. If the translator group was running upon
     * invoking this method - all of the translator machines are reset.
     *
     * @param tickLengthMillis the new value to set. if it is not positive, the method does nothing.
     */
    public void setTickLengthMillis(int tickLengthMillis) {
        if (tickLengthMillis <= 0 || tickLengthMillis == this.tickLengthMillis)
            return;

        boolean wasRunning = isRunning();
        stop();

        this.tickLengthMillis = tickLengthMillis;

        if (wasRunning) {
            resetTranslators();
            start();
        }
    }

    /**
     * Get the current interval between two ticks, in milliseconds.
     * @return current tick length, in milliseconds.
     */
    public int getTickLengthMillis() {
        return this.tickLengthMillis;
    }

    public void dispatchEvent(int handlerIndex, final SimpleInputEvent event) {
        if (!SimpleInputEvent.NOP.equals(event)){
            Log.d(TRANSLATOR_GROUP_TAG, String.format("sending %s to handler at index %d", event.name(), handlerIndex));
            handler.handleEvent(handlerIndex, event);
        }
    }

    /**
     * Receive an input vector.
     *
     * @param bitVector vector of input current state (pressed or not, for matching bit)
     */
    public void processInput(byte bitVector) {
        lastBitVectorTime = System.currentTimeMillis();
        lastBitVectorValue = bitVector;
    }

    /**
     * Get the value of a single bit out of the vector, based on its index in the vector.
     *
     * @param bitVector byte representing a bit vector
     * @param index     of the wanted bit in the vector. STARTS FROM 0 (LSB).
     * @return 'true' if the bit at given index was set ('1'), 'false' otherwise ('0').
     */
    private static boolean getInputBit(byte bitVector, int index) {
        return ((bitVector >> index) & 1) == 1;
    }

    private void resetTranslators() {
        for (int i = 0; i < translators.length; i++)
            dispatchEvent(i, translators[i].reset());
    }
}

