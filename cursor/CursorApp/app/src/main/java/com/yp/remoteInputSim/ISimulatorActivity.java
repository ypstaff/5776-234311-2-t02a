package com.yp.remoteInputSim;

/**
 * Basic interface defining the contract for an activity that acts as a BT input simulator, needed
 * by the simulator to work.
 *
 * @author Oded
 * @version 12/06/2016
 */
public interface ISimulatorActivity {

    /**
     * Get the amount of milliseconds to wait between consecutive sends.
     *
     * @return milliseconds to wait after sending an input, before sending the next.
     */
    long getSendInterval();

    /**
     * Returns a string representation of the raw simulated input, which will be sent to receiver.
     *
     * @return String representation of simulated output to be sent next.
     */
    String getSimOutput();

    /**
     * Return a 1-byte simulated output, which should be sent next.
     *
     * @return the next 1-byte simulated single-level output to be sent.
     */
    byte getSingleLevelOutput();

    /**
     * Invoked right after sending the value s to the receiver. Mainly intended for testing, and
     * validating that a certain message was actually sent out.
     *
     * @param s the value that was just sent.
     */
    void onValueSent(String s);

    /**
     * Invoked whenever the simulator needs to display a toast on the UI of the calling context.
     * Leave empty implementation to disable.
     *
     * @param s the message to be shown in the toast.
     */
    void showToast(String s);

    /**
     * Invoked whenever the simulator detects an unintentional disconnection from the receiver.
     */
    void onRemoteDisconnect();

    /**
     * Get the amount of milliseconds used for simulating a single click event, in milliseconds.
     *
     * @return milliseconds used for signle click event simulation.
     */
    long getClickLength();

    /**
     * Invoked whenever the simulator successfully establishes a connection.
     */
    void onConnect();
}
