package com.yp.bts;

import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.support.annotation.NonNull;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * A thread for listening, receiving and handling input represented as a bitmap (single byte).
 */
class AcceptThread implements Runnable {

    /* CONSTANTS */
    private static final int INPUT_BUFFER_SIZE_BYTES = 128;
    private static final String BT_ACCEPT_THREAD_TAG = "BT AcceptThread";
    private final BluetoothEventReceiver mInitiator;

    /* MEMBERS */
    private BluetoothServerSocket mServerSock;
    private BluetoothSocket sock;
    private final BluetoothReceiver mReceiver;

    /* STATE FLAGS */
    public AtomicBoolean tryToReconnect = new AtomicBoolean(true);


    /**
     * Constructor. Create a new thread for receiving and processing remote inputs.
     *
     * @param receiver Enclosing BluetoothReceiver which handles incoming input.
     * @param initiator The Receiver that initiated the connection, and is listening for events and
     *                  changes in connection state.
     * @throws IOException Failed to create a BluetoothServerSocket for accepting connections
     *                     from remote devices.
     */
    public AcceptThread(@NonNull BluetoothReceiver receiver,
                        @NonNull BluetoothEventReceiver initiator) throws IOException {

        mReceiver = receiver;
        mInitiator = initiator;

        try { // MY_UUID is the app's UUID string, also used by the client code
            mServerSock = mReceiver.mBtAdapter.listenUsingRfcommWithServiceRecord(BT_ACCEPT_THREAD_TAG, mReceiver.btUUID);
        } catch (IOException e) {
            Log.d(BT_ACCEPT_THREAD_TAG, "Failed to listen to bluetooth sock: " + e.getMessage());
            throw e;
        }
    }

    /**
     * Main method for accepting a remote connection and repeatedly receiving raw input sent
     * over an established connection.
     * <p/>
     * Upon unexpected termination of a connection - a reconnection attempt will be executed.
     */
    public void run() {
        // Keep listening until exception occurs or a sock is returned
        Log.d(BT_ACCEPT_THREAD_TAG, "AcceptThread started");

        while (true) {
            try {
                sock = mServerSock.accept();
                mInitiator.onConnect(true);
            } catch (IOException | NullPointerException e) {
                Log.d(BT_ACCEPT_THREAD_TAG, "Failed to accept a connection on sock - aborting.");
                mInitiator.onConnect(false);
                return;
            }

            // Connection was accepted - Manage the connection (in a separate thread)
            Log.d(BT_ACCEPT_THREAD_TAG, "connection established");
            if (null != sock) {
                handleIncomingInput(sock); // uses a "while (true)" loop
                closeDataSocket();
            }

            if (!tryToReconnect.get()) {
                return;
            }

            Log.d(BT_ACCEPT_THREAD_TAG, "AcceptThread: Trying to reconnect...");
            tryToReconnect.set(false); // only try to reconnect once
        }
    }

    /**
     * Closes the server socket from which data sockets are "accepted". After the call, it will be
     * 'null'.
     */
    private void closeServerSocket() {
        try {
            mServerSock.close();
        } catch (Exception e) { // exception from closing socket - no handling needed
        } finally {
            mServerSock = null;
        }
    }

    /**
     * Closes the data socket from incoming data is read. After the call, it will be 'null'.
     */
    private void closeDataSocket() {
        try {
            sock.close();
        } catch (Exception e) { // do nothing - sock is closing anyway
        } finally {
            sock = null;
        }
    }

    /**
     * Listens for, Receives and dispatched handling of incoming input from an open Bluetooth
     * socket with a remote device.
     *
     * @param socket on which this thread will listen for incoming input.
     */
    private void handleIncomingInput(@NonNull BluetoothSocket socket) {
        final InputStream is;
        try {
            is = socket.getInputStream();
        } catch (IOException e) {
            Log.d("BT_RECEIVER_TAG", "Failed to extract input stream from sock");
            return;
        }

        byte[] buffer = new byte[INPUT_BUFFER_SIZE_BYTES];

        // main loop for receiving each sent input and handling it
        while (true) {
            try {
                if (is.read(buffer) > 0) {
                    BtMessage.of((int) buffer[0]).handle(buffer, mReceiver);
                } else { // probably stream was closed, might be because of other side's termination
                    Log.d("BT_SERVER", "Socket was probably closed from the client's side");
                    tryToReconnect.set(true);
                    return;
                }
            } catch (IOException e) {
                Log.d("BT_SERVER", "Socket was probably closed from the client's side");
                tryToReconnect.set(true);
                return;
            }
        }
    }


    /**
     * Closes this thread, so it doesn't accept any more connections, freeing any internal
     * resources it was using.
     *
     * @param initiatedClose - 'true' if the close was intentionally initiated by this side, 'false'
     *                       otherwise.
     *
     * @return 'true' if a termination messsage was successfully sent, 'false' otherwise
     */
    public boolean close(boolean initiatedClose) {
        boolean sentTermination = false;
        if (initiatedClose) { // no need to send termination message otherwise
            sentTermination = sendTermination();
        }

        closeDataSocket();
        closeServerSocket();

        return sentTermination;
    }

    /**
     * Send a connection termination message for orderly intentional bluetooth disconnection.
     */
    public boolean sendTermination() {
        if (null == sock) {
            Log.d("BT_RECEIVER_TAG", "sendTermination: Can't send message - socket is null");
            return false;
        }

        try {
            Log.d("BT_RECEIVER_TAG", "sendTermination: Sending message");
            sock.getOutputStream().write(0xFF); // termination message is the single byte 0xFF
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    public BluetoothSocket getDataSocket() {
        return sock;
    }
} // end class AcceptThread
