package com.yp.classes;

import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.yp.cursor.BuildConfig;

import javax.mail.AuthenticationFailedException;
import javax.mail.MessagingException;

/**
 * Created by tbenalta on 6/12/2016.
 */
public class SendEmailAsyncTask extends AsyncTask<String, Void, Boolean> {
    GMailSender m = new GMailSender("3BoomOfficial@gmail.com", "a321321a");

    public SendEmailAsyncTask() {
        if (BuildConfig.DEBUG)
            Log.v(SendEmailAsyncTask.class.getName(), "SendEmailAsyncTask()");
    }

    @Override
    protected Boolean doInBackground(String... params) {
        //extracting parameters
        if (params.length < 3) {
            Log.e("GMAIL_T", "Not enough parameters for exporting.");
            return false;
        }
        String dirPath = params[0];
        String fileName = params[1];
        String receiverEmailAddress = params[2];


        if (BuildConfig.DEBUG)
            Log.v(SendEmailAsyncTask.class.getName(), "doInBackground()");
        try {
            String filePath = dirPath + "/" + fileName;
            m.addAttachment(filePath, fileName, "");
            m.sendMail("This file was sent from 3Boom App", "", "3BoomOfficial@gmail.com", receiverEmailAddress);
            return true;
        } catch (AuthenticationFailedException e) {
            Log.e(SendEmailAsyncTask.class.getName(), "Bad account details");
            e.printStackTrace();
            return false;
        } catch (MessagingException e) {
//            Log.e(SendEmailAsyncTask.class.getName(), m.getTo(null) + "failed");
            e.printStackTrace();
            return false;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
}