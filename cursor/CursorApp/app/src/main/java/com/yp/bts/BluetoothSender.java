package com.yp.bts;

import android.bluetooth.BluetoothSocket;
import android.support.annotation.NonNull;
import android.util.Log;

import java.io.IOException;

/**
 * Handles sending messages over a bluetooth connection, according to defined BT protocol, desinged
 * to communicate with the matching {@link BluetoothReceiver} on the other connected device.
 *
 * @author Oded
 */
public class BluetoothSender {
    private static final String BLUETOOTH_SENDER_TAG = "BLUETOOTH_SENDER_TAG";

    private BluetoothSocket mmSocket;

    public BluetoothSender(@NonNull BluetoothSocket sock) {
        this.mmSocket = sock;
    }

    /**
     * Connect this device to the receiving (server) device. Initialize the socket into which the
     * simulated input should be written to.
     *
     * @return 'true' if connected successfully, 'false' otherwise.
     */
    public boolean connect() {
        Log.d(BLUETOOTH_SENDER_TAG, "connect()");

        try {
            // Connect the device through the socket. This will block
            // until it succeeds or throws an exception
            mmSocket.connect();
            return true;
        } catch (IOException e) { // Unable to connect
            // DO NOT CLOSE the socket, to allow for reconnect attempts
            Log.d(BLUETOOTH_SENDER_TAG, "connect: Failed to connect BT from client side");

            return false;
        }
    }

    /**
     * Close this sender. Also closes the underlying socket - so it can't be of further use!
     *
     * @param intentionalClose indicate whether this was initiated by this side, hence need to send
     *                         a termination message to other side.
     * @return 'true' is termination message was sent out, 'false' otherwise
     */
    public boolean close(boolean intentionalClose) {
        boolean $ = false;
        try {
            if (intentionalClose) {
                $ = sendTermination();
            }

            mmSocket.close();
        } catch (IOException e) { // nothing to do - socket is closing anyway
        }

        return $;
    }

    byte[] terminateMsg = {(byte)0xFF};
    private boolean sendTermination() {
        Log.d(BLUETOOTH_SENDER_TAG, "sendTermination: Sending termination message");
        return send(terminateMsg);
    }

    /**
     * Send a given buffer to connected device.
     *
     * @param buffer to be sent.
     *
     * @return 'true' if buffer sent successfully, 'false' otherwise.
     */
    public boolean send(byte[] buffer) {
        if (!isActive()) {
            Log.d(BLUETOOTH_SENDER_TAG, "send: Can't send - socket is null or not open");
            return false;
        }

        try {
            mmSocket.getOutputStream().write(buffer, 0, Opcode.of(buffer[0]).GetMsgSize(buffer));

            return true;
        } catch (IOException e) {
            Log.d(BLUETOOTH_SENDER_TAG, "send: failed to send on bluetooth socket");
            return false;
        }
    }

    /**
     * Check if this client is currently active, and can send simulated input.
     *
     * @return 'true' if this client is active, 'false' otherwise.
     */
    public boolean isActive() {
        return (null != mmSocket && mmSocket.isConnected());
    }


    /**
     * Convenience inner class for more elegant OOP design, representing Opcodes for defined
     * messages.
     */
    private enum Opcode {
        SingleLevelInput(0) {
            @Override
            public int GetMsgSize(byte[] buffer) {
                return 2;
            }
        },

        MultiLevelInput(1) {
            @Override
            public int GetMsgSize(byte[] buffer) {
                // NOT IMPLEMENTED!!!
                // need to extract amount of bytes from buffer
                return 0;
            }
        },

        Terminate(-1) {
            @Override
            public int GetMsgSize(byte[] buffer) {
                return 1;
            }
        };

        public final int opcode;

        Opcode(int opcode) {
            this.opcode = opcode;
        }

        public static Opcode of(int val) {
            for (Opcode op : Opcode.values()) {
                if (op.opcode == val) return op;
            }

            throw new IllegalArgumentException("No such opcode: " + val);
        }

        /**
         * Get amount of bytes necessary for sending
         * @param buffer message of this opcode, to be sent
         * @return amount of bytes needed to be sent
         */
        abstract public int GetMsgSize(byte[] buffer);
    } // end enum BtMessage
}
