package com.yp.cursor;

/**
 * Define the behavior of a SingleInput object, in response for each of the matching simulated
 * events.
 *
 * @author Oded, Itay
 */
public interface SingleInput {
    /**
     * Handles events when the time between “input down” and “input up” was <= threshold1
     * and another click was not fired at least threshold2 millisecs afterwards.
     */
    void onClick();


    /**
     * Fires up when an “input down” started and “input up” has not been received threshold3 millisecs afterwards.
     * An “onLongPressStop” must precede a call for “onLongPressStart”.
     */
    void onLongPressStart();


    /**
     * Fires up when “input up” was received after a long press was already invoked.
     * An “onLongPressStart” must precede a call for “onLongPressStop".
     */
    void onLongPressStop();


    /**
     * Handles events when two clicks (as defined in onClick) happened in less than threshold2 millisecs.
     */
    void onDoubleClick();


    /**
     * Considering the fact that some applications do not support double clicks, we would like to be able to support either one of the following:
     * When double clicks are enabled, some clicks must be grouped (if they happened less than threshold2 apart) to form a double click.
     * When double clicks are disabled, all clicks (even when they are very close together) must initiate onClick.
     * Intuitively:
     * Double click disabled = setting threshold2 to 0.
     * Double click enabled = setting threshold2 to a value > 0.
     */
    boolean isDoubleClickEnabled();
}

