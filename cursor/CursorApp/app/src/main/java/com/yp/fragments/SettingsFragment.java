package com.yp.fragments;

import android.os.Bundle;
import android.preference.PreferenceFragment;

import com.yp.cursor.R;

/**
 * Created by Oded on 04/06/2016.
 */
public class SettingsFragment extends PreferenceFragment {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Load the preferences from an XML resource
        addPreferencesFromResource(R.xml.preferences);
    }
}
