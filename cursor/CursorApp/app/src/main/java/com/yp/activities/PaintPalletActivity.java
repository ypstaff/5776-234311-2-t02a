package com.yp.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;

import com.yp.cursor.R;

import java.util.ArrayList;

/**
 * @author Or Mauda
 */
public class PaintPalletActivity extends SideMenuActivity {

    ArrayList<String> lastUsed;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_paint_pallet);

        Bundle extras = getIntent().getExtras();
        if(extras != null) {
            lastUsed = extras.getStringArrayList("last_used_colors");
        }

        // add all the Image Button colors to the list
        LinearLayout paintLayout1 = (LinearLayout) findViewById(R.id.paint_colors1);
        for (int i = 0; i < paintLayout1.getChildCount(); i++) {
            btnList.addLast((ImageButton) paintLayout1.getChildAt(i));
        }
        LinearLayout paintLayout2 = (LinearLayout) findViewById(R.id.paint_colors2);
        for (int i = 0; i < paintLayout2.getChildCount(); i++) {
            btnList.addLast((ImageButton) paintLayout2.getChildAt(i));
        }
        if (lastUsed != null) {
            LinearLayout paintLayout3 = (LinearLayout) findViewById(R.id.paint_colors3);
            for (int i = 0; i < lastUsed.size() && i < paintLayout3.getChildCount(); i++) {
                ImageButton temp = (ImageButton) paintLayout3.getChildAt(i);
                temp.setBackgroundColor(Integer.parseInt(lastUsed.get(i)));
                temp.setTag(lastUsed.get(i));
                btnList.addLast(temp);
            }
        }

        currBtn = (ImageButton) paintLayout1.getChildAt(0);
        markCurrentButtonPressed();
    }

    @Override
    protected void markCurrentButtonPressed() {
        currBtn.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.paint_pressed));
    }

    @Override
    protected void markCurrentButtonUnpressed() {
        currBtn.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.paint));
    }

    public void onSelectClick(View v) {
        Intent i = new Intent();
        i.putExtra("color", currBtn.getTag().toString());
        setResult(RESULT_OK, i);
        finish();
    }
}
