package com.yp.cursor.onebutton;

import android.graphics.Rect;
import android.util.Log;

/**
 * Created by Oded on 15/11/2015.
 *
 * @author Oded
 */
public class AxisStateMachine {

    // Generic StateMachine members
    private SingleState state;  // keeps this machine's current state

    // Specific members for cursor application
    private Background bg;      // the current layout being displayed for cursor positioning
    private Rect canvasBounds;  // dimensions of current frame upon which the cursor is positioned


    /**
     * Create a new StateMachine, specifying its initial state.
     *
     * @param initialState Initial state for the machine.
     */
    public AxisStateMachine(SingleState initialState) {
        this.state = initialState;
    }


    public void start() {
        this.state.onEnter();
    }

    public void stop() {
        this.state.onEnter();
    }


    /**
     * Advance the StateMachine's state, to the next state, determined by the {@link SingleState#nextState()} method.
     * Upon invocation, the new state's {@link SingleState#onEnter()} method is invoked.
     *
     * @return 'true' if state changed successfully, 'false' otherwise (Machine's state stays as the previous, without "entering" it again).
     */
    public boolean advanceState() {
        this.state.onExit();
        this.state = state.nextState();
        this.state.onEnter();
        Log.d("STATE_MACHINE", "Changed to state: " + state.toString());
        return true;
    }


    /**
     * Get this StateMachine's current state.
     *
     * @return This machine's current state.
     */
    public SingleState currentState() {
        return this.state;
    }


    /**
     * Convenience wrapper for the {@link SingleState#execute(Background, Rect)} method, for
     * execution of the current state of the machine - if it should be executed.
     */
    public void doStep() {
        if (state.shouldExecute()) {
            state.execute(this.bg, this.canvasBounds);
        }
    }


    public void setBackground(Background bg) {
        if (null == bg) throw new NullPointerException("Got a null Background");

        this.bg = bg;
    }


    public void setCanvasBounds(Rect canvasBounds) {
        if (null == canvasBounds) {
            throw new NullPointerException("Got a null Rect for canvasBounds");
        }

        this.canvasBounds = canvasBounds;
    }


    /**
     * Each enum-value defines a single state in the enclosing state machine. Each of the defined
     * states can serve as the machine's current "active" state, which defines a wanted behavior;
     * upon entering a state, while in it, when leaving it and also which of the states should the
     * enclosing machine advance to.
     */
    public enum SingleState {
        SET_X(true) {
            @Override
            public void execute(Background bg, Rect canvasBounds) {
                bg.moveHorizontalAxis(canvasBounds.width());
            }

            @Override
            public SingleState nextState() {
                return SET_Y;
            }

            @Override
            public boolean shouldExecute() {
                return true;
            }
        }, // SET_X
        SET_Y(true) {
            @Override
            public void execute(Background bg, Rect canvasBounds) {
                bg.moveVerticalAxis(canvasBounds.height());
            }

            @Override
            public SingleState nextState() {
                return DONE;
            }

            @Override
            public boolean shouldExecute() {
                return true;
            }
        }, // SET_Y
        DONE(false) {
            private boolean executed = false;

            @Override
            public void execute(Background bg, Rect canvasBounds) {
                bg.putMarker();
                bg.forwardTouchEvent();
                bg.resetAxisLocation();
                executed = true;
            }

            @Override
            public SingleState nextState() {
                return SET_X;
            }

            @Override
            public boolean shouldExecute() {
                return !executed;
            }

            @Override
            public void onEnter() {
                this.executed = false;
            }
        }; // DONE

        public final boolean isPeriodical;

        /**
         * Private C'tor, for each of the implementing SingleStates above. Each state is defined to
         * either be periodical (thus actually executing every interval / iteration, even if the
         * state machine stays in the same state) or a "one-time" execution state (in which case, it
         * will only "execute" once). A one-time execution state does NOT necessarily change to a
         * different state in the next interval / iteration.
         *
         * @param isPeriodical Flag signifying if the state is periodical or "one-time".
         */
        SingleState(boolean isPeriodical) {
            this.isPeriodical = isPeriodical;
        }

        /**
         * Defines the behavior of the machine while it's in this current state. This method should
         * be called upon each iteration of the machine (after entering the state, and before
         * advancing to another).
         */
        public abstract void execute(Background bg, Rect canvasBounds);

        /**
         * Get the next state the enclosing state-machine should advance to following this one.
         * Does NOT change the state machine's state.
         *
         * @return Next state the enclosing state machine should enter.
         * @see AxisStateMachine#advanceState()
         */
        public abstract SingleState nextState();

        /**
         * Check if the this state's execution should take place, at the moment of calling. Response
         * can either be a set one (e.g: for when a state is a "one-time" execution event) or based
         * on the current state and other conditions.
         *
         * @return 'true' if this state's {@link #execute(Background, Rect)} method should be
         * invoked at the current point in time, 'false' otherwise.
         */
        public abstract boolean shouldExecute();

        /**
         * Called upon each time the enclosing state machine enters this state. Useful for resetting
         * or initialization of this state's state. States for which this is useful should override
         * this method.
         */
        public void onEnter() {}

        /**
         * Called upon each time the enclosing state machine exits this state. Useful for
         * validations, resource clean-up, etc. States for which this is useful should override
         * this method.
         */
        public void onExit() {}
    } // Enum SingleState
} // class StateMachine
