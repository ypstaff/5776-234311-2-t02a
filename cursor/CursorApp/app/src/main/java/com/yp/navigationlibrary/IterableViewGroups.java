package com.yp.navigationlibrary;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.support.v4.content.res.ResourcesCompat;
import android.util.Log;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sRoySvin on 11/7/2015.
 *
 * @author Roy Svinik
 */
public class IterableViewGroups implements IterableGroup{

    List<IterableGroup > groups;
    Integer selected;
    Boolean groupMode = true;
    Context context;
    View groupView;
    Drawable formerBackground;
    Boolean isPhoneApp = false;

    public IterableViewGroups(List<IterableGroup> groups, View groupView, Context context) {
        this.groups = groups;
        this.context = context;
        this.groupView = groupView;
        formerBackground = groupView.getBackground();
    }

    public IterableViewGroups(List<IterableGroup> groups, View groupView, Context context,
                              Boolean isPhoneApp) {
        this(groups, groupView, context);
        this.isPhoneApp = isPhoneApp;
    }

    @Override
    public boolean start() {
        if(groups.size() > 0) {
            selected = 0;
            unmarkAllChilds();
            for (IterableGroup group : groups) {
                group.unmarkAllChilds();
                group.uncircleAllChilds();
            }
            circleAllChilds();
            groups.get(selected).markGroup();
        }
        return true;
    }

    public boolean iterate() {
        if(groupMode) {
            groups.get(selected).unmarkGroup();
            groups.get(selected).circleGroup();
            selected = (selected + 1) % groups.size();
            groups.get(selected).markGroup();
        } else {
            if(!groups.get(selected).iterate()) {
                back();
                iterate();
            }
        }
        return true;
    }

    public void select() {
        if(isPhoneApp) {
            phoneAppSelect();
        } else {
            regularSelect();
        }
    }

    public void regularSelect() {
        Log.e("IterableViewGroups", "select");
        if(groupMode) {
            Log.e("IterableViewGroups", "select - if");
            uncircleAllChilds();
            if(!groups.get(selected).start()) {
                //there is a single element in the sub group, thus recircle all groups
                circleAllChilds();
                groups.get(selected).markGroup();
            }
            groupMode = false;
        } else {
            Log.e("IterableViewGroups", "select - else");
            groups.get(selected).select();
        }
    }

    public void phoneAppSelect() {
        Log.e("IterableViewGroups", "select");
        if(groupMode) {
            Log.e("IterableViewGroups", "select - if");
            uncircleAllChilds();
            if(!groups.get(selected).start()) {
                //there is a single element in the sub group, thus recircle all groups
//                circleAllChilds();
//                groups.get(selected).markGroup();
            }
            groupMode = false;
        } else {
            Log.e("IterableViewGroups", "select - else");
            groups.get(selected).select();
        }
    }

    @Override
    public boolean back() {
        if(groupMode) {
            return true;
        } else {
            if(groups.get(selected).back()) {
                circleAllChilds();
                groups.get(selected).markGroup();
                groupMode = true;
            }
            return false;
        }
    }

    @Override
    public void markGroup() {
        groupView.setBackgroundColor(Color.YELLOW);
    }

    @Override
    public void unmarkGroup() {
        groupView.setBackground(formerBackground);
    }

    @Override
    public void markAllChilds() {
        for (IterableGroup group: groups) {
            group.markGroup();
        }
    }

    @Override
    public void unmarkAllChilds() {
        for (IterableGroup group: groups) {
            group.unmarkGroup();
        }
    }

    @Override
    public void circleGroup() {
        Drawable d = ResourcesCompat.getDrawable(context.getResources(), yearlyproject.navigationlibrary.R.drawable.border, null);
        groupView.setBackground(d);
    }

    @Override
    public void uncircleGroup() {
        groupView.setBackground(formerBackground);
    }

    @Override
    public void circleAllChilds() {
        for (IterableGroup group: groups) {
            group.circleGroup();
        }
    }

    @Override
    public void uncircleAllChilds() {
        for (IterableGroup group: groups) {
            group.uncircleGroup();
        }
    }

    @Override
    public List<View> getSelectedViews() {
        List<View> views = new ArrayList<>();
        if(groupMode) {
            views.add(groups.get(selected).getGroupView());
        } else {
            views.addAll(groups.get(selected).getSelectedViews());
        }
        return views;
    }

    @Override
    public View getGroupView() {
        return groupView;
    }

    @Override
    public void stop() {
        Log.e("IterableViewGroups", "stop");
        unmarkGroup();
        uncircleGroup();
//        uncircleAllChilds();
//        unmarkAllChilds();
        for(IterableGroup son : groups) {
            son.stop();
        }
    }

}
