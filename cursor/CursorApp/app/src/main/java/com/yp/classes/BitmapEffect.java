package com.yp.classes;

import android.graphics.Bitmap;
import android.graphics.CornerPathEffect;

import java.util.LinkedList;
import java.util.Random;

/**
 * @author Inbar Donag
 */
public class BitmapEffect extends CornerPathEffect {

    private LinkedList<Bitmap> bitmapList = new LinkedList<>();
    private Random random = new Random() ;
    private int space = 3; // space between bitmaps,

    public BitmapEffect(int space, Bitmap... bitmaps) {
        super(50);
        this.space = space;
        //add the bitmaps to the list
        for (Bitmap bitmap : bitmaps)
            bitmapList.addLast(bitmap);
    }

    public Bitmap getNextBitmap() {
        int randomIndex = random.nextInt(bitmapList.size());
        return bitmapList.get(randomIndex);
    }

    public int getSpace() { return space; }
}
