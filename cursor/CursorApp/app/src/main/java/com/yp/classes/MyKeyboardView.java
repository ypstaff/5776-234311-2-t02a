package com.yp.classes;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.inputmethodservice.Keyboard;
import android.inputmethodservice.KeyboardView;
import android.util.AttributeSet;
import android.util.Log;

import com.yp.cursor.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Revivo Yehuda on 30/12/2015.
 */
public class MyKeyboardView extends KeyboardView {

    public static final String LOG_TAG = "KEYBOARD_VIEW_TAG";
    public static final int GREEN_CODE = -11;
    public static final int RED_CODE = -22;
    private final Context context;
    final private List<Keyboard.Key> allKeys = new ArrayList<>();
    final private List<Keyboard.Key> m_enabled = new ArrayList<>();
    final private List<Keyboard.Key> m_redKeys = new ArrayList<>();
    final private List<Keyboard.Key> m_blueKeys = new ArrayList<>();
    final private List<Keyboard.Key> m_mainKeys = new ArrayList<>();
    final private List<Keyboard.Key> m_autoCompleteKeys = new ArrayList<>();
    private boolean m_isReset = true;
    private boolean m_isAutoComplete = false;
    private int m_completionIndex = -1; //negative value means normal mode

    public MyKeyboardView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        Log.d("KEY", "CONSTRUCT");
        setPreviewEnabled(false);
    }

    /**
     * recolors all of the normal keys, half of them red and half blue.
     */
    public void resetKeys() {
        m_enabled.clear();
        m_enabled.addAll(getKeyboard().getKeys());

        m_mainKeys.clear();
        // the input keys are the 3 last keys.
        m_mainKeys.add(m_enabled.get(m_enabled.size() - 3)); // green
        m_mainKeys.add(m_enabled.get(m_enabled.size() - 2)); // red
        m_mainKeys.add(m_enabled.get(m_enabled.size() - 1)); // blue
        // the auto complete keys are the 3 first keys.
        m_autoCompleteKeys.add(m_enabled.get(0));
        m_autoCompleteKeys.add(m_enabled.get(1));
        m_autoCompleteKeys.add(m_enabled.get(2));

        m_enabled.removeAll(m_mainKeys);
        m_enabled.removeAll(m_autoCompleteKeys);
        updateColor();
        m_isReset = true;
    }

    public boolean isReset() {
        return m_isReset;
    }

    /**
     * this method is used to preprocess the colors of the keys for the method onDraw()
     */
    public void updateColor() {
        m_blueKeys.clear();
        m_redKeys.clear();

        final int enabledKeysCount = m_enabled.size();
        int i = 0;

        // color first half in RED
        while (i < enabledKeysCount / 2) {
            m_redKeys.add(m_enabled.get(i++));
        }

        // color second half in BLUE
        while (i < enabledKeysCount) {
            m_blueKeys.add(m_enabled.get(i++));
        }
    }

    @Override
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        allKeys.clear();
        allKeys.addAll(getKeyboard().getKeys());

        allKeys.removeAll(m_blueKeys);
        allKeys.removeAll(m_redKeys);
        allKeys.removeAll(m_mainKeys);
        allKeys.removeAll(m_autoCompleteKeys);

        for (Keyboard.Key key : allKeys) {
            myDraw(canvas, key, R.color.trans_grey);
        }

        for (Keyboard.Key key : m_blueKeys) {
            myDraw(canvas, key, R.color.trans_blue);
        }

        for (Keyboard.Key key : m_redKeys) {
            myDraw(canvas, key, R.color.trans_red);
        }
        myDraw(canvas, m_mainKeys.get(0), R.color.trans_green);
        myDraw(canvas, m_mainKeys.get(1), m_isAutoComplete ? R.color.trans_yellow : R.color.trans_red); //middle button is yellow if we are on complete mode
        myDraw(canvas, m_mainKeys.get(2), R.color.trans_blue);
        for (int i = 0; i < 3; ++i){
            if (i == m_completionIndex)
                myDraw(canvas, m_autoCompleteKeys.get(i), R.color.trans_blue); //the focused completion word will be blue
            else
                myDraw(canvas, m_autoCompleteKeys.get(i), R.color.trans_yellow);
        }
    }

    public void startAutoComplete() {
        m_isAutoComplete = true;
        m_completionIndex = 0;
        greyNormalKeys();
    }

    void greyNormalKeys(){
        m_enabled.clear();
    }

    public void stopAutoComplete() {
        m_isAutoComplete = false;
        m_completionIndex = -1;
        resetKeys();
    }

    /**
     * colors a single key on the canvas
     *
     * @param canvas
     * @param key
     * @param color
     */
    private void myDraw(Canvas canvas, Keyboard.Key key, int color) {
        final Drawable dr = context.getResources().getDrawable(color);
        dr.setBounds(key.x, key.y, key.x + key.width, key.y + key.height);
        dr.draw(canvas);
    }

    /**
     * returns true if the key should be committed
     *
     * @param code
     * @return
     */
    public boolean chooseColor(int code) {
        if (code == MyKeyboard.RED_CODE) {
            m_enabled.clear();
            m_enabled.addAll(m_redKeys);
        } else if (code == MyKeyboard.BLUE_CODE) {
            m_enabled.clear();
            m_enabled.addAll(m_blueKeys);
        } else {
            Log.e(LOG_TAG, "chooseColor() shouldn't be called with other codes");
        }
        m_isReset = false;

        return m_enabled.size() == 1;
    }

    /**
     * @return the code of the key that was chosen by 3-button-input
     */
    public int getCodeToCommit() {
        //return the code of the only button left enabled
        Log.d("KEYBOARD_TAG", "This should be 1 -> " + m_enabled.size());
        return m_enabled.get(0).codes[0];
    }

    //TODO document and extract constants from here down
    @Override
    protected boolean onLongPress(Keyboard.Key key) {
        switch (key.codes[0]) {
            case MyKeyboard.GREEN_CODE:
                getOnKeyboardActionListener().onKey(-100, key.codes); // this line types a character of ascii '-100' when preforming long press on the green button
                return true;
        }
        return super.onLongPress(key);
    }

    /**
     *
     * used to determine whether the keyboard is in autocomplete mode.
     */
    public boolean isAutoComplete() {
        return m_isAutoComplete;
    }

    /**
     *
     * @return the label of the completion button to send over the input connection
     */
    public String getCompletionForCommit() {
        int oldIndex = m_completionIndex;
        m_completionIndex = 0;
        final CharSequence label = m_autoCompleteKeys.get(oldIndex).label;
        if(label == null){
            return "";
        }
        return String.valueOf(label);
    }

    public void completionNextWord() {
        m_completionIndex = (m_completionIndex + 1) % 3;
    }
}
