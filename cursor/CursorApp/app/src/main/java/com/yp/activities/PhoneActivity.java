package com.yp.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.yp.classes.ClickOnlyView;
import com.yp.cursor.CursorApplication;
import com.yp.cursor.R;

import java.util.LinkedList;

/**
 * @author Inbar Donag
 */
public class PhoneActivity extends FragmentActivity {

    private Button b_green, b_red, b_blue;
    private String phoneNumber = "";
    private TextView phoneText;

    // color groups
    private LinkedList<View> blues = new LinkedList<>();
    private LinkedList<View> reds = new LinkedList<>();
    private ClickOnlyView[] inputs = new ClickOnlyView[3];

    // monitor phone call activities
    private class PhoneCallListener extends PhoneStateListener {

        private boolean isOnConversation = false;


        @Override
        public void onCallStateChanged(int state, String incomingNumber) {

            if (TelephonyManager.CALL_STATE_RINGING == state) {
                // phone ringing
                Log.i("phone", "RINGING, number: " + incomingNumber);

                // TODO receive a call

            }

            if (TelephonyManager.CALL_STATE_OFFHOOK == state) {
                // active
                Log.i("phone", "OFFHOOK");

                isOnConversation = true;

                // TODO options while talking on the phone like speaker, cancel conversation
            }

            if (TelephonyManager.CALL_STATE_IDLE == state) {
                // runs when class inits and phone call ended
                Log.i("phone", "IDLE");

                if (isOnConversation) { // return to dialer after a conversation

                    Log.i("phone", "restart app");

                    // restart app
                    phoneText.setText("");
                    phoneNumber = "";
                    reset();

                    isOnConversation = false;
                }

            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_phone);

        b_green = (Button) findViewById(R.id.b_green);
        b_red = (Button) findViewById(R.id.b_red);
        b_blue = (Button) findViewById(R.id.b_blue);


        b_green.setText(getResources().getString(R.string.back));
        b_green.setTextColor(Color.WHITE);

        setGreenBtnFunctionality();
        setRedBtnFunctionality();
        setBlueBtnFunctionality();

        reset();

        inputs[0] = new ClickOnlyView(this, b_green);
        inputs[1] = new ClickOnlyView(this, b_red);
        inputs[2] = new ClickOnlyView(this, b_blue);

        phoneText = (TextView) findViewById(R.id.phoneNumberText);
        phoneText.setText("");

        // add PhoneStateListener
        PhoneCallListener phoneListener = new PhoneCallListener();
        TelephonyManager telephonyManager = (TelephonyManager) this
                .getSystemService(Context.TELEPHONY_SERVICE);
        telephonyManager.listen(phoneListener, PhoneStateListener.LISTEN_CALL_STATE);

        // if was started from contacts
        if (this.getIntent().hasExtra("phone number")) {
            Bundle b = getIntent().getExtras();
            phoneText.setText(b.getString("phone number"));
            phoneNumber = b.getString("phone number");
        }
    }

    private void reset() {
        reds.clear();
        blues.clear();

        reds.add(findViewById(R.id.button1));
        reds.add(findViewById(R.id.button2));
        reds.add(findViewById(R.id.button3));
        reds.add(findViewById(R.id.button4));
        reds.add(findViewById(R.id.button5));
        reds.add(findViewById(R.id.button6));
        reds.add(findViewById(R.id.button7));

        blues.add(findViewById(R.id.button8));
        blues.add(findViewById(R.id.button9));
        blues.add(findViewById(R.id.buttonAsterisk));
        blues.add(findViewById(R.id.button0));
        blues.add(findViewById(R.id.buttonPound));
        blues.add(findViewById(R.id.buttonContacts));
        blues.add(findViewById(R.id.buttonCall));
        blues.add(findViewById(R.id.buttonDel));

        //TODO blues.add(findViewById(R.id.buttonRecents));

        setBackgroundColor(reds, Color.argb(255, 196, 0, 3));
        setBackgroundColor(blues, Color.argb(255, 0, 152, 206));
    }

    private void setGreenBtnFunctionality() {
        b_green.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                onBackPressed();

            }
        });

    }

    private void setBackgroundColor(LinkedList<View> group, int color) {
        int size = group.size();
        for (int i = 0; i<size; i++) {
            group.get(i).setBackgroundColor(color);
        }

    }

    private void divide(LinkedList<View> group) {

        // color first half red, second half blue
        int size = group.size();
        int midIndex = (size / 2) - 1;
        for (int i = 0; i<size; i++) {

            if (i<=midIndex) {
                group.get(i).setBackgroundColor(Color.argb(255, 196, 0, 3));
            } else {
                group.get(i).setBackgroundColor(Color.argb(255, 0, 152, 206));
            }
        }

        // color gray the other group
        if (group.equals(reds)) {
            setBackgroundColor(blues, Color.GRAY);
        } else {
            setBackgroundColor(reds, Color.GRAY);
        }

        // update reds and blues
        LinkedList<View> g = (LinkedList<View>) group.clone();
        reds.clear();
        blues.clear();

        for (int i = 0; i<size; i++) {

            if (i<=midIndex) {
                reds.add(g.get(i));
            } else {
                blues.add(g.get(i));
            }
        }



    }

    private String parseView(int id) {

        switch (id) {

            case R.id.button0:
                return "0";
            case R.id.button1:
                return "1";
            case R.id.button2:
                return "2";
            case R.id.button3:
                return "3";
            case R.id.button4:
                return "4";
            case R.id.button5:
                return "5";
            case R.id.button6:
                return "6";
            case R.id.button7:
                return "7";
            case R.id.button8:
                return "8";
            case R.id.button9:
                return "9";
            case R.id.buttonAsterisk:
                return "*";
            case R.id.buttonPound:
                return "#";
        }
        return null;
    }

    private void clickView(View view) {

        int id = view.getId();

       /*TODO if (id == R.id.buttonRecents) {
           // Intent intent = new Intent(getApplicationContext(), RecentsActivity.class);
          //  startActivity(intent);

        } else */if (id == R.id.buttonContacts) {
           Intent intent = new Intent(getApplicationContext(), ContactsActivity.class);
           startActivity(intent);

        } else if (id == R.id.buttonDel) {
            if (phoneNumber.equals("")) return;
            phoneNumber = phoneNumber.substring(0, phoneNumber.length() - 1);

        } else if (id == R.id.buttonCall) {
            Log.i("phone", "trying to call");

            Intent callIntent = new Intent(Intent.ACTION_CALL);
            callIntent.setData(Uri.parse("tel:"+phoneNumber));

            try {
                startActivity(callIntent);
            } catch (Exception e) {
                Log.i("phone", "no permission");
            }

        } else {
            phoneNumber += parseView(id);
        }

        phoneText.setText(phoneNumber);
    }

    private void setRedBtnFunctionality() {
        b_red.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                if (reds.size() == 1) { // only 1 button left , so it's clicked
                    clickView(reds.getFirst());
                    reset();

                } else {
                    divide(reds);
                }

            }
        });

    }

    private void setBlueBtnFunctionality() {
        b_blue.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                if (blues.size() == 1) { // only 1 button left , so it's clicked
                    clickView(blues.getFirst());
                    reset();

                } else {
                    divide(blues);
                }

            }
        });

    }

    private void unregisterFromBT() {
        for (int i = 0; i < inputs.length; ++i) {
            if(CursorApplication.getInstance().dispatcher != null)
                CursorApplication.getInstance().dispatcher.setInputToDispatch(i, null);
        }
    }

    private void registerToBt() {
        for (int i = 0; i < inputs.length; ++i) {
            if(CursorApplication.getInstance().dispatcher != null)
                CursorApplication.getInstance().dispatcher.setInputToDispatch(i, inputs[i]);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterFromBT();
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerToBt();
    }

}
