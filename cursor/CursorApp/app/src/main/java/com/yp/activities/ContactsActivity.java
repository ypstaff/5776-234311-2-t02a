package com.yp.activities;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.design.widget.FloatingActionButton;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.yp.classes.ClickOnlyView;
import com.yp.classes.CommunicationAction;
import com.yp.classes.PhoneBook;
import com.yp.classes.PhoneBookAdapter;
import com.yp.cursor.CursorApplication;
import com.yp.cursor.R;
import com.yp.cursor.SingleInput;
import com.yp.navigationlibrary.EmergencyCallActivity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author Or Mauda
 */
public class ContactsActivity extends EmergencyCallActivity {

	private ListView lvPhone;
    private ListView listDialogOptions;
    private EditText etSearch;

    private PhoneBook selectedContact;

    private FrameLayout historyContainer;
    private ViewStub viewStub;
    private final List<PhoneBook> historyList = new ArrayList<>();
    private final List<PhoneBook> historyData = new ArrayList<>();

    // 3 navigation buttons
    private SingleInput[] navSimpleInputs = new ClickOnlyView[3];

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_contacts);

        setTitle("Contacts");

		lvPhone = (ListView)findViewById(R.id.listPhone);
        etSearch = (EditText) findViewById(R.id.xEt);

        fillLvPhoneWithContacts();

        etSearch.setFocusable(true);
        etSearch.setFocusableInTouchMode(true);
        etSearch.requestFocus();

        initNavButtons();

        // hide the buttons when the keyboard is open
        for (FloatingActionButton fab : Arrays.asList(fab_left, fab_right, fab_middle)) {
            fab.setVisibility(View.GONE);
        }

        etSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {

            @Override
            public boolean onEditorAction(TextView v, int actionId,
                                          KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE || (event != null&& (event.getKeyCode() == KeyEvent.KEYCODE_ENTER))) {
                    InputMethodManager in = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);

                    in.hideSoftInputFromWindow(etSearch.getApplicationWindowToken(),
                            InputMethodManager.HIDE_NOT_ALWAYS);

                    // show the buttons when the keyboard is closed
                    for (FloatingActionButton fab : Arrays.asList(fab_left, fab_right, fab_middle)) {
                        fab.setVisibility(View.VISIBLE);
                    }

                    // Must return true here to consume event
                    return true;

                }
                return false;
            }
        });
        // open the keyboard
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_IMPLICIT, 0);

        setFilterList();
	}

    private void initNavButtons() {
        //Setting the 3 buttons navigation system for the activity
        fab_left   = (FloatingActionButton) findViewById(R.id.fab_left);
        fab_right  = (FloatingActionButton) findViewById(R.id.fab_right);
        fab_middle = (FloatingActionButton) findViewById(R.id.fab_middle);

        //make the the navigation buttons semi-transparent
        for (FloatingActionButton fab : Arrays.asList(fab_left, fab_right, fab_middle)) {
            fab.setAlpha((float) 0.5);
        }

        // "Go Down" button - scrolls down in the list of contacts
        fab_left.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                scrollOneDown();
            }
        });

        // "Select" button - select the currently checked item in the contacts list
        fab_middle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectChecked();
            }
        });

        fab_right.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        navSimpleInputs[0] = new ClickOnlyView(this, fab_left);
        navSimpleInputs[1] = new ClickOnlyView(this, fab_middle);
        navSimpleInputs[2] = new ClickOnlyView(this, fab_right);

    }

    private void selectChecked() {
        int currentCheckedPosition = lvPhone.getCheckedItemPosition();
        PhoneBook checkedPhone = (PhoneBook) lvPhone.getItemAtPosition(currentCheckedPosition);
        etSearch.setText(checkedPhone.getmName());

        // set the selectedContact variable
        selectedContact = new PhoneBook(checkedPhone);

        // open a dialog to allow the user to choose: sms or phone call
        openDialog();
    }

    /**
     * opens a custom dialog, to allow the user to choose the proper action (sms\phone call...)
     */
    private void openDialog() {

        // open custom dialog
        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.contacts_dialog);
        dialog.setTitle("Select an Action For " + selectedContact.getmName());
        listDialogOptions = (ListView) dialog.findViewById(R.id.listOptions);

        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, R.layout.contacts_select_dialog_item, CommunicationAction.getActionOptions());

        listDialogOptions.setAdapter(adapter);

        listDialogOptions.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        listDialogOptions.setItemChecked(0, true);

        initDialogButtons(dialog);

        dialog.show();
    }

    /**
     * initialize the dialog buttons.
     * @param dialog - the dialog
     */
    private void initDialogButtons(final Dialog dialog) {
        fab_left   = (FloatingActionButton) dialog.findViewById(R.id.fab_left);
        fab_right  = (FloatingActionButton) dialog.findViewById(R.id.fab_right);
        fab_middle = (FloatingActionButton) dialog.findViewById(R.id.fab_middle);

        //make the the navigation buttons semi-transparent
        for (FloatingActionButton fab : Arrays.asList(fab_left, fab_right, fab_middle)) {
            fab.setAlpha((float) 0.5);
        }

        // "Go Down" button - scrolls down in the list of contacts
        fab_left.setOnClickListener(new View.OnClickListener() {
            // scroll one down in dialog actions
            @Override
            public void onClick(View v) {
                int index = listDialogOptions.getCheckedItemPosition();
                if (index + 1 == listDialogOptions.getCount()) {
                    listDialogOptions.setItemChecked(0, true);
                    listDialogOptions.smoothScrollToPositionFromTop(0, 0, 0);
                    listDialogOptions.deferNotifyDataSetChanged();
                    return;
                }

                index++;
                listDialogOptions.setItemChecked(index, true);
                listDialogOptions.smoothScrollToPositionFromTop(index, listDialogOptions.getHeight() / 2);
                listDialogOptions.deferNotifyDataSetChanged();
            }
        });

        // "Select" button - select the currently checked item in the options list
        fab_middle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int currentCheckedPosition = listDialogOptions.getCheckedItemPosition();
                CommunicationAction action = CommunicationAction.values()[currentCheckedPosition];

                Bundle b = new Bundle();
                b.putString("phone number", selectedContact.getmPhone());

                Intent intent = action.getCommunicationIntent(b, getApplicationContext());
                startActivity(intent);
            }
        });

        fab_right.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                // let the user have another try to select a different contact
                etSearch.setSelection(etSearch.getText().length());

                etSearch.requestFocus();

                initNavButtons();

                // hide the buttons when the keyboard is open
                for (FloatingActionButton fab : Arrays.asList(fab_left, fab_right, fab_middle)) {
                    fab.setVisibility(View.GONE);
                }

                // open the keyboard
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.toggleSoftInput(InputMethodManager.SHOW_IMPLICIT, 0);
                registerToBt();

            }
        });
    }

    private void scrollOneDown() {
        int index = lvPhone.getCheckedItemPosition();
        if (index + 1 == lvPhone.getCount()) {
            lvPhone.setItemChecked(0, true);
            lvPhone.smoothScrollToPositionFromTop(0, 0, 0);
            lvPhone.deferNotifyDataSetChanged();
            return;
        }

        index++;
        lvPhone.setItemChecked(index, true);
        lvPhone.smoothScrollToPositionFromTop(index, lvPhone.getHeight() / 2);
        lvPhone.deferNotifyDataSetChanged();
    }


    /**
     * Fills the list view with the contacts, and sets its adapter.
     */
    private void fillLvPhoneWithContacts() {
        List<PhoneBook> listPhoneBook = new ArrayList<>();

        Cursor phones = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,null,null, null);
        while (phones.moveToNext())
        {
            String name=phones.getString(phones.getColumnIndex(ContactsContract.Data.DISPLAY_NAME));
            String phoneNumber = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
            if (null == phoneNumber) {
                continue;
            }
            phoneNumber = phoneNumber.replace(" ", "").replace("-", "");
            if (listPhoneBook.contains(new PhoneBook(name, phoneNumber, ""/*email*/))) {
                continue;
            }
            listPhoneBook.add(new PhoneBook(name, phoneNumber, ""/*email*/));
        }
        phones.close();

        PhoneBookAdapter adapter = new PhoneBookAdapter(this, listPhoneBook);
        lvPhone.setAdapter(adapter);

        lvPhone.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        lvPhone.setItemChecked(0, true);
    }

    @Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.

		//getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

    protected void setFilterList() {

        historyContainer = (FrameLayout) findViewById(R.id.history_container_layout);
        etSearch.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                historyContainer.removeAllViews();
                final List<PhoneBook> tempHistoryList = new ArrayList<>();
                tempHistoryList.addAll(historyList);

                for (PhoneBook aPhone : historyList) {
                    if (!aPhone.getmName().toLowerCase().contains(s.toString().toLowerCase())) {
                        tempHistoryList.remove(aPhone);
                    }
                }

                viewStub = new ViewStub(ContactsActivity.this, R.layout.history_schedule);

                // place the new view in place
                RelativeLayout.LayoutParams p = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT);
                p.addRule(RelativeLayout.BELOW, R.id.xLinearEt);
                p.addRule(RelativeLayout.ABOVE, R.id.threeButtons);

                viewStub.setLayoutParams(p);

                viewStub.setOnInflateListener(new ViewStub.OnInflateListener() {

                    @Override
                    public void onInflate(ViewStub stub, View inflated) {
                        setUIElements(inflated, tempHistoryList);
                    }
                });

                historyContainer.addView(viewStub);
                viewStub.inflate();
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

            }
        });
        setViewStub();
        lvPhone.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        lvPhone.setItemChecked(0, true);
    }

    private void setViewStub() {

        Cursor phones = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, null, null, null);
        while (phones.moveToNext()) {
            String name=phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
            String phoneNumber = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
            if (null == phoneNumber) {
                continue;
            }
            phoneNumber = phoneNumber.replace(" ", "").replace("-", "");
            if (historyList.contains(new PhoneBook(name, phoneNumber, ""/*email*/))) {
                continue;
            }
            historyList.add(new PhoneBook(name, phoneNumber,""/*email*/));
        }
        phones.close();

        viewStub = new ViewStub(ContactsActivity.this, R.layout.history_schedule);
        viewStub.setOnInflateListener(new ViewStub.OnInflateListener() {

            @Override
            public void onInflate(ViewStub stub, View inflated) {
                setUIElements(inflated, historyList);
            }
        });

        historyContainer.addView(viewStub);
        viewStub.inflate();
    }

    private void setUIElements(View v, List<PhoneBook> historyLists) {
        if(v != null) {
            historyData.clear();

            historyData.addAll(historyLists);

            lvPhone = (ListView)findViewById(R.id.listPhone);
            lvPhone.setAdapter(new PhoneBookAdapter(this, historyData));

            lvPhone.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
            lvPhone.setItemChecked(0, true);

            registerForContextMenu(lvPhone);
        }
    }



    private void unregisterFromBT() {
        if(CursorApplication.getInstance().dispatcher != null) {
            CursorApplication.getInstance().dispatcher.setInputToDispatch(0, null);
            CursorApplication.getInstance().dispatcher.setInputToDispatch(1, null);
            CursorApplication.getInstance().dispatcher.setInputToDispatch(2, null);
        }
    }


    private void registerToBt() {
        if(CursorApplication.getInstance().dispatcher != null) {
            CursorApplication.getInstance().dispatcher.setInputToDispatch(0, new SingleInput() {
                @Override
                public void onClick() {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            fab_right.callOnClick();
                        }
                    });
                }

                @Override
                public void onLongPressStart() {
                }

                @Override
                public void onLongPressStop() {
                }

                @Override
                public void onDoubleClick() {
                }

                @Override
                public boolean isDoubleClickEnabled() {
                    return false;
                }
            });
            CursorApplication.getInstance().dispatcher.setInputToDispatch(1, new SingleInput() {
                @Override
                public void onClick() {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            fab_middle.callOnClick();
                        }
                    });
                }

                @Override
                public void onLongPressStart() {
                }

                @Override
                public void onLongPressStop() {
                }

                @Override
                public void onDoubleClick() {
                }

                @Override
                public boolean isDoubleClickEnabled() {
                    return false;
                }
            });
            CursorApplication.getInstance().dispatcher.setInputToDispatch(2, new SingleInput() {
                @Override
                public void onClick() {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            fab_left.callOnClick();
                        }
                    });
                }

                @Override
                public void onLongPressStart() {
                }

                @Override
                public void onLongPressStop() {
                }

                @Override
                public void onDoubleClick() {
                }

                @Override
                public boolean isDoubleClickEnabled() {
                    return false;
                }
            });
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterFromBT();
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerToBt();
    }
}
