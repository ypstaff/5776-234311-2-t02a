package com.yp.remoteInputSim;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.support.annotation.NonNull;
import android.util.Log;

import com.yp.bts.BluetoothSender;

import java.io.IOException;
import java.io.InputStream;
import java.util.UUID;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

/**
 * Simulator for sending input over bluetooth to receiver remote device, according to defined
 * protocol.
 *
 * @author Oded
 * @version 12/06/2016
 */
public class InputSimulator {

    // CONSTANTS
    private static final String INPUT_SIM_TAG = "INPUT_SIMULATOR_TAG";
    private static long SEND_INTERVAL = 100L; // milliseconds

    // MEMBERS
    private final ISimulatorActivity activity;
    private boolean isRunning = false;

    // THREADING
    private ScheduledFuture senderHandle;
    private Thread receiverHandle;
    private final OutputSenderThread senderThread = new OutputSenderThread();
    private final ScheduledExecutorService sendScheduler = Executors.newScheduledThreadPool(1);

    // BLUETOOTH
    private final UUID btUUID;
    private final BluetoothAdapter myBTAdapter;
    private final BluetoothDevice remoteDevice;
    private final String remoteDeviceName;
    private BluetoothSender bluetoothSender;
    private SimpleReceiver bluetoothReceiver;
    private boolean terminated = false;


    private InputSimulator(ISimulatorActivity activity, String uuid, String devAddr) {
        this.btUUID = UUID.fromString(uuid);
        this.myBTAdapter = BluetoothAdapter.getDefaultAdapter();
        this.remoteDevice = myBTAdapter.getRemoteDevice(devAddr);
        this.remoteDeviceName = remoteDevice.getName();
        this.activity = activity;
        SEND_INTERVAL = activity.getSendInterval();
    }


    /**
     * Factory method to get an instance of a simulator.
     *
     * @param activity the invoking activity that is receiving the simulator instance.
     * @param uuid with which to connect
     * @param devAddr bluetooth address of remote device to connect with, should be acquired from
     *                already paired devices.
     * @return a new Simulator instance if all goes well, 'null' otherwise (NULL argument,
     * unsupported bluetooth, etc.)
     */
    public static InputSimulator GetSimulator(@NonNull ISimulatorActivity activity, @NonNull String uuid, @NonNull String devAddr) {
        BluetoothAdapter btAdapter = BluetoothAdapter.getDefaultAdapter();
        if (null == btAdapter || null == btAdapter.getRemoteDevice(devAddr)) {
            return null;
        }

        return new InputSimulator(activity, uuid, devAddr);
    }

    /**
     * Initialize the bluetooth connection of this simulator instance. Note that in order to start
     * sending simulated input, the {@link #connect()} method must be invoked.
     * @return 'true' in connection initialized successfully, 'false' otherwise.
     */
    public boolean initBluetoothConnection() {
        // Get a BluetoothSocket to connect with the given BluetoothDevice
        final BluetoothSocket sock;
        try {
            sock = remoteDevice.createRfcommSocketToServiceRecord(btUUID);
        } catch (IOException e) {
            Log.d(INPUT_SIM_TAG, "Failed to create BT socket. NOT connecting.");
            return false;
        } finally {
            myBTAdapter.cancelDiscovery(); // to avoid slowing the connection down
        }

        bluetoothSender = new BluetoothSender(sock);
        bluetoothReceiver = new SimpleReceiver(sock);

        terminated = false;
        return true;
    }

    public String getRemoteDevName() {
        return this.remoteDeviceName;
    }

    public boolean wasIntentionallyTerminated() {
        return terminated;
    }

    /**
     * Initiate a connection termination, sending out to other side the termination message.
     *
     * @return 'true' if the termination message was sent, 'false' otherwise.
     */
    public boolean terminateConnection() {
        boolean $ = false;

        if (null != bluetoothSender) {
            $ = bluetoothSender.close(true);
            bluetoothSender = null;
        }

        if (null != senderHandle) {
            senderHandle.cancel(true);
            senderHandle = null;
        }

        if (null != receiverHandle) {
            receiverHandle.interrupt();
            receiverHandle = null;
        }

        isRunning = false;
        terminated = true;
        return $;
    }

    public void pauseSending() {
        senderThread.pause();
    }

    public void resumeSending() {
        senderThread.resume();
    }

    /**
     * Connect and start sending simulated output to other remote device (and listening for
     * termination message). Must be called after  {@link #initBluetoothConnection()}, only
     * once the caller is ready to start sending.
     *
     * @return 'true' if connected successfully, and started sending simulated input, 'false'
     * otherwise.
     */
    public boolean connect() {
        if (null == bluetoothSender || null == bluetoothReceiver) {
            initBluetoothConnection();
        }

        if (bluetoothSender.connect()) {
            senderHandle = sendScheduler.scheduleAtFixedRate(
                    senderThread, SEND_INTERVAL, SEND_INTERVAL, TimeUnit.MILLISECONDS);

            receiverHandle = new Thread(bluetoothReceiver);
            receiverHandle.start();

            Log.d(INPUT_SIM_TAG, "Started SimplyReceiver (listening for termination message)");
            senderThread.resume();
            isRunning = true;
            activity.onConnect();
            return true;
        } else {
            if (null != senderHandle) {
                senderHandle.cancel(true);
            }
            return false;
        }
    }

    public boolean isRunning() {
        return isRunning;
    }

    public BluetoothSocket getSocket() {
        return bluetoothReceiver.getSocket();
    }

    /**
     * Handles the independent sending via bluetooth of the simulated value.
     */
    private class OutputSenderThread implements Runnable {
        private static final int MESSAGE_BUFFER_SIZE = 128;

        byte[] buffer = new byte[MESSAGE_BUFFER_SIZE];
        private boolean paused = false;

        public void pause() {
            paused = true;
        }

        public void resume() {
            paused = false;
        }

        @Override
        public void run() {
            if (paused) return;

            if (null == bluetoothSender) {
                Log.d(INPUT_SIM_TAG, "run: bluetoothSender is null - cannot simulate. ignoring.");
                return;
            }

            if (!bluetoothSender.isActive()) {
                senderHandle.cancel(true);
                Log.d(INPUT_SIM_TAG, "run: BT socket is NOT active. terminating simulation.");
                return;
            }

            final String simOutput = activity.getSimOutput();
            buffer[0] = 0;
            buffer[1] = activity.getSingleLevelOutput();
            if (bluetoothSender.send(buffer)) {
                Log.v(INPUT_SIM_TAG, "run: simulating " + simOutput);
                activity.onValueSent(simOutput);
            } else {
                Log.d(INPUT_SIM_TAG, "run: sending failed - closing socket and stopping simulation.");
                senderHandle.cancel(false);
                bluetoothSender.close(false);
                activity.onRemoteDisconnect();
            }
        }
    } // end private class OutputSenderThread

    /**
     * Simple receiver for listening to a bluetooth socket and handling incoming termination
     * messages. All other messages are ignored.
     */
    private class SimpleReceiver implements Runnable {
        private final BluetoothSocket sock;

        public SimpleReceiver(@NonNull BluetoothSocket sock) {
            this.sock = sock;
        }

        public BluetoothSocket getSocket() {
            return sock;
        }

        @Override
        public void run() {
            try {
                final InputStream is = sock.getInputStream();

                while (true) {
                    if (0xFF == is.read()) {
                        Log.d(INPUT_SIM_TAG, "SimpleReceiver.run: Got termination message");
                        activity.onRemoteDisconnect();
                        break;
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            terminateConnection();
        }
    } // end private class SimpleReceiver
}
