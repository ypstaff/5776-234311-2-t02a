package com.yp.classes;

import android.app.Activity;
import android.view.View;

import com.yp.cursor.SingleInput;

/**
 * Simple, basic common implementation of the {@link SingleInput} interface, for use with "Click"
 * Only widgets.
 *
 * @author Oded
 * @version 09/06/2016
 */
public class ClickOnlyView implements SingleInput {
    private final View v;
    private final Activity a;

    public ClickOnlyView(Activity a, View v) {
        this.v = v;
        this.a = a;
    }

    @Override
    public void onClick() {
        a.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                v.callOnClick();
            }
        });
    }

    @Override public void onLongPressStart() {}
    @Override public void onLongPressStop() {}
    @Override public void onDoubleClick() {}
    @Override public boolean isDoubleClickEnabled() { return false; }
}
