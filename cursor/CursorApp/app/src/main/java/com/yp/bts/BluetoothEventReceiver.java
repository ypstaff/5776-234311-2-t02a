package com.yp.bts;

/**
 * Created by Oded on 04/06/2016.
 */
public interface BluetoothEventReceiver {

    /**
     * Invoked upon connection establishment completion (NOT necessarily successful!).
     *
     * @param success 'true' if the connection was successfully established, 'false' for failure to
     *                connect for any reason.
     */
    void onConnect(boolean success);

    /**
     * Invoked upon disconnection of the bluetooth connection.
     */
    void onDisconnect();
}
