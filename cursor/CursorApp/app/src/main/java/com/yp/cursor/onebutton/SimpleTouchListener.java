package com.yp.cursor.onebutton;
// Itay

import com.yp.cursor.SingleInput;

/**
 * Main listener for simple input events, which translates them into state-changes in the
 * state-machine.
 */
public class SimpleTouchListener implements SingleInput {
    private AxisStateMachine sm;

    public void setStateMachine(AxisStateMachine sm) {
        if (null == sm) throw new NullPointerException("State machine cannot be NULL");

        this.sm = sm;
    }

    @Override
    public void onClick() {
        sm.advanceState();
    }

    @Override
    public void onLongPressStart() {

    }

    @Override
    public void onLongPressStop() {

    }

    @Override
    public void onDoubleClick() {

    }

    @Override
    public boolean isDoubleClickEnabled() {
        return false;
    }
}
