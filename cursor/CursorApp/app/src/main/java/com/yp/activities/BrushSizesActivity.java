package com.yp.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;

import com.yp.cursor.R;

/**
 * @author Or Mauda
 */
public class BrushSizesActivity extends SideMenuActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_brush_sizes);

        // add all the Image Button sizes to the list
        LinearLayout sizesLayout = (LinearLayout)findViewById(R.id.brush_sizes);
        for (int i = 0; i < sizesLayout.getChildCount(); i++){
            btnList.addLast((ImageButton)sizesLayout.getChildAt(i));
        }

        currBtn = (ImageButton)sizesLayout.getChildAt(0);
        markCurrentButtonPressed();
    }

    public void onSelectClick(View v) {
        Intent i = new Intent();
        i.putExtra("size", currBtn.getTag().toString());
        setResult(RESULT_OK, i);
        finish();
    }

}
