package com.yp.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

import com.yp.cursor.R;

/**
 * @author Inbar Donag
 *
 * Revised by:
 * @author Or Mauda
 */

public class ToolMenuActivity extends SideMenuActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tool_menu);

        ImageButton cursorBtn = (ImageButton) findViewById(R.id.cursorBtn);
        ImageButton brushBtn = (ImageButton) findViewById(R.id.brushBtn);
        ImageButton eraserBtn = (ImageButton) findViewById(R.id.eraserBtn);
        ImageButton undoBtn = (ImageButton) findViewById(R.id.undoBtn);
        ImageButton redoBtn = (ImageButton) findViewById(R.id.redoBtn);
        ImageButton paletteBtn = (ImageButton) findViewById(R.id.paletteBtn);
        ImageButton rgbBtn = (ImageButton) findViewById(R.id.rgbButton);
        ImageButton colorPickerBtn = (ImageButton) findViewById(R.id.colorPickerBtn);
        ImageButton brushSizeBtn = (ImageButton) findViewById(R.id.brushSizeBtn);
        ImageButton opacityBtn = (ImageButton) findViewById(R.id.opacityBtn);
        ImageButton effectsBtn = (ImageButton) findViewById(R.id.effectsBtn);
        ImageButton speedBtn = (ImageButton) findViewById(R.id.speedBtn);
        ImageButton saveBtn = (ImageButton) findViewById(R.id.saveBtn);
        ImageButton openBtn = (ImageButton) findViewById(R.id.openBtn);
        ImageButton deleteBtn = (ImageButton) findViewById(R.id.deleteBtn);
        ImageButton exitBtn = (ImageButton) findViewById(R.id.exitBtn);

        upBtn = (ImageButton)findViewById(R.id.upBtn);
        downBtn = (ImageButton)findViewById(R.id.downBtn);
        selectBtn = (Button)findViewById(R.id.selectBtn);

        btnList.addLast(cursorBtn);
        btnList.addLast(brushBtn);
        btnList.addLast(eraserBtn);
        btnList.addLast(undoBtn);
        btnList.addLast(redoBtn);
        btnList.addLast(paletteBtn);
        btnList.addLast(rgbBtn);
        btnList.add(colorPickerBtn);
        btnList.addLast(brushSizeBtn);
        btnList.addLast(opacityBtn);
        btnList.addLast(effectsBtn);
        btnList.addLast(speedBtn);
        btnList.addLast(saveBtn);
        btnList.addLast(openBtn);
        btnList.addLast(deleteBtn);
        btnList.addLast(exitBtn);

        currBtn = btnList.get(0);
        markCurrentButtonPressed();

    }

    @Override
    public void onSelectClick(View v) {
        Intent i = new Intent();
        i.putExtra("tool", currBtn.getTag().toString());
        setResult(RESULT_OK, i);
        finish();
    }

}
