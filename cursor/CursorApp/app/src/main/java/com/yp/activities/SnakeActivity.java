package com.yp.activities;

import android.app.Activity;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.os.Bundle;

import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;


import com.yp.cursor.CursorApplication;
import com.yp.cursor.R;
import com.yp.classes.SnakeGame;
import com.yp.classes.SnakeView;
import com.yp.cursor.SingleInput;

/**
 * @author Inbar Donag
 */

public class SnakeActivity extends Activity {

    SnakeView snakeView;
    SnakeGame game;

    Button playBtn;
    Button difficultyBtn;
    Button backBtn;
    ImageButton leftBtn;
    ImageButton rightBtn;
    TextView gameOverText;
    TextView highScoreText;
    TextView highScoreValueText;
    TextView scoreText;
    TextView levelText;
    ImageView snakePic;
    Animation animFadeIn;
    Animation animBounce;

    SnakeView.GameState state;
    boolean gameIsRunning;
    boolean gameIsPaused;
    boolean gameIsOver;
    boolean gameInit;

    int score = 0;
    int level = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_snake);
        snakeView = (SnakeView) findViewById(R.id.snakeView);
        playBtn = (Button) findViewById(R.id.playButton);
        difficultyBtn = (Button) findViewById(R.id.difficultyButton);
        backBtn = (Button) findViewById(R.id.backButton);
        leftBtn = (ImageButton) findViewById(R.id.turnLeftBtn);
        rightBtn = (ImageButton) findViewById(R.id.turnRightBtn);
        gameOverText = (TextView) findViewById(R.id.gameOverText);
        highScoreText = (TextView) findViewById(R.id.highScoreText);
        highScoreValueText = (TextView) findViewById(R.id.highScoreValueText);
        scoreText = (TextView) findViewById(R.id.scoreText);
        levelText = (TextView) findViewById(R.id.levelText);
        snakePic = (ImageView) findViewById(R.id.snakePic);

        leftBtn.setVisibility(View.GONE);
        rightBtn.setVisibility(View.GONE);
        gameOverText.setVisibility(View.GONE);
        highScoreText.setVisibility(View.GONE);
        highScoreValueText.setVisibility(View.GONE);
        scoreText.setVisibility(View.GONE);
        levelText.setVisibility(View.GONE);

        // load the animations
        animFadeIn = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fade_in);
        animBounce = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.bounce);
    }

    @Override
    protected void onPause() {
        super.onPause();
        snakeView.pauseGame();
        updateGUI();
        unregisterFromBT();

    }

    @Override
    protected void onStop() {
        super.onStop();
        snakeView.pauseGame();
    }

    @Override
    protected  void onResume() {
        super.onResume();
        updateGUI();
        registerToBt();
    }

    public void turnRight(View v) {
        Log.i("snake", "turning right");

        if (game != null)
            game.turnRight();
    }

    public void turnLeft(View v) {
        Log.i("snake", "turning left");

        if (game != null)
            game.turnLeft();
    }

    // on click play button
    public void play(View v) {
        Log.i("snake", "play clicked");

        state = snakeView.gameState;
        gameIsRunning = state == SnakeView.GameState.GAME_RUNNING;

        if (!gameIsRunning) {

            // lock orientation
            if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);
            } else {
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
            }

            snakeView.startGame();

        } else {
            snakeView.pauseGame();
        }

        updateGUI();
        snakePic.setVisibility(View.GONE);
    }

    // on click back button
    public void back(View v) {
        super.onBackPressed();

    }

    // on click difficulty button
    public void difficulty(View v) {

        String str = difficultyBtn.getText().toString();
        if (str.equals("Easy")) {
            difficultyBtn.setText(R.string.medium);
            snakeView.setDifficulty(R.string.medium);

        } else if (str.equals("Medium")) {
            difficultyBtn.setText(R.string.hard);
            snakeView.setDifficulty(R.string.hard);

        } else {
            difficultyBtn.setText(R.string.easy);
            snakeView.setDifficulty(R.string.easy);
        }
    }


    void showScoreAndLevel() {

        if (game == null)
            return;

        scoreText.setText("Score: " + Integer.toString(game.getScore()) +
                "\nHigh Score: " + Integer.toString(game.getHighScore()));
        scoreText.setVisibility(gameIsRunning || gameIsPaused ? View.VISIBLE : View.GONE);
        levelText.setText("Level: " + Integer.toString(game.getLevel()));
        levelText.setVisibility(gameIsRunning || gameIsPaused ? View.VISIBLE : View.GONE);
        if (score < game.getScore()) { // score got bigger

            scoreText.startAnimation(animBounce);
            score = game.getScore();
        }
        if (level < game.getLevel()) { // leveled up

            levelText.startAnimation(animBounce);
            level = game.getLevel();
        }
    }

    void showButtons() {
        leftBtn.setVisibility(gameIsRunning ? View.VISIBLE : View.GONE);
        rightBtn.setVisibility(gameIsRunning ? View.VISIBLE : View.GONE);
        backBtn.setVisibility(gameIsRunning ? View.GONE : View.VISIBLE);

        if (gameInit || gameIsOver) {
            playBtn.setText(R.string.play);
            difficultyBtn.setVisibility(View.VISIBLE);

        } else if (gameIsPaused) {
            playBtn.setText(R.string.resume);
            difficultyBtn.setVisibility(View.GONE);

        } else {
            playBtn.setText(R.string.pause);
            difficultyBtn.setVisibility(View.GONE);
        }
    }

    void showGameOver() {

        if (game == null)
            return;

        if (gameIsOver)
        {
            gameOverText.setVisibility(View.VISIBLE);
            if (game.isNewHighScore()) {
                highScoreText.setVisibility(View.VISIBLE);
                highScoreText.startAnimation(animFadeIn);
                highScoreValueText.setText(Integer.toString(game.getHighScore()));
                highScoreValueText.setVisibility(View.VISIBLE);
                highScoreValueText.startAnimation(animFadeIn);
            }

        } else {
            gameOverText.setVisibility(View.GONE);
            highScoreText.setVisibility(View.GONE);
            highScoreText.clearAnimation();
            highScoreValueText.setVisibility(View.GONE);
            highScoreValueText.clearAnimation();
        }
    }

    public void updateGUI()
    {
        game = snakeView.getGame();
        state = snakeView.gameState;
        gameIsRunning = state == SnakeView.GameState.GAME_RUNNING;
        gameIsPaused = state == SnakeView.GameState.GAME_PAUSED;
        gameIsOver = state == SnakeView.GameState.GAME_OVER;
        gameInit = state == SnakeView.GameState.INITIAL;

        scoreText.clearAnimation();
        levelText.clearAnimation();

        if (gameInit || gameIsOver) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
            level = 1;
            score = 0;
        }

        // show scores and level texts
        showScoreAndLevel();
        // show buttons according to game state
        showButtons();
        // show game over messages
        showGameOver();
    }

    private void unregisterFromBT() {
        CursorApplication.getInstance().dispatcher.setInputToDispatch(0, null);
        CursorApplication.getInstance().dispatcher.setInputToDispatch(1, null);
        CursorApplication.getInstance().dispatcher.setInputToDispatch(2, null);
    }

    private void registerToBt() {
        CursorApplication.getInstance().dispatcher.setInputToDispatch(0, new SingleInput() {
            @Override
            public void onClick() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if(gameInit){
                            backBtn.callOnClick();
                        }else if(gameIsRunning){
                            leftBtn.callOnClick();
                        }else if(gameIsPaused){
                            backBtn.callOnClick();
                        }else if(gameIsOver){
                            backBtn.callOnClick();
                        }
                    }
                });
            }

            @Override
            public void onLongPressStart() {
            }

            @Override
            public void onLongPressStop() {
            }

            @Override
            public void onDoubleClick() {
            }

            @Override
            public boolean isDoubleClickEnabled() {
                return false;
            }
        });
        CursorApplication.getInstance().dispatcher.setInputToDispatch(1, new SingleInput() {
            @Override
            public void onClick() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        playBtn.callOnClick();
                    }
                });

            }

            @Override
            public void onLongPressStart() {
            }

            @Override
            public void onLongPressStop() {
            }

            @Override
            public void onDoubleClick() {
            }

            @Override
            public boolean isDoubleClickEnabled() {
                return false;
            }
        });
        CursorApplication.getInstance().dispatcher.setInputToDispatch(2, new SingleInput() {
            @Override
            public void onClick() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if(gameInit){
                            difficultyBtn.callOnClick();
                        }else if(gameIsRunning){
                            rightBtn.callOnClick();
                        }else if(gameIsPaused){
                            // do nothing
                        }else if(gameIsOver){
                            difficultyBtn.callOnClick();
                        }
                    }
                });

            }

            @Override
            public void onLongPressStart() {
            }

            @Override
            public void onLongPressStop() {
            }

            @Override
            public void onDoubleClick() {
            }

            @Override
            public boolean isDoubleClickEnabled() {
                return false;
            }
        });

    }

}
