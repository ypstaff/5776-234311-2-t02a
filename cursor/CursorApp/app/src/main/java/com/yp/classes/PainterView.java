package com.yp.classes;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Point;
import android.util.AttributeSet;
import android.view.View;
import com.yp.cursor.R;

/**
 * @author Inbar Donag
 */


public class PainterView extends View
{
    public Painter painter = null;
    public PainterCursor cursor;

    public PainterView(Context context)
    {
        super(context);
    }

    public PainterView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public PainterView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }


    @Override
    protected void onDraw(Canvas canvas)
    {
        super.onDraw(canvas);

        if (painter != null) {
            painter.render(canvas);
        }

        cursor.render(canvas);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh)
    {
        super.onSizeChanged(w, h, oldw, oldh);
        painter = new Painter(w,h);
        Bitmap cursorPic = BitmapFactory.decodeResource(getResources(), R.drawable.arrow, null);
        cursor = new PainterCursor(cursorPic,0, w,h);
        cursor.setCursorPos(new Point(w/2,h/2));

        //bitmap = Bitmap.createBitmap(getWidth(), getHeight(), Bitmap.Config.ARGB_8888); // this creates a MUTABLE bitmap
        //canvas = new Canvas(bitmap);
    }
}
