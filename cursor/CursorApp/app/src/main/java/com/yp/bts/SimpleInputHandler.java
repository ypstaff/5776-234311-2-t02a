package com.yp.bts;

import com.yp.SimpleInputEvent;

/**
 * Created by Oded on 08/12/2015.
 */
public interface SimpleInputHandler {

    /**
     * called when an event is dispatched for the index given as prarameter.
     * @param index - index of input.
     * @param ev - type of event dispatched.
     */
    void handleEvent(int index, SimpleInputEvent ev);
}
