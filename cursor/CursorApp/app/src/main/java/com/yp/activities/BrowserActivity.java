package com.yp.activities;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;

import com.yp.cursor.CursorApplication;
import com.yp.cursor.CursorService;
import com.yp.cursor.R;
import com.yp.cursor.ViewInputHolder;
import com.yp.cursor.facedetect.FdView;
import com.yp.cursor.onebutton.AxisMoverView;

import java.util.Stack;

/**
 * @author Inbar Donag
 */
public class BrowserActivity extends Activity {

    private WebView webView;
    private EditText editTextUrl;
    private ImageButton prevBtn;
    private ImageButton nextBtn;
    private ImageButton scrollUpBtn;
    private ImageButton scrollDownBtn;

    private String currUrl = "";
    private Stack<String> prevUrls = new Stack<>();
    private Stack<String> nextUrls = new Stack<>();

    final Handler handler = new Handler();
    boolean scrolling = false;
    private Button goButton;
    private SharedPreferences sharedPref;
    private boolean stopCursorService;

    private class Client extends WebViewClient {
        public Client() {}

        @Override
        public void onPageFinished(WebView view, String url)
        {
            // next
            if (!nextUrls.empty() && url.equals(nextUrls.peek())) {
                nextUrls.pop();
                prevUrls.push(currUrl);
            }

            // prev
            else if (!prevUrls.empty() && url.equals(prevUrls.peek()) ) {
                prevUrls.pop();
                nextUrls.push(currUrl);
            }
            // other
            else if (!currUrl.equals(""))
            {
                prevUrls.push(currUrl);
                nextUrls.clear();
            }

            currUrl = url;

            editTextUrl.setText(url);
            prevBtn.setEnabled(!prevUrls.empty());
            nextBtn.setEnabled(!nextUrls.empty());
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_browser);

        webView = (WebView) findViewById(R.id.webView);
        webView.setWebViewClient(new Client());
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setLoadsImagesAutomatically(true);
        webView.loadUrl("http://www.google.com");

        editTextUrl = (EditText) findViewById(R.id.urlEditText);

        prevBtn = (ImageButton) findViewById(R.id.b_prev);
        nextBtn = (ImageButton) findViewById(R.id.b_next);

        scrollUpBtn = (ImageButton) findViewById(R.id.b_up);
        scrollDownBtn = (ImageButton) findViewById(R.id.b_down);

        goButton = (Button) findViewById(R.id.b_go);
        setupScrolling();
        setUrlDoneListener();

        sharedPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
    }

    @Override
    protected void onResume() {
        super.onResume();
        startCursorService();
    }

    @Override
    protected void onPause() {
        stopCursorService();
        super.onPause();
    }


    private void startCursorService() {

        final CursorService cursorService = CursorApplication.getInstance().getCursorService();
        if (cursorService.isRunning()) {
            stopCursorService = false;
            return;
        }

        stopCursorService = true;
        boolean isFaceCursorDefault = sharedPref.getBoolean(getString(R.string.pref_is_face_cursor_default), false);

        ViewInputHolder cursorView;
        if (isFaceCursorDefault) {
            final Point screenDimensions = new Point();
            getWindowManager().getDefaultDisplay().getSize(screenDimensions);
            cursorView = new FdView(getApplicationContext(), screenDimensions);
        } else {
            cursorView = new AxisMoverView(getApplicationContext());
        }
        if (cursorService != null) {
            cursorService.start(cursorView);
        }
    }

    private void stopCursorService() {
        if (!stopCursorService) {
            return;
        }
        CursorService cursorService = CursorApplication.getInstance().getCursorService();
        if (cursorService != null) {
            cursorService.stop();
        }
    }

    private void setUrlDoneListener() {
        editTextUrl.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if(event.getAction() == KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_ENTER){
                    View view = BrowserActivity.this.getCurrentFocus();
                    if (view != null) {
                        InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                    }
                    goButton.callOnClick();
                }
                return false;
            }
        });
    }

    private void scrollUp() {

        if (webView.canScrollVertically(-100))
            webView.scrollBy(0,-100);
    }

    private void scrollDown() {

        if (webView.canScrollVertically(100))
            webView.scrollBy(0,100);
    }

    private void setupScrolling() {
        scrollDownBtn.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {

                    scrolling = true;

                    handler.post(new Runnable() {

                        @Override
                        public void run()
                        {
                            if (scrolling)
                            {
                                scrollDown();
                                handler.postDelayed(this, 10);
                            }
                        }
                    });
                    return true;
                } else if (event.getAction() == MotionEvent.ACTION_UP) {
                    scrolling = false;
                    return true;
                }
                return false;
            }
        });

        scrollUpBtn.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {

                    scrolling = true;

                    handler.post(new Runnable() {
                        @Override
                        public void run()
                        {
                            if (scrolling) {
                                scrollUp();
                                handler.postDelayed(this, 10);
                            }
                        }
                    });
                    return true;
                } else if (event.getAction() == MotionEvent.ACTION_UP) {
                    scrolling = false;
                    return true;
                }
                return false;
            }
        });
    }

    public void onGoBtn(View v) {
        String str = editTextUrl.getText().toString();
        if (str.equals("")) return;

        if (!str.startsWith("http://") && !str.startsWith("https://")) {
            str = "http://" + str;
        }

        webView.loadUrl(str);
        editTextUrl.clearFocus();
    }

    public void onPrevBtn(View v) {
        webView.loadUrl(prevUrls.peek());
    }

    public void onNextBtn(View v) {
        webView.loadUrl(nextUrls.peek());
    }

    public void onBackBtn(View v) {
        super.onBackPressed();
    }

}
