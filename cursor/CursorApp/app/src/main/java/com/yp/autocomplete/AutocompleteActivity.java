package com.yp.autocomplete;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.ArrayAdapter;

import com.yp.cursor.R;

import java.util.List;

public class AutocompleteActivity extends AppCompatActivity {

    /*
     * Change to type CustomAutoCompleteView instead of AutoCompleteTextView
     * since we are extending to customize the view and disable filter
     * The same with the XML view, type will be CustomAutoCompleteView
     */
    CustomAutoCompleteView myAutoComplete;

    // adapter for auto-complete
    ArrayAdapter<String> myAdapter;

    // for database operations
    NgramDatabaseSuggester databaseH;

    int currentCorpus = 0;

    // just to add some initial value
    String[] item = new String[]{"Please search..."};

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_autocomplete);

        try {

            // instantiate database handler
            databaseH = new NgramDatabaseSuggester(AutocompleteActivity.this);
            databaseH.loadDB(R.raw.sample_text_2);

            // autocompletetextview is in activity_main.xml
            myAutoComplete = (CustomAutoCompleteView) findViewById(R.id.myautocomplete);

            // add the listener so it will tries to suggest while the user types
            final android.content.Context context1 = this;
            myAutoComplete.addTextChangedListener(new TextWatcher() {
                public static final String TAG = "AutoComplete:TextWatcher";
                Context context = context1;

                @Override
                public void afterTextChanged(Editable s) {
                    // TODO Auto-generated method stub

                }

                @Override
                public void beforeTextChanged(CharSequence s, int start, int count,
                                              int after) {
                    // TODO Auto-generated method stub

                }

                @Override
                public void onTextChanged(CharSequence userInput, int start, int before, int count) {
                    AutocompleteActivity autocompleteActivity = ((AutocompleteActivity) context);
                    autocompleteActivity.item = autocompleteActivity.getCompletions(userInput.toString());

                    // update the adapater
                    autocompleteActivity.myAdapter.notifyDataSetChanged();
                    autocompleteActivity.myAdapter = new ArrayAdapter<String>(autocompleteActivity, android.R.layout.simple_dropdown_item_1line, autocompleteActivity.item);
                    autocompleteActivity.myAutoComplete.setAdapter(autocompleteActivity.myAdapter);

                }
            });

            // set our adapter
            myAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, item);
            myAutoComplete.setAdapter(myAdapter);

        } catch (NullPointerException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // this function is used in CustomAutoCompleteTextChangedListener.java
    public String[] getCompletions(String line) {

        String[] item = databaseH.suggest(line);

        return item;
    }

    public void switchCorpus(View view) {
        currentCorpus = (currentCorpus + 1)%2;
        if (currentCorpus == 0)
            databaseH.loadDB(R.raw.sample_text_2);
        else
            databaseH.loadDB(R.raw.sample_text_1);
    }
}