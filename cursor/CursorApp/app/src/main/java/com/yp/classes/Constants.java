package com.yp.classes;

/**
 * Created by avner on 13/06/2016.
 */
public class Constants {

    public class FaceCursor{
        public static final int DEFAULT_X_MOVEMENT_PORTRAIT = 5;
        public static final int DEFAULT_Y_MOVEMENT_PORTRAIT =10;
        public static final int DEFAULT_EYE_BLINK_THRESHOLD = 7; // minimum number of frames an eye should be closed while blinking
    }

}
