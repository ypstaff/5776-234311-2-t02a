package com.yp.navigationlibrary;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.support.v4.content.res.ResourcesCompat;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by sRoySvin on 11/6/2015.
 *
 * @author Roy Svinik
 */

public class IterableViewGroup implements IterableGroup {

    List<View> elements;
    Integer selected;
    Map<View, Drawable> formerBackgrounds;
    View groupView;
    Context context;
    //default color is myTileColor
    Drawable background;
    Drawable etBackground;
    int markColor = Color.YELLOW;

    public IterableViewGroup(List<View> views, View groupView, Context context) {
        elements = views;
        formerBackgrounds = new HashMap<>();
        for (View v: elements) {
            formerBackgrounds.put(v, v.getBackground());
        }
        this.groupView = groupView;
        formerBackgrounds.put(groupView, groupView.getBackground());
        this.context = context;
        background = ResourcesCompat.getDrawable(context.getResources(), yearlyproject.navigationlibrary.R.drawable.border, null);
        etBackground = ResourcesCompat.getDrawable(context.getResources(), yearlyproject.navigationlibrary.R.drawable.edit_text_border, null);
    }

    //customize drawable color
    public IterableViewGroup(List<View> views, View groupView, Context context, Drawable d,
                              int markColor) {
        this(views, groupView, context);
        background = d;
        this.markColor = markColor;
    }

    public boolean start() {
        if(elements.size() > 0 ) {
            unmarkAllChilds();
            circleAllChilds();
            selected = 0;
            mark(elements.get(selected));
        }
        return true;
    }

    public boolean iterate() {
        unmark(elements.get(selected));
        circle(elements.get(selected));
        selected = (selected + 1) % elements.size();
        mark(elements.get(selected));
        return true;
    }

    /* invokes the onClick listener of the selected view
     * Note: EditView onClick listener does NOT open keyboard */
    public void select() {
        elements.get(selected).performClick();
    }

    @Override
    public boolean back() {
        unmarkAllChilds();
        uncircleAllChilds();
        return true;
    }

    @Override
    public void markGroup() {
        mark(groupView);
    }

    @Override
    public void unmarkGroup() {
        unmark(groupView);
    }


    public void markAllChilds() {
        for (View v: elements) {
            mark(v);
        }
    }

    public void unmarkAllChilds() {
        for (View v: elements) {
            unmark(v);
        }
    }

    @Override
    public void circleGroup() {
        circle(groupView);
    }

    @Override
    public void uncircleGroup() {
        uncircle(groupView);
    }

    @Override
    public void circleAllChilds() {
        for (View v: elements) {
            circle(v);
        }
    }

    @Override
    public void uncircleAllChilds() {
        for (View v: elements) {
            uncircle(v);
        }
    }

    @Override
    public List<View> getSelectedViews() {
        List<View> views = new ArrayList<>();
        views.add(elements.get(selected));
        return views;
    }

    @Override
    public View getGroupView() {
        return groupView;
    }

    private void mark(View v) {
        v.setBackgroundColor(markColor);
    }

    private void unmark(View v) {
        v.setBackground(formerBackgrounds.get(v));
    }

    private void circle(View v) {
        if(v instanceof EditText) {
            v.setBackground(etBackground);
        } else {
            v.setBackground(background);
        }
    }

    private void uncircle(View v) {
        unmark(v);
    }

    @Override
    public void stop() {
        Log.e("IterableViewGroup", "stop");
        unmarkGroup();
        uncircleGroup();
        unmarkAllChilds();
        uncircleAllChilds();
    }

}
