package com.yp.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.yp.cursor.R;

import java.util.Arrays;
import java.util.LinkedList;

/**
 * @author Or Mauda
 */
public class RgbColorActivity extends SideMenuActivity {

    private ImageView colorView;

    private ImageView redView;
    private ImageView greenView;
    private ImageView blueView;

    private EditText redText;
    private EditText greenText;
    private EditText blueText;

    private Button selectColor;
    private String selectColorTag;

    private LinkedList<TextView> viewsList = new LinkedList<>();
    private TextView currView;
    private int currIndex = 0;

    private int currColor;
    private int currRed;
    private int currGreen;
    private int currBlue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rgb_color);

        colorView = (ImageView)findViewById(R.id.colorView);
        redView = (ImageView)findViewById(R.id.redView);
        greenView = (ImageView)findViewById(R.id.greenView);
        blueView = (ImageView)findViewById(R.id.blueView);

        redText = (EditText)findViewById(R.id.redText);
        greenText = (EditText)findViewById(R.id.greenText);
        blueText = (EditText)findViewById(R.id.blueText);

        Bundle b = getIntent().getExtras();
        if (b != null) {
            currColor = b.getInt("initial_value");
        }
        currRed = Color.red(currColor);
        currGreen = Color.green(currColor);
        currBlue = Color.blue(currColor);
        currColor = Color.rgb(currRed, currGreen, currBlue);

        redView.setColorFilter(Color.rgb(currRed, 0, 0));
        greenView.setColorFilter(Color.rgb(0, currGreen, 0));
        blueView.setColorFilter(Color.rgb(0, 0, currBlue));
        colorView.setColorFilter(currColor);

        redText.setText(Integer.toString(currRed));
        greenText.setText(Integer.toString(currGreen));
        blueText.setText(Integer.toString(currBlue));

        redText.setFocusable(true);
        greenText.setFocusable(true);
        blueText.setFocusable(true);

        redText.setCursorVisible(false);
        greenText.setCursorVisible(false);
        blueText.setCursorVisible(false);

        redText.setBackground(null);
        greenText.setBackground(null);
        blueText.setBackground(null);

        selectColor = (Button)findViewById(R.id.selectColor);
        selectColorTag = selectColor.getTag().toString();

        viewsList.addLast(redText);
        viewsList.addLast(greenText);
        viewsList.addLast(blueText);
        viewsList.addLast(selectColor);

        initEditTextListeners();

        currView = viewsList.get(currIndex);
        highlightCurrentTextView();
    }

    private void highlightCurrentTextView() {
        currView.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.yellow));
    }

    private void removeHighlightCurrentTextView() {
        if (currView.getTag().toString().equals(selectColorTag)) {
            currView.setBackgroundResource(android.R.drawable.btn_default);
            return;
        }
        currView.setBackgroundColor(Color.TRANSPARENT);
    }

    @Override
    public void onUpClick(View v) {
        removeHighlightCurrentTextView();
        if (currView.equals(viewsList.getFirst())) {
            currIndex = viewsList.size()-1;
            currView = viewsList.get(currIndex);
            highlightCurrentTextView();
            return;
        }
        currIndex--;
        currView = viewsList.get(currIndex);
        highlightCurrentTextView();
    }

    @Override
    public void onDownClick(View v) {
        removeHighlightCurrentTextView();
        if (currView.equals(viewsList.getLast())) {
            currIndex = 0;
            currView = viewsList.get(currIndex);
            highlightCurrentTextView();
            return;
        }
        currIndex++;
        currView = viewsList.get(currIndex);
        highlightCurrentTextView();
    }

    @Override
    public void onSelectClick(View v) {

        if (currView.getTag().toString().equals(selectColorTag)) {
            // return with the current color
            Intent i = new Intent();
            i.putExtra("color", Integer.toString(currColor));
            setResult(RESULT_OK, i);
            finish();
            return;
        }

        // change the red\green\blue color
        EditText currText = (EditText) currView;

        currText.setFocusable(true);
        currText.setFocusableInTouchMode(true);
        currText.requestFocus();
        currText.selectAll();

        // open the keyboard
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_IMPLICIT, 0);
    }

    private void initEditTextListeners() {
        for (final EditText et : Arrays.asList(redText, greenText, blueText)) {
            et.setOnEditorActionListener(new TextView.OnEditorActionListener() {

                @Override
                public boolean onEditorAction(TextView v, int actionId,
                                              KeyEvent event) {
                    if (event != null&& (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) {
                        InputMethodManager in = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);

                        in.hideSoftInputFromWindow(et.getApplicationWindowToken(),
                                InputMethodManager.HIDE_NOT_ALWAYS);

                        EditText currText = (EditText) currView;

                        // validate input
                        if (currText.getText().toString().matches("[0-9]+") == false) {
                            Toast.makeText(getApplicationContext(),"must be an integer between 0 and 255",Toast.LENGTH_SHORT).show();
                            currText.setText("0");
                            return true;
                        } else {
                            int num = Integer.parseInt(currText.getText().toString());
                            if (num < 0 || num > 255) {
                                currText.setText("0");
                                Toast.makeText(getApplicationContext(),"must be an integer between 0 and 255",Toast.LENGTH_SHORT).show();
                                return true;
                            }
                        }

                        int subColor;
                        if (currText.equals(redText)) {
                            currRed = Integer.parseInt(redText.getText().toString());
                            subColor = Color.rgb(currRed, 0, 0);
                            redView.setColorFilter(subColor);
                        } else if (currText.equals(greenText)) {
                            currGreen = Integer.parseInt(greenText.getText().toString());
                            subColor = Color.rgb(0, currGreen, 0);
                            greenView.setColorFilter(subColor);
                        } else if (currText.equals(blueText)) {
                            currBlue = Integer.parseInt(blueText.getText().toString());
                            subColor = Color.rgb(0, 0, currBlue);
                            blueView.setColorFilter(subColor);
                        }

                        // now lets change the result color
                        int color = Color.rgb(currRed, currGreen, currBlue);
                        colorView.setColorFilter(color);
                        currColor = color;

                        currText.clearFocus();

                        // Must return true here to consume event
                        return true;

                    }
                    return false;
                }
            });
        }
    }

}
