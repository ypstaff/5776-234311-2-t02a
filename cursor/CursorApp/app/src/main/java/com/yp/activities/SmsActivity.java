package com.yp.activities;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.yp.classes.ClickOnlyView;
import com.yp.cursor.CursorApplication;
import com.yp.cursor.R;
import com.yp.cursor.SingleInput;
import com.yp.navigationlibrary.EmergencyCallActivity;
import com.yp.navigationlibrary.IterableGroup;
import com.yp.navigationlibrary.IterableSingleView;
import com.yp.navigationlibrary.IterableViewGroups;

import java.util.ArrayList;
import java.util.List;


/*
*
* *@author Matan Revivo
*
 */

public class SmsActivity extends EmergencyCallActivity implements View.OnClickListener {

    private static Class c;
    private EditText et_number, et_message;
    private Button b_send;
    private String toast = null;
    private boolean testMode = false;
    private SingleInput[] navSimpleInputs = new ClickOnlyView[3];
    private Animation fabPressAnimation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sms);
        et_number=(EditText)findViewById(R.id.et_number);
        et_message=(EditText)findViewById(R.id.et_message);
        b_send = (Button)findViewById(R.id.b_send);
        fabPressAnimation = AnimationUtils.loadAnimation(getApplication(), R.anim.fab_main_press);
        handleOpenFromContacts();
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        c = getClass();

        //Setting EditorActionListener for closing keyboard on enter
        et_number.setOnClickListener(this);
        et_number.setImeOptions(EditorInfo.IME_ACTION_DONE);
        et_number.setSingleLine();

        et_message.setOnClickListener(this);
        et_message.setImeOptions(EditorInfo.IME_ACTION_DONE);
        b_send.setOnClickListener(this);
        initNavigation();
    }

    private void initNavigation() {
        //Setting the 3 buttons navigation system for the activity
        fab_left = (FloatingActionButton) findViewById(R.id.fab_left);
        fab_right = (FloatingActionButton) findViewById(R.id.fab_right);
        fab_middle = (FloatingActionButton) findViewById(R.id.fab_middle);

        //Setting the action buttons to be semi transparent
        fab_left.setAlpha((float) 0.5);
        fab_middle.setAlpha((float) 0.5);
        fab_right.setAlpha((float) 0.5);

        //Setting the listeners for the navigation buttons
        fab_left.setOnClickListener(this);
        fab_right.setOnClickListener(this);
        fab_middle.setOnClickListener(this);
        fab_left.setOnLongClickListener(this);
        fab_right.setOnLongClickListener(this);
        fab_middle.setOnLongClickListener(this);

        //Creating the groups for the navigation
        //Row 1 group
        IterableGroup row1_group = new IterableSingleView(et_number, this);
        IterableGroup row2_group = new IterableSingleView(et_message, this);
        IterableGroup row3_group = new IterableSingleView(b_send, this);

        //The general group
        List<IterableGroup> all_rows_list = new ArrayList<>();
        all_rows_list.add(row1_group);
        all_rows_list.add(row2_group);
        all_rows_list.add(row3_group);

        group = new IterableViewGroups(all_rows_list, findViewById(R.id.ll_all_rows), this);
        group.start();


        navSimpleInputs[0] = new ClickOnlyView(this, fab_right);
        navSimpleInputs[1] = new ClickOnlyView(this, fab_middle);
        navSimpleInputs[2] = new ClickOnlyView(this, fab_left);

    }

    void handleOpenFromContacts(){
        // if was started from contacts
        if (this.getIntent().hasExtra("phone number") == true) {
            Bundle b = getIntent().getExtras();
            et_number.setText(b.getString("phone number"));
        }
    }

    public String getToast(){
        return toast;
    }

    public void setTestMode(boolean b){
        testMode = b;
    }

    Boolean isStringValidPhoneNumber(String number){
        String regex = "[0-9]+";
        // if wasn't stated from contacts - do some checks on validity of the input
        if(!number.matches(regex)){
            makeToast("number must contain only digits");
            return false;
        }
        if(number.length() != 10){
            makeToast("number must contain 10 digits");
            return false;
        }
        return true;
    }

    Boolean isValidMessage(String message){
        if(message.length() == 0){
            makeToast("message can not be empty");
            return false;
        }
        return true;
    }


    protected void sendSms() {
        String number=et_number.getText().toString();
        String message=et_message.getText().toString();

        Log.d("size number",Integer.toString(number.length()));
        Log.d("size message", Integer.toString(message.length()));

        if (this.getIntent().hasExtra("phone number") == false) {
            if(!isStringValidPhoneNumber(number))
                return;
        }

        if(!isValidMessage(message))
            return;

        if(!testMode) {
            SmsManager manager = SmsManager.getDefault();
            manager.sendTextMessage(number, null, message, null, null);
        }
        makeToast("sent succesfully");
        et_number.setText("");
        et_message.setText("");
    }


    @Override
    public void onClick(View v) {
        //For the navigation buttons
        super.onClick(v);
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        switch(v.getId()) {
            case R.id.et_number:
                Log.d("sms", "et_number pressed");
                et_number.requestFocus();
                imm.showSoftInput(et_number, 0);
                break;
            case R.id.et_message:
                Log.d("sms", "et_messagepressed");
                et_message.requestFocus();
                imm.showSoftInput(et_message, 0);
                break;
            case R.id.b_send:
                Log.d("sms", "send sms pressed");
                sendSms();
                break;
            case R.id.fab_left:
            case R.id.fab_middle:
            case R.id.fab_right:
                v.startAnimation(fabPressAnimation);
                break;
        }
    }

    void makeToast(String s){
        Toast.makeText(getApplicationContext(), s, Toast.LENGTH_LONG).show();
        toast = s;
    }

    private void unregisterFromBT() {
        for (int i = 0; i < navSimpleInputs.length; ++i) {
            if(CursorApplication.getInstance().dispatcher != null)
                CursorApplication.getInstance().dispatcher.setInputToDispatch(i, null);
        }
    }

    private void registerToBt() {
        for (int i = 0; i < navSimpleInputs.length; ++i) {
            if(CursorApplication.getInstance().dispatcher != null)
                CursorApplication.getInstance().dispatcher.setInputToDispatch(i, navSimpleInputs[i]);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterFromBT();
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerToBt();
    }


}
