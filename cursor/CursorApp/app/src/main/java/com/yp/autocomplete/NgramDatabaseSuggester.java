package com.yp.autocomplete;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.net.Uri;
import android.util.Log;

/**
 * Created by etayhn on 21-May-16.
 */

/**
 * @class NgramDatabaseSuggester
 */
public class NgramDatabaseSuggester {
    private NgramDatabaseHandler dbHandler;

    /**
     * @param context         the current context
     * @param n               the length of each NGram
     * @param numSuggestions  the number of suggestions to be returned from <i>suggest</i>
     * @param tableNamePrefix a prefix for the name of the table in the database
     */
    public NgramDatabaseSuggester(Context context, int n, int numSuggestions, String tableNamePrefix) {
        dbHandler = new NgramDatabaseHandler(context, n, numSuggestions, tableNamePrefix);
    }

    /**
     * Same as NgramDatabaseSuggester(Context, int, int, String), only that tableNamePrefix is set to a default value of 'my_sample_text_'.
     *
     * @param context        the current context
     * @param n              the length of each NGram
     * @param numSuggestions the number of suggestions to be returned from <i>suggest</i>
     */
    public NgramDatabaseSuggester(Context context, int n, int numSuggestions) {
        dbHandler = new NgramDatabaseHandler(context, n, numSuggestions, "my_sample_text_");
    }

    /**
     * Same as NgramDatabaseSuggester(Context, int, int, String), only that:
     * tableNamePrefix is set to a default value of 'my_sample_text_'
     * n is set to a default value of 3
     * numSuggestions is set to a default value of 3
     *
     * @param context the current context
     */
    public NgramDatabaseSuggester(Context context) {
        this(context, 3, 3, "my_sample_text_");
    }

    /**
     * loads the database with the raw resource file whose id is inputFileId
     *
     * @param inputFileId
     */
    public void loadDB(int inputFileId) {
        dbHandler.loadDB(inputFileId);
    }

    /**
     * loads the database with the raw resource file whose id is inputFileId
     *
     * @param uri
     */
    public void loadDB(Uri uri) {
        dbHandler.loadDB(uri);
    }

    /**
     * Suggests the next word given the current inputLine
     *
     * @param inputLine the inputLine to complete
     * @return an array containing (up to) <i>numSuggestions</i> suggestions to complete the given <i>inputLine</i>.
     */
    public String[] suggest(String inputLine) {
        return dbHandler.suggest(inputLine);
    }

}

class NgramDatabaseHandler extends SQLiteOpenHelper {

    // for our logs
    public static final String TAG = "NgramDatabaseSuggester";

    // database version
    private static final int DATABASE_VERSION = 4;

    // database name
    protected static final String DATABASE_NAME = "NinjaDatabase2";
    private final int n;

    // table details
    private String tableNamePrefix = "my_sample_text_";
    private String tableName = tableNamePrefix;
    private String fieldObjectId = "id";
    private String fieldObjectTerm = "term";
    private String fieldObjectRank = "rank";
    private final String TIMESTAMP = "__TIMESTAMP__";
    private String fieldObjectPreviousTerms = "previousTerms";
    private int numSuggestions;
    private boolean isDBInCreation = false;
    private Context context;

    /**
     * @param context         the current context
     * @param n               the length of each NGram
     * @param numSuggestions  the number of suggestions to be returned from <i>suggest</i>
     * @param tableNamePrefix a prefix for the name of the table in the database
     */
    public NgramDatabaseHandler(Context context, int n, int numSuggestions, String tableNamePrefix) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.n = n;
        this.context = context;
        this.numSuggestions = numSuggestions;
        this.tableNamePrefix = tableNamePrefix;
        this.tableName = tableNamePrefix;
    }

    // creating table
    @Override
    public void onCreate(SQLiteDatabase db) {

    }

    /**
     * loads the database with the raw resource file whose id is inputFileId
     *
     * @param inputFileId
     */
    public void loadDB(int inputFileId) {
        isDBInCreation = true;
        tableName = tableNamePrefix + intFileIdToStringFileId(inputFileId);
        createTable();
        final int fileId = inputFileId;
        Executors.newSingleThreadExecutor().execute(new Runnable(){

            @Override
            public void run() {
                generateNgramDB(fileId);
                NgramDatabaseHandler.this.isDBInCreation = false;
            }
        });
    }

    /**
     * loads the database with the raw resource file whose id is inputFileId
     *
     * @param uri
     */
    public void loadDB(Uri uri) {
        isDBInCreation = true;
        tableName = tableNamePrefix + uri.getLastPathSegment();
        createTable();
        final Uri fileUri = uri;
        Executors.newSingleThreadExecutor().execute(new Runnable(){

            @Override
            public void run() {
                generateNgramDB(fileUri);
                NgramDatabaseHandler.this.isDBInCreation = false;
            }
        });
    }

    private void createTable() {
        String sql = "";
        sql += "CREATE TABLE IF NOT EXISTS " + tableName;
        sql += " ( ";
        sql += fieldObjectId + " INTEGER PRIMARY KEY AUTOINCREMENT, ";
        sql += fieldObjectTerm + " TEXT, ";
        sql += fieldObjectPreviousTerms + " TEXT, ";
        sql += fieldObjectRank + " INTEGER ";
        sql += " ) ";

        getWritableDatabase().execSQL(sql);

    }

    // When upgrading the database, it will drop the current table and recreate.
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    }

    private int countWords(String phrase) {
        if (phrase == null)
            return -1;
        return phrase.split(" ").length;
    }

    /**
     * Takes the fileId that is given as an int and turns it into a string. This allows
     * us to give the table in the database "the same name" as the file from which it was created.
     *
     * @param fileId the fileId as an integer
     * @return a string representing the fileId
     */
    private String intFileIdToStringFileId(int fileId) {
        if (fileId < 0)
            return null;
        String s = "";
        while (fileId > 0) {
            s = (char) ('a' + fileId % 10) + s;
            fileId /= 10;
        }
        return s;
    }

    /**
     * Takes the fileId that is given as an String and turns it into a int. This allows
     * us to give the table in the database "the same name" as the file from which it was created.
     *
     * @param fileId the fileId as a String
     * @return an integer representing the fileId
     */
    private int stringFileIdToIntFileId(String fileId) {
        if (fileId == null || fileId.isEmpty())
            return -1;
        int x = 0;
        for (int i = 0; i < fileId.length(); i++)
            x = x * 10 + ((int) (fileId.charAt(i) - 'a'));
        return x;
    }

    /**
     * Reads the text from the given input file, and creates d-grams from it (for every d between 1 and this.n)
     *
     * @param inputFileId the id of the raw resource file from which the NGrams are created.
     * @throws IOException
     */
    private void generateNgramDB(int inputFileId) {
        for (int i = 1; i <= n; i++) {
            try {
                InputStream inputStream = context.getResources().openRawResource(inputFileId);
                generateNgramDB(inputStream, i);
                inputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Reads the text from the given input file, and creates d-grams from it (for every d between 1 and this.n)
     *
     * @param uri the uri of the file from which the NGrams are created
     * @throws IOException
     */
    private void generateNgramDB(Uri uri){
        for (int i = 1; i <= n; i++) {
            try {
                InputStream inputStream = context.getContentResolver().openInputStream(uri);
                generateNgramDB(inputStream, i);
                inputStream.close();
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

    /**
     * Creates d-grams in the database using the text read from the input stream, where d is the number of words in each d-gram.
     *
     * @param inputStream from this stream the NGrams are being created.
     * @param d           the number of words to include in <i>previousTerms</i> of each NGram.
     * @throws IOException
     */
    private void generateNgramDB(InputStream inputStream, int d) throws IOException {
        Log.v(TAG, "generateNgramDB: d=" + d);
        String term = "", previousTerms = "";
        Scanner sc2 = new Scanner(inputStream);
        boolean longEnough = false;
        while (sc2.hasNextLine()) {
            Scanner s2 = new Scanner(sc2.nextLine());
            while (s2.hasNext()) {
                previousTerms += " " + term;
                previousTerms = previousTerms.trim();
                if (countWords(previousTerms) == d) {
                    longEnough = true;
                    previousTerms = previousTerms.substring(previousTerms.indexOf(" ") > -1 ? previousTerms.indexOf(" ") : previousTerms.length()).trim();
                }
                term = s2.next().replaceAll("[^a-zA-Z0-9]", "").toLowerCase();
                if (longEnough)
                    create(term, previousTerms);
                longEnough = false;
            }
        }
    }


    /**
     * Creates an NGram in the database that consists of the given term and previousTerms. If there already
     * exists such an NGram in the database, it increments its rank.
     *
     * @param term          the term
     * @param previousTerms the previous terms
     * @return true iff the creation/update was successful.
     */
    private boolean create(String term, String previousTerms) {
        if (term == null)
            return false;
        term = term.replaceAll("[^a-zA-Z0-9]", "").toLowerCase();
        previousTerms = previousTerms.replaceAll("[^a-zA-Z0-9 ]", "").toLowerCase();

        boolean createSuccessful = false;

        NGram nGram = get(term, previousTerms);

        if (nGram == null) {

            SQLiteDatabase db = this.getWritableDatabase();

            ContentValues values = new ContentValues();
            values.put(fieldObjectTerm, term);
            values.put(fieldObjectPreviousTerms, previousTerms);
            values.put(fieldObjectRank, 0);
            createSuccessful = db.insert(tableName, null, values) > 0;

            db.close();

            if (createSuccessful) {
                Log.v(TAG, "(" + previousTerms + ", " + term + ") created.");
            }
        } else {
            String strSQL = "UPDATE " + tableName + " SET " + fieldObjectRank + " = " + (nGram.rank + 1) + " WHERE " + fieldObjectTerm + " = '" + nGram.objectName + "' AND " + fieldObjectPreviousTerms + " = '" + nGram.previousTerms + "'";
            this.getWritableDatabase().execSQL(strSQL);
            createSuccessful = true;
            Log.v(TAG, "(" + previousTerms + ", " + term + ") updated.");
        }

        return createSuccessful;
    }

    /**
     * Returns an NGram based on the data that appears in the database
     *
     * @param term          the term
     * @param previousTerms the previousTerms
     * @return the NGram found, or null if none was found.
     */
    private NGram get(String term, String previousTerms) {
        NGram nGram = null;
        term = term.replaceAll("[^a-zA-Z0-9]", "").toLowerCase();
        previousTerms = previousTerms.replaceAll("[^a-zA-Z0-9 ]", "").toLowerCase();

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM " + tableName + " WHERE " + fieldObjectTerm + " = '" + term + "' AND " + fieldObjectPreviousTerms + " = '" + previousTerms + "'", null);


        if (cursor != null) {

            if (cursor.getCount() > 0) {
                cursor.moveToFirst();
                String objTerm = cursor.getString(cursor.getColumnIndex(fieldObjectTerm));
                String objPreviousTerms = cursor.getString(cursor.getColumnIndex(fieldObjectPreviousTerms));
                int rank = cursor.getInt(cursor.getColumnIndex(fieldObjectRank));
                nGram = new NGram(objTerm, objPreviousTerms, rank);
            }
        }

        cursor.close();
        db.close();

        return nGram;
    }

    /**
     * Suggests the next word given the current inputLine
     *
     * @param inputLine the inputLine to complete
     * @return an array containing (up to) <i>numSuggestions</i> suggestions to complete the given <i>inputLine</i>.
     */
    public String[] suggest(String inputLine) {
        if (isDBInCreation) {
            String[] $ = new String[numSuggestions];
            for (int i=0; i<$.length; i++){
                if (i == numSuggestions/2 - 1)
                    $[i] = "generating";
                else if (i == numSuggestions/2)
                    $[i] = "suggestion";
                else if (i == numSuggestions/2 + 1)
                    $[i] = "database..";
                else
                    $[i] = "";
            }
            return $;
        }
        List<NGram> suggestions = new ArrayList<>();
        if (inputLine == null) {
            return new String[0];
        }

        String[] inputArr = inputLine.split("\\s+");
        String previousTerms = "";

        boolean isLastWordSpace = inputLine.length() >= 1 && inputLine.charAt(inputLine.length() - 1) == ' ';
        int i = isLastWordSpace ? inputArr.length - n + 1 : inputArr.length - n;
        int stoppingIndex = isLastWordSpace ? inputArr.length : inputArr.length - 1;

        for (; i < stoppingIndex; i++)
            if (i >= 0)
                previousTerms += " " + inputArr[i];
        previousTerms = previousTerms.trim();
        String term = isLastWordSpace ? "" : inputArr[inputArr.length - 1];

        while (!previousTerms.equals("")) {
            addAllUnique(suggest(term, previousTerms), suggestions);
            previousTerms = previousTerms.substring(previousTerms.indexOf(" ") > -1 ? previousTerms.indexOf(" ") : previousTerms.length()).trim();
        }
        addAllUnique(suggest(term, ""), suggestions);
//        if (!term.equals(""))
//            addAllUnique(suggest("", ""), suggestions);

        String[] $ = new String[numSuggestions];
        for (i = 0; i < suggestions.size() && i < numSuggestions; i++)
            $[i] = suggestions.get(i).objectName;
        for (i = suggestions.size() + 1; i < numSuggestions; i++)
            $[i] = "";

        return $;
    }

    private boolean isSuggestionInList(List<NGram> suggestions, NGram ngram) {
        for (NGram s : suggestions)
            if (s.objectName.equals(ngram.objectName))
                return true;
        return false;
    }

    private void addAllUnique(List<NGram> from, List<NGram> to) {
        for (NGram ngram : from)
            if (!isSuggestionInList(to, ngram))
                to.add(ngram);
    }

    /**
     * An auxiliary function, used by <i>List<String> suggest(String)</i>.
     *
     * @param term          the word to complete.
     * @param previousTerms the previous words that appear in the text.
     * @return a list of the highest ranked possible completions.
     */
    private List<NGram> suggest(String term, String previousTerms) {
        Log.v(TAG, "suggest: (" + previousTerms + ", " + term + ")");
        List<NGram> recordsList = new ArrayList<NGram>();
        term = term.replaceAll("[^a-zA-Z0-9 ]", "").toLowerCase();
        previousTerms = previousTerms.replaceAll("[^a-zA-Z0-9 ]", "").toLowerCase();

        // select query
        String sql = "";
        sql += "SELECT * FROM " + tableName;
        sql += " WHERE " + fieldObjectTerm + " LIKE '" + term + "%'";
        sql += " AND " + fieldObjectPreviousTerms + " = '" + previousTerms + "'";
        sql += " ORDER BY " + fieldObjectRank + " DESC";
        sql += " LIMIT 0," + numSuggestions;

        Log.v(TAG, "suggest: " + sql);

        SQLiteDatabase db = this.getWritableDatabase();

        // execute the query
        Cursor cursor = db.rawQuery(sql, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {

                String objTerm = cursor.getString(cursor.getColumnIndex(fieldObjectTerm));
                String objPreviousTerms = cursor.getString(cursor.getColumnIndex(fieldObjectPreviousTerms));
                int rank = cursor.getInt(cursor.getColumnIndex(fieldObjectRank));
                NGram nGram = new NGram(objTerm, objPreviousTerms, rank);

                // add to list
                recordsList.add(nGram);

            } while (cursor.moveToNext());
        }

        cursor.close();
        db.close();

        // return the list of records
        return recordsList;
    }

}

/**
 * @class NGram represents an n-gram in an n-gram NLP model.
 */
class NGram {
    /**
     * A word from the text
     */
    public String objectName;
    /**
     * The preceding words to objectName in the text
     */
    public String previousTerms;
    /**
     * the number of times that the combination previousTerms+objectName occurs in the text
     */
    public int rank;

    /**
     * @param objectName    the word
     * @param previousTerms the words that appear before it
     * @param rank          the number of times that the combination appears in the text
     */
    public NGram(String objectName, String previousTerms, int rank) {
        this.objectName = objectName;
        this.previousTerms = previousTerms;
        this.rank = rank;
    }

}