package io.github.mthli.knife;

import android.support.test.runner.AndroidJUnit4;
import android.util.Log;

import junit.framework.Assert;

import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;
import java.util.List;

import static junit.framework.Assert.assertEquals;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by tbenalta on 6/18/2016.
 */
@RunWith(AndroidJUnit4.class)
public class HTMLParserTest {

    @Test
    public void contractSpacesTest() {
        String after = "The tested method should replace any sequence of white spaces with one space";
        String before = "The        tested \n\t\nmethod\nshould\treplace\t any\n sequence of      white spaces with one    space";
        HTMLParser parser = new HTMLParser("");
        assertEquals(after, parser.contractWhiteSpaces(before));
    }

    @Test
    public void plainTextAfterTagExtraction() {
        String after = "This is the remaining text after removing all the tags";
        String before = "This <b>is</b> <span style=\"font-size: 33px;\">the</span> <ul><li>remaining</li></ul> text after removing all the tags";
        HTMLParser parser = new HTMLParser("");
        assertEquals(after, parser.extractTags(before));
    }

    @Test
    public void extractedTags() {
        String after = "This is the remaining text after removing all the tags";
        HTMLTag[] expectedTags = new HTMLTag[8];
        String before = "This "
                + "<b>i<u>s</b></u>"
                + " <span style=\"font-size: 50px;\">the</span>"
                + " <ul><li>remaining</li></ul>"
                + " text after removing all the tags";
        expectedTags[0] = new HTMLTag(HTMLTagType.BOLD, 5, true);
        expectedTags[1] = new HTMLTag(HTMLTagType.UNDERLINE, 6, true);
        expectedTags[2] = new HTMLTag(HTMLTagType.BOLD, 7, false);
        expectedTags[3] = new HTMLTag(HTMLTagType.UNDERLINE, 7, false);
        expectedTags[4] = new HTMLTag(HTMLTagType.SPAN, 8, true);
        expectedTags[4].addSize(50);
        expectedTags[5] = new HTMLTag(HTMLTagType.SPAN, 11, false);
        expectedTags[6] = new HTMLTag(HTMLTagType.BULLET, 12, true);
        expectedTags[7] = new HTMLTag(HTMLTagType.BULLET, 21, false);

        HTMLParser parser = new HTMLParser("");
        parser.extractTags(before);
        assertEquals(expectedTags.length, parser.getTagsForTest().size());

        //not done in loop to determine which one causes the failure
        Assert.assertEquals(expectedTags[0], parser.getTagsForTest().get(0));
        Assert.assertEquals(expectedTags[1], parser.getTagsForTest().get(1));
        Assert.assertEquals(expectedTags[2], parser.getTagsForTest().get(2));
        Assert.assertEquals(expectedTags[3], parser.getTagsForTest().get(3));
        Assert.assertEquals(expectedTags[4], parser.getTagsForTest().get(4));
        Assert.assertEquals(expectedTags[5], parser.getTagsForTest().get(5));
        Assert.assertEquals(expectedTags[6], parser.getTagsForTest().get(6));
        Assert.assertEquals(expectedTags[7], parser.getTagsForTest().get(7));
    }

    @Test
    public void extractedTags2() {
        HTMLParser parser = new HTMLParser("");
        HTMLTag[] expectedTags = new HTMLTag[20];
        String before = "<b>This line is bold.</b><br/>" +
                        "<i>This line is italic.</i><br/>" +
                        "<del>This line is strike through.</del><br/>" +
                        "<ul><li>This line is with bullet.</li></ul>" +
                        "<blockquote>This line is with quote.</blockquote>" +
                        "<a href=\"google.com\">This line is link.</a><br/>" +
                        "<span style=\"color: #FFA500;\">This line is orange.</span><br/>" +
                        "<span style=\"font-family: david;\">This line is of font david</span><br/>" +
                        "<span style=\"font-family: impact;\">This line is of font impact</span><br/>" +
                        "<span style=\"font-size: 50px;\">This line with a font size of 100.</span><br/>";

        String commited = parser.commitCharTags(before);
        commited = parser.addEnterAfterBulletQuote(commited);
        parser.extractTags(commited);
        int i = 0;
        expectedTags[0] = new HTMLTag(HTMLTagType.BOLD, i, true);
        i += "This line is bold.".length();
        expectedTags[1] = new HTMLTag(HTMLTagType.BOLD, i, false);
        i++;
        expectedTags[2] = new HTMLTag(HTMLTagType.ITALIC, i, true);
        i += "This line is italic.".length();
        expectedTags[3] = new HTMLTag(HTMLTagType.ITALIC, i, false);
        i++;
        expectedTags[4] = new HTMLTag(HTMLTagType.STRIKETHROUGH, i, true);
        i += "This line is strike through.".length();
        expectedTags[5] = new HTMLTag(HTMLTagType.STRIKETHROUGH, i, false);
        i++;
        expectedTags[6] = new HTMLTag(HTMLTagType.BULLET, i, true);
        i += "This line is with bullet.".length();
        expectedTags[7] = new HTMLTag(HTMLTagType.BULLET, i, false);
        i++;
        expectedTags[8] = new HTMLTag(HTMLTagType.QUOTE, i, true);
        i += "This line is with quote.".length();
        expectedTags[9] = new HTMLTag(HTMLTagType.QUOTE, i, false);
        i++;
        expectedTags[10] = new HTMLTag(HTMLTagType.URL, i, true);
        i += "This line is link.".length();
        expectedTags[10].addURL("google.com");
        expectedTags[11] = new HTMLTag(HTMLTagType.URL, i, false);
        i++;
        expectedTags[12] = new HTMLTag(HTMLTagType.SPAN, i, true);
        i += "This line is orange.".length();
        expectedTags[12].addColor("#FFA500");
        expectedTags[13] = new HTMLTag(HTMLTagType.SPAN, i, false);
        i++;
        expectedTags[14] = new HTMLTag(HTMLTagType.SPAN, i, true);
        i += "This line is of font david".length();
        expectedTags[14].addFont("david");
        expectedTags[15] = new HTMLTag(HTMLTagType.SPAN, i, false);
        i++;
        expectedTags[16] = new HTMLTag(HTMLTagType.SPAN, i, true);
        i += "This line is of font impact".length();
        expectedTags[16].addFont("impact");
        expectedTags[17] = new HTMLTag(HTMLTagType.SPAN, i, false);
        i++;
        expectedTags[18] = new HTMLTag(HTMLTagType.SPAN, i, true);
        i += "This line with a font size of 100.".length();
        expectedTags[18].addSize(50); // this value is multiplied by 2
        expectedTags[19] = new HTMLTag(HTMLTagType.SPAN, i, false);



        assertEquals(expectedTags.length, parser.getTagsForTest().size());
        //not done in loop to determine which one causes the failure
        i = 0;
        List<HTMLTag> actualTags = parser.getTagsForTest();
        Assert.assertEquals(expectedTags[i], actualTags.get(i)); ++i; // 0
        Assert.assertEquals(expectedTags[i], actualTags.get(i)); ++i; // 1
        Assert.assertEquals(expectedTags[i], actualTags.get(i)); ++i; // 2
        Assert.assertEquals(expectedTags[i], actualTags.get(i)); ++i; // 3
        Assert.assertEquals(expectedTags[i], actualTags.get(i)); ++i; // 4
        Assert.assertEquals(expectedTags[i], actualTags.get(i)); ++i; // 5
        Assert.assertEquals(expectedTags[i], actualTags.get(i)); ++i; // 6
        Assert.assertEquals(expectedTags[i], actualTags.get(i)); ++i; // 7
        Assert.assertEquals(expectedTags[i], actualTags.get(i)); ++i; // 8
        Assert.assertEquals(expectedTags[i], actualTags.get(i)); ++i; // 9
        Assert.assertEquals(expectedTags[i], actualTags.get(i)); ++i; // 10
        Assert.assertEquals(expectedTags[i], actualTags.get(i)); ++i; // 11
        Assert.assertEquals(expectedTags[i], actualTags.get(i)); ++i; // 12
        Assert.assertEquals(expectedTags[i], actualTags.get(i)); ++i; // 13
        Assert.assertEquals(expectedTags[i], actualTags.get(i)); ++i; // 14
        Assert.assertEquals(expectedTags[i], actualTags.get(i)); ++i; // 15
        Assert.assertEquals(expectedTags[i], actualTags.get(i)); ++i; // 16
        Assert.assertEquals(expectedTags[i], actualTags.get(i)); ++i; // 17
        Assert.assertEquals(expectedTags[i], actualTags.get(i)); ++i; // 18
        Assert.assertEquals(expectedTags[i], actualTags.get(i)); ++i; // 19
    }
}
