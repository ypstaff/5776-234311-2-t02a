package io.github.mthli.knife;

import android.content.Context;
import android.graphics.Typeface;
import android.text.ParcelableSpan;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.AbsoluteSizeSpan;
import android.text.style.BulletSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.QuoteSpan;
import android.text.style.StrikethroughSpan;
import android.text.style.StyleSpan;
import android.text.style.URLSpan;
import android.text.style.UnderlineSpan;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by tbenalta on 6/20/2016.
 */

class HTMLParser {

    private static final String LOG_TAG = "HTML_PARSER_TAG";
    List<HTMLTag> m_tags = new ArrayList<>();
    String m_plainText;
    SpannableString m_spannedText = null;

    public HTMLParser(String htmlText) {
        String tmp = contractWhiteSpaces(htmlText);
        tmp = commitCharTags(tmp);
        tmp = addEnterAfterBulletQuote(tmp);
        m_plainText = extractTags(tmp);

    }

    String addEnterAfterBulletQuote(String str) {
        return str.replace("</blockquote>", "</blockquote>\n").replace("</li></ul>", "</li></ul>\n");
    }

    /**
     * @param context the application context
     * @return The text with the styles applied after parsing the html
     */
    public Spanned getSpannedText(Context context) {
        m_spannedText = SpannableString.valueOf(m_plainText);
        for (int i = 0; i < m_tags.size(); ++i) {
            HTMLTag tag = m_tags.get(i);
            if (!tag.isOpening())
                continue; // we set spanned only when encountering an opening tag
            HTMLTag closeTag = findClosingTag(i);
            if (closeTag == null) {
                Log.e(LOG_TAG, "couldn't find closing tag");
                return null;
            }
            switch (tag.getType()) {
                case UNDERLINE:
                    m_spannedText.setSpan(new UnderlineSpan(), tag.getIndex(), closeTag.getIndex(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                    break;
                case STRIKETHROUGH:
                    m_spannedText.setSpan(new StrikethroughSpan(), tag.getIndex(), closeTag.getIndex(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                    break;
                case BOLD:
                    m_spannedText.setSpan(new StyleSpan(Typeface.BOLD), tag.getIndex(), closeTag.getIndex(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                    break;
                case PARAGRAPH:
                    //not supported
                    break;
                case QUOTE:
                    m_spannedText.setSpan(new QuoteSpan(), tag.getIndex(), closeTag.getIndex(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                    break;
                case BULLET:
                    m_spannedText.setSpan(new BulletSpan(), tag.getIndex(), closeTag.getIndex(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                    break;
                case ITALIC:
                    m_spannedText.setSpan(new StyleSpan(Typeface.ITALIC), tag.getIndex(), closeTag.getIndex(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                    break;
                case URL:
                    //NO BREAK!!, do same as SPAN
                case SPAN:
                    if (tag.getAttributes() == null) {
                        Log.e(LOG_TAG, "Missing attributes of span tag");
                        return null;
                    }
                    ParcelableSpan span = null;
                    if (tag.getAttributes().getFontName() != null) {
                        String fontName = tag.getAttributes().getFontName();
                        String ttfName = fontName.toLowerCase().replaceAll("\\s+", "") + ".ttf";
                        Typeface face = Typeface.createFromAsset(context.getAssets(), ttfName);
                        span = new CustomTypefaceSpan("", face, fontName);
                    } else if (tag.getAttributes().getColor() != null) {
                        span = new ForegroundColorSpan(tag.getAttributes().getColor());
                    } else if (tag.getAttributes().getSize() != null) {
                        span = new AbsoluteSizeSpan(tag.getAttributes().getSize());
                    } else if (tag.getAttributes().getUrl() != null) {
                        span = new URLSpan(tag.getAttributes().getUrl());
                    }
                    m_spannedText.setSpan(span, tag.getIndex(), closeTag.getIndex(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                    break;
                default:
                    break;
            }
        }
        return m_spannedText;
    }

    // finding the matching tag of an opening tag
    HTMLTag findClosingTag(int i) {
        HTMLTag tag = m_tags.get(i);
        if (tag.isOpening() == false){
            Log.e(LOG_TAG, "shouldn't be called with a closing tag index");
            return null;
        }
        int j = i + 1;
        int nestedTagsCounter = 0;
        while (j < m_tags.size()) {
            HTMLTag candidate = m_tags.get(j);
            if (candidate.getType() == tag.getType() && !candidate.isOpening() && nestedTagsCounter == 0) {
                return candidate;
            } else if (candidate.getType() == tag.getType() && !candidate.isOpening() && nestedTagsCounter != 0) {
                nestedTagsCounter--;
            } else if (candidate.getType() == tag.getType() && candidate.isOpening()) {
                nestedTagsCounter++;
            }
            j++;
        }
        return null; //we didn't find a closing tag
    }

    public List<HTMLTag> getTagsForTest() {
        return m_tags;
    }

    String extractTags(String text) {
        String withTags = new String(text);
        String $ = "";
        int tagsToConsume = 0;
        for (int i = 0, j = 0; i < withTags.length(); ++i, ++j) {
            char c = withTags.charAt(i);
            if (c == '<') {
                tagsToConsume = 1; //default case
                if (withTags.startsWith("<a href=\"", i)) {
                    m_tags.add(createURLTag(withTags, j, i));
                } else if (withTags.startsWith("</a>", i)) {
                    m_tags.add(new HTMLTag(HTMLTagType.URL, j, false));
                } else if (withTags.startsWith("<span", i)) {
                    m_tags.add(createSpanTag(withTags, j, i, true));
                } else if (withTags.startsWith("</span>", i)) {
                    m_tags.add(createSpanTag(withTags, j, i, false));
                } else if (withTags.startsWith("<b>", i)) {
                    m_tags.add(new HTMLTag(HTMLTagType.BOLD, j, true));
                } else if (withTags.startsWith("</b>", i)) {
                    m_tags.add(new HTMLTag(HTMLTagType.BOLD, j, false));
                } else if (withTags.startsWith("<u>", i)) {
                    m_tags.add(new HTMLTag(HTMLTagType.UNDERLINE, j, true));
                } else if (withTags.startsWith("</u>", i)) {
                    m_tags.add(new HTMLTag(HTMLTagType.UNDERLINE, j, false));
                } else if (withTags.startsWith("<i>", i)) {
                    m_tags.add(new HTMLTag(HTMLTagType.ITALIC, j, true));
                } else if (withTags.startsWith("</i>", i)) {
                    m_tags.add(new HTMLTag(HTMLTagType.ITALIC, j, false));
                } else if (withTags.startsWith("<del>", i)) {
                    m_tags.add(new HTMLTag(HTMLTagType.STRIKETHROUGH, j, true));
                } else if (withTags.startsWith("</del>", i)) {
                    m_tags.add(new HTMLTag(HTMLTagType.STRIKETHROUGH, j, false));
                } else if (withTags.startsWith("<ul><li>", i)) {
                    tagsToConsume = 2;
                    m_tags.add(new HTMLTag(HTMLTagType.BULLET, j, true));
                } else if (withTags.startsWith("</li></ul>", i)) {
                    tagsToConsume = 2;
                    m_tags.add(new HTMLTag(HTMLTagType.BULLET, j, false));
                } else if (withTags.startsWith("<blockquote>", i)) {
                    m_tags.add(new HTMLTag(HTMLTagType.QUOTE, j, true));
                } else if (withTags.startsWith("</blockquote>", i)) {
                    m_tags.add(new HTMLTag(HTMLTagType.QUOTE, j, false));
                }

                //move index to 1 char after the tag
                for (int k = 0; k < tagsToConsume; ++k) {
                    while (i < withTags.length() && withTags.charAt(i) != '>') {
                        ++i;
                    }
                    ++i;
                }
                --i;
                --j;

                HTMLTag lastTag = m_tags.get(m_tags.size() - 1);
                Log.v("PARSING", "tag type: " + lastTag.getType());
                Log.v("PARSING", "tag index: " + lastTag.getIndex());
                Log.v("PARSING", "tag isOpening: " + lastTag.isOpening());


            } else {
                $ = $.concat(String.valueOf(c));
            }
        }
        return $;
    }

    private HTMLTag createURLTag(String withTags, int indexInNormal, int indexInHtml) {
        HTMLTag tag = new HTMLTag(HTMLTagType.URL, indexInNormal, true);
        String tagPrefix = "<a href=\"";
        if (withTags.startsWith(tagPrefix, indexInHtml)) {
            int start = indexInHtml + tagPrefix.length();
            int end = start;
            while (end < withTags.length() && !withTags.startsWith("\"", end)) {
                ++end;
            }
            if (end > withTags.length()) {
                Log.e(LOG_TAG, "should not happen");
                return null;
            }
            String strURL = withTags.substring(start, end);
            tag.addURL(strURL);
        }

        return tag;
    }

    /**
     * @param withTags      the html string with the tags
     * @param indexInNormal
     * @param indexInHtml
     * @param isOpening     is opening or closing tag?
     * @return a span style html tag
     */
    private HTMLTag createSpanTag(String withTags, int indexInNormal, int indexInHtml, boolean isOpening) {
        HTMLTag tag = new HTMLTag(HTMLTagType.SPAN, indexInNormal, isOpening);
        String tagPrefix = "<span style=\"color: ";
        if (withTags.startsWith(tagPrefix, indexInHtml)) {
            int start = indexInHtml + tagPrefix.length();
            String hexColor = withTags.substring(start, start + 7); //the format is #dddddd (d is a digit)
            tag.addColor(hexColor);
        }
        tagPrefix = "<span style=\"font-family: ";
        if (withTags.startsWith(tagPrefix, indexInHtml)) {
            int start = indexInHtml + tagPrefix.length();
            int end = start;
            while (end < withTags.length() && withTags.charAt(end) != ';') {
                ++end;
            }
            if (end > withTags.length()) {
                Log.e(LOG_TAG, "should not happen");
                return null;
            }
            String fontName = withTags.substring(start, end);
            tag.addFont(fontName);
        }
        tagPrefix = "<span style=\"font-size: ";
        if (withTags.startsWith(tagPrefix, indexInHtml)) {
            int start = indexInHtml + tagPrefix.length();
            int end = start;
            while (end < withTags.length() && !withTags.startsWith("px;", end)) {
                ++end;
            }
            if (end > withTags.length()) {
                Log.e(LOG_TAG, "should not happen");
                return null;
            }
            String strSize = withTags.substring(start, end);
            tag.addSize(Integer.parseInt(strSize));
        }
        return tag;
    }

    String commitCharTags(String str) {
        return str.replace("&nbsp;", " ").replace("<br/>", "\n");
    }

    String contractWhiteSpaces(String htmlText) {
        String $ = "";
        boolean isInWSSequence = false;
        for (int oldIdx = 0; oldIdx < htmlText.length(); oldIdx++) {
            char c = htmlText.charAt(oldIdx);
            boolean wasInWSSequence = isInWSSequence;
            isInWSSequence = Character.isWhitespace(c);
            if (!wasInWSSequence || !isInWSSequence) {
                if (isInWSSequence)
                    $ = $.concat(" ");
                else
                    $ = $.concat(String.valueOf(c));
            }
        }
        return $;
    }
}

enum HTMLTagType {
    UNDERLINE, STRIKETHROUGH, BOLD, PARAGRAPH, QUOTE, BULLET, ITALIC, SPAN, URL
}

class HTMLTag {
    private HTMLTagType type;

    private int index;

    private boolean isOpening = false;

    private SpanAttr attributes = new SpanAttr();

    public HTMLTagType getType() {
        return type;
    }

    public int getIndex() {
        return index;
    }

    public boolean isOpening() {
        return isOpening;
    }

    public SpanAttr getAttributes() {
        return attributes;
    }

    public HTMLTag(HTMLTagType type, int index, boolean isOpening) {
        this.type = type;
        this.isOpening = isOpening;
        this.index = index;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (!HTMLTag.class.isAssignableFrom(obj.getClass())) {
            return false;
        }
        final HTMLTag other = (HTMLTag) obj;
        if (!type.equals(other.type))
            return false;
        if (index != other.index)
            return false;
        if (isOpening != other.isOpening)
            return false;
        if (!attributes.equals(other.attributes))
            return false;
        return true;
    }

    //extracting the appropriate color integer from the html tag - requires casting from hex to int;
    public void addColor(String hexColor) {
        attributes.color = 0xff000000 | Integer.parseInt(hexColor.substring(1), 16);
    }

    public void addFont(String fontName) {
        attributes.fontName = fontName.toLowerCase();
    }

    public void addSize(int htmlSize) {
        attributes.size = htmlSize * 2; // multiply by two because i'm dividing by two on html creation
    }

    public void addURL(String strURL) {
        attributes.url = strURL;
    }

    class SpanAttr {
        private Integer size = null;
        private String fontName = null;
        private Integer color = null;
        private String url = null;

        public String getUrl() {
            return url;
        }

        public Integer getSize() {
            return size;
        }

        public String getFontName() {
            return fontName;
        }

        public Integer getColor() {
            return color;
        }

        //Overridden equals method for testability of this class
        @Override
        public boolean equals(Object obj) {
            if (obj == null) {
                return false;
            }
            if (!SpanAttr.class.isAssignableFrom(obj.getClass())) {
                return false;
            }

            final SpanAttr other = (SpanAttr) obj;
            if ((size == null) != (other.size == null))
                return false;
            else if (size != null && !size.equals(other.size))
                return false;

            else if ((fontName == null) != (other.fontName == null))
                return false;
            else if (fontName != null && !fontName.equals(other.fontName))
                return false;

            else if ((color == null) != (other.color == null))
                return false;
            else if (color != null && !color.equals(other.color))
                return false;

            else if ((url == null) != (other.url == null))
                return false;
            else if (url != null && !url.equals(other.url))
                return false;

            return true;
        }
    }
}

